Source files for the website are in the `src/` directory. They are written in
the *frundis* markup language, so you need first to install the [frundis
tool](https://codeberg.org/anaseto/gofrundis) if you want te rebuild the html
content. Note that frundis, like goal, is written in Go with no external
dependencies. You also need to install the short *goalfrundis*
program in [src/cmd/goalfrundis](src/cmd/goalfrundis), which extends *frundis*
with html highlighting capability for Goal code.

Rebuilding the webpage is then done by running `make` in the root directory.
There are other targets for things like EPUB.
