.PHONY: all goal-docs goal-docs-epub index frundis dplyr lil

all: goal-docs index

goal-docs:
	rm -rf goal-docs
	(cd src/goal && goalfrundis -o ../../goal-docs goal.frundis)
index:
	(cd src && frundis -a -s -T xhtml -o ../index.html index.frundis)
goal-docs-epub:
	rm -rf goal-docs-gen
	(cd src/goal && goalfrundis -epub -o ../../goal-docs-gen goal.frundis)
	mv goal-docs-gen.epub misc/goal/
frundis:
	(cd src/frundis && frundis -x -a -s -T xhtml -o ../../frundis/faq.html faq.frundis)
	(cd src/frundis && frundis -x -a -s -T xhtml -o ../../frundis/index.html index.frundis)
dplyr:
	(cd src && goalfrundis -a -o ../misc/goal/dplyr.html dplyr.frundis)
lil:
	(cd src && goalfrundis -a -o ../misc/goal/lil.html lil.frundis)
