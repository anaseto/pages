This short *goalfrundis* program extends *frundis* with html highlighting
capability for Goal code.

Install
-------

To install the program, you only need to have the [go
compiler](https://golang.org/) installed (Go 1.20 or later required). There are
no extra dependencies.

You can then build the intepreter with:

	go install

You may add `$(go env GOPATH)/bin` to your `$PATH` (for example `export
PATH="$PATH:$(go env GOPATH)/bin"`), so that your shell can find the
`goalfrundis` executable.

Alternatively, you may put the resulting binary in a custom location (usually
in your $PATH) by using instead the following command:

    go build -o /path/to/bin/goalfrundis .

In both cases, you should now have a `goalfrundis` executable somewhere. Type
`goalfrundis --help` for a short summary of command-line usage.
