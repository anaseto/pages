module goalfrundis

go 1.22

toolchain go1.23.1

require (
	codeberg.org/anaseto/goal v1.1.0
	codeberg.org/anaseto/gofrundis v0.15.1-0.20250218165144-655dd293ede5
)
