package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"codeberg.org/anaseto/goal/scan/html"
	"codeberg.org/anaseto/gofrundis/exporter/xhtml"
	"codeberg.org/anaseto/gofrundis/exporter/xhtml/epub"
	"codeberg.org/anaseto/gofrundis/frundis"
)

func main() {
	optEPUB := flag.Bool("epub", false, "output EPUB (instead of XHTML)")
	optAllInOneFile := flag.Bool("a", false, "all in one file (for xhtml only)")
	optOutputFile := flag.String("o", "", "`output-file`")
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [-epub] -o output-file path\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()

	log.SetPrefix(fmt.Sprintf("%s: ", os.Args[0]))
	log.SetFlags(0)

	args := flag.Args()
	var filename string
	if len(args) > 0 {
		filename = args[0]
	} else {
		Error(true, "filename required")
	}
	if len(args) > 1 {
		Error(true, "too many arguments")
	}

	if *optOutputFile == "" {
		Error(true, "-o option required")
	}
	format := "xhtml"
	if *optEPUB {
		format = "epub"
	}

	exp := xhtml.NewExporter(&xhtml.Options{
		AllInOneFile: *optAllInOneFile,
		Format:       format,
		OutputFile:   *optOutputFile,
		Standalone:   true})
	exp.Context().Vars["goalfilter"] = "yes"
	h := html.NewHighlighter(html.Options{})
	exp.Context().Filters["goal"] = func(s string) string {
		var sb strings.Builder
		err := h.Execute(&sb, s)
		if err != nil {
			exp.Context().Errorf("goal filter: %v", err)
		}
		return sb.String()
	}
	export(exp, filename)
	if *optEPUB {
		err := epub.WriteEpub(*optOutputFile, *optOutputFile+".epub")
		if err != nil {
			Error(false, err)
		}
	}
}

func export(exp frundis.Exporter, filename string) {
	err := frundis.ProcessFrundisSource(exp, filename)
	if err != nil {
		Error(false, err)
	}
}

func Error(usage bool, msgs ...interface{}) {
	log.Println(msgs...)
	if usage {
		flag.Usage()
	}
	os.Exit(1)
}
