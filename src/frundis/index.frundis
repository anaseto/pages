.\" This line is a comment.
.\"
.\" Setting some global eXport parameters for title,
.\" favicon, css, and language.
.X set document-title "Frundis: a semantic markup language"
.X set xhtml-custom-ids 1
.X set xhtml-favicon /frundis/favicon.png
.X set xhtml-css /frundis/style.css
.X set lang en
.\" Markup tag declarations: option -c will provide the name
.\" of the html element, while -t will provide the html
.\" class and frundis tag. The frundis utility will complain
.\" if you use an undeclared tag, catching typos. The -f
.\" option means that this tag declaration has to be used
.\" for XHTML output only, allowing for example to provide a
.\" different declaration for a LaTeX, EPUB or markdown
.\" output.
.X mtag -f xhtml -t frundis -c span
.X mtag -f xhtml -t path -c code
.\" Macro definition to not repeat too much the same
.\" boilerplate and improve maintenance.
.#de frundis
.\" Semantic markup using a previously defined tag
.Sm -t frundis frundis \$@
.\" Now we can write .frundis instead of the preceding
.\" line. \$@ is substituted by the list of arguments.
.\" Useful to add punctuation, for example. End of macro
.\" definition:
.#.
.\" Table of Contents
.Tc
.\" A Section (without numbering)
.Sh -nonum Overview
.\" Call to user defined .frundis macro
.frundis
is a tool for compiling documents written in the
.Lk /man/frundis_syntax-5.html "frundis language" ,
a semantic markup language primarily intended for
supporting authoring of novels, but also well suited for
many other kinds of documents.
The
.Lk /man/frundis-1.html Bm -t frundis frundis Em \& tool
can export documents to LaTeX, XHTML\~5, EPUB, markdown
and groff mom.
.P
The language has a focus on simplicity. It provides a few
flexible built-in macros with extensible semantics. It
strives to provide good error messages and catch typos,
while still allowing one to finely control output for a
specific format when needed.
.P
Here is a list of its main features:
.Bl \" Begin list
.It
Common elements such as links, images, cross-references,
lists (including verse), simple tables, table of
contents, etc.
.It
EPUB with arbitrary metadata. Indexed HTML files.
.It
User defined
.Sm tags
with configurable rendering.
.It
Raw blocks, file inclusion, filters, conditionals, macros
and variables.
.It
Roff-like syntax: simple, clear and friendly to grep and
diff.
.El \" End list
.Sh -nonum Documentation
Both the tool and the language are documented in manual
pages:
.Bl
.It
.Lk /man/frundis-1.html frundis(1)
describes command line usage.
.It
.Lk /man/frundis_syntax-5.html frundis_syntax(5)
describes the language.
.El
For a quick overview, look at the
.\" cross-reference to the element with identifier “examples”
.Sx examples "example below" .
There is also a
.Lk faq.html "short FAQ" .
There is a 
.Lk https://www.youtube.com/watch?v=QqDOkcrh1eU "recorded presentation"
(in French) by kaoseto that took place at the
.Lk https://capitoledulibre.org/ "Capitole du Libre" .
.Sh -nonum Installation
Follow the README instructions in the
.Lk https://codeberg.org/anaseto/gofrundis codeberg
repository.
The program is written in Go and released under the ISC free
software license (BSD-like). 
.P
Questions, suggestions, patches and pull requests are welcome!
.Sh -nonum -id examples Examples
There are some simple examples in the
.Sm -t path doc/examples
directory, and in the manual pages.
.P
Moreover,
.frundis
is being used in production since around 2015.
All the free (as in freedom) books by kaoseto in the
.Lk https://bardinflor.perso.aquilenet.fr/kaosfantasy/index-en.html kaosfantasy
multilingual project are using
.frundis .
The sources can be found
.Lk https://codeberg.org/kaoseto/kaosfantasy "on Codeberg" .
.P
As stated above,
.frundis
is not limited to producing books. As an example, we give as
follows the code that was used to generate this page:
.\" We include a file defining macros for code block
.\" inclusion and footer.
.If macros.frundis
.\" Code is filtered through a small html coloration
.\" script and included "as-is" by defining and using a
.\" filter tag.
.X ftag -t frundis -shell "perl html_coloration.pl"
.include-code-with-filter frundis index.frundis
The
.Sm -t path macros.frundis
file with the macro definitions is the following:
.include-code-with-filter frundis macros.frundis
.footer
