This directory contains the source files for Goal documentation. They are
written in the [*frundis*](https://codeberg.org/anaseto/gofrundis) markup
language, whose implementation is a Go library and program with no external
dependencies, like Goal.

The sources in the present directory are built using the *goalfrundis program*
in the [../cmd/goalfrundis](../cmd/goalfrundis) directory, that embeds frundis
and extends it with highlighting capability for Goal code.  You need to install
it first.

You can build the html with:

    goalfrundis -o goal-docs goal.frundis

You may want to change the `../footer.html` file in the parent directory that
sets the footer of the pages, and/or modify some parameters in `goal.frundis`.

The EPUB version can be built with:

    goalfrundis -epub -o goal-docs goal.frundis
