.Ch -id intro Introduction
Goal is an embeddable
.Lk https://en.wikipedia.org/wiki/Array_programming array programming language
with a bytecode interpreter, written in Go. The command line
interpreter can execute scripts or run in interactive mode.
For installation, see the
.code README.md
file in the
.Lk https://codeberg.org/anaseto/goal "project's repository" .
You can also 
.Lk https://anaseto.codeberg.page/try-goal/ try Goal on the browser .
.#if -f xhtml
You might be interested in the
.Lk https://anaseto.codeberg.page/misc/goal/goal-docs-gen.epub EPUB version
of the present documentation.
.#;
Moreover, you can follow these
.Lk https://codeberg.org/anaseto/pages/src/branch/master/src/goal README instructions
to build the documentation from sources and deploy it
locally to avoid depending on a network connection.
.P
Like in most array programming languages, Goal's builtins
vectorize operations on immutable arrays and encourage a
functional style for control and data transformations,
supported by a simple dynamic type system and mutable
variables (but no mutable values).
.P
You can read about Goal's origins and influences in the
.Sx origins "Origins section"
of the FAQ. If you already know about the K language, you
might want to read the
.Sx from-k Differences from K
chapter.
.P
Goal's main distinctive features among array languages can
be summarized as follows:
.Bl
.It
Dictionaries and table functionality (similar to most K dialects)
.It
Atomic strings:
.goal \&"ab" \&"bc"=\&"bc" → 0 1
.It
Unicode-aware string primitives:
.goal \&_"AB" \&"Π" → \&"ab" \&"π"
.It
Perl-like quoting constructs and string interpolation:
.goal qq/$var\en or ${var}/
and
.goal rq#raw strings#
.It
Format strings:
.goal \&"%.2g"$1 4%3 → \&"0.33" \&"1.3"
.It
Dedicated regular expression syntax:
.goal rx/\es+/
.It
.Sx error-values Error values
.It
Embeddable and extensible in Go
.El
Goal also has support for standard features like I/O, CSV,
JSON, and time handling.
.P
Goal shines the most in common scripting tasks, like
handling columnar data or text processing. Goal is also
suitable for exploratory programming.
.P
The
.Sx tutorial next chapter
gives a tour of Goal's features and showcases the language
in a couple of practical examples.
