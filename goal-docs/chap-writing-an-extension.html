<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Writing an extension</title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.png" />
    <link rel="stylesheet" href="/style.css" />
  </head>
  <body>
<div class="top">
<p><a href="/">Home</a>.
Documentation for
<a href="https://codeberg.org/anaseto/goal">Goal</a>.
<em>Last update: 2025-02-18</em>.</p>
</div>
    <div class="topnav">
      <ul class="topnav">
        <li><a href="chap-working-with-tables.html">&lt;</a></li>
        <li><a href="index.html">Index</a></li>
        <li>&gt;</li>
      </ul>
    </div>
<h1 class="Ch" id="chap-writing-an-extension">7 Writing an extension <a class="anchor" href="#chap-writing-an-extension">#</a></h1>
<p>This chapter is an introduction to the more advanced topic
of writing an extension for Goal in Go.</p>
<div class="toc">
  <ul>
    <li><a href="chap-writing-an-extension.html#extension-intro">7.1. Introduction</a>
    </li>
    <li><a href="chap-writing-an-extension.html#extension-setup">7.2. Setting up an interpreter</a>
    </li>
    <li><a href="chap-writing-an-extension.html#extension-zipfs-value">7.3. Defining a zip file-system value</a>
    </li>
    <li><a href="chap-writing-an-extension.html#extension-variadic-function">7.4. Defining a variadic function</a>
    </li>
    <li><a href="chap-writing-an-extension.html#extension-whole-code">7.5. The whole code</a>
    </li>
    <li><a href="chap-writing-an-extension.html#extension-more">7.6. Learn more</a>
    </li>
  </ul>
</div>
<h2 class="Sh" id="extension-intro">7.1 Introduction <a class="anchor" href="#extension-intro">#</a></h2>
<p>Goal is designed to be easily embedded and extended in Go,
using a relatively high-level API.  The Go API for Goal is
provided by a set of packages belonging to a
<a href="https://pkg.go.dev/codeberg.org/anaseto/goal">single module</a>,
with no dependencies outside Go’s standard library.</p>
<p>As an example, we’ll implement an extension for reading zip
files as Goal file system values, based on Go’s standard
<a href="https://pkg.go.dev/archive/zip">archive/zip</a>
package. Once done, we’ll be able to write the following
code in Goal:</p>
<div class="goal-code">
<pre><code><span class="dyad">import</span><span class="string">&#34;fs&#34;</span> <span class="comment">/ assuming GOALLIB points to goal&#39;s lib/ dir</span>
<span class="comment">/ open the epub file made from Goal docs (epub is zip)</span>
<span class="special">&#39;</span>epubfs<span class="stmt">:</span>zip.open<span class="string">&#34;goal-docs-gen.epub&#34;</span>
<span class="comment">/ read and display the &#34;mimetype&#34; file</span>
<span class="dyad">say</span><span class="string">&#34;mimetype: &#34;</span><span class="dyad">+</span> <span class="special">&#39;</span>epubfs <span class="dyad">read</span><span class="string">&#34;mimetype&#34;</span>
<span class="comment">/ use fs.ls to get all non-dir file paths in the file system</span>
fpaths<span class="stmt">:</span>(fs.ls epubfs)<span class="stmt">..</span>path<span class="dyad">@</span><span class="dyad">&amp;</span><span class="dyad">~</span>dir
<span class="comment">/ display number of files and list of paths</span>
<span class="dyad">say</span><span class="string">&#34;total %d files:<span class="escape">\n</span>%s&#34;</span><span class="dyad">$</span>(<span class="dyad">#</span>fpaths)<span class="dyad">,</span><span class="string">&#34;<span class="escape">\n</span>&#34;</span><span class="adverb">/</span>fpaths
<span class="monad">close</span> epubfs
</code></pre></div>
<p>Executing the script with the extended interpreter will
produce:</p>
<div class="code">
<pre><code>mimetype: application/epub+zip
total 14 files:
mimetype
EPUB/chap-FAQ.xhtml
EPUB/chap-from-k.xhtml
EPUB/chap-help.xhtml
EPUB/chap-intro.xhtml
EPUB/chap-tutorial.xhtml
EPUB/chap-working-with-tables.xhtml
EPUB/chap-writing-an-extension.xhtml
EPUB/content.opf
EPUB/index.xhtml
EPUB/nav.xhtml
EPUB/stylesheet.css
EPUB/toc.ncx
META-INF/container.xml
</code></pre></div>
<h2 class="Sh" id="extension-setup">7.2 Setting up an interpreter <a class="anchor" href="#extension-setup">#</a></h2>
<p>We
first create a
<code class="code">goalzipfs</code>
directory somewhere. There, in a
<code class="code">main.go</code>
file, we write:</p>
<div class="code">
<pre><code>package main

import (
	&#34;os&#34;

	&#34;codeberg.org/anaseto/goal&#34;
	&#34;codeberg.org/anaseto/goal/cmd&#34;
	&#34;codeberg.org/anaseto/goal/help&#34;
	gos &#34;codeberg.org/anaseto/goal/os&#34;
)

func main() {
	ctx := goal.NewContext() // new evaluation context for Goal code
	ctx.Log = os.Stderr      // configure logging with \x to stderr
	gos.Import(ctx, &#34;&#34;)      // register all IO/OS primitives with prefix &#34;&#34;
	cmd.Exit(cmd.Run(ctx, cmd.Config{Help: help.HelpFunc(), Man: &#34;goal&#34;}))
}
</code></pre></div>
<p>The code above is a commented copy of
<code class="code">cmd/goal/main.go</code>
as found in Goal’s repository, and produces a goal
interpreter equivalent to the default one in just four
short lines of code. We’ll use this as a basis for our
extension.</p>
<p>As you can see, other than importing the relevant packages,
the code creates a new evaluation context with
<code class="code">goal.NewContext</code>,
then configures and registers any extensions (like the
IO/OS primitives), and finally the
<code class="code">cmd.Run</code>
function runs an interpreter using the created
context, handling command-line arguments too, with some
optional extra configuration for REPL help (using Goal’s
default help here). The
<code class="code">cmd.Exit</code>
function handles the return value of
<code class="code">cmd.Run</code>
to format any errors on stderr, and exits the program with
the appropriate status code.</p>
<p>Before actually compiling and running our interpreter, we
need to provide a
<code class="code">go.mod</code>
file for it by running:</p>
<div class="code">
<pre><code>$ go mod init goalzipfs
</code></pre></div>
<p>The latest released version of Goal will automatically be
downloaded and added as a dependency in
<code class="code">go.mod</code>
with the following command:</p>
<div class="code">
<pre><code>$ go get codeberg.org/anaseto/goal@latest
</code></pre></div>
<p>You can then build a
<code class="code">goalzipfs</code>
executable in the current directory with:</p>
<div class="code">
<pre><code>$ go build
</code></pre></div>
<p>Because we haven’t implemented any extensions yet, that
<code class="code">goalzipfs</code>
executable is still the same as the upstream
<code class="code">goal</code>.</p>
<h2 class="Sh" id="extension-zipfs-value">7.3 Defining a zip file-system value <a class="anchor" href="#extension-zipfs-value">#</a></h2>
<p>Goal values are represented by the opaque struct type
<code class="code">goal.V</code>.
That type can represent both unboxed and boxed values.
Examples of unboxed values are integers and floats: they fit
into a struct’s field, without extra indirection. Boxed
values represent most other values, including strings,
arrays, projections, and so on. They don’t fit into the
struct: they are stored in a field as a Go interface value
type, which points to heap-allocated memory.</p>
<p>Goal allows extensions to define new kinds of boxed value
types.  In order to do so, one has to define a Go type
satisfying the
<a href="https://pkg.go.dev/codeberg.org/anaseto/goal#BV">goal.BV</a>
interface, which is the interface implemented by all boxed
Goal values.
That interface is described by a set of three methods.</p>
<ul>
<li><p><code class="code">Append</code>
is used for implementing
<code class="goal-expr"><span class="dyad">$</span><span class="xyz">x</span></code>.</p></li>
<li><p><code class="code">Matches</code>
implements
<code class="goal-expr"><span class="xyz">x</span><span class="dyad">~</span><span class="xyz">y</span></code>
for the new kind of value.</p></li>
<li><p><code class="code">Type</code>
implements
<code class="goal-expr"><span class="dyad">@</span><span class="xyz">x</span></code>.</p></li>
</ul>
<p>Boxed value types can implement extra methods when more
functionality is desired: in our case, we’ll inherit methods
from the
<a href="https://pkg.go.dev/archive/zip#ReadCloser">*zip.ReadCloser</a>
type from Go’s standard
<code class="code">archive/zip</code>
package, which satisfies the
<a href="https://pkg.go.dev/io/fs#FS">fs.FS</a>
file system interface, as well as the
<code class="code">io.Closer</code>
interface.</p>
<div class="code">
<pre><code>// zipFS is a wrapper around zip.ReadCloser that implements the goal.BV, fs.FS
// and io.Closer interfaces.
type zipFS struct {
	*zip.ReadCloser
	s string // program string representation
}

// Append appends a program representation of the value to dst.
func (fsys *zipFS) Append(ctx *goal.Context, dst []byte, compact bool) []byte {
	return append(dst, fsys.s...)
}

// Matches reports whether fsys~y.
func (fsys *zipFS) Matches(y goal.BV) bool {
	yv, ok := y.(*zipFS)
	return ok &amp;&amp; fsys == yv
}

// Type returns &#34;/&#34; for file system types.
func (fsys *zipFS) Type() string {
	return &#34;/&#34;
}
</code></pre></div>
<p>As you can see, we defined a struct type with two fields:
one of them simply embeds the
<code class="code">*zip.ReadCloser</code>
type, while the extra
<code class="code">s</code>
field is there simply to provide a program string
representation. Without any extra work, the
<code class="code">*zipFS</code>
type represents a Goal generic value that is supported by
many primitives, with specific behavior for
<code class="goal-expr"><span class="dyad">$</span><span class="xyz">x</span></code>,
<code class="goal-expr"><span class="dyad">@</span><span class="xyz">x</span></code>,
and matching. It is also automatically supported by
<code class="goal-expr"><span class="monad">close</span></code>
and all the IO primitives working on file system values,
just because it inherits
<code class="code">Open</code>
and
<code class="code">Close</code>
methods from the
<code class="code">*zip.ReadCloser</code>
type. To make the code compile we need to add
<code class="string">&#34;archive/zip&#34;</code>
to the list of imports, along
<code class="string">&#34;os&#34;</code>
and the others.</p>
<h2 class="Sh" id="extension-variadic-function">7.4 Defining a variadic function <a class="anchor" href="#extension-variadic-function">#</a></h2>
<p>Unlike user-defined lambdas, new Goal functions implemented
in Go are variadic, like all the built-in verbs.  We’ll
hence define the function that opens a zip file and produces
a
<code class="code">*zipFS</code>
value as a variadic function. We’ll also need to add the
<code class="string">&#34;fmt&#34;</code>
package to the list of imports.</p>
<p>Variadic functions take a
<code class="code">*goal.Context</code>
argument, and a list of Goal values in stack order (last
argument is the first one).</p>
<div class="code">
<pre><code>// VFZipOpen implements the zip.open variadic function.
func VFZipOpen(ctx *goal.Context, args []goal.V) goal.V {
	if len(args) &gt; 1 {
		return goal.Panicf(&#34;zip.open : too many arguments (%d)&#34;, len(args))
	}
	s, ok := args[0].BV().(goal.S)
	if !ok {
		return goal.Panicf(&#34;zip.open s : bad type %q in s&#34;, args[0].Type())
	}
	r, err := zip.OpenReader(string(s))
	if err != nil {
		return gos.NewOSError(err)
	}
	return goal.NewV(&amp;zipFS{r, fmt.Sprintf(&#34;zip.open[%q]&#34;, s)})
}
</code></pre></div>
<p>The function is a straightforward wrapper around
<code class="code">zip.OpenReader</code>
that does some additional argument type checking and error
processing, and provides a concrete program string
representation too (mainly useful in REPL for a file system
value). The
<code class="code">goal.NewV</code>
function is used to produce a generic
<code class="code">goal.V</code>
value from a Go type satisfying the
<code class="code">goal.BV</code>
interface of boxed values.</p>
<p>All that’s left is registering the
<code class="code">VFZipOpen</code>
variadic function into our main context
<code class="code">ctx</code>
in the
<code class="code">main</code>
function, before
<code class="code">cmd.Run</code>.</p>
<div class="code">
<pre><code>ctx.RegisterMonad(&#34;zip.open&#34;, VFZipOpen)
</code></pre></div>
<p>This registers a new
<code class="code">zip.open</code>
monadic verb. Often, introducing new syntax is not
desirable, so storing the function into a global instead is
preferable:</p>
<div class="code">
<pre><code>ctx.AssignGlobal(&#34;zip.open&#34;, ctx.RegisterMonad(&#34;.zip.open&#34;, VFZipOpen))
</code></pre></div>
<p>In that case, introducing new syntax with
<code class="code">RegisterMonad</code>
is avoided by using an invalid identifier
<code class="string">&#34;.zip.open&#34;</code>,
which is only used for identifying and display purposes.</p>
<p>Running
<code class="code">go build</code>
again will produce an extended interpreter that can run the
Goal code shown in introduction, so you can open an EPUB
file and process it as any other kind of file system value!</p>
<h2 class="Sh" id="extension-whole-code">7.5 The whole code <a class="anchor" href="#extension-whole-code">#</a></h2>
<p>As a summary, we reproduce the whole code below:</p>
<div class="code">
<pre><code>package main

import (
	&#34;archive/zip&#34;
	&#34;fmt&#34;
	&#34;os&#34;

	&#34;codeberg.org/anaseto/goal&#34;
	&#34;codeberg.org/anaseto/goal/cmd&#34;
	&#34;codeberg.org/anaseto/goal/help&#34;
	gos &#34;codeberg.org/anaseto/goal/os&#34;
)

func main() {
	ctx := goal.NewContext() // new evaluation context for Goal code
	ctx.Log = os.Stderr      // configure logging with \x to stderr
	gos.Import(ctx, &#34;&#34;)      // register all IO/OS primitives with prefix &#34;&#34;
	ctx.AssignGlobal(&#34;zip.open&#34;, ctx.RegisterMonad(&#34;.zip.open&#34;, VFZipOpen))
	cmd.Exit(cmd.Run(ctx, cmd.Config{Help: help.HelpFunc(), Man: &#34;goal&#34;}))
}

// zipFS is a wrapper around zip.ReadCloser that implements the goal.BV, fs.FS
// and io.Closer interfaces.
type zipFS struct {
	*zip.ReadCloser
	s string // program string representation
}

// Append appends a program representation of the value to dst.
func (fsys *zipFS) Append(ctx *goal.Context, dst []byte, compact bool) []byte {
	return append(dst, fsys.s...)
}

// Matches reports whether fsys~y.
func (fsys *zipFS) Matches(y goal.BV) bool {
	yv, ok := y.(*zipFS)
	return ok &amp;&amp; fsys == yv
}

// Type returns &#34;/&#34; for file system types.
func (fsys *zipFS) Type() string {
	return &#34;/&#34;
}

// VFZipOpen implements the zip.open variadic function.
func VFZipOpen(ctx *goal.Context, args []goal.V) goal.V {
	if len(args) &gt; 1 {
		return goal.Panicf(&#34;zip.open : too many arguments (%d)&#34;, len(args))
	}
	s, ok := args[0].BV().(goal.S)
	if !ok {
		return goal.Panicf(&#34;zip.open s : bad type %q in s&#34;, args[0].Type())
	}
	r, err := zip.OpenReader(string(s))
	if err != nil {
		return gos.NewOSError(err)
	}
	return goal.NewV(&amp;zipFS{r, fmt.Sprintf(&#34;zip.open[%q]&#34;, s)})
}
</code></pre></div>
<h2 class="Sh" id="extension-more">7.6 Learn more <a class="anchor" href="#extension-more">#</a></h2>
<p>At this point, you might want to look at the
<a href="https://pkg.go.dev/codeberg.org/anaseto/goal">API docs</a>
of the module’s various packages. As for examples of
extensions, the
<code class="code">os</code>
built-in package is actually written as an extension: it can
be a good first place to look at. Moreover, there are
currently
<code class="code">math</code>
and
<code class="code">io/fs</code>
packages in the repos,
as well as an
<code class="code">archive/zip</code>
package that provides the functionality we implemented in
this tutorial.
We could just have imported it in a similar way as the
<code class="code">os</code>
one.</p>
<p>If you’re also interested in the internals, the
<code class="code">docs/implementation.md</code>
file in goal’s repository gives a short introduction.</p>
    <div class="topnav">
      <ul class="topnav">
        <li><a href="chap-working-with-tables.html">&lt;</a></li>
        <li><a href="index.html">Index</a></li>
        <li>&gt;</li>
      </ul>
    </div>
<footer class="footer">
<hr><p>This website’s content is licensed under
<a href="https://creativecommons.org/licenses/by/4.0/">cc-by</a>
or the ISC license, as you wish. Contact:
<code class="email"><span class="string">&#34;@&#34;</span><span class="adverb">/</span>(<span class="string">&#34;anaseto&#34;</span><span class="sep">;</span><span class="string">&#34;.&#34;</span><span class="adverb">/</span><span class="dyad">!</span><span class="string">&#34;bardinflor perso aquilenet fr&#34;</span>)</code>.</p>
</footer>
  </body>
</html>
