<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Differences from K</title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.png" />
    <link rel="stylesheet" href="/style.css" />
  </head>
  <body>
<div class="top">
<p><a href="/">Home</a>.
Documentation for
<a href="https://codeberg.org/anaseto/goal">Goal</a>.
<em>Last update: 2025-02-18</em>.</p>
</div>
    <div class="topnav">
      <ul class="topnav">
        <li><a href="chap-FAQ.html">&lt;</a></li>
        <li><a href="index.html">Index</a></li>
        <li><a href="chap-working-with-tables.html">&gt;</a></li>
      </ul>
    </div>
<h1 class="Ch" id="chap-from-k">5 Differences from K <a class="anchor" href="#chap-from-k">#</a></h1>
<p>This chapter gives a tour of the main differences with other
K-like languages.  It uses
<a href="https://codeberg.org/ngn/k">ngn/k</a>
as reference, because it’s the one I know the best.</p>
<div class="toc">
  <ul>
    <li><a href="chap-from-k.html#new-features">5.1. New Features</a>
    <ul>
      <li><a href="chap-from-k.html#atomic-strings">5.1.1. Atomic strings</a>
      </li>
      <li><a href="chap-from-k.html#quoting">5.1.2. String quoting constructs</a>
      </li>
      <li><a href="chap-from-k.html#regexps">5.1.3. Regular expressions</a>
      </li>
      <li><a href="chap-from-k.html#error-handling">5.1.4. Error handling</a>
      </li>
      <li><a href="chap-from-k.html#dict-syntax-expressions">5.1.5. Dict syntax and field expressions</a>
      </li>
    </ul></li>
    <li><a href="chap-from-k.html#other-primitives">5.2. Miscellaneous Changes</a>
    <ul>
      <li><a href="chap-from-k.html#no-digraphs-shifts">5.2.1. No digraphs: Each, Windows and Shifts</a>
      </li>
      <li><a href="chap-from-k.html#tacit-compositions-differences">5.2.2. Minor differences in tacit verb trains</a>
      </li>
      <li><a href="chap-from-k.html#group">5.2.3. Group by, index-count</a>
      </li>
      <li><a href="chap-from-k.html#tables-dicts">5.2.4. Tables and dicts</a>
      </li>
      <li><a href="chap-from-k.html#domain-swap-self-dict">5.2.5. Domain-swap and self-dict</a>
      </li>
      <li><a href="chap-from-k.html#zero-values">5.2.6. Zero values</a>
      </li>
      <li><a href="chap-from-k.html#numeric-conversions">5.2.7. Numeric conversions</a>
      </li>
      <li><a href="chap-from-k.html#without-take-drop">5.2.8. Take, Drop, Without</a>
      </li>
      <li><a href="chap-from-k.html#sort">5.2.9. Sort and Grade for dicts</a>
      </li>
      <li><a href="chap-from-k.html#conditionals">5.2.10. Cond <code class="dyad">?</code> and logical <code class="dyad">and</code>/<code class="dyad">or</code></a>
      </li>
      <li><a href="chap-from-k.html#new-zero-values">5.2.11. New false values</a>
      </li>
      <li><a href="chap-from-k.html#list-eval-order">5.2.12. List syntax uses left-to-right evaluation</a>
      </li>
      <li><a href="chap-from-k.html#amend-assignment">5.2.13. Amend assignment is just sugar around amend</a>
      </li>
      <li><a href="chap-from-k.html#no-splice">5.2.14. There is no splice triadic form</a>
      </li>
      <li><a href="chap-from-k.html#find">5.2.15. Rank-insensitive find <code class="dyad">?</code></a>
      </li>
      <li><a href="chap-from-k.html#no-deep-where">5.2.16. No deep-where</a>
      </li>
      <li><a href="chap-from-k.html#get-parse-eval">5.2.17. Get Global, Eval and Parse</a>
      </li>
      <li><a href="chap-from-k.html#IO">5.2.18. Input/Output with keywords</a>
      </li>
    </ul></li>
  </ul>
</div>
<h2 class="Sh" id="new-features">5.1 New Features <a class="anchor" href="#new-features">#</a></h2>
<h3 class="Ss" id="atomic-strings">5.1.1 Atomic strings <a class="anchor" href="#atomic-strings">#</a></h3>
<p>One of the main differences in Goal is that strings are
atoms and are handled as such by the primitives. Most have
specific behavior for strings, providing built-in support
for common string-handling functionality. Because strings
are atoms, those primitives are pervasive (string-atomic)
when possible. The main ones are summarized in the following
table:</p>
<table>
<tr>
<td><p><code class="goal-expr">s<span class="dyad">+</span>s</code></p></td>
<td><p>concatenate strings</p></td>
</tr>
<tr>
<td><p><code class="goal-expr">s<span class="dyad">*</span>i</code></p></td>
<td><p>repeat string</p></td>
</tr>
<tr>
<td><p><code class="goal-expr">s<span class="dyad">%</span>s</code></p></td>
<td><p>match glob pattern</p></td>
</tr>
<tr>
<td><p><code class="goal-expr">i<span class="dyad">!</span>s</code></p></td>
<td><p>pad string fields with spaces</p></td>
</tr>
<tr>
<td><p><code class="goal-expr"><span class="dyad">!</span>s</code></p></td>
<td><p>split a string into Unicode-space-separated fields</p></td>
</tr>
<tr>
<td><p><code class="goal-expr"><span class="dyad">=</span>s</code></p></td>
<td><p>split a string into lines (handles
<code class="goal-expr"><span class="string">&#34;<span class="escape">\r</span><span class="escape">\n</span>&#34;</span></code>
too)</p></td>
</tr>
<tr>
<td><p><code class="goal-expr"><span class="dyad">-</span>s</code></p></td>
<td><p>trim spaces right</p></td>
</tr>
<tr>
<td><p><code class="goal-expr">s<span class="dyad">-</span>s</code></p></td>
<td><p>trim suffix (if present)</p></td>
</tr>
<tr>
<td><p><code class="goal-expr">s<span class="dyad">_</span>s</code></p></td>
<td><p>trim prefix (if present)</p></td>
</tr>
<tr>
<td><p><code class="goal-expr">s<span class="dyad">^</span>s</code></p></td>
<td><p>trim a string on both sides using a cutset</p></td>
</tr>
<tr>
<td><p><code class="goal-expr">s<span class="dyad">#</span>s</code></p></td>
<td><p>number of non-overlapping instances of a
substring</p></td>
</tr>
<tr>
<td><p><code class="goal-expr"><span class="dyad">&amp;</span>s</code></p></td>
<td><p>number of bytes</p></td>
</tr>
<tr>
<td><p><code class="goal-expr">s<span class="dyad">?</span>s</code></p></td>
<td><p>index of a substring</p></td>
</tr>
<tr>
<td><p><code class="goal-expr"><span class="dyad">_</span>s</code></p></td>
<td><p>map Unicode letters to lower case</p></td>
</tr>
<tr>
<td><p><code class="goal-expr"><span class="monad">uc</span> s</code></p></td>
<td><p>map Unicode letters to upper case</p></td>
</tr>
</table>
<p>Generally, there’s some mnemonic for each of those meanings.
For example
<code class="dyad">=</code>
is drawn with two lines, so
<code class="goal-expr"><span class="dyad">=</span>s</code>
splits into lines (it could be seen as a kind of grouping by
line number, so somewhat related to
<code class="goal-expr"><span class="dyad">=</span>d</code>);
<code class="goal-expr"><span class="dyad">#</span>X</code>
is used for count,
so
<code class="goal-expr">s<span class="dyad">#</span>s</code>
does substring count;
<code class="goal-expr"><span class="dyad">!</span>s</code>
produces a list from an atom, like
<code class="goal-expr"><span class="dyad">!</span>i</code>;
and so on.</p>
<p>The format/cast/parse verb
<code class="dyad">$</code>
has some new functionality, including sprintf-like
formatting. Also, the
<code class="dyad">sub</code>
verb provides various string substitution facilities,
including regexp support.</p>
<p>It’s worth noting that, although strings are atoms, Goal
provides idempotent
<code class="goal-expr"><span class="string">&#34;b&#34;</span><span class="dyad">$</span></code>
and
<code class="goal-expr"><span class="string">&#34;c&#34;</span><span class="dyad">$</span></code>
to transform from and to arrays of bytes or codepoints
respectively, so array-like processing for strings is still
possible when appropriate. Also,
<code class="goal-expr"><span class="string">&#34;&#34;</span><span class="adverb">\</span></code>
can be used to split a string into a list of 1-char strings.</p>
<p>Also, note that because strings are already atomic, Goal
does not support symbols. If interning is really necessary
for performance reasons, classify
<code class="dyad">%</code>
manually the strings into integers.</p>
<h3 class="Ss" id="quoting">5.1.2 String quoting constructs <a class="anchor" href="#quoting">#</a></h3>
<p>Goal comes with more flexible and expressive string quoting
constructs, supporting the same set of escapes as Go’s
double-quoted strings (including Unicode escape sequences),
as well as Perl-like variable interpolation, like for
example in
<code class="goal-expr"><span class="string">&#34;some <span class="escape">$var</span>&#34;</span></code>.
Additionally, there is a Perl-like
<code class="goal-expr"><span class="string">qq/text <span class="escape">$x</span>/</span></code>
form that allows for various kinds of delimiters, not only
the slash. Also, there is a raw string quoting construct
<code class="goal-expr"><span class="string">rq/raw string/</span></code>
without escapes nor interpolation and with custom delimiter.</p>
<p>See the question about
<a href="chap-FAQ.html#number-string-syntax">number and string literal syntax</a>
in the FAQ for more details.</p>
<h3 class="Ss" id="regexps">5.1.3 Regular expressions <a class="anchor" href="#regexps">#</a></h3>
<p>Regular expressions are a built-in type in Goal. They can be
built either at runtime with
<code class="goal-expr"><span class="monad">rx</span> s</code>,
or using regexp literals of the form
<code class="goal-expr"><span class="rx">rx/PATTERN/</span></code>,
like
<code class="goal-expr"><span class="rx">rx/<span class="escape">\s</span><span class="rxsym">+</span>/</span></code>.
The latter are compiled and checked at compile-time,
avoiding both the need to pre-compile regexps for several
uses, and issues with escaping special characters.</p>
<p>See the question about
<a href="chap-FAQ.html#number-string-syntax">regexp syntax</a>
in the FAQ for more details.</p>
<h3 class="Ss" id="error-handling">5.1.4 Error handling <a class="anchor" href="#error-handling">#</a></h3>
<p>Goal makes a distinction between fatal errors, often due to
programming errors, and other kinds of errors, for example
from I/O. The former are just panic strings, while the
latter can represent arbitrary kinds of values and are
callable in a special way to produce an error message
suitable for user consumption.
In addition, Goal provides some specific syntax support for
making error handling less verbose using a
<code class="goal-expr"><span class="special">&#39;</span>expr</code>
statement similar to
<code class="code">?</code>
in Rust but using prefix form. Note that there’s no
confusion with the adverb each
<code class="code">&#39;</code>,
which has to follow a verb or noun (without spaces).</p>
<p>See the
<a href="chap-FAQ.html#error-values">How do error values work?</a>
question of the FAQ for more details.</p>
<h3 class="Ss" id="dict-syntax-expressions">5.1.5 Dict syntax and field expressions <a class="anchor" href="#dict-syntax-expressions">#</a></h3>
<p>In addition to the dict
<code class="dyad">!</code>
verb, Goal supports a special syntax for defining (and
amending) dicts
<code class="goal-expr"><span class="stmt">..</span>[a<span class="stmt">:</span>expr1;b<span class="stmt">:</span>expr2]</code>,
as well as (field) expressions
<code class="goal-expr"><span class="stmt">..</span>expr</code>
that allow to evaluate things under a dict by using the key
names as variables (as long as they are identifier-like
strings). Expressions are actually syntax sugar for a
regular lambda projection with some additional convenience
features, including referring to arguments
<code class="xyz">x</code>,
<code class="xyz">y</code>
and
<code class="xyz">z</code>
like in lambdas, and special prefixes
<code class="code">p.</code>
and
<code class="code">q.</code>
to either project (like in compositions) or insert as-is
(quote) regular variables, making expressions usable as a
limited form of immutable closure.</p>
<p>See the question about
<a href="chap-FAQ.html#field-expressions">field expressions</a>
in the FAQ for more details.</p>
<h2 class="Sh" id="other-primitives">5.2 Miscellaneous Changes <a class="anchor" href="#other-primitives">#</a></h2>
<h3 class="Ss" id="no-digraphs-shifts">5.2.1 No digraphs: Each, Windows and Shifts <a class="anchor" href="#no-digraphs-shifts">#</a></h3>
<p>In Goal, all adverb digraphs were removed.
You still can append a colon and use
<code class="stmt">+:</code>
to refer to the monadic version of a primitive. Because
there are no digraph adverbs, they also follow the same
convention as verbs: they are dyadic by default but accept
a colon at the end to become monadic: this is a minor
difference from K.</p>
<p>Both each-left
<code class="code">\:</code>
and each-right
<code class="code">/:</code>
can be done using
<code class="adverb">`</code>
and
<code class="adverb">´</code>.
The second one is a common character found even in Latin1,
but it is not ASCII!
As an alternative, each-right can be obtained by projecting
onto the left argument and regular each
<code class="goal-expr">f<span class="adverb">&#39;</span></code>,
as in
<code class="goal-expr">f[<span class="xyz">x</span>;]<span class="adverb">&#39;</span></code>.
It can often be simplified further as in
<code class="goal-expr">(a<span class="dyad">+</span>)<span class="adverb">&#39;</span></code>
for primitive verbs. The
<code class="code">lib/mods.goal</code>
file provides common adverb combinations involving
each-right as user-defined functions.</p>
<p>The use of each-prior
<code class="code">&#39;:</code>
is replaced by the windows verb
<code class="goal-expr">i<span class="dyad">^</span>Y</code>
and the shift verbs
<code class="dyad">«</code>
and
<code class="dyad">»</code>
(with ASCII alternatives
<code class="dyad">shift</code>
and
<code class="dyad">rshift</code>).
Both approaches do have their strong and weak points. The
rationale here for Goal was that the shifting verbs are more
general and allow for more varied shifts of any size. Verbs
are also somewhat easier to use than adverbs. Also, I don’t
want digraphs, and I don’t know which symbol I could have
used for the adverb.</p>
<p>Last but not least, binsearch is now done with the
<code class="goal-expr">X<span class="dyad">$</span><span class="xyz">y</span></code>
verb form, and the first bin gets index
<code class="code">0</code>,
instead of
<code class="code">-1</code>,
like in BQN.</p>
<h3 class="Ss" id="tacit-compositions-differences">5.2.2 Minor differences in tacit verb trains <a class="anchor" href="#tacit-compositions-differences">#</a></h3>
<p>Tacit compositions work mostly the same in Goal, except for
adverbs being dyadic by default and needing a
<code class="code">:</code>
for the monadic version, which is possible because adverb
digraphs are gone. Also, in Goal compositions are just sugar
for a lambda or a lambda projection. As a result, we get
precise locations for any errors, and access to debugging
tools such as
<code class="goal-expr"><span class="special">\</span><span class="xyz">x</span></code>
or early-return.</p>
<p>See the question about
<a href="chap-FAQ.html#tacit-compositions">tacit compositions</a>
in the FAQ for more details.</p>
<h3 class="Ss" id="group">5.2.3 Group by, index-count <a class="anchor" href="#group">#</a></h3>
<p>The group verb
<code class="goal-expr"><span class="dyad">=</span></code>
in Goal uses BQN’s semantics. It works on indices only and
returns an array of groups based on those indices, not a
dictionary. As a bonus, negative indices can be used to
discard values.
Also, Goal only has the “group by” variant, using either the
<code class="goal-expr"><span class="dyad">=</span>d</code>
form, that groups dictionary keys using values for group
indices, or the
<code class="goal-expr">f<span class="dyad">=</span>Y</code>
form, that groups
<code class="code">Y</code>
using indices
<code class="goal-expr">f<span class="dyad">@</span>Y</code>.
The
<code class="goal-expr"><span class="dyad">=</span>I</code>
form is used for index-counting, often called unwhere,
similar to freq
<code class="code">#&#39;=</code>
in K but working on indices only and producing an array.
Grouping indices can still be done with
<code class="goal-expr"><span class="lambda">{</span><span class="dyad">=</span>(<span class="dyad">!</span><span class="dyad">#</span><span class="xyz">x</span>)<span class="dyad">!</span><span class="xyz">x</span><span class="lambda">}</span></code>.</p>
<p>This approach seems more Unix-like to me, in that it doesn’t
need to do any kind of self-search on the values: that goes
into separate verbs, like distinct or the new self-classify
<code class="goal-expr"><span class="dyad">%</span>X</code>,
equivalent to
<code class="goal-expr"><span class="lambda">{</span><span class="xyz">x</span><span class="dyad">?</span><span class="xyz">x</span><span class="lambda">}</span></code>.
While a bit more low-level and verbose in some cases, Goal’s
group makes it easy to implement various kinds of groupings,
including
<a href="chap-working-with-tables.html#table-group-by">“group by” functionality</a>
for tables.
Also, K’s semantics can still easily be obtained by
combining group with self-searching verbs: for example
<code class="goal-expr"><span class="lambda">{</span>(<span class="dyad">?</span><span class="xyz">x</span>)<span class="dyad">!</span><span class="dyad">=</span>(<span class="dyad">!</span><span class="dyad">#</span><span class="xyz">x</span>)<span class="dyad">!</span><span class="dyad">%</span><span class="xyz">x</span><span class="lambda">}</span></code>
for K’s
<code class="goal-expr"><span class="dyad">=</span>X</code>.
See
<code class="code">lib/k.goal</code>
at the root of the distribution for efficient user-defined
equivalents of K’s “group”, “group keys by” and “freq”.</p>
<h3 class="Ss" id="tables-dicts">5.2.4 Tables and dicts <a class="anchor" href="#tables-dicts">#</a></h3>
<p>While Goal doesn’t have a dedicated table type, several
primitives (like for filtering and indexing) offer
table-like functionality when dict keys are strings.  Along
with field expressions, this allows for quite concise table
manipulation in Goal.</p>
<h3 class="Ss" id="domain-swap-self-dict">5.2.5 Domain-swap and self-dict <a class="anchor" href="#domain-swap-self-dict">#</a></h3>
<p>Because Goal doesn’t have a dedicated table type, only
dictionaries, the
<code class="goal-expr"><span class="dyad">+</span>d</code>
form swaps keys and values, instead of producing a table.
Another nice new verb form for dicts is self-dict
<code class="goal-expr"><span class="dyad">.</span>X</code>
for lists, equivalent to
<code class="goal-expr"><span class="lambda">{</span><span class="xyz">x</span><span class="dyad">!</span><span class="xyz">x</span><span class="lambda">}</span></code>.</p>
<h3 class="Ss" id="zero-values">5.2.6 Zero values <a class="anchor" href="#zero-values">#</a></h3>
<p>Goal makes much less use of null values than ngn/k. For
example, outdexing results in various kinds of zero values,
depending on the array type.
For example, numeric arrays give
<code class="code">0</code>
and string arrays give
<code class="goal-expr"><span class="string">&#34;&#34;</span></code>.
See the
<a href="chap-FAQ.html#zero-value-fills">How do zero value fills work?</a>
question of the FAQ for details.</p>
<p>One nice thing about this approach is that outdexing the
result of group will always return an empty array.  Another
advantage is that zero is common to all numeric types,
including the smallest ones.</p>
<p>Also, the new
<code class="goal-expr">i<span class="dyad">@</span><span class="xyz">y</span></code>
form, called take/pad, takes advantage of zero-values. It’s
like
<code class="goal-expr">i<span class="dyad">#</span><span class="xyz">y</span></code>,
but when going out of range, instead of repeating elements,
it pads with zero value fills. It’s similar to
<code class="goal-expr"><span class="xyz">y</span><span class="dyad">@</span><span class="dyad">!</span>i</code>,
but it avoids actually generating indices.</p>
<h3 class="Ss" id="numeric-conversions">5.2.7 Numeric conversions <a class="anchor" href="#numeric-conversions">#</a></h3>
<p>In Goal, floating point numbers that can be represented as
an integer can be used anywhere the equivalent integer can
be used. This makes it so that, except for operations that
can overflow, the user doesn’t have to think about numeric
representations.</p>
<p>One thing worth noting is that flat numeric arrays
cannot be generic: if there’s a mix of integers and floats,
all values will become floats. This restriction rarely
matters, and it simplifies both implementation and
semantics.  Also, for convenience,
<code class="goal-expr"><span class="dyad">~</span></code>
returns true when matching integers with equivalent floating
point numbers.</p>
<p>See questions about
<a href="chap-FAQ.html#NaNs">NaNs</a>
and
<a href="chap-FAQ.html#conversions-overflow">numeric conversions</a>
in the FAQ for more details.</p>
<h3 class="Ss" id="without-take-drop">5.2.8 Take, Drop, Without <a class="anchor" href="#without-take-drop">#</a></h3>
<p>There are some differences in these primitives. Without
<code class="goal-expr">X<span class="dyad">^</span>Y</code>
has its arguments reversed with respect to ngn/k, for
consistency with the renamed weed out
<code class="goal-expr">f<span class="dyad">^</span>Y</code>
and the symmetry with the new
<code class="goal-expr">X<span class="dyad">#</span>Y</code>
that does intersection now, like it did in K9 last time I
read about it. Note that, for consistency,
<code class="goal-expr">X<span class="dyad">#</span>d</code>
removes from
<code class="code">d</code>
entries with keys not in
<code class="code">X</code>,
which is different from ngn/k, though it will probably only
matter when there are duplicate keys.
The
<code class="goal-expr">f<span class="dyad">_</span>Y</code>
form now does “drop where”, which is more in line with the
<code class="goal-expr">I<span class="dyad">_</span>Y</code>
form.</p>
<p>In Goal, there are also two new filtering forms
<code class="goal-expr">X<span class="dyad">#</span>t</code>
and
<code class="goal-expr">X<span class="dyad">^</span>t</code>
that provide a select-where kind of functionality for
tables which, together with
<a href="chap-FAQ.html#field-expressions">field expressions</a>,
allows for convenient table processing.</p>
<p>Also, reshape was removed, replaced with “cut shape”
<code class="goal-expr">i<span class="dyad">$</span>Y</code>,
which can cut into lines (positive
<code class="code">i</code>)
or columns (negative
<code class="code">i</code>).</p>
<h3 class="Ss" id="sort">5.2.9 Sort and Grade for dicts <a class="anchor" href="#sort">#</a></h3>
<p>Sorting works similarly as in ngn/k, except that
<code class="goal-expr"><span class="dyad">^</span>X</code>
was added as a means to sort a list without grading it, and
<code class="goal-expr"><span class="dyad">&lt;</span>d</code>
and
<code class="goal-expr"><span class="dyad">&gt;</span>d</code>
return dictionary results, preserving both keys and values.
Searching for null values is much less useful in Goal,
because few primitives can generate them (namely, parsing
with
<code class="goal-expr"><span class="string">&#34;n&#34;</span><span class="dyad">$</span></code>),
so finding
<code class="goal-expr"><span class="number">0n</span></code>
values is now done with the
<code class="goal-expr"><span class="dyad">nan</span></code>
keyword. The null integer value, called
<code class="goal-expr"><span class="number">0i</span></code>
in Goal (following the type name), can be searched for with
equality too.</p>
<h3 class="Ss" id="conditionals">5.2.10 Cond <code class="dyad">?</code> and logical <code class="dyad">and</code>/<code class="dyad">or</code> <a class="anchor" href="#conditionals">#</a></h3>
<p>Cond is written with
<code class="goal-expr"><span class="dyad">?</span>[cond;then;else]</code>
instead of
<code class="goal-expr"><span class="dyad">$</span>[cond;then;else]</code>.
For convenience, there are also short-circuiting
<code class="goal-expr"><span class="dyad">and</span>[<span class="xyz">x</span>;<span class="xyz">y</span>;…]</code>
and
<code class="goal-expr"><span class="dyad">or</span>[<span class="xyz">x</span>;<span class="xyz">y</span>;…]</code>,
which are nice for handling error cases.</p>
<h3 class="Ss" id="new-zero-values">5.2.11 New false values <a class="anchor" href="#new-zero-values">#</a></h3>
<p>Goal introduces new false values in conditionals like
<code class="number">0i</code>,
<code class="number">0n</code>,
and
<code class="number">-0w</code>.
The reason behind this change for
<code class="number">0i</code>
is that it makes
<code class="goal-expr"><span class="dyad">|</span><span class="adverb">/</span><span class="dyad">!</span><span class="number">0</span></code>
return a false value, avoiding a typical problematic edge
case. The other two new numeric false values were added for
consistency, so that numeric null and smallest values are
all false, though they’re more unlikely to be used in
conditionals.</p>
<p>See question about
<a href="chap-FAQ.html#false-values">false values</a>
in the FAQ for more details.</p>
<h3 class="Ss" id="list-eval-order">5.2.12 List syntax uses left-to-right evaluation <a class="anchor" href="#list-eval-order">#</a></h3>
<p>In Goal, expressions in a list are evaluated left-to-right
and are like sequences that collect all the values. This is
more convenient when using the list syntax together with
assignments, in particular in multi-line lists.</p>
<p>The only case of right-to-left evaluation in Goal happens
for arguments due to such order being the most useful in
dyadic infix operations, given verb associativity rules.</p>
<h3 class="Ss" id="amend-assignment">5.2.13 Amend assignment is just sugar around amend <a class="anchor" href="#amend-assignment">#</a></h3>
<p>I’m not sure if all K implementations do the same thing
about this but, in Goal, the
<code class="goal-expr"><span class="xyz">x</span>[<span class="xyz">y</span>]op<span class="stmt">:</span><span class="xyz">z</span></code>
form is equivalent to
<code class="goal-expr"><span class="xyz">x</span><span class="stmt">:</span><span class="dyad">@</span>[<span class="xyz">x</span>;<span class="xyz">y</span>;op;<span class="xyz">z</span>]</code>.
It returns the whole modified array, not just the parts that
where modified.</p>
<h3 class="Ss" id="no-splice">5.2.14 There is no splice triadic form <a class="anchor" href="#no-splice">#</a></h3>
<p>Splice is mainly useful for array-style text-handling.
String-handling primitives cover that usage in Goal.</p>
<h3 class="Ss" id="find">5.2.15 Rank-insensitive find <code class="dyad">?</code> <a class="anchor" href="#find">#</a></h3>
<p>Some K dialects, like ngn/k, have a rank-sensitivity
concept, that makes
<code class="goal-expr">X<span class="dyad">?</span></code>
behave like an inverse of
<code class="goal-expr">X<span class="dyad">@</span></code>
and facilitates searching for non-atomic values, at the cost
of not working for generic lists with mixed “ranks”.</p>
<p>The find dyad
<code class="dyad">?</code>
is rank-insensitive in Goal. That means that the result
always has the same length as the right argument and is
always flat. Find works for any kind of value, but searching
for a single non-atomic value requires enlisting it.</p>
<h3 class="Ss" id="no-deep-where">5.2.16 No deep-where <a class="anchor" href="#no-deep-where">#</a></h3>
<p>Because Goal uses monadic
<code class="monad">&amp;</code>
pervasively for strings, having deep-where would not be very
natural given its dependency on the rank concept (which we
don’t follow for find
<code class="dyad">?</code>
either).</p>
<h3 class="Ss" id="get-parse-eval">5.2.17 Get Global, Eval and Parse <a class="anchor" href="#get-parse-eval">#</a></h3>
<p>The
<code class="goal-expr"><span class="dyad">.</span>s</code>
form in goal has the same meaning as for symbols in K: it
gets the value of a named global.</p>
<p>The usual general eval is done using the
<code class="goal-expr"><span class="monad">eval</span> s</code>
form in Goal.</p>
<p>There is also a new
<code class="goal-expr"><span class="monad">eval</span>[s;loc;pfx]</code>
triadic form. The location string
<code class="code">loc</code>
serves two purposes: it is used to provide error locations
and to avoid evaluating twice a same location in the main
context. The prefix string
<code class="code">pfx</code>
is used for globals in
<code class="code">s</code>
during evaluation. This new form of eval is the basis for
the new
<code class="goal-expr"><span class="dyad">import</span></code>
keyword, that provides a more typical import mechanism.</p>
<p>Goal also provides
<code class="goal-expr"><span class="string">&#34;v&#34;</span><span class="dyad">$</span>s</code>
as a partial inverse for
<code class="goal-expr"><span class="dyad">$</span><span class="xyz">x</span></code>
that works on values whose atoms are only of numeric, string
or regexp type. It does not support arbitrary execution of
Goal code, but it can be implemented more efficiently than
general evaluation, without requiring byte-compilation nor
execution. It can be used as a serialization method with
acceptable performance and the advantage of better matching
Goal’s types than for example
<code class="dyad">json</code>.</p>
<h3 class="Ss" id="IO">5.2.18 Input/Output with keywords <a class="anchor" href="#IO">#</a></h3>
<p>Interacting with the OS is done using keywords in Goal.
There’s currently support for typical input and output
operations, running commands and pipes, and interacting with
the environment. There’s currently no networking support.
Until then, because Goal is usable as a library, such things
could be added by a user using Go’s standard library.</p>
    <div class="topnav">
      <ul class="topnav">
        <li><a href="chap-FAQ.html">&lt;</a></li>
        <li><a href="index.html">Index</a></li>
        <li><a href="chap-working-with-tables.html">&gt;</a></li>
      </ul>
    </div>
<footer class="footer">
<hr><p>This website’s content is licensed under
<a href="https://creativecommons.org/licenses/by/4.0/">cc-by</a>
or the ISC license, as you wish. Contact:
<code class="email"><span class="string">&#34;@&#34;</span><span class="adverb">/</span>(<span class="string">&#34;anaseto&#34;</span><span class="sep">;</span><span class="string">&#34;.&#34;</span><span class="adverb">/</span><span class="dyad">!</span><span class="string">&#34;bardinflor perso aquilenet fr&#34;</span>)</code>.</p>
</footer>
  </body>
</html>
