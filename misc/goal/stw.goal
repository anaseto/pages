/ We translate to goal most examples from:
/ https://dplyr.tidyverse.org/articles/dplyr.html
/ We put in a comment the dplyr version for each example (uncomment to print
/ it before the table).

/ pp[t] pretty prints dict as table, assuming string keys and same-length flat
/ columns.
pp:{
  (nr;nc):(#*x;#x); say "==> Table $nr x $nc"
  x:x[!5&nr;(8&nc)#!x] / keep up to 5 rows and 8 columns
  k:"s"$!x; v:{?[(@x)¿"nN";"%.1f"$x;$'x]}'.x; w:(&k)|(|/&:)'v
  (k;v):(-w)!'´(k;v); say"\n"/,/(" "/k;" "/"-"*w;" "/'+v)
}
dplyr:{c:80*"=";say"$c\n>>> $x"}     / pretty-print dplyr code
't:json 'read"starwars.json"         / read json file into t (starwars in dplyr examples)
t:+sub["_";""]@+t                    / clean non-identifier column names
t:@[t;!"height mass birthyear";"i"$] / cast integer columns from json float
/ We reorder columns to match the dplyr article because goal's json builtin
/ sorts keys.
t:t[;!"name height mass haircolor skincolor eyecolor birthyear sex
       gender homeworld species films vehicles starships"]
/ dplyr"dim(starwars)"
say(nr:#*t;#t) / dimensions
/ dplyr"starwars"
pp t
/ dplyr rq/starwars %>% filter(skin_color == "light", eye_color == "brown")/
pp(1;..skincolor="light";..eyecolor="brown")#t
/ dplyr"starwars %>% arrange(height, mass)"
oby:{y@`x y}; rmnan:{(0;..|/nan'x@!p.x)^y} / order-by and removing nans
pp oby[..<height]oby[..<mass]rmnan["mass height";t]
/ dplyr"starwars %>% arrange(desc(height))"
pp oby[..>height]rmnan["height";t]
/ dplyr"starwars %>% slice(5:10)"
pp t[4+!5]
/ dplyr"starwars %>% slice_head(n = 3)"
pp t[!3]
/ dplyr"starwars %>% slice_sample(n = 5)"
pp t[5?nr]
/ dplyr"starwars %>% slice_sample(prop = 0.1)"
pp t[(-10!nr)?nr]
/ dplyr"starwars %>% filter(!is.na(height)) %>% slice_max(height, n = 3)"
pp 3#'oby[..>height]rmnan["height";t]
/ dplyr"starwars %>% select(hair_color, skin_color, eye_color)"
pp t[;!"haircolor skincolor eyecolor"]
/ dplyr"starwars %>% select(hair_color:eye_color)"
krng:{[t;x;y](k?x)_(1+k?y)#k:!t} / inclusive key range from x to y
pp krng[t;"haircolor";"eyecolor"]#t
/ dplyr"starwars %>% select(!(hair_color:eye_color))"
pp (krng[t;"haircolor";"eyecolor"]^(!t))#t
/ dplyr rq/starwars %>% select(ends_with("color"))/
pp (rx/color$/!:)#t
/ dplyr"starwars %>% select(home_world = homeworld)"
pp t[;"homeworld"]
/ dplyr"starwars %>% rename(home_world = homeworld)"
pp (,"homeworld")^t..[homeWorld:homeworld] / camelCase instead because already removed _
/ dplyr"starwars %>% mutate(height_m = height / 100)"
pp rmnan["height";t]..[heightm:height%100]
/ dplyr"starwars %>% mutate(height_m = height / 100) %>% select(height_m, height, everything())"
pp rmnan["height";t]..[heightm:height%100][;"heightm","height",(,"height")^!t]
/ dplyr"starwars %>% mutate( height_m = height / 100, BMI = mass / (height_m^2)) %>% select(BMI, everything())"
pp rmnan["mass height";t]..[heightm:q.hm:height%100;BMI:mass%(q.hm*q.hm)][;"BMI",!t]
/ dplyr rq#starwars %>% mutate( height_m = height / 100, BMI = mass / (height_m^2), .keep = "none")#
pp rmnan["mass height";t].. ..[heightm:q.hm:height%100;BMI:mass%(q.hm*q.hm)]
/ dplyr"starwars %>% summarise(height = mean(height, na.rm = TRUE))"
avg:{?[x:(nan)^x;+/x%#x;0n]}
pp@..[height:,avg t..height]
/ dplyr"starwars %>% group_by(species, sex) %>%
/       select(height, mass) %>%
/       summarise(
/         height = mean(height, na.rm = TRUE),
/         mass = mean(mass, na.rm = TRUE)
/       )"
/ rby groups by cols k, summarises with reducing f[t;by] (by contains grouping
/ indices), sorts result by k.
rby:{[t;k;f]
  m:¿by:%?["A"~@g:t k;{(1+|/'x)/x}@%'g;g]
  (((..&x!p.m)'t[;k]),f[t;by]){x@`<x y}/|k
}
pp rby[t;!"species sex";{..[height:avg'=x..height!y; mass:avg'=x..mass!y]}]
/ dplyr"select(starwars, name)"
pp t[;"name"]
/ dplyr"select(starwars, 1)"
pp 1#t
/ dplyr rq/name <- "color"; select(starwars, ends_with(name))/
name:"color"; pp {~(!x)=(!x)-name}#t
/ dplyr"df <- starwars %>% select(name, height, mass)"
df:"name""height""mass"#t
/ dplyr"mutate(df, height + 10)"
pp df..["height+10":height+10]
/ dplyr"var <- seq(1, nrow(df)); mutate(df, new = var)"
var:1+!#*df; pp df..[new:q.var]
/ dplyr"starwars %>% filter(species == "Droid")"
pp (1;..species="Droid")#t
/ dplyr"starwars %>% group_by(species) %>% summarise( n = n(), mass = mean(mass, na.rm = TRUE)) %>% filter( n > 1, mass > 50)"
pp (1;..n>1;..mass>50)#rby[t;"species";{..[n:=y; mass:avg'=x..mass!y]}]
