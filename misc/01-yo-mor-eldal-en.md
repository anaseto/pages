I, Mor-Eldal: The Necromancer Thief, Book I
===========================================

Work under a [Creative Commons
By](http://creativecommons.org/licenses/by/4.0/)
license. Translation of *Yo, Mor-Eldal*.

2022, Marina Fernández de Retana alias “Kaoseto”.

mail: *kaoseto AT bardinflor P perso P aquilenet P fr*
site: https://bardinflor.perso.aquilenet.fr/kaosfantasy/index-en.html

Redaction achieved using frundis and Vim.

Notes for reading
=================

**Time** Time is counted in hours, days, moons, and
years. A year has twelve moons; a moon, thirty days; a
week, six days (Youngday, Thunderday, Brushday, Mistday,
Kindday, Sacredday).

**Names of moons** Winter: Purple - Darkness - Mud |
Spring: Straw - White - Celestial | Summer: Gadflies -
Redmoon - Wells | Autumn: Joys - Wolves - Squalls

**Saïjits** The saïjits (a group of twenty humanoid
races) have a life expectancy of 80 to 120 years
depending on the place.

**Currency** Half-nail (coin with a hole), nail,
five-nail, dixclous (rhomboidal shape), coin of
twenty-five nails, coin of half a siato, siato (100
nails or cents, a goldy), coin of five siatos, crown (20
siatos, a whitewheel); assignat of 1 siato, of 2, 5, and
20 siatos

So long, my friends, so long
============================

—“If only you’d think before you act, Mor-eldal.”

I looked up in frustration. I had climbed a tree to get
a closer look at a pretty, colorful bird and, thinking I
was an expert climber, I had gone too fast, slipped, and
fallen. And to my great disappointment, the big bird
flew up just then, the branch shook, and a shower of
snow landed on my head.

My master let out a dry laugh.

—“It’s amazing how stubborn you can be, boy. Birds are
like squirrels: you can look at them, but from a
distance. Come on, get up and go home. You’ll end up
freezing in here.”

Not him, though, I thought. My master was a *nakrus*, an
undead wizard. He was never cold.

I stood up, and with his skeletal hand, he ruffled my
snow-soaked hair. He said in a light tone:

—“You know, Mor-eldal? Sometimes I wonder what the hell
you’re doing here, with this grumpy old skeleton,
instead of going off to find your own. Did I ever tell
you that?”

I rolled my eyes.

—“Well, I don’t know… around a thousand times,
*Elassar*?”

—“Ah! That many times, eh? And what do you say?”

—“That I don’t want to go,” I replied like a refrain.

—“Yeah. What if I make you?”

His magical green eyes were so intense that they were
like stars. I tilted my head, confused. Usually, when he
asked me that question, he never pressed the issue. But
now he’d spent most of the winter talking about grumpy
skeletons, and I was getting tired of this.

So I just looked at him, stubbornly, and we started up
the snowy slope towards our cave.

Frankly, I wondered if my master would ever be able to
say to me: go away, Mor-eldal. Inwardly, sometimes I
hoped not, and other times I hoped he would, but I had
to admit that I had no intention of leaving him. He was
the one who had found me, lost in the mountains, when I
was almost six years old, he was the one who had saved
me from the cold and from certain death, and he was the
one who had taught me everything I knew. I loved him
like a father. So, logically, I wasn’t going to leave
him any time soon. Maybe one day, I thought, maybe when
I grew up, to see a bit of the world. But to tell the
truth, I was quite satisfied with my master. What the
hell, I was happy.

—“Ah, darn it,” I said, breaking the silence. “I forgot
to take the rabbit. One fell into the trap. I saw it,
and then I saw the squirrels and went to greet them, and
then the bird came, and… Well, I forgot. Should I go get
it?”

—“Go,” my master sighed.

I ran my way down the hill. We were high up in the
mountains, and though it was already spring down in the
valley, there was still snow where we lived, and the
trees had not yet awakened. I reached the trap, picked
up the rabbit, and heard a bird sing.

—“Ah, no,” I muttered. “You again?”

It was perched on a lower branch, on the same tree as
before. I approached and watched him, fascinated. How
beautiful it was! It had yellow, blue, green, and red
feathers. It was a yarack.

I whistled, imitating its song, and I thought I saw it
look down at me in surprise. I laughed and pointed at
the bird.

—“You thought you could sing better than me, yarack? You
big conceited bird!”

It let out a shrill cry, and I sighed.

—“And a touchy one at that! Eh, no need to get angry,
friend. Here, if you give me a feather, I forgive you.
Do we have a deal?”

It flew away, and I growled.

—“And it’s a coward, at that!”

Then I saw a yellow feather fall, and my eyes widened.

—“I can’t believe it!” I exclaimed.

I picked up the feather, twisted it between my fingers,
and suddenly I took my legs, climbed the hill, and
stormed into the cave, shouting:

—“It left me a feather! Look, master! I asked it for a
feather, and the yarack left me one! Do you believe me?”

—“I believe what my eyes see,” my master replied,
amused. He was sitting on his big trunk with a big book
in his hand. We had three books in all. A small book of
stories with pictures, a dictionary, and a big, fat old
volume that talked about necromancy. Yes, we were
necromancers. Well, especially my master.

I left the rabbit on the floor, removed my wet clothes
and, once wrapped in my blanket, picked up my yellow
feather with a smile.

—“It was generous, after all. To think I called it
conceited, touchy, and a coward!”

—“All that, huh? Well, you’ve told it a lot in a short
time,” my master snorted without even looking up.

—“Hey, I know, I spoke too soon.”

—“What did I tell you before? You have to think before
you act. If you don’t think, you do something stupid.”

—“Yes… Well.” I left my new feather in my bundle with my
things. “I’m going to prepare the rabbit. Do you want
the bones now or shall I save them for you?”

My master needed it to regenerate the morjas in his
bones and keep himself alive. Just as I needed to eat
the flesh.

—“Save them for later,” my master replied, distracted.
“And don’t sing, please. I am reading.”

I huffed.

—“Yeah, right, you’re reading. You know your book by
heart, though!”

And I began to sing: Larilan, larilon, hey, Spring, come
out now, bombumbim, how nice, it is spring… Then I saw
my master’s eyes widen. And I sighed.

—“Okay, okay. I’ll shut up.”

So I worked, and in the evening, I had already filled my
stomach, I had cleaned the bones, and I lay on my pallet
and contemplated my feather pensively. The light of the
lantern shone in the cave. It was my master who fixed it
whenever it broke. To my left was a mirror. It was
centuries old, millennia old, who knows. But it worked,
and I looked at myself for a moment, holding the feather
in my right hand, the only thing in me that resembled my
Master: it was made of bone. My Master had saved me when
I had been on the verge of freezing to death, that
famous winter’s night… how many years now? Five winters
ago. He had taught me to activate my hand with mortic
energy and to feel with it. I waved the yellow feather,
and… my jaw dropped.

—“Master!”

—“Mm?”

I looked up at him. He hadn’t changed his position in
hours. He had no problem with pins and needles or sore
muscles. He was never in pain anywhere, and yet I knew
he was feeling things, just as I was with my right hand.

—“The mirror!” I exclaimed. I stood up. I waved my
feather. And I said, “You never told me the mirror was
lying!”

—“You say?”

—“The mirror lies! I’m sure of it. Look at it. I’ve got
the feather here, in my right hand. And when it passes
in the mirror, hop, it’s in the other hand. And, hop,
it’s in the other hand,” I said, changing the feather
several times. Then I stammered: “But this is not my
right hand!”

Mor-eldal’s skeletal hand in the mirror was his left.

My master burst out laughing. In the mirror, I saw his
mandible come off in a clattering back and forth.

—“I said something stupid,” I concluded, questioningly.

—“No, no, son, you are quite right!” my master assured
me. “What makes me laugh is that you are only finding
out now. Mirrors don’t reflect reality. They’re too
rigid for that.”

—“Too rigid?” I repeated as I settled myself at the foot
of the trunk. “What do you mean?”

—“I mean they’re lazy: they take the color they have
right in front of them and they bring it back in a
straight line.”

I pondered about what he said. Well, okay. If you put it
that way, it sounded logical.

—“And you knew about it and never told me?”

—“You still have so much to learn, my boy!” He turned
his head towards me. “If you were a little more curious
and let go of me a little, you’d go out and see the
world of your own kind and you’d learn a lot faster, a
lot quicker than staying here on this mountain, playing
with squirrels and listening to an old fool like me talk
about dead ages. You would learn and, above all, you
would live, son, you would meet real friends, friends
like you, with two legs and two hands. But that doesn’t
interest you, does it? You’re as stubborn as a mule, and
that’s why you’ll never see a mule and you’ll stay here
counting stars and lazing around, and you’ll end up like
me!”

*And what’s wrong with that?* I wanted to reply. But his
words left me speechless. It was not the first time he
had given me speeches like that. However, hearing it so
often was getting on my nerves and making me worry.

I bit my lip.

—“But, Elassar,” I asked, “you don’t mean it, do you?”

—“Yes, my boy, I mean it,” my master assured as he
turned a page of the book with his skeletal finger.

I looked at him, I looked at us in the mirror, and I
said:

—“Well, you know, if you ask me, I wouldn’t mind being
like you. Okay, you’re less agile, but you’re not cold.
And this winter, I’ve been damn cold, I’m telling you!”

—“Don’t get on my bones,” my master growled.

I sighed and went to lie down again on my pallet with my
feather. After a moment, I chewed:

—“Good night, Elassar.”

—“Good night, Mor-eldal.”

I put down the feather, closed my eyes, and shook my
head when I opened them again, suddenly very serious.

—“Look, Elassar. I don’t want to leave. So, if you tell
me once again to leave, make it clear, and I’ll leave
for good, even if I don’t want to, but otherwise, don’t
get on my bones, either. There, I’ve said it. I love
you, master.”

He did not answer me. And I was almost asleep when I
heard him whisper:

—“I love you too, little one. I love you too.”

The next day when I awoke, I found him sitting as usual
at the entrance of the cave. I stretched, dressed, and
said with a yawn:

—“Good morning, Elassar!”

He answered me cheerfully:

—“A great morning indeed! Today is the best day ever!”

I scratched my head, curious.

—“Oh, yeah?”

—“Yeah. You’re going to leave and go see the world! Ha!
Isn’t that wonderful?”

I turned deathly pale and realized that I had said too
much the day before.

—“I… don’t understand,” I said.

—“Yes, you do! You understand perfectly! You promised me
that you would go away if I asked you to leave. And now
the big day has arrived! You’re leaving!”

He said it in such a cheerful tone!

—“But, yesterday, you said you loved me!” I protested.

—“What’s that got to do with it?” His eyes smiled with
joy. “You’re going to make acquaintances, you’re going
to learn amazing things, you’re going to see the
pictures in the storybook but in real life! Don’t tell
me you don’t want to go?”

—“But to go where?” I exclaimed, agitated.

—“What do I know! There, towards the sunrise, where the
sun is born everyday. You’ll find a sajit on the way.
Walk and you will see. Look, I’ve packed the bag for
you, with the provisions we had left. And this, son, is
your new hand. I’ve been preparing it all winter. It’s a
surprise. You like it? Put it on. Then I’ll fix it. And
the blanket, give it to me, I’ll improve the spell, so
you won’t be cold. Come on, move it, lazybones!”

I moved. Only a few inches. My gaze fell, bewildered, on
an object which, in fact, looked exactly like my left
hand, except that this new hand was not real, it was a
magara. Understanding more clearly that this was
serious, my confusion gradually gave way to horror, then
to a feeling of abandonment, and finally, I burst into
tears.

—“Elassaaaaar! This is too cruel!”

—“My boy! What’s all this crying? You are grown up now,
you are past the age of crying. Quiet.”

I looked at him in dismay as he took my blanket and
concentrated. My tears flowed freely. I waited for him
to finish his spell almost without making a sound, but
when he did, I sobbed:

—“Please, master. Throw me out if you want, but come
with me, then.”

—“Yeah, right, so they can burn me at the stake? No, my
boy, I’m past the age of going on adventures. My bones
are old, I’m a thousand years old, and I can’t run up
hill and dale anymore…”

—“That’s not true, that’s too cruel,” I repeated.

—“Tell me, Mor-eldal. Will you stop talking nonsense and
think for a moment? Take your blanket. And your hand.
Put it on. Come on, come on. Let’s see how it fits.”

He put it on me, and I didn’t resist. I was too sad. He
proceeded to attach my hand to the bones and my wrist
while saying:

—“I taught you to look after yourself and see the
reality as it is. So face it, swallow your tears, and
listen to me. Wherever you go, never mention necromancy
or your hand. Never, do you hear me? You know it’s not
right. Avoid talking about me to anyone, but if it slips
out, say I’m dead, I’m not kidding, and never say I’m a
nakrus. I don’t want to end up with curious adventurers
looking for a wizard hermit: I hate visitors. And one
more thing,” he added. “Don’t forget everything I’ve
taught you and, above all, Mor-eldal, above all, never
stop being yourself.”

I looked at him, my mouth agape, the blanket under my
arm, and with my hand now almost completely fixed.

—“So… this is serious,” I muttered.

My master huffed.

—“Of course it’s serious! You doubt it? Now hold still,
don’t move.”

I did not move, but my tears were still flowing,
unstoppable. When he finished his work, my master hummed
happily.

—“Shake it, see if it works.”

I stirred it, and for a moment, I almost forgot my
sadness and smiled, impressed.

—“It’s almost like the other one!”

—“And that’s the goal. But, look closely, the thumb is
on the other side.”

Of course, I thought. Of course it was. It was like the
mirror.

—“Take care of it, huh? It’s tough, but don’t put it in
an oven. I don’t think it would burn; as I say, it’s
tough, but that’s precisely what might attract
attention. Be careful not to pierce it. The sajits would
expect to see it bleed. And… remember, if it does get
damaged, you can regenerate it, and you can also make it
grow to be the same size as the other one. It’s almost
like waking up the morjas of the bones, except that you
have to wake up the morjas of the skin. I taught you how
to do it, remember? Well, I’ll show you again, in case
you forgot.”

He showed me, and folding and unfolding my hand, I
asked, curious:

—“How did you make it?”

—“With patience, art, and a beaver skin,” my master
said, smiling. “And now, boy, get up and go. Say, don’t
you want to take the Drionsan dictionary with you? But I
am talking nonsense, what would you want it for?
Modern-day words have surely changed. You’d only get
into trouble if you were caught with such an old
dictionary. Be clever, kid, speak in Drionsan and not in
Caeldric, or Morelic, as they call it… Be friendly, and
try not to open your mouth too much at first, eh? That
way you’ll avoid trouble.”

At the entrance, he handed me the bag. The day was
bright, though a little cool, and the cold wind hurt my
eyes. My master patted me on the shoulder, obviously
moved.

—“Ah, boy. You don’t know how much I’ve dreamed of this
day. Not that I want to see you go, but I want to see
you discover new things. And you will! Woe betide you if
you die on the way before you find a sajit, eh? Or I’ll
pull your ears off. Come on, don’t cry. Give me a hug.
That’s it.”

I hugged him gently, my cheeks moist.

—“Do you really want me to go?”

—“Yes.”

—“But I can come back, right?”

—“Don’t even think about it. Not until you find a
ferilompard bone. These are the best. When you get it,
you can come back. Not until then. Come on, go, follow
the dawn sun.”

I turned to the east and saw only mountains and forests.
Then I stepped out of the cave… and turned around.

—“Wait, I forgot my yellow feather.”

I went to get it, put it in my bag, and when I got back
outside, I stopped and said:

—“Damn. My staff.”

My Master gave a patient grunt. I went to fetch my staff
and, once again outside, breathed in, remembered the
lesson my master had once given me about courage, and
sighed.

—“Well, I’m going. But only because you’re throwing me
out. You know what? There’s something I never told you.
The most stubborn one here is you, not me.”

—“Ha! Like father, like son, as they say!” my master
laughed, and he made a gesture to encourage me. “Now
go!”

I walked away, turning almost at every step, then every
fifth step, then every twentieth step, and then I lost
sight of the only person I could remember knowing.
There, I was gone. And I was just beginning to realize
what that meant. I was all alone, I didn’t know where I
was going, I had no one to talk to… It was scary. I only
hoped that my master was right and that there were
indeed humans, and elves, and caitians out there, beyond
the forests. And I also hoped that the ferilompard
wouldn’t be too hard to find.

I saw a black squirrel on a branch and raised my new
hand to say goodbye.

—“Good luck, my friend! Elassar wants me to leave, and
I’m leaving. But when I get the ferilompard bone I’ll be
back, I promise you!”

Another squirrel appeared on the same trunk, and my
heart sank again. Squirrels had been my friends for so
long! I would never forget them, their games, and all
they had taught me about acorns and trees and so much
more! I took a deep breath and sang:

Squirrels, squirrels,\
mountains, sunshine, and myrtles!\
Flowers blossom, jonquils grow,\
and love rises from snow!\
Squirrels, squirrels,\
oh how I love you all!\
So long, my friends, so long.\
Don’t forget me, please, nor my song.


Long after I had lost sight of my squirrels, I heard a
shriek in the sky, stuck my staff in the snow, and
looked up. It was the yarack! It was going much faster
than I was.

—“If only I could fly like you, bird!” I exclaimed.

I saw it disappear behind the trees and growled:

—“Mm, yeah. Cowardly maybe not, but you can’t say you’re
not conceited.”

Humans are nicer than I thought
===============================

I traveled for days before I left the mountains behind.
Once, I met a bear, and if I hadn’t been able to climb
trees as fast as the squirrels, who knows how I might
have ended up.

I did not have many provisions left in my pack when, one
day, I came to some treeless land, relatively flat and
dotted with flowers. I wandered through it with
curiosity and saw a large herd of deer in the distance.
I continued to move forward when I came upon a huge
white snake. Well, it wasn’t a snake, it was a trail.

—“A path,” I said aloud.

I was pretty sure that this was what was called a path.
And as my master had told me that paths were made for
walking, so I did.

—“Where will you take me?” I asked.

—“To the east, to your own kind!” I exclaimed.

Well, since my master wasn’t by my side for real, I
pretended he was. It was always comforting to hear him
speak, even if it was through my own voice.

When the sky began to darken, I strayed from the path
and went back into the woods, only to realize that the
white snake had slightly led me away from the mountains.
I didn’t quite like that. I walked between the trunks,
patting them and watching them, and then stopped in
front of one and smiled.

—“Tonight I sleep with you, big tree. You don’t mind? I
swear I don’t snore anymore. My master told me so.”

I climbed the branches and settled down in the heart of
the tree. The sounds of the night were similar to those
back in the cave. Which is why, tired and serene, I fell
asleep right away. I dreamed that I was sitting on a
huge branch, surrounded by several squirrels, and one of
them, with a serious expression, was taking an acorn and
throwing it far, far away, to a field of flowers that
lit up like lanterns in the middle of the night.

When I awoke, I yawned, ate some of the roots from my
supplies, and went on my way, but this time, I did not
go back to the white snake: I went along the mountains,
through a land covered with tall grass. I was whistling
when I came to the top of one of those small mountains
that must have been called hills and saw the forest. It
was very large, but it was not in the mountains.

It was frightening to be so alone, I must admit, because
sometimes, I imagined that a lynx or one of those
monsters my master had told me about would appear and
attack me. And walking through a treeless meadow worried
me. How could I escape a wolf if it ran faster than me?

With relief, I entered the forest and felt almost at
home again. Not completely, because my master was not
there, nor the slopes, nor the squirrels, and the trees
were different. The forest was very dense and I lost the
sunrise. I found some berries, but my master had told me
that I should not eat what I did not know, so I did not
touch them. Instead, I ate the eggs from a bird’s nest.
I found them delicious.

—“I like you very much, trees,” I said one day, a little
irritated. “But I did not leave my master to come and
see you. I came to see my own kind, my people. And I
have been looking for them for days. And nothing, I
can’t find them. I should have followed the white
snake,” I muttered. “Shouldn’t I?”

But it was too late to turn back: I didn’t know where I
had come from. So I kept going. And luck smiled on me
when, on the evening of that same day, I saw that the
forest was disappearing and giving way to hills of
grass. I did not dare to go far from the edge when,
suddenly, I saw the white snake, and I laughed.

—“Ah, there you are!”

Despite the rain, I ran out of the forest and went to
check that it was a path. I followed it in the rain, and
then, freezing, I took refuge in a large, lonely tree
near the snake, climbed up, and wrapped myself in my
blanket. The spell on the blanket was not working as
well as before, and it was not as warm either. How many
days had it been since I had left my master? I didn’t
know, but quite a few days already.

The next morning, I awoke with a start when I heard a
noise. I opened my eyes and clung to the largest branch
in wonder. On the white snake, a hornless deer was
passing by with a person on it.

—“My own kind,” I murmured.

A wave of memories came over me, from when I was a
little boy playing in front of a house. The deer was
trotting along and soon disappeared behind a hill.

—“It can’t be a deer,” I muttered, remembering a story
my master had told me. What then? A mule?

The best thing to do was to ask my own kind. I looked up
and was suddenly left speechless. There, in the
distance, I saw a city. There were houses. And people.

When I got there, I first looked around at least twenty
times. I took a few more steps and entered the city.
Curiosity and wonder made me turn my head like a
restless squirrel. I found a larger place where I saw
people behind tables with containers full of food. In
one of them, there were strawberries, and my mouth
watered.

It was only after a long time that I gathered my courage
to speak with a person a little taller than me, who was
eating something with a good appetite.

—“Do you speak my language?” I asked.

He looked me up and down, eyes wide, and swallowed what
was in his mouth.

—“By all the Spirits, what’s that?” he let out.

I smiled, relieved to understand him, then thought about
the meaning of his question and returned a puzzled look.

—“Excuse me?”

—“Excuse me,” the boy repeated, he looked at me again
from head to foot, then laughed. “I can’t believe it!
Are you human?”

I shrugged.

—“That’s what I’ve heard. What about you?”

The boy was laughing. At least he was cheerful.

—“Human to the core,” he replied. “You, on the other
hand, look like an out-and-out savage.”

—“Never heard of Atenout,” I confessed. “Is it a new
place?”

—“That one’s crazy,” the boy commented. And he turned to
another person who was busy at a table loaded with food.
He shouted, “Hey, Mama! Did you see him?”

Mama looked at me, and her expression changed.

—“Oh, you poor little thing!” she exclaimed. “You are so
skinny!”

I smiled at her.

—“Hello,” I said. I had not finished the word when Mama
handed me something that looked like a fruit. I took it
with an exclamation of surprise. “How kind!”

I was hungry, so I took a bite. It burned my tongue a
little. I asked:

—“This city, what’s it called?”

—“A city?” Mama repeated as the boy burst out laughing.
“It’s a village, kid. If you’re looking for a city,
you’ll have to go on this way. Where are you from?”

I made a vague gesture with my hand.

—“From over there.”

—“From the forest?”

—“From further away.”

—“From the Evon-Sil Valley?”

—“That’s it!” I nodded, relieved. At least that name
hadn’t changed.

—“And you came all this way alone? Why?”

I shrugged.

—“To go see my people.”

—“Your people! And where do they live?” As I cast a
puzzled glance at her, she suggested, “At Estergat?”

I nodded slowly and asked:

—“Is Estergat a city?”

—“Hahaha…!” the boy laughed.

—“Hishiwa!” Mama scolded. “Yes, kid, it’s a city. It’s
the capital of the Republic of Arkolda.”

—“Estergat, then,” I said, smiling. “By the way, thanks
for the fruit. It’s really good.”

The boy collapsed with laughter.

—“The fruit, he says!”

—“Quiet, Hishiwa!” Mama demanded, her eyebrows furrowed.
“Tell me, little one. What’s your name?”

—“Mor-eldal,” I replied.

—“Well, Mor-eldal, you are in luck. My son will travel
to Estergat to work for his uncle the glassmaker. He
will leave in an hour with old Dirasho, on his cart. If
you like, you can wait and he will take you too, what do
you think?”

—“Ah, thank you, Mama!” I said, not knowing how to thank
her better.

Mama smiled at me, and I smiled back. Her smile widened
and then faded as Hishiwa laughed harder.

—“Ah, Mama, I’m going to travel with him? And there I
thought I was going to be bored to death with old
Dirasho!”

—“Calm down, Hishiwa!” Mama growled. “Can’t you see he’s
from somewhere else? And a little respect for old
Dirasho. He may not be much of a talker, but he’s a
straight man with a good heart. Anyway… Be kind to your
new companion. Mor-eldal,” she added in an affable tone.
“Will you wait here for a bit? I’ll go and talk to
Dirasho. Sit down there on the stool, there, good. I’ll
be right back.”

Mama made sure to leave me sitting on the stool and
walked away. More serious now, Hishiwa sat down on the
stool next to mine. He had a small nose, blue eyes, pale
skin, and brown hair. Exactly like me, except my nose
was medium-sized, my eyes were dark gray, my skin was
tan, and my hair was black.

—“Here,” he said. He handed me something. I took it, and
he explained, “There’s nothing like a piece of bread to
get the onion through. So, you’re from the mountains?”

—“Yeah,” I said, as I ate. “A few days ago, I was
surrounded by trees, and suddenly, everything changed. I
came all the way down and followed the rivers and paths.
It’s amazing how everything changes.”

Hishiwa looked at me with a smile, but he didn’t laugh
anymore.

—“It sure must impress,” he said. “You’ve never been to
Estergat, have you?”

—“The city? No, never,” I confessed.

Hishiwa shook his head.

—“Are your people really there?”

—“I think sho,” I replied. I swallowed my mouthful.
“Actually, I’m going there mostly to see. And to look
for a ferilompard.”

Hishiwa opened his eyes wide.

—“A ferilompard?”

—“Yes.”

—“And what’s a ferilompard?”

I pouted.

—“Well… It’s a creature. But I don’t know which one,
yet. I’ll find out,” I assured him. And I smiled at him.
“And, believe me, when I want to find out a thing, I
do.”

Hishiwa looked at me thoughtfully.

—“I believe you. Hey, if you need any help, you let me
know.”

I looked at him, genuinely surprised.

—“You want to help me find the ferilompard? Seriously?”

—“Seriously,” he assured with a smile. “It intrigues me.
I’ve never heard of this creature, you understand.”

—“Oh, well, thank you! Hey, you guys around here are
really nice.”

I had to admit that I was beginning to find the sajits
really friendly. We exchanged a smile. And he said:

—“But, by the way, what’s the ferilompard for?”

I hesitated.

—“Well. First, we have to find it. Then we’ll see.”

He did not insist, for Mama was already coming back. She
looked satisfied.

—“He says it’s fine, of course!” she announced. “By the
way, he’s asking you if you’re afraid of dogs.”

*The dogs*, I thought. Ah, yes. I remembered: they were
the same as foxes, but civilized.

—“No, I don’t think so,” I replied.

—“Glad to hear it,” she smiled, “for he has two of
them.”

—“But they’re puppies!” Hishiwa informed, laughing.
“He’s going to give them to his niece. He says she’s a
celmist.”

—“A celmist!” I exclaimed. I knew that word.

—“Yeah, like, a magician,” Hishiwa explained, just in
case.

—“Well, you don’t have long to wait, you’re going to
leave soon,” Mama interjected. “Hishiwa, go home and get
your bag. You don’t want to keep Dirasho waiting!”

—“I’m going!” the boy said, and he waved his hand at me.
“Come on. I’ll show you the house.”

I followed him curiously into his cave. It was made of
wood. He took me inside and said in a ceremonious voice:

—“Make yourself at home.”

I looked at everything with great attention. There were
so many objects!

—“What’s that?” I asked, pointing to a huge box.

—“This? That’s the stove. Didn’t you ever have one?
That, over there, is a canvas,” he added when he saw me
watching it. “It’s my grandfather. A portrait we did of
him years and years ago. Now he’s very old.”

—“How old is he?” I asked.

—“One hundred and thirty years!” Hishiwa replied as he
busied himself. He picked up a bag from his pallet and
spun around. “Well. I think that’s all. Wait, I want to
show you something.”

He stood on his tiptoes to reach something on a strange
branch. He showed me a transparent and luminous object
in the shape of a bird. I looked at him in wonder.

—“What’s that?”

—“My uncle, the glassmaker, gave it to my mother. It’s a
little sparrow, but made of glass. It’s pretty, right?”

—“Very pretty!” I said. “But he has no feathers.”

—“Of course he does! Don’t you see them?”

He showed me some feather shapes on the glass, but I was
not convinced. I pouted, reached into my bag, and showed
him my yellow feather.

—“This is a real feather. Not those.”

—“I didn’t tell you they were real,” Hishiwa huffed. He
placed the glass bird back in its place and waved me
out. “By the way, nice feather.”

—“It was a yarack who gave it to me,” I informed him. “A
real one.”

—“What’s a yarack?”

I shrugged.

—“A bird with lots of colors.”

We returned to Mama, and she nodded to her son and ran a
hand through his hair tenderly.

—“Dirasho is waiting for you. Take care of yourself in
the capital, son. And behave yourself with your uncle.”

—“Yes, Mama,” Hishiwa replied, and he mumbled something
unintelligible when she placed her lips on his forehead.
“Are you sure you’re not forgetting anything? No? Good.
Well, don’t keep Dirasho waiting. Good luck, Mor-eldal.”

—“Thank you, Mama,” I replied cheerfully.

We found old Dirasho on the other side of the clearing,
already sitting on the cart.

—“Get on, boys!” he said.

We climbed up beside him on a piece of wood, and I
stretched my neck while Dirasho shook the ropes. The
animal did not move. I laughed.

—“The mule is stubborn, isn’t it?”

Hishiwa guffawed.

—“It’s not a mule, it’s a horse!”

At that moment, the horse started up and the cart with
it. I let out a snort, tensed up, and looked on in
amazement. Almost right away, though, I recovered and
smiled broadly.

—“We’re flying!”

Hishiwa let out a loud laugh.

—“We’re not! We’re rolling, not flying.”

I saw him look back and wave a hand. I turned around and
saw Mama waving her hand as well, and I suddenly felt a
surge of understanding for Hishiwa.

—“Don’t worry, one day you’ll come back,” I said.

Hishiwa gave me a thoughtful look and nodded.

—“Of course. I’ll be back in the fall for the harvest.”

I gave him a pout of incomprehension, but he did not
seem to notice. He was lost in thought. So I looked
around. We were already leaving the village on the white
snake, and when I saw that it was passing over a river I
stood up and stretched my neck in astonishment.

—“And what is this?”

—“What, boy?” old Dirasho asked. And as I showed him, he
said, “Mmph. Sit down, or you’ll fall. This is a
bridge.”

A bridge. I sat down, meditative. There were so many
words that I knew but that I had never seen in real
life! I asked more questions, and old Dirasho answered
sparingly. This was a hat, that was a pipe, what he was
carrying at the rear was a barrel of wine, and that over
there was a crate. And inside the crate were the
puppies.

—“They are very quiet,” I remarked.

Moving away from the bench, I passed into the cart, and
old Dirasho grunted:

—“Don’t do anything stupid, huh?”

I rolled my eyes. How many times had my master told me
the same thing? I replied:

—“No, no.”

The crate was open at the top, and I saw the sleeping
puppies in a tangle of cloth. I watched them for a
while, until Hishiwa asked:

—“Have you named them yet, sir?”

—“No,” Dirasho replied.

—“Not even with a temporary name?” Hishiwa insisted,
disappointed. He, too, passed by the cart and observed
the puppies.

—“This one has a white nose. Why don’t we call him
White-Nose?”

Old Dirasho did not answer. Then I said:

—“I like the name. The other one has red paws. It almost
looks like a red fox!”

—“White Nose and Red Fox,” Hishiwa concluded.

I took White Nose between my two hands and saw him open
his eyes and give a puppy bark. I smiled, kissed him on
the forehead, put him back in the crate, and lay down on
the planks of the wagon, looking up at the sky. It
seemed as big as the world I had yet to discover.

—“Hishiwa,” I said after a moment. “Why is Estergat the
capital of Arkolda?”

Hishiwa arched a mocking eyebrow and replied:

—“Because Estergat is huge.”

Mr. Tavern-keeper, I want two cheese snacks!
============================================

The first impression that Estergat made on me was
unforgettable. Even from a distance, one could see the
sea of houses rising and rising on the huge Rock. The
Imperial Road that led north from the city was so
crowded with carts and people that my ears were deafened
and buzzed like bumblebees. The puppies poked their
heads out of the crate, curious, and I did the same,
clinging to one edge of the cart. Dirasho had already
told me twice not to bend over too much, but I kept
forgetting.

We crossed the Estergat River once, and then again,
through the Gates of Moralion, as Hishiwa called them.
We were climbing a wide, noisy street when he called
out:

—“And this is the Esplanade!” We had just come out onto
a huge square. “Look! That’s the Manticore Fountain. Do
you see the creature?”

I saw it, petrified, with an impressive stream of water
gushing from its huge mouth. I opened my eyes wide in
awe, and hoped that the ferilompard would not look like
that. Old Dirasho stopped the cart not far from the
manticore.

—“I suppose you’ll know how to get to your uncle’s
glassworks from here, won’t you?” he said.

—“Yes, sir, it’s not far,” Hishiwa assured. And after
patting White-Nose, he descended with a nimble hop.
“Thank you so much for taking us!”

—“You’re welcome, lad, you’ve brightened my trip with so
many questions,” he said with a smile. “What about you,
kid?” he added as I slid down next to Hishiwa, the Red
Fox in my arms. “Do you know where to go?”

I shrugged, and Hishiwa said:

—“I’ll give him a hand, don’t worry, sir.”

—“Well. Hey, kid, give me back the Red Fox, it’s not for
you, it’s for my niece’s daughter, you understand?”

I bit my lip and nodded in disappointment. I gave him
the puppy, and old Dirasho put his hand to his hat.

—“Good luck, boys!”

He waved the reins, and the cart soon disappeared amidst
the tumult of people, wheels, and quadrupeds.

—“Well?” Hishiwa said to me. “Do you know which way your
house is?”

I looked around. And I looked up at a large tree without
branches. I pointed to it.

—“What a funny tree!”

Hishiwa smiled.

—“No wonder. It’s a street lamp, not a tree. It’s used
to light the streets. You’ll see it soon: it’ll be dark
before long. Well, listen, follow me to the glass
factory. Maybe my uncle has an idea how to find your
people.”

I nodded and, making sure that I still had my bag and
blanket, followed my companion. I was amazed at what I
saw. Suddenly, I exclaimed:

—“Oh, no, my stick!”

I had left it in the cart. I turned around and ran off.
Hishiwa shouted something behind me. I went back to the
Esplanade and followed the path which I thought old
Dirasho had taken. After a while, I realized that I had
no idea how to find him among so many people.

I looked around, and a dull fear came over me when I
didn’t see Hishiwa anywhere. Where could he be? I turned
back, but it was a long time before I found the
Esplanade. I climbed the manticore fountain, scanned the
area… Nothing, my companion was nowhere to be seen.
Well, how could I have imagined that one day I would
lose a companion simply because I had lost sight of him
in a sea of sajits?

—“What a crowd!” I exclaimed.

Suddenly I saw a light appear. I looked up at the
illuminated lamp post and smiled in wonder as I saw a
man light another.

—“Wow, it sure is beautiful,” I muttered.

Only then did I realize that it was getting dark, and I
had not yet found any shelter. I started down a wide
street when I saw a tree at the end of another. With a
sigh of relief I ran towards it, but found that it was
too thin, so I kept looking. I walked for a while along
a deserted path that ran along the Estergat River, and
finding no trees, I asked myself: what should I do? My
master said that the stars guide the lost man, so I
stopped and looked up at the stars, but because of the
light or whatever, I could not see them. There I was,
straining to look for them, when a child a little older
than I, with a crooked cap on his head and his hands on
his hips, stepped in my way.

—“Wait a minute, shyur… are your clothes made of skins?”

I arched an eyebrow when I saw him reach for my clothes.
I nodded.

—“Rabbit skins, yes.”

—“Damn! Did *you* hunt them?”

—“Yes.”

—“Oh, come on,” he laughed. I noticed that he was
missing a tooth. “What about this blanket?”

—“It’s rabbit skin, too,” I replied.

—“Is it warm?”

—“Not as much as before,” I confessed.

He snorted, as if I had said something funny.

—“Tell me. Where do you plough the deep?”

—“What?”

—“Where did you sleep last night,” he specified.

I shrugged.

—“In the forest.”

—“In the forest?” He laughed. “In a park or in a real
forest?”

—“In a real forest,” I said.

—“Um. I see. And you come all this way every day?”

I shook my head.

—“I just got here.”

—“Ah! I understand. What about your folks?” I shrugged.
He shook his head. “And you’re going back into the
forest in the dark? Do you have kabor eyes?”

—“Kawhat?”

—“Kabor, shyur, kabor. It is an animal on two legs that
lives along the paths and goes out to look for haddocks
of gold beans to fill its own. He didn’t get that,” he
laughed at my confused expression. “Now seriously: you
lend me a spot under your blanket and we share the
shelter. What do you say?”

He did not wait for my answer. He walked away, beckoning
me to follow him. And I did.

He led me to the porch of a house, a shelter so well
protected from the wind that it reminded me of a hollow
tree, and I liked it. My companion lay down, I lay down,
and he pulled the blanket over us. After a silence
during which my new companion cleared his throat and we
both tried to find a comfortable position, I asked:

—“Are haddocks made of gold beans for real?”

—“Outside no, but inside yes,” he answered.

—“Oh. But if they contain gold, you can’t eat them; so
why do the kabors hunt them?”

I heard him gasp.

—“Mothers of the Light! Are you seriously asking? Let’s
see, kinchin: the kabors are bandits, and the haddocks
of gold beans are purses of gold. And now shut up, or
the owners will hear us.”

I opened my eyes wide.

—“Are there people behind the door?”

—“Blasthell, yeah. What do you think? That we stay out
here because we like being cold? Shut your top lights
and snooze.”

I listened to him and was so tired that I fell asleep in
a few seconds. At dawn, we were awakened by the cry of
the owner of the house.

—“Out!” he roared. The big man was waving his stick
angrily, and he frightened me so much that I sprung to
my feet like a hare. Cursing, my companion ran off with
the blanket, and I followed him as the man said:

—“May the devils catch you soon, you rascals!”

I lost sight of my companion, and a few moments later, I
found myself in a street and realized that I did not
know where I was. I tried to look for my companion—he
had taken my blanket with him—but my efforts were in
vain.

—“Yet another companion lost,” I sighed.

More than a year would go by before I saw that boy
again.

The day was dawning, and the streets were already a
little busy. I walked for a long time, and I was so
hungry that at one point I stopped and said to a woman:

—“I’m hungry.”

She passed without stopping, a half-compassionate,
half-saddened pout on her face. Normally, when I said
that to my master, he’d say, well, go dig up some roots.
The problem was that there were no roots in this city.
After repeating my complaint maybe a hundred times, out
loud, through my teeth and in my head, I burst out:

—“I’m hungry!”

I was walking past a table with food when I saw a young
woman put a round object in the hand of the one behind
the stall and take a dark colored bar… I frowned and
walked over.

—“What’s that?” I asked.

The man glanced around before answering me:

—“What do you mean, what’s that? It’s barley bread, son.
You want to buy? It’s two and a half nails the bun, one
fivenail the loaf, and seventeen nails the bloomer,” he
recited.

I did not understand him.

—“Nails?” I repeated. And I pointed to the ones tacked
into the table, similar to the ones in my master’s
trunk. “Like those?”

The man whistled, looking dumbfounded.

—“Good mother. Where do you come from, boy?”

—“From the mountains,” I replied.

He shook his head, and after another look around, he
gave me a bun and showed me a silver disk.

—“This is a half-nail. Take it, keep the coin. When you
get five like this you can buy a bun as the Spirits
dictate. And now go away, and not a word of this or I’ll
twist your ear off.”

I widened my eyes and moved quickly away, taking a
generous bite of my bread and scrutinizing the coin with
my other hand. It had a hole in the middle, and
something was carved around it on both sides. Spirits, I
thought, looking up. I was curious about these stories
of spirits. I had also heard Hishiwa and old Dirasho
mention them several times.

I wandered through the streets, silently observing all
this strange world, and then I came to a fountain and
leaned back to look at my reflection. I made faces, blew
on the water, and the image became blurred. I smiled.

—“Water mirrors are not so rigid.”

I touched the water. It was warm! Not hot, but a little
warm. I put my hand in and then my arm. A few moments
later, I put my other arm in and then I saw something at
the bottom. I picked it up. It was a bone. Like the ones
I used to give my master. I sat down on the stone ledge
and nibbled on the bone, feeling the mortic energy
flowing from it. Though I didn’t need it like my master,
I absorbed the energy.

—“Who are you, boy?” a voice suddenly asked.

I looked up and saw an old man watching me. He had large
pointed ears and wore a large dark green cloak, very
similar to my master’s.

I took the bone out of my mouth and said:

—“I am Mor-eldal.”

The old elf smiled, revealing spoiled teeth.

—“Well, what a name. You’re not from around here, are
you? You have a horrible accent. How long have you been
in Estergat?”

—“One day,” I said.

—“One day! And where are you from?” he asked me in a
light tone.

—“From the valley of Evon-Sil,” I answered. “From the
mountains. I am here to discover the world.”

—“Now that’s having goals,” he grinned. “Come, let’s go
sit over there on that wider low wall. Your story
interests me. It’s not every day you meet a valley boy
with that wild look and a bone in his teeth, you know?
Heh. But tell me, did you travel alone?”

I went over and sat down on the low wall next to the old
man, saying:

—“Yes, I came alone. I walked for days. But then I found
some kind people, and an old man brought me to Estergat
on a horse-drawn cart.”

—“It was kind of him indeed. And he left you alone? That
was less kind of him,” the old man commented.

—“No, no, he was very kind,” I assured him, frowning.

The old man looked thoughtful.

—“Um… So you don’t know anyone here? And what are you
going to do in Estergat?” I pondered, looking for an
answer to that. In fact, what was I going to do? There
was no hunting or climbing trees. The old elf nodded
with a slight smile. “You don’t seem to have thought of
that yet, huh. Well, I’ll tell you what will happen to
you, listen. Today, or tomorrow, or a week from now,
some people will notice you and say, ‘Hey, isn’t that an
abandoned kid?’ And, in no time, they will take you to
their gang; the kap, seeing you so helpless, will accept
you and make you a beggar. With those looks of yours, I
predict success for you: even a heartless man would give
you a nail. And, like that, little by little, you will
learn the life of the Estergat Cat, you will make
yourself a thief and deceive people, and, in scarcely a
few moons, you will have become an incurable street
gwak.”

I looked at him, impressed. This elf seemed to know my
entire future.

—“I don’t know,” I hesitated. “My master told me that
thieves were evil.”

—“Your master? So you have a master,” the old man
muttered, his brow wrinkled. “Where is he now?”

I put on a reserved face and looked down at my bone.

—“Dead.”

It was true, in a way: he was a nakrus.

—“I see. So you’ve arrived in a city you don’t know, and
you’re as lonely as the Limping Knight. You have no
money, do you?”

I squinted, then smiled, and took out the half-nail
which the man of the barley bread had given me. The old
man rolled his eyes.

—“This will buy you a crust of stale bread at most. That
pendant you wear around your neck might buy you a loaf
of bread, though. Where did you get it?”

I looked down at my pendant. It was a strap of good
leather with a small metal plate and signs that even my
master had never been able to understand. I shrugged.

—“I’ve always had it. My master told me that maybe my
family gave it to me.”

The old man looked at him and nodded thoughtfully.

—“And you don’t remember your family?”

—“Hardly,” I admitted. “My master says that, when he
took me in, I told him I had six winters. But I don’t
remember.”

—“And when did this happen?”

I shrugged and turned dark. I didn’t like to think about
the distant past. But I answered:

—“Four years ago.”

—“I see. Well, then. Now, look here, lad. I may be a
poor, lame old elf, but I’m not evil, and I’m not going
to let a little one like you, so innocent and good, end
up in the dragon’s den. I’ll give you a hand. Listen,”
he added. “There, across the square, do you see the
Daglat Star on that door, with a rose under it? That’s
the insignia of *The Wind Rose*. It’s a tavern. Cheap
and not very good, but one of the best in the Cat
Quarter. Are you hungry?”

—“Ah, well, pretty hungry, yes,” I admitted.

The old man smiled and gave me some coins.

—“Then leave that bone and go buy some cheese snacks.
Have you ever been in a tavern? No? Well, you go to the
counter, stand on a stool so that the tavern-keeper can
see you and say loudly: Mr. Tavern-keeper, I want two
cheese snacks! You give him the coins and come back here
with the snacks. Do you understand?”

I nodded emphatically, put the bone in my bag, got up,
and with the coins in my hand, ran to the door, and was
about to push it when it suddenly opened and I heard a
din of voices and glasses. I waited for the man who was
going out to pass, then I went in. What I saw left me
speechless. There were tables, people, and even dogs. I
recognized the counter easily, climbed up on a stool,
and shouted:

—“Mr. Tavern-keeper, I want two cheese snacks!”

The racket in the tavern immediately subsided, and the
strong man in front of me laughed, watching me with
amusement.

—“Tone it down, you little savage, you’re startling my
customers. You’ve got quite a voice, you know. Sit down
on the stool before you fall, and I’ll bring you some
snacks in no time. Give me those coins.”

I left them in his big hand, sat down, with my back to
the counter, and looked around curiously. Conversations
had resumed, and the noise was dulling my ears. Before I
had time to get bored, the tavern-keeper appeared on the
other side of the counter and announced:

—“Two cheese snacks, boy!”

—“Thank you,” I said. I took them, looked at them
curiously, took a bite and huffed, chewing. “It’s good!
Thank you, Mr. Tavern-keeper!”

And under his amused gaze, I ran out. I crossed the
square and found the old elf where I had left him. He
smiled at me.

—“I see you’ve already started without me.”

I gave him his snack and sat down, eating my fill.

—“It’s good, huh? Well, I should say it was,” the old
man laughed. In fact, I had just stuffed the last piece
into my mouth. He continued, “You know what? If you
listen to me carefully, I might be able to find someone
who can buy you a good meal every day.”

I stared at him and swallowed the last bite.

—“Really?” I said. “Where is that person?”

The old elf watched me carefully before replying:

—“Do you know what a brotherhood is?”

I nodded.

—“A lot of brothers. I had a few once. I think.”

The old man smiled.

—“Yes. Brotherhoods are families where people learn and
cooperate. Well, I happen to belong to one. And I
thought it wouldn’t hurt to expand the family. Have you
heard of the Black Daggers? No, of course not, as
expected. Well. We Black Daggers are a special
brotherhood. We don’t have petty thieves or cheap
hustlers among us, let alone bad people. We Black
Daggers have a code. And we even know how to do a little
magic.”

I almost said to him, ah, I can do a little magic too,
and more than a little. But I stopped just in time,
because I shouldn’t talk about it. I bit my tongue and
kept my eyes on the old man. He continued:

—“I thought about it and decided that you could be a
good Black Dagger. You would have food, a home to sleep
in, and even a master who would teach you many things.”
He watched me from the corner of his eye. “What do you
think? Do you like the idea?”

I smiled.

—“Oh yes, I like it very much. What should I do?”

—“Come back here tonight at dusk and sit on this very
fountain. One of our members will come and get you. And,
of course, don’t say anything about this to anyone, eh?
We Black Daggers are a secret brotherhood,” he said with
a wink.

And as I nodded, he smiled and reached out a wrinkled
hand to ruffle my hair as my master did. I immediately
felt a surge of trust for this elf.

—“See you soon, kid. Don’t forget: come here at dusk.
The kid’s name is Yalet.”

He got up, and I saw him limp away. It was only when he
disappeared that I thought I had forgotten to ask him
what his name was.

The Black Daggers
=================

I spent the day marking the territory without straying
too far from the square of *The Wind Rose*, for, with so
many streets, I did not yet trust my sense of direction.
With my sharpened stone in hand, I drew signs at every
corner, to create some recognizable landmarks, until a
tall dark elf called out to me:

—“What are you doing, you little devil?”

His tone reminded me so much of that of the owner of the
house who had chased me and my companion with the
missing tooth, that I had the prudence to run away and
continue to mark the streets in a more discreet manner.

As evening fell, I returned to the square, sat on the
edge of the fountain, and watched the sajits. I heard
bits of conversation, but I understood little. At one
point, not far away, an elf sat with a large pile of
papers. From the way his eyes darted back and forth, I
knew it was some sort of book. Curious, I went over to
see what was written and… I only had time to realize
that I did not understand the signs at all before the
sajit growled:

—“Give me some space, brat.”

I did: I went all the way round the square and waited
for the elf to go away before returning to the fountain.

When night fell, the square was still crowded. Obviously
the sajits were not like the squirrels, which
disappeared with the sun. That night the full moon was
shining, and that soothed me. My Master said that, as
long as the Moon, the Gem, or the Candle were shining in
the sky, one could always find one’s way.

*‘The three Moons are the sun of the night,’* he said,
*‘and the stars, their rays.’*

On warm nights, we often went out to count the stars.
Well, I don’t know if he counted them, but he would ask
me to count them, multiply them, and share my
calculations with him. *‘Learn, Mor-eldal. Even
squirrels can count their acorns!’* he would tell me.

I was playing with my yellow feather, distracted, when a
human sat down next to me on the edge of the fountain
and commented:

—“Nice feather.”

I cocked my head to one side and watched him carefully.
He was young, dressed in a dark cloak, and something in
his eyes told me that he had not sat there by chance.

—“Yalet?” I said, questioningly.

I saw him smile and nod.

—“Himself. And I suppose you are Mor-eldal.” I confirmed
silently, looking at him brazenly, and he observed, “A
strange name. Did your parents give it to you?”

I shook my head.

—“I have no parents.”

Yalet nodded quietly.

—“You are not the only one, rest assured. Tell me, do
you know why Rolg sent you to see me?”

I assumed Rolg was the old elf. I shrugged.

—“He said you were going to teach me and buy me good
food.”

Yalet smiled.

—“Absolutely. If you behave well, you’ll be my *sari*,
and I’ll be, like, your mentor and teach you a lot of
things. Do you know what it means to behave well?”

I nodded. My master had told me that. I explained to him
with application:

—“That means I must not disturb, I must not speak when I
am told to be quiet, and I must not do anything foolish
like eat mushrooms I don’t know or go near snakes.”

He smiled broadly.

—“I couldn’t have said it better myself. Try not to
forget this, and we’ll both get along fine, okay? Come
on, follow me. I’ll show you something.”

He rose nimbly from the edge of the fountain, and I did
not hesitate: I put my yellow feather in the bag and
followed him curiously. Yalet led me through the
streets, and I had to trot to keep up. Finally, he
stopped at a blind alley and turned to me with a
laughing face.

—“Can you climb?”

I smiled.

—“Of course! I am a great climber. Whenever my master
saw me on a tree, he said I was a reckless fool. But I
can count on one hand the times I’ve fallen.”

I saw him raise an amused eyebrow.

—“Well, here, you’d better not fall, because where I
want to take you is much higher than a tree. Now watch
and learn.”

He climbed onto a barrel, gained momentum, and clung to
a beam of the lowest roof. In a few seconds, he was
perched on it and looking down at me through the
shadows, seemingly waiting for me.

My eyes widened in amazement, and even more so when I
saw him wave his hand. Uh… Really? I had to do the same
thing? *Okay*, I thought. *Go ahead. You can do it,
great climber.* I took a deep breath to give myself
courage, climbed the barrel and tried to reach the beam,
but it was too far. Well, this way was not possible. I
climbed down and found a slightly large metal log that
went up and up to the top. I grabbed it and started to
climb. It was almost like climbing a tree, but less
convenient, because there were no branches. I reached
the roof, grabbed the eaves, and… slipped, and if
Yalet’s hand hadn’t caught me at that moment, I would
have taken a good fall. The Black Dagger helped me up
onto the roof, and despite my dubious performance, he
smiled and said:

—“Not bad for a first try. Remember: the important thing
is to always hold on to a good grip. If you can’t climb
on one side, be patient: you’ll find another way. What
you mustn’t do, of course, is jump off, right? Come on,
follow me and be careful.”

I followed him on all fours over the tiles to a higher
roof. Yalet did not separate himself from me, as if he
feared that at any moment I might slip and fall. We
climbed balconies, crossed terraces cluttered with junk,
and climbed over facades. This was definitely not the
same as climbing trees. It was fun, yes, but it was…
well, different, and more unsettling: instead of birds
and squirrels, we came across rats and bats.

Finally, after climbing and climbing, we came to a very
high terrace, and Yalet patted me on the shoulder.

—“We’ve arrived. Welcome to the classroom of Master
Yalet and his disciple! It’s a bit messy, but that’s
okay: we don’t allow visitors anyway. And now turn and
gaze upon the city, Mor-eldal.”

I turned and let out a muffled gasp. I gazed at the
night city, transfixed. It looked like an ocean of
fireflies. Or an ocean of stars, I thought in wonder.

—“Nice, isn’t it?” Yalet raised his hand. “Look. That,
over there in the distance, is the Esplanade. See that
lighted dome? That’s the Great Temple. And right next to
it is the Capitol, with the parliament. The quarter just
after that is Riskel, the merchants’ den, and all that
is Tarmil, the craftsmen’s quarter. This is the river.
And beyond that, there are the factories of the Canals.
Ah, where you can’t see the light, that’s the Wild
Garden, do you see it? There they keep the strangest
animals of Prospaterra. And, well, behind us are the two
rich districts. Atuerzo and the Harp.” He indicated the
upper part of the city with a vague gesture. “I work in
a tavern in the Harp. And believe me, I make more from
the customers’ tips than I do from what that cheapskate
tavern-keeper gives me. What you see over there, grand
as a palace, is the Conservatory, where magicians study.
And behind that wall is the Citadel. The city of the
Untouchables. The nobles. And that,” he added, turning
his back to the Rock again and looking at the houses
directly in front of us with a solemn expression. “That,
Mor-eldal, is the Cat Quarter. This is where I grew up,
and this is where you will live.”

I arched my eyebrows.

—“On this terrace?”

I pronounced the word carefully, for I had just learned
it. Yalet laughed under his breath.

—“No. Not on this terrace. That is a secret refuge that
only you and I know, Mor-eldal. You will live with old
Rolg. His house is that way,” he said, waving his hand
vaguely. “I’d gladly put you up with me, but I live in a
boarding house in Atuerzo and can’t get you in. Don’t
worry, right now there are two other *saries* living
with Rolg too.”

—“Are they children like me?” I asked.

Yalet glanced at me, perhaps amused by my keen interest.

—“Yerris is thirteen and Slaryn… I think she’s thirteen
too. Sla is the daughter of—well, of a Black Dagger who
got caught in the guards’ nets… Mmph. Look, as soon as
we get back down from this terrace, I’ll take you to
Rolg’s place and you can get to know them both. Okay?”

I nodded and rested my elbows on the wall, gazing at the
city, fascinated. I was beginning to think that my
master had been right to drive me from his cave. I had
so much to learn! So many new places to explore! I held
up a hand and pointed to the tall, tightly packed
buildings with terraces standing out in the Cat Quarter.

—“And what’s that over there? The Cat Quarter too?”

Like me, Yalet was leaning against the wall. He looked
up to see what I was pointing at, and the moon lit up
his face.

—“That… is the Labyrinth. It’s part of the Cats, it’s
the center of the neighborhood, but…” He hesitated.
“You’d be better off not going in there. It’s a
dangerous place. Like those mushrooms and snakes, you
know?”

I rolled my eyes.

—“Sure. And that over there?”

I pointed to an area on the left, full of small, low
houses.

—“That is the Black Quarter. And further down the river
is Menshaldra, the town of the boatmen. See those lights
over there? Those are the lights of the Valiant Bridge.
And there, straight ahead, even if you can’t see it, is
the untouchable forest of Estergat. Everyone calls it
the Crypt for some reason. No one is allowed to cut down
a single tree in the forest. It’s the property of the
Fal, a noble family.”

He was pointing his finger at a place engulfed in
darkness beyond the river, and I tried to see, but saw
nothing. A forest, I thought. It was comforting to know
that there was one so near.

—“What does crypt mean?” I asked.

—“Huh. It’s a place where… well, where they bury the
dead. Don’t worry,” he added in a light tone. “No ghosts
or anything like that come out of there. At worst, there
might be some bear or wolf. Last year, a red nadre came
out of there, one of those little dragons that run
really fast and burst when they die, have you ever seen
one? No? Well, I must still have a print I cut out of
the newspaper. I’ll show it to you.”

—“What’s a print?” I asked.

Yalet was left speechless.

—“A print is a drawing.”

—“Ah. And what’s a newspaper?”

—“Spirits, well… A newspaper is a… a bunch of papers
with information written on them,” he replied, clearing
his throat.

—“Okay. Thanks,” I said. “And what are spirits?”

—“Thunders…” Yalet muttered, massaging his forehead.
“Spirits are… Look, Mor-eldal, that’s a better question
to ask a priest, okay?”

I bit my lip and nodded. I decided not to ask him what a
priest was.

—“So, the forest is dangerous?”

Yalet shook his head and smiled at me.

—“Not as long as you stay here in the Cats.”

I pouted and looked away at the countless buildings of
the Cats. The wind was cold and I shivered. I broke the
silence.

—“You said before that a Black Dagger got caught in the
guards’ nets. Who are these guards?” I asked.

I saw him roll his eyes and stifle a laugh.

—“No kidding. You know what a policeman is? No? Dear
Mother. In the Cats, they call them flies. Does it ring
a bell with you? No? Well, sari,” he breathed in,
looking at me intently. “The guards are the ones who
watch and arrest those who do things that shouldn’t be
done, and they put them in jail, a place with bars that
you can’t get out of. Do you understand?”

I nodded.

—“Yes.”

*More or less,* I added inwardly, but I kept that to
myself, because I felt that my supreme ignorance was
making him a little desperate.

—“Rolg told me you were from the valley,” Yalet resumed
after a silence. “He said… that you were living with an
old man.”

—“That’s right. But he drove me away,” I admitted,
suddenly glum. I rested my chin on my hands, my eyes on
the city, and explained, “He wanted me to go live with
you people, to learn things. I miss him,” I confided in
a quiet voice. “But he doesn’t want me to come back
until… until I find a ferilompard.”

Yalet arched an eyebrow.

—“A ferilompard? Hold on,” he said suddenly. “Rolg told
me that your master was dead.”

I opened my mouth, closed it again, and pouted.

—“Well… the thing is, he’s not really dead.”

Yalet raised his eyebrows, and as I said nothing more
and looked away to the distant street lights, he ruffled
my hair and said cheerfully:

—“Come on. Don’t be sad. If you and your master really
lived alone in the mountains… well, you know? I think he
did you a great favor by chasing you away. If he hadn’t,
you would never have seen Estergat. And you’d never have
seen this,” he added, gesturing broadly towards the
night landscape. “Let’s go, I’ll show you your new home.
But first: be careful on the way down.”

I saw him go over the wall to the roof below and smiled.
I thought this sajit was quite nice. And besides, it
seemed that it was mutual. And even better: he was my
new master. I bit my fingers, still smiling.

—“Hey, Mor-eldal! What are you doing?”

Oops. I hurried over the wall and made my way back down
the roofs. When we finally reached the blind alley,
Yalet observed:

—“You know? You should change your name. Mor-eldal is
too… strange. Everyone would notice. What do you think
if we change it into…?” He pondered for a few seconds
under my quizzical gaze, then blurted out: “Draen. There
are lots of them in Estergat. And it’s a good name. Do
you like it?”

I smiled. My master didn’t usually ask my opinion. When
he’d called me Survivor, at least he hadn’t asked me if
I liked it. I nodded, feeling that Yalet had just given
me a gift.

—“Yes. I like it. Yalet is a good name too,” I added.

He glanced at me mockingly.

—“Call me Yal and I’ll call you Draen, okay?” I nodded,
and he smiled at me. “Come on, let’s go.”

I followed him through the alleys to a narrow courtyard
full of junk. The house on the right was in much worse
shape than the one on the left. Yal went to the latter,
climbed some wooden stairs and pushed open the door.

There was light inside. When I entered behind Yal, I saw
a room more or less like the one in Hishiwa’s house.
There were four straw mattresses, a table, some stools,
and some other piece of furniture which I could not
name.

—“Well, it looks like Yerris and Slaryn aren’t here,”
Yal said, frowning.

—“Lately, they hardly stop by,” a voice muttered. I
turned and saw the old elf come out of the next room. He
pushed aside the muffler that protected him from the
cold, and the light from the lantern shone on his
wrinkled, serene face. He smiled at me. “Welcome to the
Den, lad. I’d tell you about all the chores and rules of
this house, but I do believe there’s something more
pressing to do.” He set a pile of clothes on the table
and pointed to a huge bucket full of water. “Wash
yourself in the tub, you have a soap and sponge right
there next to it. I want you to scrub until we see your
skin, okay? And then put this on,” he said, patting my
clothes. I nodded, without a word, because I wasn’t sure
I understood everything. I saw him roll his eyes. “Yal,
boy, I need to talk to you for a moment.”

Yalet smiled at me to encourage me to do as Rolg asked,
and I walked over to the tub. I stopped in front of it,
glanced to the next room for help, just when the door
closed. Right. Well, I’ll have to manage on my own. I
took off my rabbit skins, picked up the soap and sponge
and looked at them for a moment before dipping them into
the water. My eyes widened as I saw the white foam. I
licked the soap and…

—“Ugh…” I let out.

I spat, and cleaned my mouth with the water, and then I
thought it would be easier to get into the tub, so I
went in and began to scrub as the old elf had said. When
Yal came out of the room, I was scrubbing a foot. He
looked at me and smiled.

—“Well, sari, I think you’re on the right track. Now
you’re starting to look like a human. But you know, we
also use the sponge. Look, I’m going to scrub your back,
it’s not easy on its own. Pass me the soap and get up.”

He took the sponge and soap and began to scrub.

—“Hey. You’ve got a nice scar here on your arm,” Yal
observed.

—“A lynx did it to me last summer,” I explained.

—“A lynx?” Yal gasped. “And you made it out alive?”

—“Well…” I fidgeted, embarrassed, because I had just
remembered that I couldn’t talk about that incident.
After all, if I had escaped the lynx, it was because of
a necromantic spell: I had thrown a mortic discharge at
the feline, and it had become frightened. After a
silence, I said, “I was lucky.”

—“You sure were,” Yal snorted as he continued to scrub
vigorously. “Anyway, at least in Estergat you won’t have
to fight lynxes. Well, Rolg wants me to explain a little
about how his house works. There aren’t many rules, but
you have to respect them. You listening?”

—“A lot,” I assured him.

—“Fine. First rule: don’t let anyone in who isn’t a
member of the brotherhood. Second rule: Do whatever Rolg
tells you. Third rule: Don’t go into his room. And that
is all. See how easy it is?” He took a few steps away,
lifted a small bucket full of water and, without
warning, threw it all over my head. I squealed, and he
laughed, “You enjoy baths as much as I did at your age.
But it was a must, believe me. I told you about the
guards earlier, remember? Well, they would have taken
you off the streets eventually, sent you to one of their
shelters, and you wouldn’t want to be there, even dead,
believe me. Now dry yourself off with this and get
dressed. Tomorrow night, I’ll come here to get you, so
make sure you’re here. Try to get along with Yerris and
Slaryn, hmm? They’ll probably teach you a lot, so listen
to them. Good night, sari,” he said, patting me on the
shoulder.

I don’t know what excited me more: the prospect of
getting to know these two saries or the prospect of
seeing Yal again the next day. I replied, smiling:

—“Good night, Elassar.”

Already near the door, Yal looked at me, surprised.

—“Elassar? What does that mean?”

Oops. I shrugged and explained:

—“Master. Can I call you that? It’s just that… my
master, I always called him Elassar.”

Yal arched an eyebrow and smiled.

—“Hmm. Suit yourself. See you tomorrow, sari.”

—“See you tomorrow, Elassar!”

His smile widened. He closed the door behind him, and I
hurried to get dressed because it wasn’t exactly warm in
the room, let alone with wet hair. I was putting on the
shirt when Rolg limped out of his room.

—“How do you like your new home, son?”

—“Very pretty,” I assured him. “It’s bigger than the one
I had before. But it’s cold here, too.”

—“That’s why I brought you a blanket. Here.” Rolg handed
me the blanket and indicated a straw mattress with a
gesture. “This one is for you. You don’t mind if I throw
these skins away, do you? They smell worse than a sewer.
Even the shabbiest beggar would throw them away. Come
on, lie down and sleep, you’re probably tired, and if
you’re not, whatever you do, don’t make a sound: I warn
you that, if I’m woken up before sunrise, I always get
up on the wrong side of the bed, and you don’t want to
know what that means, do you?” He smiled mockingly at
me. “Good night, kid.”

—“Good night, Rolg,” I replied cheerfully. “And thank
you very much for… for the soap. It tastes horrible, but
it’s very nice. And thank you for the clothes and the
blanket. And for the straw mattress, it’s better than
the one I had in the mountains.”

The old elf’s eyes sparkled with amusement.

—“Well, I’m glad.”

I wrapped myself in the blanket and saw Rolg take the
lantern to his room. I closed my eyes, and then opened
them again, and looked about the room in the faint
moonlight coming through the window. I could hear the
distant sounds, the strange rumors. I heard the creaking
of wood under the lame footsteps of the old elf in the
next room, the sound of voices in the courtyard, and…
Suddenly the door opened with an almost inaudible
whisper and two figures entered. It was two children.

—“That’s the last time I’m going to the theatre with
you,” the girl whispered to the boy.

—“You’re overreacting,” the other replied in a whisper.

They said no more. They lay down on their pallets, and I
hesitated to say anything to them. Finally, I decided
not to, because Rolg might be asleep already and I
didn’t want to wake him. So I listened to the breathing
of Yerris and Slaryn, calmed myself by convincing myself
that I was sleeping in a friendly house, and remembering
those promised snacks, I fell asleep peacefully.

Should I tell him or not?
=========================

When I awoke and opened my eyes, the first thing I saw
was the strange flattened face of a child. He had very
light blue, slanted eyes and skin as black as night.

—“You’re new,” he said.

It was not a question, it was an observation. At that
instant, Slaryn woke up, saw me, and let out a gasp
before sitting up abruptly.

—“Good mother! Who are you?”

Her long, disheveled red hair surrounded a thin, dark
blue face with very green eyes enlarged by surprise. She
was obviously a dark elf. Quickly, I sat down and
hesitated for a moment before answering:

—“Good morning. I am Draen.”

Slaryn looked at me thoughtfully without answering, but
Yerris smiled as he uncovered white teeth.

—“Good morning. I’m Yerris. And this is Slaryn the
Solitary. Where did you come from?”

—“Well, you see… Yal said he was going to take care of
me,” I explained. “And that he was going to give me
snacks and teach me lots of things. He also said you
were going to teach me because… I’m from the valley, and
there’s a lot of things I don’t know. For instance, I’d
never seen a loaf of bread before I came here.”

Slaryn laughed in disbelief.

—“For real?”

—“Well, yes. In the mountains, there’s no such thing,” I
explained.

—“And what did you use to eat then?” the girl asked.

—“Well… roots, rabbits, crayfish, and berries. Things
like that.”

—“Wow, wow,” Yerris let out. He leapt to his feet. “So,
you’re a real mountain kid. Allow me to introduce myself
properly. I am Yerris the Black Cat. Some say that
mixing gnome blood with dark elf blood gives bad
results, but,” he pointed to himself with a proud
gesture, “here’s proof to the contrary. You could not
have found a better guide to explore the civilized
world. Onward,” he added as he moved away toward the
door. “Leave that bag behind, we won’t steal it. And put
on your cap, you don’t want your ears to freeze.”

I got up, took the cap which Rolg had left me, and put
it on like the half-gnome. The three of us went out of
the Den. It was a spring day, and the sun was already
shining on the Cat Quarter in a greyish but warm light.

—“Come on, come on,” Yerris urged me. “You know?” he
said to me as we left the small courtyard. “You got a
good mentor. Yal’s only sixteen, he’s almost like a
comrade, and besides, they say he’s one of the best
Black Daggers in the brotherhood. And I assure you it’s
true! Last fall, to become officially a member, he
entered a noble house in the Citadel and took a hundred
goldies. He gave fifty to the brotherhood, but do you
know what he did with the other fifty? He bought gifts
for all the saris. Well, he bought a hat for himself.
One of those top hats. Hey, he bought me a harmonica.
I’m the best musician in Estergat, I must say. I’m
hardly exaggerating, I’ll give you a demonstration
later… Oh, here’s Rarko. Ayo, Rarko!” he cried. I saw
him raise a hand to a boy sitting on the threshold of a
house. “How’s it going, mate?”

—“Wind in the sails! And you?” the boy called Rarko
replied.

Yerris responded with a thumbs up and continued,
lowering his voice:

—“Well, as I was telling you, shyur, everybody likes
Yal. You’re a lucky bastard. Because, believe me, my
mentor is far from giving me gifts. That guy doesn’t
know what it means to be a social person. Fortunately,
right now, he’s on a trip, somewhere, hunting for
treasures or negotiating with some colleague from
another city—Whatever, I don’t know. In any case, it
means I’ll have some free days to teach you how to be a
good Estergatese!”

With some amazement, I looked alternately at the street
and at Yerris’ hyperactive lips. The semi-gnome kept
talking and telling stories about the brotherhood, the
city, and whatever else came into his head. He had a
peculiar gait, zigzagging, spinning around, and
occasionally greeting an acquaintance with grand
gestures. Slaryn, on the other hand, was silent and kept
watching me; I saw her smile two or three times with a
mocking face, but I do not know whether she was mocking
me or Yerris.

In some street, we came across a group of girls, and
Slaryn left us, saying to Yerris:

—“Hey, watch what you teach the newbie, I know you:
you’re capable of sending him to the slammer on the
first day.”

—“Bad tongues! You look more and more like your slowed
mother every day!” Yerris replied.

The dark elf glared at him and hissed:

—“Watch your mouth!”

Yerris sighed and brought a fist to his chest.

—“My apologies, princess, my tongue betrayed me.” Slaryn
rolled her eyes, and the semi-gnome pulled me away from
the girls, whispering, “Sla’s mother is in the slammer,
and she doesn’t want anyone to know. So… watch your
mouth!” he bantered to me.

And he continued to speak. First, we walked through the
alleys of the Cats, then Yerris stopped before a narrow
staircase and commented:

—“No, it’s too early for that, it might scare him.”

And he turned back. It was only after we had gone a good
distance that I realized that Yerris had been about to
enter that dangerous Labyrinth Yal had told me about. My
curiosity was aroused, but I dared not interrupt the
semi-gnome’s continuous flow of words. In a way, it was
wonderful to listen to him. It was a little dizzying,
especially as I could only understand a tenth of what he
was saying, but his loquacity fascinated me.

The semi-gnome led me down Tarmil Avenue to the
Esplanade, where he bought me a bun from his own pocket,
and we ate sitting on the huge white stoop that
surrounded the Capitol. I was enjoying the moment of
silence, contemplating the people and sorting out my
thoughts, when Yerris asked me with his mouth full:

—“And what did you use to do in the valley?”

I swallowed. I didn’t think much about the answer, since
Yerris had already asked me a few questions before
without giving me time to answer.

—“I don’t know… things. I went hunting. And played with
squirrels.”

Yerris looked at me, eyes wide open.

—“For real? Damn. And you used to hunt squirrels too?”

—“I didn’t!” I said, offended.

—“Ah. Well, if you like squirrels, you must like trees
too, then?”

—“I do, I like them very much,” I assured him.

—“No, no, no, you mean, ‘ragingly’,” he corrected me,
kindly. “It sounds more Cat-like and clearer.”

—“All right, ragingly,” I said.

—“Uh… That runs,” Yerris pointed out. “We don’t say
okay, we say ‘that runs’ or ‘runs for me’.” Seeing me
nod and take in his lessons, he patted my shoulder with
a smile. “Thunders. I’ll cut off my hand if you don’t
become a Cat in less than a moon. Come on, let’s go.
I’ll show you the Evening Park. You’ll like it, for
sure.”

I liked it ragingly. There were trees, and I saw a
fountain of golden water and a red bird I had never seen
before.

—“Over there, at the back, is the Wild Garden,” Yerris
told me. “But my pockets don’t weigh enough to pay the
entrance. Alvon, my master, is a first-class cheapskate.
And since I’m not allowed to dive into other people’s
pockets, well, as you see, I’m as skinny as you, Rarko,
Syrdio, or any other gwak on the street. Skinny but
honest,” he smiled.

We spent the afternoon in that park, and Yerris gave me
a demonstration of his skill with the harmonica. I
couldn’t be sure how well he played it, but it sounded
nice, and the instrument looked even nicer. He let me
blow once, just once, but it made me euphoric. Who would
have thought that one day I would be able to make a
sound like that, so strange, and with my own breath!
Amused by my enthusiasm, Yerris took me by the shoulders
with obvious affection and said it was time to move on,
so we returned to the Cat Quarter. Without him, I think
I would have been unable to find Rolg’s house. There
were so many streets, so many corners! When we came to
the small courtyard, Yerris stopped his chatter,
straightened up, spun round, and said,

—“I’m leaving you at home, shyur, I have business to
attend to. Tell me. Did you enjoy the walk?”

I nodded vigorously and said:

—“Ragingly!”

Yerris looked at me approvingly.

—“Then, tomorrow, I’ll give you another ride. Runs for
you?”

I smiled.

—“Runs for me. Thank you, Yerris.”

The dark face of the semi-gnome lit up with a white
smile.

—“You’re welcome. Old cats teach kittens: it’s the law
of nature. Ayo.”

—“Ayo,” I replied, and I saw him trotting off down the
street again, this time in a straight line. When he was
gone, I turned to the Den, climbed the wooden stairs
quickly, and entered.

There was no one in the room. I took a few steps towards
the other door, listened, and heard nothing.

—“Rolg?” I called out.

No one answered me. I reached for the handle, turned…
And the door resisted. Closed, I understood. The doors
opened and closed. My master had told me that. And I
thought it was fortunate that it was closed, for I
remembered at that moment one of the rules of the Den:
one must not enter Rolg’s room.

I sat down on my pallet and checked that my yellow
feather was still in the bag. There was not much left in
it. My supplies were long gone, and all that was left
was the bone, the sharp stone which I used for
everything, and the remains of a dried flower I had
picked on my journey. I threw these out and noticed that
the sun would soon disappear. Suddenly, I saw a small,
furry, four-legged animal at the end of the alley, and I
opened my eyes wide when I recognized what it was. A
cat! It had red and white fur, but it was much dirtier
than the one in the storybook drawing. It looked like a
small lynx. Fascinated, I descended the stairs, sank my
feet into the mud, and moved forward cautiously; all of
a sudden, the cat showed its teeth and gave a muffled
growl.

I stopped and huffed.

—“You have the same temper as a lynx. Conceited! Don’t
growl at me like that, you bad lynx!”

I recoiled when I saw the cat take a step forward, but
the feline merely took off like an arrow towards the
ruined house across the small courtyard, jumped up, and
disappeared through a broken window.

—“Dart,” I muttered. Curiosity impelled me to approach
the house where the cat had disappeared, but I dared not
poke my head through the window. It was very dark in
there. Who knows, maybe the cat had friends or maybe a
lynx was looking after him. So, cautiously, I turned
back and went up the stairs of the Den, and after
listening to the rumor of the city, I pulled myself up
the banister, sat astride it, and began to sing:

Larilan, larilon,\
Hey, Spring,\
Come out now,\
Bombumbim,\
How nice, it is spring!\
Larilan, larilon,\
A little haughty, it is true,\
Bombumbim, larilon,\
But you’re the most beautiful ever,\
’cause always, always, O Spring,\
You are the first one to sing.


I heard an amused snort and turned. Yalet was at the
foot of the stairs, arms crossed, and he was looking at
me with a mocking smile.

—“Hello, Draen.”

—“Hello, Elassar,” I greeted him.

Yalet approached and went up the stairs saying:

—“You shouldn’t leave the door open: the house cools
down much faster, and it’s still spring, as you say.
Anybody else at home?”

I shook my head, and noticing the curious stick he
carried strapped to his back, I asked him curiously:

—“What’s that?”

—“This?” He smiled as he closed the door. “It’s a broom.
And you’re definitely going to try it today. Come on,
get down from there and follow me. How was your day? Did
you get to talk with Yerris and Slaryn?”

I huffed and puffed my way down the ramp.

—“I didn’t talk much, but I listened a lot. I like
Yerris. Though he talks even more than I do, it’s
impressive. To think my master said I was a
blabbermouth… well, I wonder what he would say about
him.”

Yalet laughed.

—“Well, I’m glad you’re getting along with them. To tell
you the truth, I personally don’t know much about them;
lately, I don’t come by here much, but I know Yerris is
quite a character.”

We left the courtyard, and I asked:

—“Are we going to the peak?”

—“Where?” Yal asked, puzzled.

—“To the terrace,” I specified. “I called it the Peak,
what do you think? Because from up there you can see
everything, like when you’re on the peak of the valley.
From there, you could see very high mountains in the
distance. It took me days to get there, but every time I
did, it was impressive, almost as impressive as the Peak
from here.”

Yal nodded, amused.

—“Okay then, let’s call it the Peak. And yes, that’s
where we’re going. Today, you’re going to learn how to
clean. It’s not the most exciting lesson, but… it’s
necessary for us to be comfortable at the Peak, as you
call it.”

He smiled, and when we reached the blind alley of the
previous day, we began the climb. This time, I did
better and managed to climb to the first roof without
help. When we reached the top, Yalet began to explain:

—“These things over there must all be piled up into a
corner. Leave this here; I’ll arrange the awning, to
protect us when the wind blows.”

We finished putting everything in order when there was
hardly any light left in the sky, and while Yalet was
fixing broken planks to one side of the awning, I
wandered through the junk, poked around a bit, and
finally climbed the wall to see Estergat in all its
glory. I turned to the rich quarters. Then back to the
Cats. How much more I had to explore! A city was much
more tangled than a forest.

—“Hey!”

I was startled to hear Yalet’s voice.

—“Get down from there, damn it, you’re going to fall.”

I leapt onto the terrace and ran towards him.

—“Did you finish the house?”

—“More or less. Where did you put the broom?” I pointed
to it. He went to pick it up and gently hit a box with
it. “That will be your seat, and this one will be mine.
Sit down. Tell me, do you already have some idea of what
I’m going to teach you?”

—“Yes,” I said firmly. “You’ll teach me how to rob those
nasty nail-pinching evil swells.”

Yal was about to sit down and he froze for a moment
before he finished sitting down with a gasp.

—“What?”

I bit my lip innocently.

—“That’s what Yerris told me.”

Yalet cleared his throat.

—“Um. Well… it’s not exactly that. I mean… it’s not just
that. Listen, we Black Daggers make our living—how do I
put it—not always in a very legal way, but morally, in a
way, and well, whatever, I’m not going to give you
ethics lessons. I just want you to know that we’re not
criminals, we don’t assault anyone, we don’t kill, we
don’t steal from the poor, and we don’t take any
business that’s offered to us. That said, the first
thing I’m going to teach you is to understand what kind
of city you live in, I mean, what the world you live in
is like, so that your innocence doesn’t get you into
trouble with the guard or the Cats’ gangs, hmm? Then
I’ll teach you the basics of what a Black Dagger should
know. And… despite what some people say about scholars,
I’d like to teach you to read as well.”

Read! I smiled and said:

—“I already know how to read.”

Yal was stunned.

—“What? *You* can read?”

—“Yes, my master taught me. Well, a little. But… to tell
the truth, I’m not sure,” I admitted with a sudden
hesitation.

—“Let’s be clear, do you know or not?” Yal asked
impatiently.

—“Well… I don’t know, Elassar. My master taught me to
read three books, two were in Drionsan, and I thought
with that, it was enough to be able to read, but today I
saw the signs in the stores and I didn’t understand
anything. So… I don’t know if I know.”

Yal cleared his throat.

—“So you had three books? In the mountains? Well… Never
mind, I’ll teach you if you’re willing to learn.”

—“Of course, Elassar. I want to learn,” I said. “There
are so many things I don’t know. I’ll be more attentive
than an owl.”

Suddenly, I saw a light appear from nowhere, and I
exclaimed. *Light*, I thought. Magic light. Yal leaned
over and stared at me, holding a small orb of light in
his hand.

—“More attentive than an owl, eh?” he smiled. “Well, I
hope so, sari, because you don’t know how much more you
have to learn.”

And he was right, I had no doubt about that, and even
less so at that moment.

—“What magic is that?” I asked curiously.

My master had never told me of a magic that illuminated.
Yal looked down at the light, and I feared he would
destroy it, but he kept it on as he replied:

—“Some call it deceptive magic, because they are
illusions, altered waves. But we Black Daggers call them
harmonies.” And he raised his other hand. “We can make
light. And we can make shadows.”

His other hand was wrapped in darkness, and mouth agape,
I stared at the orb of light and the orb of shadows.

—“Wow!” I exclaimed. “That’s amazing!”

Yal smiled.

—“It’s magic. Some scholars say that they are not
celmist arts, that they are mere illusionist tricks. But
they say that because, for them, harmonies are inferior
arts compared to the arts of diserrance, invocation and
all that. They are wrong, of course, but because of
them, very few people know how to use harmonies. They
don’t teach them at the Conservatory of Magicians. They
think of them as… deceptive magic, as I say. But to us,
Mor-eldal, it is a vital tool.”

He undid the harmonies, and the darkness of the night
returned. He asked in a cheerful voice:

—“Well, then, do you want to know how I did it?”

—“You bet I do!” I assured him fervently. This magic of
light and shadow had enthralled me.

—“And you want to learn how to do it too, don’t you?”
Yal continued, playfully.

I smiled and nodded.

—“Well, yes. I want to learn. And believe me, I’m a fast
learner because I listen better than a squirrel. My
master says it’s not true, but it is true. Really.”

I could see his smile in the darkness. He replied:

—“Do you know what jaypu is?”

—“Pfft, of course I do. The internal energy of every
living thing,” I replied with application. “My master
says that, for most spells, it’s best to use it, because
it helps you be stable and not end up like a rabbit in a
trap.”

There was a silence. And then Yalet uttered a muffled
gasp and half rose for a second, stammering:

—“By all the Spirits, I can’t believe it! Your master
was a magician?”

Oh, my, did I just blunder? *No*, I thought. How could
telling Yal the truth be a blunder? This was my second
Elassar, after all. So I nodded.

—“He’s a magician. But he says he’s special. And that he
doesn’t like visitors, which is why he told me never to
talk about him.”

And I was doing exactly the opposite, I thought,
suddenly embarrassed. After a brief silence, Yal cleared
his throat and asked:

—“What has this master taught you?” I felt in his voice
a slight tremor of excitement and disbelief.

—“He taught me things,” I replied.

—“What kind of things?”

—“Well…” I fidgeted nervously, remembering my master’s
warnings more and more clearly. “A lot of things. He
taught me a perceptist spell, to sense around me without
needing my eyes to find food, but it consumes a lot of
the energy stem and I don’t like it much. And he taught
me to…”

I fell silent and lowered my head, undecided.

—“To?” Yal encouraged in an inquisitive whisper.

Should I tell him or not? I wasn’t supposed to tell
anyone about this. But… what if my master was
exaggerating? What if necromancy didn’t scare sajits so
much anymore? I had to find out. I waddled in my seat,
swallowed my saliva, and tried to think of a roundabout
way to tell him, but I couldn’t think of anything, and
since my master said that bad news were better to be
told all at once than in dribs and drabs, I decided to
blurt out the truth, just like that, in one go.

—“He taught me how to use morjas. Especially that of
the… bones. Because he’s a nakrus and he knows a lot
about that stuff. And he taught me because of my hand.
He saved it for me, years ago, but not quite. And when
he chased me away, he made me a magara so people
couldn’t see the bones.”

There was a silence, this time a long one, so long that
I thought to myself, this is it, my master warned me,
the sajits do not like necromancers, and then wham, you
go and tell your new master everything! I almost heard
the nakrus say to me: if only you would think before you
act, Mor-eldal, if only you would stop experimenting!
But what was done was done.

I broke the silence with a hesitant voice.

—“Elassar? Elassar, you’re not mad, are you? Necromancy
isn’t a bad thing. It’s just about… bones. Morjas is
like jaypu. There’s no need to be afraid of them. My
master says that sajits are stupid to be afraid of it,
because it’s everywhere. He also says that people think
we are monsters, but I am not, and neither is my master.
Please, you must believe me,” I begged him.

—“Spirits and demons,” Yal breathed out slowly. “Of
course I believe you. And I’m not mad, why would I be?
No, I was just thinking. The more I learn about you, the
more surprised I am. A nakrus lost in the mountains with
a necromancer kid and three books. You could almost
write a terror tale about it, you know? Reassure me, is
there… anything else important you forgot to tell me
about your life?”

He had cast a light spell again, probably to get a
better look at my face, but the thing was, I saw his
too, and his half-alarmed, half-fascinated expression
confused me. I bit my lip.

—“Well… not that I know of.”

—“You sure? Come on, Mor-eldal, tell me everything. If
you turn into a demon on full Gem nights, just tell me,
don’t worry, while we’re at it…” he cleared his throat.

I shook my head and looked at him questioningly.

—“So… you’re not chasing me away because I’m a bit
undead?”

—“Of course not,” Yal snorted to my relief. “Look. As
long as you promise me that you won’t reanimate any
skeletons or anything like that, I’ll be fine. But…
don’t tell anyone else about this. Not Yerris, not Rolg,
anyone. You shouldn’t have even told me. Well, actually,
it’s good that you told me, but… merciful spirits…” He
ran a worried hand through his hair. “Tell me… Since
yesterday, there’s been quite a scandal in the Cats
because they found some evil signs on a bunch of street
corners. Very old signs that, according to what the
local priest says, come from the world of the Dead. One
guy said he saw a little black imp. Uh… Tell me, was it
you by any chance?”

I blinked, bewildered. Evil signs? I huffed.

—“These are good signs, not evil signs. I use them to
mark my way.”

Yalet rubbed his eyes then suddenly burst out laughing.

—“Do you know that you kept a whole troop of
parishioners busy for hours removing those signs?
Mor-eldal,” he pronounced abruptly, and I looked at him
carefully. “What does Mor-eldal mean?”

—“Survivor,” I replied.

—“In Morelic,” Yal muttered. “The language of the dead,
isn’t it? You know how to speak Morelic.”

I did not deny it. I remembered that my master had once
explained to me that centuries before, a great sect of
fanatical warriors had used Caeldric as a secret
language, and it had come to be regarded as an evil
language in the whole region of Prospaterra. My master
was already a nakrus at the time, and he had heard the
news from one of his “old friends” who came to visit him
from afar. That was why he had advised me not to speak
in Caeldric, but how could I have imagined that I should
also avoid using those signs? Yalet sighed, clasped his
hands together, and leaned toward me; his face was
grave.

—“Listen, sari. Now, I don’t think anyone recognizes you
or associates you with that little black imp, but I
don’t want you to… uh… to start marking your way with
those signs again, you hear me? Don’t speak Morelic and
don’t do… anything that master taught you. Maybe you
don’t realize it, but necromancy is dark magic, bad
magic. Or at least, that’s how the celmists at the
Conservatory define it. To them, it’s a hundred thousand
times worse than harmonies. Nakrus are horrible
creatures to them. And… I have to say that it would give
me the creeps to meet one. Especially since such
creatures don’t normally save children. If they could
eat them, they would, you know what I mean… Anyway. This
is serious, Mor-eldal. Very serious. If you let
something like this slip out with another person, you
might not end up in prison but at the stake. Do you know
what the stake is?”

I shrugged.

—“A fire that burns. Something bad.”

—“Mmph. Yes. Something bad, sari. That fire would send
you to the after-quarter. And not to Tarmil or Riskel:
to the quarter of the dead, you understand me?”

—“My master had already warned me,” I assured, and I
tightened my arms around my knees, shivering. “I just… I
just thought that… since you were Elassar, too…”

Yalet nodded several times, thoughtfully, then reached
out and took my arm affectionately.

—“I know that, Mor-eldal. And it moved me to hear it
from you. But… you’ve only known me for two days. You
don’t really know what I’m like. It turns out you got
lucky. But that doesn’t happen all the time. Not all
sajits are able to assimilate… that. Some would betray
you thinking they are doing something right. Some would
reveal your deepest secrets. Learn to be wary, or else…”
I saw him swallow. “Or else you will end up very badly.
Do you understand?”

I nodded, shocked. Of course I understood, I wasn’t
stupid. But at least now I was certain that I shouldn’t
talk. And that there was someone in this foreign city
who knew my secrets and would not divulge them. Someone
I could fully trust as I could with my master. And that…
that was very comforting.

I don’t take money from snakes
==============================

The music of the harmonica floated in the warm summer
afternoon air, quiet and serene. Lying flat on the dry
earth in a corner of a square deep down the Cats, I
listened to the music as a pleasant torpor gradually
took over me. Whenever Yerris took out the harmonica, I
sighed with relief, for it meant that he would stop
talking for a while. Really, his verbiage was
impressive, he didn’t know how to keep quiet. I listened
at first, and if he gave me a second to get a word in, I
would say “yeah,” or “sure,” or “yes, yes,” but after a
while, if his chatter was not particularly interesting,
I would end up giving him a saturated look, I would look
away, and his words would become a buzz of bees to me.
He didn’t seem to mind, and one day when I told him he
was more talkative than the turtle-doves in the morning,
he mockingly replied: *‘How could I let the turtle-doves
beat me to it, shyur?’*. And he went on talking and
talking. He talked about everything, about famous
musicians, about stories that had happened in the Cats,
about things he’d been told here and there about a
certain thief or certain employer… The semi-gnome was a
torrent of information. Fortunately, over time, just as
Slaryn, I had learned to recognize his different tones
and to know when it was important to listen to him and
when I could relax a little.

I yawned… and a scream suddenly broke the serenity of
the afternoon.

—“Yerriiis!”

Startled in mid-yawn, I looked up, blinking from the
light, and saw a panicked silhouette appear, running
across the square, with her long red hair loose floating
behind her. It was Slaryn. I hadn’t seen her for a week,
because her mother had just gotten out of prison and
taken her back home.

—“Yerris,” the dark elf repeated, stopping beside us,
panting. “I finally find you.”

—“Slaryn?” Yerris said, puzzled, pushing the harmonica
aside. “What’s the matter?”

—“It’s Korther,” Slaryn explained. “He says you have to
go see him at once, he has a job for you.”

Yerris’s face darkened as if he had been told that he
had been sentenced to hard labor.

—“Hold on a sec,” the semi-gnome said. “What’s this all
about? Korther never gave me any job. Normally, it’s
Alvon who—”

—“Precisely, it’s about him,” Slaryn muttered, crouching
down. “Your mentor is behind bars.”

I sat up with a start, bewildered, while Yerris, for the
first time since I had known him, stammered:

—“Al-Alvon? It can’t be. Al, behind bars? But Al’s the
best Black Dagger in—!”

—“Keep your voice down, you idiot!” Slaryn hissed. She
glanced at a group of children dawdling a little further
away and then continued in a low voice: “He’s in the
detention center in Menshaldra, and they gave him a
hefty fine because they caught him with a forbidden
magara. And apparently he can’t pay it.”

The semi-gnome grunted something unintelligible.

—“Yerris!” Slaryn snapped impatiently. “Korther will
explain. Get a move on, come on. You’re not going to let
your mentor down, are you?”

Yerris pouted.

—“Of course not,” he protested. “But, what an idea, to
let himself be captured by flies, he, the thief of the
Pearl of Aodance, who has travelled half the world in
his youth and is always bragging about it. And now it
turns out that he can’t pay a fine! Surely his mania for
dressing like an eccentric has turned against him, I
warned him: never, Al, never should an old Cat dress
like a buffoon, beware that the clothes might rub off on
the soul, and he wouldn’t even listen to me! He never
does, he’s…”

—“Why don’t you stop blabbing and go!” Slaryn cut him
off.

At times, Slaryn’s tone of voice was so commanding that
it would have made a seasoned mercenary waver. The
semi-gnome and I exchanged glances, and he reluctantly
stood up.

—“Is he at the Hostel?”

—“He is,” Slaryn confirmed.

The Hostel was more or less like the headquarters of the
Brotherhood. It was a bit beyond the Grey Square,
according to Yerris. I had never been in there, but my
companion said that it was better for me never to go in
there because, according to him, every time you did, you
came out older, with more responsibilities and more
worries.

As if reluctantly, Yerris took a step… and stopped,
looking at Slaryn curiously.

—“And what were *you* doing at the Hostel?”

The dark elf huffed.

—“Just some issues about my mother. Go ahead, or Korther
will pull your ears.”

Yerris rolled his eyes.

—“I’d like to see him try.” And he smiled at me. “Be
good, shyur. Sla, do you know that, this noon, that
shanty boy found a ten-nail coin on the ground there on
the Esplanade? Literally. We shared a dish of hot rice
at *The Ballerinas* and you don’t know how good it felt!
A treat! And—”

—“Yerris!” Slaryn exclaimed, losing her patience.

—“Whoa, I’m going, princess, don’t rush me. You’re worse
than Al. You’d make a good Black Dagger kap, you know?
You’d make us go straight as a command staff,” the
semi-gnome scoffed. He raised a soothing hand under
Sla’s exasperated gaze, placed the harmonica between his
lips, and zigzagged out of the square, playing his
instrument.

I heard Slaryn’s sigh loud and clearly.

—“One of these days Alvon’s going to wring his neck.
Unless I do it first. Tell me, Draen. You’re going back
to the Den, aren’t you?”

—“I am, I am,” I said.

—“Well, then go. I’m going home,” she declared.

She was about to walk away when I jumped up and said:

—“How much money are the flies asking for?”

Slaryn smiled wryly.

—“Thirty siatos. Nice sum, huh?”

I scratched my head, worried.

—“Is Yerris going to steal them?”

—“I don’t think so. If I had to bet, I’d say Korther has
a job lined up and he’ll send Yerris to pay the thirty
goldies if in exchange Alvon does what he says… Our kap
is very pragmatic,” she grinned. “Ayo, brat.”

She gave me a friendly pat on the cap and walked away in
a hurry. I saw her disappear in the direction of Tarmil
Avenue, and I bit my cheek thoughtfully as I put my cap
back on. Thirty siatos, or goldies as they called it in
the Cat Quarter… That was a lot of money. I hoped Sla
was right and that Korther was willing to pay up and get
Alvon out of the slammer. I had never seen the mentor of
Yerris, but after hearing my companion talk so much
about him and his exploits and eccentricities, I almost
felt as if I knew him, and knowing that he was in
Menshaldra so close by, I was excited to finally see
this mysterious, unsociable character in the flesh.
During those three moons he had been absent, off doing
who knows what, stealing or hunting treasures… As Rolg
would have said, even the Spirits couldn’t know what the
Black Daggers were doing when they wandered in the wild.
As Yerris had explained to me, the Black Daggers were a
rather loose brotherhood. Although they had kaps in many
of the major cities of Prospaterra, their members often
worked independently of each other and, as fellow
members, they were committed only to helping and keeping
the brotherhood going by donating a portion of their
earnings and, from time to time, seeking out new
recruits.

I headed for the Den and trotted up the street. However,
halfway up, I changed my mind and turned back. There
were still a few hours to go before it got dark, and
besides, I wouldn’t have any lessons this afternoon
because Yal was in the middle of exams, studying harder
than a mage, he said, and he couldn’t lose focus
because, if he failed, goodbye graduation. I didn’t
quite understand why he cared so much about the diploma;
all I knew was that I missed his quiet, friendly
lessons. I was saddened by the thought of not having
them for half a moon, although I didn’t stay idle.
During the day, I followed Yerris everywhere, and when
the half-gnome left me in the Den to attend to “personal
business”, old Rolg always found some task for me to do,
such as fetching water from the well, washing clothes,
or delivering a letter to someone. At night, I slept so
soundly that, if a bell had rung over my head, I
wouldn’t have heard it.

So, fleeing Rolg’s tasks, I went down the hill and
through muddy streets, zigzagging to get around a few
passers-by and avoiding two ladies who were hurling
insults in each other’s faces with such vivacity that
they reminded me of those actors at Scion Theatre where
Yerris had taken me one afternoon.

At last, I came to a narrow staircase, stopped, glanced
around and, without further hesitation, began to
descend. Yerris had already told me about the Labyrinth.
He said that it was a real kingdom in the city, that
there were streets that passed over the houses and
houses over the streets, that it was, in short, a chaos,
a wonder of the sajit nature, a jungle full of
mysteries, that there were lots of people and that
spending even one or two hours in this den made you a
Cat for all your life. And as I wanted to check if this
was true—and as my nakrus master had told me that in
order to learn how to live, you need courage and
bravery—I left aside Yal’s warnings and entered this
world with the discretion of a cat and the curiosity of
a pup.

The streets were even narrower than the rest of the Cat
Quarter, many of them mere corridors with a sea of
hanging laundry floating over them, and from which one
could barely see the sky. I passed an elf walking with
his hands in his pockets, wearing a huge cloak and a
wide-brimmed hat that hid most of his face. Then I saw a
very small girl sitting on the threshold of a house, she
looked at me with very large blue eyes, and I smiled,
ruffling her hair as I passed.

—“Ayo, little one,” I said.

I walked on with a light step. I went up and down
stairs, crossed bridges over alleys, and on the way, I
passed sajits of all sorts and races, old and young,
ragged and well-dressed, and everything in between.

I was passing through an alleyway which was a little
less narrow, when suddenly a door opened and a drunkard
came out singing and walked away, leaving the door open.
A white square was drawn on it. And inside, there were
noisy tables and a counter on the left held by a burly,
smiling dark elf. A tavern! Hearing a thunder of
laughter, I approached, curious, and was about to enter
when a hand grabbed my arm and I turned to face a young
dark elf with green eyes much taller than me. I
recognized him immediately: I had seen him talking with
Yerris a few times.

—“Warok!” I said, surprised.

—“I wouldn’t advise you to go in there, shyur,” the dark
elf told me calmly. “*The Drawer* is no place for
innocent saints.”

He spoke in a mocking tone, and I gave him a bold pout.

—“I am nothing like an innocent saint,” I replied.

Warok gave a crooked smile.

—“Did Yerris send you?”

I shrugged and said:

—“No. Why?”

Warok grimaced.

—“Do you know where he is?” I shook my head and heard
him mutter, “The Darble knows what’s that cove up to.
Hey, shyur,” he said loudly. “If you see him, tell him
to come by my hideout and tell him I’ll give him his
share, will you?”

—“*Natural*,” I said.

He smiled, patted me on the shoulder and was about to
walk away when I asked him:

—“Where is your hideout?”

Warok arched an eyebrow and shook his head.

—“That’s the kind of thing you don’t say out loud or to
strangers.” I looked insulted, and his eyes smiled. “But
maybe I can show it to you. Come.”

Enthused, I said:

—“Thanks!”

And I followed him briskly through the alleys.

—“Why does Yerris say the Labyrinth is wonderful?” I
asked.

Warok huffed.

—“He says that? Well… I guess it’s because the Black Cat
is a crazy Cat, plus a musician,” he joked.

I cocked my head to one side, thinking, and after a
silence, I asked:

—“And why do others say it is dangerous?”

—“Hmm. Because it is, but not so much if you know how to
protect yourself,” the dark elf assured. With a nimble
movement, he pulled out a dagger and showed it to me
more closely. He smiled. “That kid… It doesn’t even
scare him. Well, it should, you know?” he said, putting
the weapon away. “Only the cautious survive in the
Labyrinth. That’s why I’ll only tell you that my hideout
is near here, a few meters away. If you find it, I’ll
give you a nail.”

Well, a challenge, eh? I spun around, looked up, and
pointed to a hole between a terrace and a house.

—“There?”

Warok looked at me with an annoyed pout.

—“And on the first try,” he murmured. He tossed a nail
coin, and I picked it up with a big smile. He rolled his
eyes. “Don’t believe everything the Cats say, shyur. My
hideout is not here. And now go back to yours: if the
night catches you here, you may well turn into a
spirit.”

Before walking away, he poked my head, and I watched in
disappointment as he disappeared around a corner. After
a few moments of indecision, I followed him silently. At
one point, he turned, and I had to crouch down
hurriedly. I even managed to cast a harmonic shadow
spell to perfect my hiding place: Yal said I was quite
talented. But then again, I already knew a lot about
jaypu controlling; I think I knew more than he did.

Hidden as I was, I saw the dark elf pass under a gate,
and I followed him to see him slip through the gap in a
palisade and…

—“Well, well, well,” Warok said. I stopped short. “You
look like you’ve been running like a demon, shyur.”

I breathed a silent sigh of relief that he had not
discovered me and approached the stockade.

—“Bad news, right?” Warok went on.

A snort answered him, then a:

—“I need your help.”

My eyes widened as I recognized the voice. Yerris? His
voice sounded so fearful and pleading that I convinced
myself I was wrong.

—“I’ve been helping you for three moons, remember?”
Warok replied. “What do you want now? Goldies? If you
think I’m going to give them to you just for your gnome
face—”

—“It’s not that,” the other one cut him off. It *was*
Yerris, I thought. “It’s… Korther. I went to see him two
hours ago.”

—“Very clever of you,” Warok scoffed. “Didn’t you say he
suspected you?”

—“I wouldn’t have sworn to it before… but I do now,”
Yerris sighed.

—“Fool. Why on earth did you go to see him?” Warok
questioned in a curt tone.

—“Demons, and what do I know, I didn’t want to,” Yerris
assured. “It was a trick of his. He let me know that my
mentor was in the slammer. And that was true. But
Korther had already sent someone to pay the fine. He
wanted to talk to me alone. He told me that… if he finds
out I’m involved with the Labyrinth mob, he’ll fire me.”

—“So you told him everything?” Warok said indignantly.

—“No, of course not!” Yerris protested. “He has his own
informants, Warok. And anyway, what could I have told
him? I don’t know anything about the Black Hawk. And I’d
rather not know anything about him ever. Please, Warok.
You gotta help me. I want… to put all this behind me. I
never wanted to be a spy, and I never wanted to steal
for… for *him*. I’m not a traitor. Tell the Black Hawk
I’m giving up his money. I don’t want it, tell him that,
Warok—”

—“Unbelievable,” Warok muttered in a scornful tone.
“Yerris the Black Cat growls and bolts away like a
coward. You know that? The Black Hawk hates cowards.
Remember, you didn’t become a Black Dagger on your own.
You did so thanks to us. Now that you’re an accomplished
little magician thief, you think you have the right to
speak your mind, but it’ll only bring you trouble, you
hear me? Whether you like it or not, you’re gonna have
to explain it to his face.”

—“No, no, please, Warok, don’t do this to me,” Yerris
gasped. My heart was beating faster and faster.
Something serious was happening. Something that smelled
very bad. “Please,” Yerris repeated. “I swear I won’t
talk. I won’t say anything about you or the other
Ojisaries. I’m even willing to swear that I’ll leave the
Black Daggers if the Black Hawk asks me to. But I will
not betray Korther again. You have to understand me, if
he catches me working for the Black Hawk, I’m dead.”

Suddenly, I heard a noise behind me and I turned around,
just in time to see, horrified, a big hand grabbing my
neck. I screamed. Another hand tried to gag me, and I
bit and kicked until arms lifted me up and slammed me
against a wall.

—“Don’t move!” my attacker bellowed.

I received a slap, and stifling my mountain kid
instincts, I refrained from defending myself by
releasing mortic shocks and stopped struggling. The blue
eyes of my attacker were watching me, displeased. He was
a fairly young blond caitian.

I heard Warok sigh.

—“That’s all we need… Tif! Get him in.”

Without a word, Tif pushed me through the palisade, and
I staggered back, my heart pounding. I had never been
hit by a sajit before, and I could see that it hurt,
both physically and mentally. Warok’s hideout was a
small muddy courtyard with a sort of canopy and a rocky
corner with straw pallets.

—“What are you doing here, shyur?” Yerris threw at me in
disbelief.

He had just got up from one of the pallets. I rushed to
him and shouted:

—“Yerris!”

I did not say another word and clung to him tightly,
wanting to forget Tif and Warok. Now I didn’t find the
dark elf sympathetic, quite the opposite.

—“Calm down, shyur,” the semi-gnome whispered to me.
“They won’t hurt you.”

—“You assume a lot,” Warok replied, a crooked smile on
his lips. “The brat is a Black Dagger too, isn’t he?
He’s been listening to us. And he knows what gang we
belong to. He’s a walking danger.”

I watched in terror as he pulled out the dagger.

—“Don’t you dare do that!” Yerris stepped in, appalled.

Warok shrugged.

—“It would do you a favor, though: if the kid talks,
you’re dead.”

—“He won’t talk,” Yerris asserted, panting. “I swear he
won’t talk. Right, Draen? You won’t say anything about
what you heard, right? Because, if you do, you won’t see
the Black Cat or the musician again, you hear me?”

I nodded and said:

—“I’ll say nothing. Not even if they rip out my bones
one by one. I swear it, Yerris.”

The semi-gnome ruffled my hair and said:

—“See, Warok? This kid is a treasure. Take a cue from
him and tell me you’ll try to convince the Black Hawk to
forget me. Forever. Please.”

Warok looked at me, looked at Yerris, then pouted
wearily.

—“I’ll talk to him. But he’s not gonna let you off the
hook, Yerris. Not until you do… what he asked you to do
and you haven’t done yet.”

The semi-gnome was now holding my arm, and I felt him
tense up.

—“It runs,” he muttered. “He’ll get those documents.
But, after he does, he’ll have to leave me alone.”

Warok smiled, stepped forward, and placed a small bag of
money in the half-gnome’s hand.

—“Get the hell out and go back to your Den. Don’t set
foot in the Labyrinth again until you get the documents.
Send the brat if you have any news, and he’ll give you
the money. Don’t worry: I won’t do anything to him as
long as he behaves. And now, get out of here,” he
repeated.

Yerris glared at him, but he silently walked away
without letting go of me. Once at the fence, I turned to
glower at Warok, pulled out the nail he’d given me
before, and tossed it into the mud. The dark elf gave me
a mocking expression in return, but I didn’t care: I
didn’t want to take money from people like that snake.
Yerris pulled at me, and I followed him under the gate
and through the corridors.

Certainly, the conversation had shocked me, but not as
much as the semi-gnome’s silence on the way back. We
were already leaving the Labyrinth when I blurted out:

—“These guys are worse than lynxes. They smile and then
attack.”

Yerris sighed heavily, and giving him a worried look, I
asked:

—“Do you have to steal documents?” I saw him nod,
distracted. “Is that dangerous?”

Yerris sighed again.

—“It is, shyur. It’s dangerous. Because the documents…
it’s not those nasty nail-pinching evil swells who have
them. Korther has them.”

My eyes widened. Yerris was going to rob the Black
Dagger kap of Estergat?

—“But… who is this Black Hawk? Why—?”

—“Shut up, shyur,” Yerris whispered. “Please. Don’t ask
questions.”

I bit my lip, walked beside him and, after a silence,
said, a little disappointed:

—“The Labyrinth is not all wonders, eh?”

Yerris shook his head and gave a smiling pout.

—“When this whole Ojisaries thing is over, I’ll show you
the Labyrinth properly. You’ll love the Wool Square:
every afternoon, a guy called the Breaky-Hand comes and
starts telling stories. We gwaks give him nails, and he
lives on them. You don’t know what great stories he
tells! And there are taverns that you’ll love too. At
first, some of the guys may impress, but once you get to
know them, you see that they actually have a heart as
big as a castle. And…”

And he kept talking all the way to the Den; when we
arrived, we heard voices inside. The door was ajar.

—“He’ll be here soon, I’m sure,” old Rolg’s voice said.

—“I think I heard something outside,” a deep voice said.

The door opened further, and I saw a tall, pale human in
a long blue cloak. He wore a strange red hat and green
boots. “Eccentric”, Yerris had said… I smiled. Even I
thought the way he was dressed was strange.

—“Al!” Yerris exclaimed, and he climbed the wooden
stairs, “It’s been ages! Still, to think you let
yourself be snaffled by the flies just for a little
magic lantern. I’ve missed you, especially since you
said you’d be back for the Celestial Moon and we’re in
Wells, and like, you know, everything you gave me has
long since run out, and I had to pawn even my ears to
stay honest, mind you—”

—“Silence,” Alvon thundered. He wrinkled his nose,
looked down at his sari, and pouted. “You haven’t
changed a bit. Rolg, thank you for taking care of him.
I’m taking him back. Come, Yerris.”

He passed by him on his way down the stairs, and when he
passed by me, I smiled, but he did not even glance my
way. The semi-gnome looked at me with a worried
expression, and as he came closer, he whispered to me:

—“Don’t worry, shyur—I’ll see you soon. Al can’t stand
me more than two days in a row. I’m a compulsive talker,
but don’t tell anyone,” he joked. And I understood, from
his eloquent look, that with these last words, he was
trying to remind me of my oath of silence.

—“Yerris!” Alvon growled.

I gave Yerris a pout of complicity, and he took off
running behind his mentor. After watching them disappear
from the courtyard with some disappointment, I glanced
up at the darkening sky and, yawning, entered the Den.
Old Rolg was sitting at the table eating a plate of
porridge. I too sat down, rested my chin on my crossed
arms, and after listening for a moment to the elf’s slow
chewing, I asked:

—“Do I have to do something, Rolg?”

He looked up, smiled slightly, and shook his head.

—“No. I already went to get water.”

I felt a little guilty, because, with his lame leg, it
was not good for old Rolg to walk with a heavy load.

—“Tomorrow, I’ll go and get it, don’t worry,” I said.
And after a silence, I added: “Rolg, did you steal
precious things when you were young?”

—“Mm… Of course I did,” Rolg replied while swallowing
his porridge. “Beads, jewels, magaras, relics… and items
you can’t even imagine.”

I smiled at his comically mysterious pout and hesitated.

—“And… why did you decide to become a Black Dagger?”

—“Ah!” the old elf smiled. “Well, this is going to sound
odd to you, but, unlike other veterans like myself, I
don’t talk about the past. I’m too practical to get lost
in eras that have long since ceased to exist.”

—“Gee,” I muttered, surprised. “But… if you don’t talk
about it, is it because you don’t want to or because you
don’t remember?”

Old Rolg rolled his eyes.

—“Both. No, seriously, kid, of course I remember. I’ll
just tell you that, when I was your age, I was such a
shy kid that I didn’t even dare to leave my house alone.
At that time, I lived in the country, and at night, you
would hear terrible wolf howls. When I heard them coming
closer, I would get up and run to my parents’ room
shouting: daddy, mommy, the dragon is coming!”

I returned his smile, amused, and asked:

—“And why did you leave the country if you had a
family?” The old man darkened, and I with him, thinking
I understood. “Did they chase you away?”

The old man shook his head.

—“No. One day, the dragon came for good in the form of
murderous bandits and… I was left alone. You see. And
like you, I made the journey to Estergat, crossed the
Arkolda Forest, and arrived in the capital as ragged as
you. And I ended up becoming a Black Dagger… exactly
like you.”

His eyes shone, smiling, and I stood there thinking,
trying to imagine the old elf, young like me, walking
lost among dense trees, lynxes, poisonous mushrooms, and
snakes…

—“Did you have dinner?” the elf asked me. As I shook my
head, he pushed the plate of porridge towards me. There
was still a quarter of it left. “Here you go. Enjoy your
meal. I’m going to sleep. And don’t let anyone disturb
me, eh?”

I saw him get up and walk away to his room, and I
hastened to say:

—“Hey, Rolg. Thank you. For the dinner and for the
story. And don’t worry. The past is still the past. My
master used to say that, if you had to remember
everything, you’d go crazy. He didn’t talk much about
when he was… uh… young either.”

That is, neither about when he was alive nor about when
he was a young undead, I added mentally. The old elf
looked at me with a slight smile.

—“Good night, kid.”

—“Good night, Rolg!”

As soon as the door closed I took the plate with both
hands, and leaving aside the sajits’ habit of eating
with a spoon, I swallowed the porridge in a
“peace-and-virtue”. Then, I took my yellow feather, went
to the window, and looked up at the night sky, convinced
that my nakrus master must be looking at them at this
very moment. Softly, I whispered:

—“Good night, Elassar.”

I met a disciple and a slacker
==============================

Yerris was wrong. Alvon put up with his sari for more
than two days in a row. And I, seeing myself without a
guide or mentor, spent the first few days wandering
around the city with no clear purpose, going in and out
of taverns, roaming through parks and even begging, as
Yerris had taught me to do, with discretion and a
wet-dog, beaten-down look. On those days, I watched a
group of newspaper sellers, and realizing that they were
collecting quite a lot of money, on the third day I went
up to them and asked them where they got the papers
from, to which one of them replied: from the office! And
he was kind enough to show it to me, because they were
already on their way to return the morning papers and
collect the afternoon ones.

In front of the press office, there was a noisy,
cheerful crowd of children and teenagers who were having
fun making bets by playing dice. One of them had his
pockets emptied and, as he found himself three siatos in
debt, the boy lamented:

—“May you hang, my father will kill me!”

And while he shook his head, uttering desperate
imprecations, his friends, and especially the winner,
laughed and tried to console him in a light tone.

—“Don’t worry, Tens! The same thing happened to me last
moon,” a youngster of about twelve years reminded him.
“And you know what I did? I said I was attacked in the
street by mobsters. Daddy, Mommy, I was attacked and
they stole all my money!” he cried.

—“And did it work?”

—“Well, yes, but somehow, it was worse: my father called
me a coward because I had given them everything without
defending myself. Believe me and tell your father that
you were attacked by a whole gang and that you smashed a
guy’s head in, but that you were outnumbered.”

—“Well, I’ll give it a go,” Tens assured.

Others continued the game. I was watching them when
another scene caught my attention: a redheaded elf had
just violently pushed a little boy a few meters away on
the Imperial Avenue. It reminded me so much of what
Warok’s friend, the caitian, had done to me that, with a
frown, I went over to see what was going on.

—“You owe me twenty nails!” the attacker said to the
attacked. The latter did not resist but did not lower
his head either, he looked away, acting as if the
attacker’s words were, to his ear, nothing more than a
bird song.

—“You hear me, you isturbag?” the red-haired elf
insisted, shoving him.

—“Isturbag yourself! Don’t hit him,” a little dark elf
shouted, stepping in and standing up like a watchdog.
“Go away!”

—“Well, well, Manras. Are you defending the demon now?
Don’t you know it’s contagious to associate with those
devil-eyes?” the boy threw at him.

—“Shut up! Dil is my friend!” the little dark elf
growled.

—“He owes me twenty nails,” the redhead replied.

—“He doesn’t owe you anything!”

—“Yes, he does: he’s a devil.”

—“It’s not fair!”

At that moment, the red-haired boy turned to me when he
saw me so close and frowned.

—“What are you looking at?”

It must be said that, after I became interested in the
scene, my eyes had remained intensely fixed on the
redhead’s face. It was totally ravaged by a nasty
disease. I shrugged.

—“Why do you say he’s a devil?”

The redhead huffed.

—“Why, you ask? Just look at his eyes.”

Only then did I notice, in fact, that the kid called Dil
had almost reddish purple eyes with a vertical pupil
like snakes. I shrugged again.

—“Have you ever seen a devil? No, right? Then how do you
know it’s a devil?”

The redhead arched an eyebrow.

—“What? Well, shyur, don’t you know they say sajits with
eyes like that are devils? This kinchin is not a human:
he’s a devil.”

I saw a trembling gleam in Dil’s eyes. So he was
actually listening to us and predictably didn’t like
being called a devil. I faced the attacker.

—“Stop calling him a devil, it’s cruel. Get out of here
or I’ll wring your ears off.”

To my surprise, the boy smiled.

—“You don’t lack guts. Can’t you see I’m way older and
bigger than you, shyur? Keep it down. You’re a gwak of
the Cats, right?” It hurt to admit that he was right: he
was much stronger and taller than me, he must have been
Yerris’s age, which meant that my chances of coming out
on top in a fight were pretty slim. I moistened my lips
and nodded. “It shows. And you sell newspapers?” I shook
my head. “No? Then what are you doing here?”

—“I was taking a look around,” I explained. “To see how
this newspaper thing works. I heard somebody say he made
fifteen nails in five hours, and sometimes more. But I
don’t know how it works.”

The red-haired elf looked at me mockingly. And,
suddenly, he held out his hand to me.

—“My name is Draen the Swift, from the Cat Quarter too,”
he introduced himself.

I was still apprehensive about holding out my hand to
shake someone else’s, but Yalet assured me that my right
hand looked real, or almost real: the only difference
was that it wasn’t as warm as the other. So, after a
hesitation, I shook Draen’s hand and replied:

—“I’m Draen, too.”

Draen smiled.

—“Oh, a namesake. You know, shyur? Fifteen nails for
five hours is a pittance. I earn more begging in the
temples. Tell me, are you in a gang?”

I looked at him warily and shrugged.

—“Why do you ask?”

A commotion arose in front of the desk, and as the
children clumped together to fetch the papers, the
red-haired elf inhaled casually through his nose and
shrugged as well.

—“No reason. Ayo, namesake. Ayo, devil,” he threw at
Dil. “Don’t dawdle on your way home, you don’t want to
get attacked!”

He gave us an amused, joking, and mocking look, and
walked away down Imperial Avenue with a quick step.

—“Quite a vulture…” I huffed.

—“Come on, Dil,” the little dark elf said, urging the
alleged devil. “If we don’t hurry, there won’t be any
papers for us.”

Dil nodded without much enthusiasm, glanced at me, and
said a laconic:

—“Thank you.”

I smiled.

—“You’re welcome. Say, guys, can you explain to me how
this office thing works?”

The little dark elf stopped pulling Dil by the sleeve
and bit his lip, looking into my eyes. He couldn’t have
been more than eight years old, I judged.

—“Okay,” he said suddenly. “Come along.”

I followed them inside to a counter where I had to pay a
nail in exchange for a small brass plate with the office
symbol engraved on it. When we reached the dimly lit
basement, we waited our turn to ask the clerk how many
papers we wanted. When it was my turn, I said:

—“Twenty.”

And the clerk looked at me with a sideways glance.

—“You’re new, aren’t you?”

—“Yes, this is Draen, a friend of mine,” the little dark
elf interjected.

I arched an eyebrow, and my new friend smiled at me. He
whispered to me:

—“By the way, my name is Manras. How about we sell
together?”

I smiled.

—“It runs.”

I picked up my twenty newspapers, and a few minutes
later, the three of us were walking the streets, “*“The
Red Rumor”*! *“The Red Rumor”*! For a penny!”. I was
pretty good at shouting, and I was pretty good at
singing too, and by the time the four bells of the Great
Temple rang, I had composed my sales refrains:

Hey, the Red Rumor,\
informs in good humor!\
What happened today?\
Find out for one nail\
in the Red Rumor!\
The Red Rumor!\
The Red Rumor!\
Smarten your brain\
for one nail!\
The Red Rumor!\
The Red Rumor!


Manras imitated me, and in a few days, we formed a
terrible newspaper-selling duo. In the parks, in the
squares, everywhere, you could hear us going by,
shouting, singing, handing out newspapers and collecting
nails. It must be said that Manras was ready to imitate
me in everything, and his support also encouraged me to
improvise, to play at being the old-timer: one could
hardly tell that only a few moons ago I could not
remember what a house or a loaf of bread was. Dil, on
the other hand, was less inclined to imitate me—he had
nothing against me, but, according to Manras, Little
Prince was simply a first-class slacker. When we sang,
he sometimes looked at us as if we had gone mad, other
times he rolled his eyes or scratched his head and
reluctantly walked up to a gentleman to hand him a
newspaper almost without opening his mouth. All in all,
if Manras was as active a kid as I was, Dil was a
sleepy, friendly lebrine bear, but also a total
I-don’t-care sort of person and silent as hell…
Definitely the opposite of the Black Cat!

Precisely, a few weeks after I began my work as a
newsboy, seeing that Yerris had not yet returned, I
asked Rolg when my guide and companion would return, and
the old elf told me that, from what he had heard, Alvon
had taken him on a mission outside Estergat. The first
thing I thought was that he had taken him to keep him
safe from that Black Hawk. I was about to ask Rolg for
confirmation, but I thought twice and told myself that
doing so might have been a bad idea. Besides, an oath
was an oath. Anyway, I was impressed that Yerris was
going to work with his mentor; however, lying alone in
the Den, I also felt a loneliness I was not used to. And
I thought: I can’t wait for Yal to finish his studies
and come back to give me some attention. Honestly, if I
had known where Yalet lived, I would have gone to visit
him to disturb him a little, even if only for a few
minutes. I sighed, caressed my small silver pendant with
my right hand, and reached out to try and hear Rolg’s
breathing on the other side of the door, but to no
avail. It almost seemed as if, when the old elf
disappeared through that door, he disappeared from the
world. What could be behind it? I don’t know why, some
nights I thought there was something dangerous there,
and I could hardly fall asleep. And that night was one
of them. Alone in the room, I felt unsafe, just like
when I was traveling in the woods. And this was
something that had never happened to me with my master,
because I knew that he never needed to sleep, that he
was always watching, and that he was out there, looking
at the stars and repelling the mountain monsters with
his presence alone.

—“Ferilompard,” I muttered. “I must find a ferilompard.”

Then I could go back to my master. With this thought in
mind, I finally managed to fall asleep.


%%%


—“Earthquake in Veliria! *“The Night Gazette”*! *“The
Night Gazette”*!” I shouted.

It was a feast day, the first Kindday of Joys, a day for
celebrating the harvest, and the streets were crowded
with people. It was the best day for a newspaper seller:
sales were more than good. The only drawback was that I
hadn’t yet found a word that rhymed with gazette and
made a nice refrain, but people didn’t seem to mind
either.

I saw a gloved hand reach out with two nails, gave the
newspaper, and took the coins, shouting:

—“*“The Night Gazette”*! Earthquake in Veliria!”

Manras stopped beside me, panting.

—“Gee, Sharpy, you only have one left?”

—“And you, ten, I see,” I said. “What’s the matter with
you, shyur?”

Manras shrugged, looking grim.

—“My throat is hoarse. My brother’s gonna wring my ears
off…”

I gave him a sympathetic pout. Manras had the misfortune
to have a brother who took everything he brought back.
He lived with Dil, in the Labyrinth, but I had never
been to see them there: not only did Manras say his
brother didn’t allow visitors, but I didn’t want to run
into Warok on the way.

—“Listen,” I said to the little dark elf. “I can give
you a little. I have thirty nails left. If you want,
I’ll give you fifteen. With what’s left, I’ll have
dinner and lunch, don’t worry. Nobody’s going to wring
my ears off.”

Manras looked at me, his eyes wide.

—“Really?”

—“For real and in Drionsan,” I assured him, putting the
coins in his hand. “Here. Don’t you know that friends
help each other out? Well, there you go. But I’m doing
this because you’ve got a bad throat, eh? Any other day,
it won’t work.”

The little dark elf smiled broadly and jumped on my neck
with all the newspapers.

—“Thank you, Sharpy!”

I smiled and rolled my eyes. A newspaper vendor had
started calling me Sharpy because I made up my own
refrains and competed with him, and the nickname had
stuck. I hit Manras with my newspaper in a friendly
manner.

—“You’re welcome, shyur. Say, in return you take my
paper; I’m gonna have dinner and go home. Where’s Little
Prince?”

I saw him standing next to a lighted street lamp,
handing a newspaper to an opulent lady.

—“It can’t be! Isn’t he selling better than you?” I
said, impressed. “Well, well, ayo, Manras, see you
tomorrow!”

—“Ayo!”

I walked away, passed behind Dil, and pulled on his cap.

—“Good night, Little Prince!”

He gave me a half-exasperated, half-amused look before
readjusting his cap and making an almost imperceptible
gesture by way of greeting.

I ran down Tarmil Avenue, returned to the Cats without
almost slowing down, and, arriving at *The Wind Rose*,
threw to the tavern-keeper:

—“Mr. Tavern-keeper, I want some rice!”

And the tavern-keeper served me rice. In the last few
weeks, for the first time since I had been in Estergat,
I had started to feel like a real Cat, for I was earning
my meals, just as in the old days in the mountains when
I hunted rabbits, except that now, instead of hunting
them directly, I was hunting silver coins. I smiled as I
ate my rice, and turning to a group of people who were
singing in celebration of the Day of Joys, I quickly
finished my plate and went over to listen to them.

—“Kid!” a man said, seeing me standing there idly by
their table. “Sing with us, come on, we’ve gotta
celebrate!”

—“But I don’t know the words,” I said.

—“Well, you’ll learn them quickly, it’s simple!”

In fact, the lyrics were so simple that I learned them
in no time, and I ended up shouting along with them. My
throat was already a little hoarse from shouting for
*“The Night Gazette”*, but it didn’t seem to matter
whether I sang well or badly. At one point, I tugged at
the sleeve of Fiks, the old worker who had asked me to
join them, and pointed to a bottle.

—“What’s that?”

In all honesty, I knew perfectly well what it was, but I
played the naive on purpose, and my tactic worked: Fiks
looked at me, his eyes widened.

—“What? Don’t you know what wine is, boy?”

I shrugged.

—“I’m from the valley, sir. There’s no such thing
there.”

—“You never drank it? Want to taste it?”

—“Ragingly!” I exclaimed with a smile.

The worker smiled and shook his head.

—“Well, you came at the right time! Dabel! Pass us the
wine, the kid’s thirsty.”

I really was, so I took several long sips before someone
laughed:

—“That kid’ll knock back the whole bottle, Fiks!”

—“He sure can hold his drink!” another said, laughing.

The old workman forcibly took the bottle from my hands,
and I cried out joyfully:

—“Damn, it burns!”

Fiks huffed.

—“Who taught you to drink like that? This ain’t spring
water, boy! Come on, get a move on and go home, wherever
that is, before you turn a blind eye. On religious
holidays, we drink to be merry, not to get drunk. Ah,
young people…!”

He ruffled my hair, handed me back my cap that had
fallen off, and gently pushed me out the door. I gave
him a big smile and laughed.

—“Thank you, Fiks!”

I staggered, hit a dog, and backed away. I stumbled
across the Grey Square, staggering and looking very
bright. Fortunately, the Den was not too far away, or I
wouldn’t have made it. In the streets, I passed some
blurred figures, and strangely enough, I thought it was
a good thing that I had given the rest of my coins to
Manras, for now, if I were attacked, the attackers would
be in for a disappointment. I was not far from the house
when I stopped to look at the sky and saw some very
bright stars.

—“Count them, Mor-eldal, count them!” I exclaimed. “One,
two, three, four…”

I continued to count each step I took, and at last, I
reached the small courtyard. I climbed the stairs,
counted the steps and frowned.

—“Twenty? Impossible.”

I went back down the stairs, up the stairs, and this
time, I counted six. That made more sense. I was about
to push open the door when it opened with a bang and a
light hurt my eyes.

—“Draen?” a surprised voice called out.

I blinked, and when I saw that my master’s eyes were
wide open, I laughed.

—“Ayo, Elassar!”

And I sang:

Give me your hand,\
brother,\
Follow the path\
And let’s sing together:\
Long live summer!\
Long live autumn!\
Long live the Daglat and the Hundred Spirits!\
Long live wine!


I lost my balance, and fortunately, the railing kept me
from falling.

—“By the Four Spirits of Dawn,” Yalet muttered
incredulously.

He took my hand and led me in. The sound of the door
closing seemed as loud as if lightning had fallen on my
head, and I let out an “Ouch,” followed by a brief
chortle.

—“Thunders, can you explain to me how the hell you got
in this state?” Yal asked.

—“The how is obvious enough,” Rolg said, amused.

The old elf sat at the table with a paper in his hands.
I greeted him with a wave of my hand and a smile, and
turned to Yal, but I had to look up so much that I felt
dizzy, so I lowered my eyes again, and seeing that I was
free to move, I wobbled to my pallet.

—“Bah, leave him be, son,” Rolg added; “it’s Day of
Joys.”

—“Day of Joys!” Yal repeated, bewildered, as I lay
humming. “When I was his age, I didn’t do that sort of
thing!”

—“Times are changing, son—”

—“Yeah, sure! As if they change in just six years. You
know what? I think my sari has learned enough from the
Cats for now. What I’m going to do now is find him a job
where he doesn’t wander off. I don’t want him to end up
like some people I know who are Black Daggers in name
only. Do you hear me, Draen? Draen!”

I opened one eye and saw him crouching beside me,
watching me with an expression of… concern? Joy? Fear? I
couldn’t tell. I smiled and said:

—“Yes, yes, Elassar.”

—“And you better work hard,” Yal continued.

—“Yes, yes. Say, Yal!” I exclaimed suddenly. “How is the
dip-hip-diploma thing going? Eh?”

—“Uh… I’m taking the exams in four days,” Yalet replied.
“And now sleep, sari. Tonight there will be no lesson,
obviously. I don’t teach my art to drunks.”

I yawned and was about to say a “yes, yes,” again, but
before I could even open my mouth, I fell into a sleep
like a log.

I helped my master to graduate
==============================

A week later I was on my way with Yalet to the Harp
Quarter. My master wanted to place me with a wealthy
family as a page in exchange for meals and domestic
experience. To do this, he had me wash my face, cut my
hair and put on a new, white shirt. I followed him
cheerfully, though a little reluctantly, for it broke my
heart to have to leave Manras and Dil to serve the
nail-pinchers. Yerris would have called me a suck-up.

—“Elassar,” I said, as we walked up a pretty cobblestone
street. I trotted to catch up with him. “You really
don’t know when Yerris is coming back?”

—“No,” Yal answered without stopping. “Alvon left
Estergat with him, that’s all I know.”

I sighed sadly.

—“And am I really going to have to work in a house?”

Yal gave me a half-annoyed, half-mocking look.

—“Come on, cheer up, Mor-eldal. Think that I am doing
this for your own good. You’ll learn good manners,
you’ll see a different world from the Cats and those
newsboys, and in short, it will do you a lot of good.”

I pouted skeptically and observed:

—“That is, if they’ll take me.”

Yal sighed.

—“Indeed, if they’ll take you. I warn you, if you do
anything stupid now, I’ll make you read *The Ways of
Twentyberries* ten times.”

I opened my eyes wide in horror. To teach me how to read
Drionsan modern signs, Yal had brought me a book to the
Peak which he had borrowed from the Elms Library. And I
was moving at a snail’s pace. It would have taken me ten
years to read it ten times!

—“Yikes…” I swallowed. “Not that, Elassar. Don’t make me
do that.”

—“Well, then, behave yourself.”

We came to the end of the street, and my master went
straight on to a red mansion—all the houses in this
neighborhood were huge. He pulled the doorbell, and as
it rang, he turned to me and said,

—“By the way, remember we’re cousins, right?”

I nodded and widened my eyes slightly as the door opened
and a dark-haired man dressed in black appeared. He
looked like a raven. But it was a human. He had a
pointed face, slanting eyes, and a gloomy expression
that did not please me.

—“Good morning,” Yalet said. He touched the brim of his
top hat. My master was dressed like a real gentleman. “I
am Yalet Ferpades. A friend told me that this house was
looking for a boy to work as a page, and I’d like to—”

—“Absolutely not, no one is needed here,” the
raven-faced man interrupted.

And he shut the door in our faces. I huffed.

—“Raven face,” I growled.

Yal gave me a warning look, and then he went down the
stoop and growled, too.

—“Damn it, did I get the wrong house?” He glanced at the
number, shook his head, and repeated, “Damn it.”

—“Selling newspapers is not bad either, Elassar,” I
interjected. “If I went there right away, I’d have time
to get the afternoon ones and…”

I sighed at his exasperated look. He gestured for me to
follow him and commented in a low voice:

—“Don’t call me Elassar in the middle of the day, okay?”

—“You called me Mor-eldal,” I said.

Yal rolled his eyes and nodded with a slight amused
smile, before adopting a more serious expression.

—“Listen, Draen. I only want you to learn something
other than to wander the streets and sing like a
leprechaun.”

—“What’s wrong with that?” I replied. “Besides, that’s
not all I’m learning. You’re teaching me a lot of things
too. Magic locks, the Twentyberries guy, harmonies—”

—“Keep your voice down, will you?” Yal gasped.

I put on an apologetic face.

—“Sorry.”

Yal sighed and was about to say something when we both
heard a voice calling:

—“Mr. Ferpades!”

We turned and saw a young elf leaping down the stoop of
the red mansion. He was wearing a loose white shirt even
whiter than mine. The way he panted, it looked like he
had run to the door.

—“Bingo,” Yal muttered. He smiled at me and pushed me by
the shoulder toward the stoop. I approached without much
enthusiasm.

—“Mr. Ferpades,” the young nobleman repeated. “I am
Miroki Fal. I regret this misunderstanding, I had not
warned the butler of the announcement I had made. In
fact, I didn’t think anyone would come so soon. That’s
the boy, isn’t it? How old is he?”

—“Ten years,” Yal replied. He said it without a moment’s
hesitation, and I almost believed him. Well, it was most
likely true: my nakrus master had told me that my
birthday was in late spring, and we were already in
autumn. Yal continued, “His name is Draen, he’s my
cousin and… so far, he doesn’t have much experience, but
he’s a good boy, and at least, he has a good
disposition, and he knows how to obey.”

He said that too without hesitation, but this time, I
did not believe him. Good disposition? More like, I was
willing to do whatever I wanted, what the hell. I raised
my head in pride. Perhaps misinterpreting my gesture,
the nail-pinching elf smiled.

—“Good. Well, look, I’ll take him on probation for a few
days, and, if he satisfies me, he can stay more.”

—“You’re… going to hire him?” Yal said. This time he
didn’t seem to believe it himself, I noticed with a
small smile.

—“I will, if I am satisfied with his work,” the young
nail-pincher repeated.

—“Ah. Well, fine,” Yal said cheerfully. “Say, I only
wish to make one condition. Accommodation will not be
possible. He already has a house, and his grandfather
would rather he go home to sleep. I hope that’s not a
problem.”

—“Not at all,” Miroki Fal assured. “As long as he
arrives on time in the morning. I assure you that I
shall release him every day before eight o’clock at
night, probably much earlier. Good day, Mr. Ferpades.
Draen, let’s go in.”

I glanced apprehensively at my master, but his
expression invited me to obey the nail-pincher, and
uneasy and nervous as a rabbit entering the wolf’s den,
I crossed the threshold under the indifferent gaze of
the Raven and the smiling eyes of the Nail-pincher. The
latter led me upstairs, and sooner than I could have
imagined, my apprehension turned to curiosity. This
house was unimaginable. There were such strange objects!
My nakrus master, who said that he did not want me to
fill the cave with useless things, like pretty stones or
carved sticks, what would he have said about this house?

—“Sir,” I said, as the Nail-pincher led me down a wide
corridor. This one had three doors on each side, all
closed. I said, “Sir! What’s that?”

I pointed to a large golden object of incomprehensible
shape. Miroki Fal cleared his throat.

—“This is a piece of art my father bought years ago. My
mother wouldn’t take it to Griada when my family moved,
and by all means, how I understand her! It’s a horror.
Truthfully, I have no idea what it is.”

I arched an eyebrow. Damn.

—“What about these people?” I inquired, pointing to
large paintings hanging all along the hallway.
“Ancestors?”

—“A few,” Miroki Fal nodded, stopping in front of a door
and pulling out a key. “This house is my uncle’s, so
there are plenty of paintings of his children and
grandchildren. But there are also religious works that
represent illustrious spirits, and modern works. Look at
this one, my best friend painted it. He sold it to me
for a mere two hundred siatos last spring, but it’s the
painting I love the most.”

The picture was strange, completely black with silver
things that looked like spider webs. I looked puzzled.

—“But it’s not painted,” I said.

Miroki Fal was speechless for a second and then burst
out laughing.

—“Of course it’s painted! It’s art, boy. Don’t worry,
you can’t understand it.” *Oh, I’m not worried*, I
thought, but I didn’t say anything. He opened his office
and went in, adding, “I suppose your cousin understood
the terms of the announcement: in return for your work
as a page, I’ll give you food, lodging—but I see that
won’t be necessary—and maybe a tip.”

As he closed the door, I nodded and looked around. I saw
shelves with books and books and more books and figures
and vases and very high white curtains. I thought I had
gone to another world. For me, who had seen a total of
four books in my life, all this… was an enormity.

—“Whoa,” I blurted out. “Are they all real?” I asked as
I approached the books.

—“Uh… yeah, don’t touch them, okay? Just sit down. I’ll
explain quickly what you’ll have to do: it’s very
simple.”

Dazzled, I sat down on a stately chair. My feet were
nowhere near the floor. I looked intently at Miroki Fal,
sitting behind the desk. What could this Nail-pincher
want?

—“Well,” the young elf continued, “I don’t know if you
know what the Conservatory is.”

I frowned and nodded.

—“The school for magicians, right?”

Miroki Fal nodded.

—“That’s right. I’m studying there, and it turns out I
haven’t had a page for four moons. The previous one ran
away. And… well, I definitely need a page. You’ll have
to carry my notes, run errands, send messages… Nothing
very complicated. In the afternoon, I don’t have any
classes, so you’ll be helping Rux with dinner, cleaning
the house, and whatever else he asks of you. Got it?”

I blinked. Mothers of the Light… all that?

—“Who’s Rux?” I asked.

—“The butler, the one you saw earlier,” the elf
explained. I grimaced, and he smiled, “Don’t worry, he
won’t bite.”

As long as he really didn’t… I swallowed.

—“Sir? Why did the other page run away?”

This time, it was he who grimaced.

—“Well… The Conservatory sometimes holds surprises.
There was a little accident during an experiment, the
boy was scared to death, and the next day, I didn’t see
him again.” He shook his head and rose from his chair
under my apprehensive gaze. “That’s why I strictly
forbid you to enter the classrooms. And if I tell you
that you can go, you go, leave the Conservatory, and
come back at the appointed time, all right?”

I huffed. That was good news.

—“It runs, it runs,” I said, hopeful. “How many free
hours is that?”

The Nail-pincher looked at me with a mixture of surprise
and amusement.

—“Well… It depends on my schedule. But, when I say
you’re getting out of the Conservatory, it’s to come
here and help Rux out.”

I looked at him, aghast.

—“Really?”

Miroki hesitated and cleared his throat.

—“Well… I guess Rux can take care of himself in the
morning.”

I smiled broadly at him.

—“So I can go wherever I want? Right, sir? Thank you,
sir, that so kind of you!”

The young elf rolled his eyes.

—“I’m one of those people who thinks that even poor
people need time off. So yes, you can go play with your
friends, as long as you come back on time without being
late,” he insisted. “If you do, even once, our deal
falls through, understand?”

I nodded vigorously without a word, and he smiled,
frowned, and added:

—“One last thing. If I see one thing missing from this
house, I’ll blame you. So be careful what you touch.” I
nodded again, and he thundered, “Rux!”

The butler was late in arriving; he didn’t limp like
Rolg, but he walked with great composure.

—“Yes, Mr. Fal?” he asked in a dry voice.

—“Draen will be your assistant,” Miroki Fal declared.
“Please show him his new duties and see that tomorrow,
at eight o’clock, he is outside the door and ready to
go.”

Rux the Raven nodded briefly, and not without a certain
reluctance, I got up and followed him to the corridor
and then down the stairs. I did not trust this fellow.
Without a word he led me to the large main hall and
pointed to a closed door.

—“There, that’s the pantry. You must not open it,” he
warned me. He moved away to an open door. “That’s the
kitchen. You don’t go in there. I’m the one who prepares
the meals, understand?”

He stared at me so hard that I did not answer, and I
stood rooted to the spot, intimidated. He frowned.

—“I’m talking to you. Are you listening?”

I snapped out of it.

—“Yeah, yeah. I’m not going into the kitchen or the
pantry. I understand you ragingly,” I assured.

Rux frowned even more.

—“Good,” he said. “Then take this broom and clean the
upstairs hallway. Without touching anything or opening
any doors.”

I jumped for joy. Finally, a chance to get away from the
raven!

—“There I go,” I said. I took the broom and ran up the
stairs. I decided to start from the back, where there
was a huge and beautiful window. But, on the way, I
stopped to look at the pictures. There were all sorts,
portraits of moustachioed gentlemen and ladies in wacky
hats, landscapes with pretty girls dressed all in white…
as well as the blackboard of spider webs. I looked at
the latter with curiosity. Why did the Nail-pincher like
this unpainted picture so much? After glancing at the
closed office door, I reached out with my right hand and
touched the black surface. I felt a strange energy and
jerked away. An enchanted painting! If Rux had warned me
not to touch, it wasn’t for nothing. Fortunately, my
right hand was relatively impervious to external spells.

I moved away quickly, shook my hand to get rid of the
unpleasant feeling, and began to sweep vigorously as I
glanced out of the window. From there, I could see the
Conservatory, a large castle full of windows, with walls
as black as the Rock.

—“A stronghold,” I murmured.

I remembered that in the book of stories with pictures
of my nakrus master, there was a drawing of this kind
with the word: stronghold. There lived a little
princess, alone and distraught…

—“But she was brave, and one spring day she set out on
an adventure with, in her heart, a sun of joy,” I
whispered, continuing the tale. I knew it by heart: my
nakrus master had read it to me many times, as many
times as I had read it on my own. It was quite
frustrating to see that the writing then was not at all
the same now.

I sighed, and realizing that I had stopped sweeping, I
went on to sing:

Trataratrata!\
As one sweeps one sweeps,\
one sweeps as one sweeps,\
I sweep and you sweep.\
As we sweep, we sweep!\
Trataratrata!


I continued on and had already reached the end of the
corridor when the office door opened and the
Nail-pincher appeared.

—“Little boy!” he said to me.

I stopped, fell silent, and looked at him questioningly.

—“Uh… Here, you can sing all you want, but outside and
at the Conservatory, don’t do that. I don’t want you to
stick out, okay?”

I shrugged.

—“It runs, sir!”

And I threw all the dirt that had gathered on the first
step, moved to the next and continued to throw all the
dirt while humming my song. When I got downstairs, I
looked around the kitchen and saw Rux sitting at a table
cutting something with a knife. He looked up and, to my
surprise, he smiled at me. At that moment, his smile
scared the hell out of me, especially because with the
knife in his hand, the vibes he gave off were, how do I
put it, a little disturbing. However, after thinking
about it, I told myself that his smile, although a
little sinister, was not bad.

And I wasn’t wrong. I spent a good two hours singing
while sweeping and dusting, and despite what he said
before, Rux let me into the kitchen to help him wash the
plates and even shared with me the leftover soup Miroki
Fal had left the night before. As we ate, sitting at the
small kitchen table, I enthused:

—“So yummy! What is it?”

—“Mmph,” Rux said. “Veggies, meat… There’s a lot of
ingredients, like in all soups. And don’t talk with your
mouth full.”

I closed my mouth, and after a silence, finished my
bowl, and asked:

—“What are all these instruments for?”

I pointed to a pile of pots and ladles of various sizes.
Rux let out another: Mmph. And after a silence during
which I looked at him, waiting for his answer, he
explained:

—“They are pans, saucepans, and cooking pots… Have you
never seen a pan?”

I wasn’t sure, so I shook my head. Rux said again: Mmph.
And he added a:

—“Whatever, clean the bowls.”

He handed me his, and I got up to clean them. In the
end, Rux maybe was not all that bad, but he was as
expressive as Dil, or even less so.

—“When you’re done, you can go,” Rux said.

I opened my eyes wide at such good news and nearly
dropped the bowl.

—“Come back tomorrow at eight o’clock on the dot,” Rux
added. “If you come late, Mr. Fal will wring your ears
off.”

I left the bowls neatly on the table and gave Rux a wide
smile.

—“I’ll be here at eight o’clock!” I assured him.

And I ran off. Just before I left the kitchen, I thought
I saw another slight amused smile from the butler.

I went down and down the streets to the Esplanade. It
was barely three o’clock in the afternoon. I went to the
office and from there to the places where we, the
singing duo, and Little Prince used to sell. After an
hour of wandering, I finally found my companions and
shouted to them:

—“Ayo, cronies!”

Seeing me, the little dark elf smiled and came running
up, followed by Dil.

—“Where have you been?”

—“I have found a new job,” I explained. “In the Harp
Quarter.”

Manras gaped at me.

—“For real? With the princes?”

I nodded.

—“With a magician who studies at the Conservatory.”

—“Mothers of the Light!” Manras gasped, impressed.

I smiled.

—“Yes, but, don’t worry, as soon as I leave the
Nail-pincher at the Conservatory, I can go wherever I
want—if he tells me I can, of course. So, as soon as I
can, I’ll come with you. How are the sales?”

—“Wind in the sails,” Manras assured. “But what are you
going to do now if you don’t have any papers?”

—“Sell Dil’s!” I replied. “Anyway, you’ve always got
some left over,” I said to Little Prince. “Give me some.
Then I’ll give you half the nails, because I’ve already
eaten at the magician’s, what do you say?”

Of course, Little Prince thought that was fine. As long
as it wasn’t about getting noticed or doing bad tricks,
everything seemed fine to him. So I spent the last few
hours of the afternoon with them, had half a snack of
cheese at *The Wind Rose*, and chewing the last bite,
trotted straight to the Blind Alley. After making sure
no one was passing by on the adjoining street, I
concentrated, joined my jaypu to my surroundings, and
wrapped myself in harmonic shadows. Honestly, I was
doing quite well—I smiled. I grabbed the gutter without
looking at it, climbed up and landed on the roof in
silence. I crossed it and continued to climb, still
clinging to the same projections, with the speed that
comes with habit.

Finally, I climbed over the wall of the terrace and
reached the summit. It was not yet completely dark, few
streetlights were lit, and the stars were visible in the
sky. They were not always visible. So I lay down on my
back to see them in all their splendour. It was a dark
moon that night, but a crescent Gem was already pointing
up above the tallest sharp needles of the Rock. I raised
an index finger and hid the Gem so that I could see only
a ring of blue light around it. Then I let my hand fall
back, yawned, and heard a:

—“How was the first day on the job? Tough?”

I turned my head and saw the figure of Yal appear above
the terrace wall.

—“Elassar!” I said, sitting up. “Do you know that the
Nail-pincher is going to take me to the Conservatory?”

Yal had just sat down, leaning against the wall, and he
brusquely looked away from the stars.

—“What did you say?” he snorted.

—“I’m telling you. He’s a student magician, and he wants
me to run errands for him there,” I explained.

There was silence. Yal coughed slightly.

—“The announcement didn’t say anything of the sort,” he
growled. “I understand now why he didn’t take a noble
servant…”

—“Is the Conservatory really dangerous?” I inquired.

Yal crossed his legs, clearing his throat.

—“It isn’t if you stay away from their experiments.
Surely your former master warned you of the dangers of
the celmist arts.”

—“For yourself, yes, not for others,” I said, perplexed.

—“Well, they can be dangerous,” Yalet asserted. “Alchemy
in particular. This spring, without looking any further,
shortly before you arrived, there was an explosion in a
whole wing of the Conservatory. You can’t imagine the
smoke that came out, dense and all green; you could see
it from the Cat Quarter. Several people were severely
intoxicated. Hm. Reassure me, this Miroki Fal is not an
alchemist, is he?” he worried.

I shook my head in concern.

—“I don’t know, he didn’t tell me. But he said I’m not
allowed into the classrooms.”

—“And you better listen to him,” Yal replied, and he
said more cheerfully, “By the way, by the way, did you
know that I graduated?”

I took a deep breath of air.

—“Wow! That’s great!”

Yal nodded, looking thoughtful.

—“I’ve studied harder than a mage, and the spirits know
I deserve this degree. If it weren’t for Korther, I
wouldn’t have come in half asleep to the exam… Anyway,”
he cleared his throat and looked up. “Let’s get to work,
sari. Tell me, what did we learn yesterday?”

—“Picklocks and traps and picklocks,” I recited with a
theatrically annoyed pout, then smiled. “And harmonies!”

—“Precisely,” Yal said, rising to his feet. “Today we’re
going to put them into real practice. How does that
sound?”

I looked at him, astonished, and jumped to my feet in
excitement.

—“Are we going to steal anything valuable?”

—“No, tonight we are not thieves: we are ghosts,” my
master smiled.

And, with agility, he began to descend from the Peak. I
followed him.

—“Be careful where you step,” he said, when he saw me
land next to him on a roof.

—“I am very careful,” I assured him.

—“A false step at this height means death,” he replied,
very seriously.

I sighed, because he was starting to repeat those words
as often as my nakrus master did his stories of grumpy
skeletons.

—“Yes, Elassar.”

We did not land in the Blind Alley, but in a different
place, and as soon as I set foot on the ground, Yal
moved away. I had to run to catch up with him.

—“Where are we going?” I asked.

—“Follow me and you’ll see.”

Instead of going downhill, we went up. Soon, we came to
the wide stairs which marked the end of the Cats and the
beginning of Atuerzo. We crossed the Stone Park, and
when I saw my master crouching behind a shrub, I did the
same.

—“A night guard,” Yal explained in a low voice.

A few moments later, I saw the said guard pass with his
lantern through the dark park. With his other hand, he
was smoking a pipe. He stopped for a moment by a bench
to light it again, and then he went on. As soon as he
was gone Yal got up and crossed the street to a large
building. I frowned and asked:

—“What about this place?”

—“The Elm School,” Yal answered in a whisper. “Except
for the janitor, everything is empty. Over here. Use the
harmonies.”

He climbed quickly over the gate. I smiled and followed
him, excited to visit the place where Yal had studied
for three years. We walked through a cobblestone
courtyard, shrouded in harmonic shadows. Yal opened the
first door with a key, and once inside, he whispered to
me with amusement.

—“I made a copy using wax. Come on.”

We walked down a corridor full of doors, but we did not
open any of them, and Yal led me directly up the stairs
to the third floor. Knowing exactly where he was going,
my master stopped in front of a door and reached for the
handle without touching it.

—“Tell me if there is a trap,” he asked.

I shrugged and placed my right hand on the door. I heard
my master gasp; he still hadn’t gotten used to the fact
that the magic theft traps didn’t detect my hand as an
intruder. I felt an energy and nodded.

—“There is.”

It made sense—otherwise, Yal wouldn’t have asked me to
look for one.

—“Well, turn it off,” he invited me.

I concentrated, examined the pattern of the trap, and
recognized it as one of those which Yal had shown me. I
quickly located the detonator, broke the bonds around it
and… I stopped.

—“Should I turn it off or undo it?”

Yal laughed under his breath.

—“You deactivate it, sari; deactivating it is much more
professional. When we get out, you turn it back on, so
no one will know anyone’s been there.”

—“Well, it’s done, then,” I informed him.

He checked it by turning the handle, and I saw him so
calm that I asked:

—“It’s okay if we get caught, right?”

—“Uh… No, sari, it’s not okay. But we won’t be caught,
don’t worry, I know this place by heart.”

I entered behind him, and the smell of paper overwhelmed
me. The light from the Gem gently illuminated the
interior, and I stifled a cry of surprise.

—“There are more books here than at the Nail-pincher!”

—“Speak lower, Mor-eldal,” Yal growled.

—“Sorry, sorry,” I murmured as he moved away between the
shelves.

I ran to the back, then back by another shelf, and as
Yal had not told me not to touch, I ran my hand over the
spine of each book. After browsing for a while, I saw my
master with a book open and a harmonic light on, and I
went over.

—“Elassar,” I murmured. “Are they all written?”

Yal’s smile appeared in all its splendor.

—“The books?” I nodded. “Of course, sari. In libraries,
there are only written books.”

I stood on tiptoe to see which book he was looking at.

—“What does this one tell?”

—“History stuff,” Yal replied. And he closed it and put
it back on the shelf. I heard him mutter, “Four thousand
three hundred and sixty-eight.”

He consulted another book and muttered:

—“Satranin. Four thousand three hundred and
sixty-eight.”

I looked at him, puzzled.

—“Satranin? What’s that?”

—“A white powder, a powerful sedative,” Yal replied
absentmindedly. And he put the book back in its place
before heading for the door. “Come, sari, and be quiet.”

I followed him, growing more and more disconcerted. I
reactivated the trap on the library door, and this time,
we went down the stairs to the second floor. Yal looked
at a massive door that was different from the others,
and then he waved his hand at me and whispered:

—“You turn it off, you do it very well.”

This time, I had more trouble, because the pattern was
not one that Yal had taught me, but I managed to
deactivate the trap, and Yal put a caring hand on my
cap.

—“I can already see you sleeping under golden drapes a
year from now, sari.”

I rolled my eyes and followed him inside. This time, we
found no books, but mountains of files and papers laid
out on several desks. Yal quickly went through several
piles, and suddenly picking one up, he sat down in a
chair with a sigh.

—“I taught you to read the name Yalet, didn’t I?” he
whispered to me. “Well, take this.”

He gave me a third of the pile, and, growing more and
more astonished, I cast a spell of light, but it went
out almost immediately. I concentrated and cast it
again. What I saw on the sheets made me frown. It was a
printed form with things written on it by hand. I tried
to read the first line at the top, written in large
letters:

—“Ex… Exam… of… theontia?”

I shook my head, and Yal helped me:

—“Theology, sari.”

—“Oh! Of course.” I squinted and cast another light
spell. I still hadn’t quite mastered it. I was about to
continue reading when Yal held up three joined sheets
like a trophy and exclaimed in a whisper:

—“I’ve got ya! Look no further, sari, I have found
them.”

Then he took out a lot of equipment, including a quill
pen, dipped it into the inkwell, and very
conscientiously began to write. I heard him murmur: Four
thousand three hundred and sixty-eight. Satranin. And I
don’t know what else.

—“Done,” he smiled. He renewed his harmonic light, which
was beginning to fade, and dried the ink on his paper. I
looked at him, dumbfounded. He put the inkwell away, put
the stack back in its place, and we left the office as
stealthily as we had come in. Still assimilating what my
master had done, I reactivated the trap, Yal made sure
that I had done it correctly, and once in the courtyard,
we passed over the gate again and were soon on our way
back to the Cat Quarter. After a long silence, I huffed.

—“Elassar… I thought you said you graduated.”

Yalet gave me a mocking look.

—“Didn’t I?”

I smiled and laughed.

—“You ragingly did!”

Yal huffed, amused, put an arm around my shoulders and
said:

—“And you too, sari, you did even better than I
expected. In the end, you may be right when you say you
pay more attention than an owl.”

My smile widened, and he added, more seriously:

—“Look, I don’t want you to think I’m an hardcore
cheater. I did it for a reason. You see, Korther asked
me to go see him the night before the exam. I never
really had a particular mentor, but… he taught me how to
use harmonies and… well, I couldn’t not go. He’s the
kap. The problem is, because of him, I barely slept all
night. Otherwise, I would have passed the exam without
cheating, believe me.”

I nodded, letting him know that his reasons seemed to me
more than legitimate. He smiled and patted me on the
shoulder.

—“I’ll walk you to the Den.”

You may find slugboneheads among nail-pinchers and gwaks
========================================================

The next day, I arrived at the Red Mansion before eight
o’clock and had to wait in the living room until half
past eight before the Nail-pincher was ready for class.
He greeted me with a smile, held out his bag with his
notes and other belongings, and said:

—“Don’t leave my side.”

I did not. The Conservatory was really close to his
house, and we only had to walk a few minutes before we
reached the main door. This one was huge.

—“It looks like the mouth of a monster,” I blurted out.

Miroki Fal glanced at me, his eyebrows arched, but he
did not answer, he greeted the janitor, and we entered
the wizard school. There, we walked longer, and we
passed through many staircases and corridors, coming
across people from time to time. Sometimes, the
Nail-pincher waved, other times he did not. Finally, he
greeted another elf with brown hair who was waiting at a
door.

—“Good morning, my friend!” he said to him in a pompous
tone. “I am not late, I hope?”

—“Quite late, but so is our teacher, as usual,” his
friend replied, smiling. His eyes landed on me. “Is that
your new assistant? What a look! Where did you find him?
In a hospice?”

—“Not at all, his cousin brought him to me. And, for the
moment, I am satisfied with him,” Miroki Fal assured.

—“A human,” his friend observed. “And with copper skin,
at that.”

—“Shudi,” the young nobleman snorted, “what have you got
against humans and coppers?”

Shudi shrugged, tauntingly, and at that moment, the door
opened, and there appeared the one who, without a doubt,
must have been the teacher. He was a human. But he
wasn’t copper-skinned: he was blond, tall, thin, and
quite young.

—“Hello, Professor,” the two elves greeted.

As they were about to enter, I gave the bag to the
Nail-pincher and asked him in a low voice:

—“Hey, mister, what’s a hospice?”

Miroki Fal looked at me in amazement.

—“A hospice, you ask? It’s a place where children
without families live. Still, not knowing that at your
age… Are you sure you’re ten, kid?”

I shrugged; he handed me a piece of paper and said:

—“Here, it’s a message. Go to the Endarsic Library at
gate fifty-six and give it to Miss Lesabeth. She’s a
lady elf with curly blonde hair and blue eyes, you can’t
go wrong: she’s one of a kind. As soon as you give it to
her, you come back here and wait, understand?”

I nodded silently and watched the classroom door close
with a mixture of disappointment and curiosity;
disappointment because the Nail-pincher was not going to
give me the morning off, and curiosity because I had an
entire castle to explore.

Fifty-six, I thought as I walked away down the hall. I
was not likely to forget the number, for more than once
I had heard my Master swear by the fifty-six phalanges
of his hands and feet. And, fortunately, the numbers
were still written as my master had taught them to me,
so, I imagined that I would have no trouble finding the
door. I was wrong. This place reminded me of the
Labyrinth. It was not muddy, but otherwise, it was much
the same.

I wandered for some time through the deserted stone
corridors until I found number twenty. From there I came
to number twenty-nine and went straight to two hundred
and three. I stopped dead in my tracks, bewildered.

—“What’s up with this place?” I said.

I turned around. And I found myself facing a young
half-elven wizard who was passing by. He had one green
eye and the other black. As he was about to pass by
without barely glancing at me, I called out to him:

—“Sir! Where is the fifty-sixth door?”

The wizard slowed down, but he didn’t stop, and I had to
walk beside him as he replied:

—“It’s on the other side of the Conservatory, in another
wing. Are you looking for someone?”

—“Yes,” I said. “Miss Lesabeth. The Nail-pin—er—I mean,
Mr. Fal asked me to give her a message.”

This time, the young magician stopped, and his different
colored eyes troubled me as I met them.

—“Lesabeth?” I saw him make an amused pout. “Well then…
You’re Miroki Fal’s messenger? Interesting. May I take a
look?” he said, reaching a casual hand towards the
letter.

I frowned, backing away, but he snatched the paper from
my hand.

—“Hey!” I protested.

—“Hands off!” the wizard growled, pushing the letter out
of my hands. “I just want to take a look at it. I’m
Jarey Edans, a friend of his.”

He unfolded the paper, read it, and I saw a smile light
up his face. A smile that did not please me.

—“Hey, give me that back,” I said. I took the paper, but
he did not let go, and I glared at him. “Let it go!”

He finally did and growled at me:

—“Speak to me with more respect, brat!”

He gave me a slap on the neck, and I ran off. At the end
of the hall, I turned around and said:

—“Slugbonehead!”

I turned the corner and ran out of sight. Surely this
Jarey Edans did not understand the insult. Only my
nakrus master used it against vultures or lynxes that
came too close to the cave.

When I found the Endarsic Library and entered, the first
thing I saw was the clock hanging in the entrance. It
showed twenty to eleven. I bit my tongue, and looking at
the tables and books, I went forward and…

—“Kid, where are you going?” A small man with binoculars
stopped me with his hand. “This is the Endarsic Library,
you can’t just walk in. What are you doing here?”

I explained that I had to deliver a message and added in
a plaintive tone:

—“I got lost in the hallways. It’s very complicated, and
the numbers are not in order, and…”

—“I know,” the little man sympathized with a smile.
“It’s not easy for newcomers, nor for the seniors
sometimes, believe me. I’m sorry to tell you that Miss
Lesabeth is no longer here, she left just a few minutes
ago. I heard her and her friends say that they were
going out to the park outside. If you hurry, you may
catch up to them.”

I thanked him and ran down the corridor he pointed out.
I went down all the stairs I could and finally came to
the bottom floor. I went out the main door with some
relief, but I barely had time to catch my breath
because, at that moment, I saw the blonde elf with a
group of girls. She was walking away into the park that
surrounded the Conservatory. I rushed forward before she
could slipped away from me and shouted:

—“Miss Lesabeth! Miss Lesabeth!” I saw her turn around,
and her identity thus confirmed, I ran to her and held
out the message, explaining, “Miroki Fal gave me a
message for you.”

Lesabeth pouted and glanced at her friends before taking
the message. She unfolded it and blushed a little. And
as her friends tried to read over her shoulder, she
abruptly folded the message and snorted.

—“Nonsense,” she said. And without further ado, she tore
the message into four pieces, threw it away, turned her
back on me, and went off with her friends.

—“What did he say, what did he say?” one asked.

—“Bah, nothing, as usual, a poem full of pretty
nonsense,” Lesabeth answered.

I watched her walk away, my eyes wide with indignation.
Two hours wandering the halls of this wizard jungle, and
for what? To see that witch throw away my message?

—“Witch,” I muttered.

And I stooped to pick up the four pieces of the torn
message. I put it back together on the grass and tried
to read it. It took me a long time to decipher the first
two lines, but when I did, I was amazed. It said, *‘Oh
beautiful soul for whom I long, for you my heart loves
and raves.’*

“Pretty nonsense,” she had said? It sounded like a love
song! And the blonde elf was treating such warm verses
like this? I couldn’t understand such absurdity.

—“Draen!” a voice suddenly cried behind me. “When I told
you to wait, I thought you were going to stay in the
hallway, not outside. Well, it doesn’t matter. Did you
give Lesabeth the message?”

I stood up and discreetly slipped the pieces of paper
into my shirt before turning around and answering:

—“Yes, yes, sir, I gave it to her.”

The Nail-pincher was accompanied by his friend, Shudi.
At my answer, he looked pleased and hesitated.

—“And what did she say?”

I winced and swallowed a little.

—“Uh… Well… she was like, ‘oh goodness’.”

—“Oh goodness?” Miroki Fal gasped.

—“Yes, that’s what she said: oh goodness,” I said,
clearing my throat.

The two elves smiled at my expression, and Miroki Fal
rejoiced:

—“See, Shudi? I’m making progress.”

The dark elf rolled his eyes, and I followed them home
as they talked about whatever show was being put on at
*The Emerald* that night. Miroki had invited his friend
to eat at his house, and Rux let me carry the plates and
food to them, making me promise not to drop anything
first. Back in the kitchen, I took advantage of a moment
when Rux was away to throw the four pieces of the
message into the fire. And, as I passed, I touched the
burning metal plate with the finger of my right hand to
see what would happen. Nothing: I withdrew it, and the
hand was still intact. I only felt the raw burning
energy fade away as soon as I broke contact. I smiled.
My nakrus master was definitely an expert magara maker.

—“Hey, kid,” Rux said to me as he came back to the
kitchen. “Mr. Fal wants to talk to you.”

I entered the living room; sitting casually in one of
the armchairs, Miroki Fal said to me:

—“Second errand, kid. Go buy a jar of blue ink at
*Rochinel’s Shop*, it’s in the Grand Gallery, do you
know where it is?”

—“Yes, sir,” I said, “the newspapers there sell like
hotcakes.”

—“All right, then, go ahead with that paper and tell the
clerk to put the ink on my account. Hurry up!”

I went out, ran down the street and arrived at
*Rochinel’s Shop*, panting.

—“S-Sir,” I panted to the redhead behind his counter. “I
want a jar of green ink.”

I put down Miroki Fal’s paper, took the jar and… stopped
at the door.

—“Ah, no, wait, it wasn’t green ink, it was…” I
pondered. “Damn, was it red? Black?”

I couldn’t remember.

—“We’ve got a lot of colors, lad,” the shopkeeper said
with a clearing of his throat. “You’d better go and ask
and come back when you’re sure.”

—“Right,” I agreed. “Be right back!”

And forgetting to return the green ink pot, I ran off
again up the street. When I told him about my memory
lapse, Miroki Fal looked at me with an exasperated
expression.

—“I said, blue.”

—“Blue!” I exclaimed. “Of course!”

And I ran back to the shop. I had almost reached the
Grand Gallery when a cart nearly ran me over, I leapt
sideways in a panic, fell to the ground, and the pot
flew out of my hands. At that moment, I heard the sound
of breaking glass and a roar.

—“By all the spirits and devils and everything in
between!”

Coincidentally, the young man who had just screamed was
none other than Warok, the Ojisary who worked for the
Black Hawk. His face and shirt were stained with green
ink because the jar had crashed into the wall behind
him. I let out a loud laugh, the dark elf saw me, and
scrambling to my feet, I ran into the Gallery. I stormed
into the shop.

—“Blue ink!” I said, out of breath, to the shopkeeper.
“Blue,” I repeated, catching my breath.

—“And I suppose Mr. Fal wanted the other jar too?” he
asked, handing me the pot of blue ink.

Ahem… I nodded silently and said:

—“Thank you, sir. Ayo.”

And I left, taking the other exit from the Grand
Gallery, in case Warok had a vindictive mind. The
problem was that in addition to having a vindictive
spirit, Warok could also use his brain. The damned man
was waiting for me outside. Without warning, he grabbed
me by the arm, and I shouted:

—“It was an accident! It was an accident!”

He looked daggers at me.

—“You’re going to pay for this, you little brat. You
tell me,” he said, shaking me. Fortunately, this time I
had put the ink pot in my pocket, or I would surely have
dropped it again. “Do you know where the Black Cat is?”

I shook my head.

—“I don’t.”

—“You lie,” Warok growled.

—“He’s gone with his mentor; I don’t know anything, I
swear. You’re hurting me,” I informed him with all the
dignity I could muster.

Warok glared at me.

—“I know exactly where you live, you know? And your life
is worth less than a grain of sand. You’ll pay me for
the ink. And you’ll pay a lot more if you lie.” He let
go of me roughly. “Hook it.”

I walked away massaging my arm, my eyes brimming with
tears. To think that this was only my first day in the
service of the Red Mansion’s Nail-pincher…

First impressions are the most lasting
======================================

Outside, it was windy, snowy, and as my nakrus master
would have said, you wouldn’t put a bone out in this
weather.

—“Your turn, Rolg,” I said to the old elf.

The old elf looked down at his cards, rubbing his chin
with his long fingernails, and Yalet yawned, leaning
back in his chair.

—“Hey, sari, you haven’t told us how your day went yet,”
he observed.

I pouted and huffed. Yal smiled.

—“More stories about Lesabeth and the Nail-pincher?”

I sighed.

—“If it was only that…”

—“Come on, tell us,” Yal encouraged me. “There’s nothing
like a love story to occupy the winter nights.”

I gave him a mocking look and said:

—“Love, you say. Lesabeth is a witch. I’ve already told
the Nail-pincher, but he doesn’t listen to me, he even
got angry. He likes to suffer. Today, Lesabeth told him
he was a hopeless case, a failed poet, and a fool, and
Miroki, instead of telling her she was an arrogant,
conceited witch, he told her,” and I intoned, raising my
hand as Miroki had done, “Oh, you graceful and cruel
butterfly who becomes more beautiful the more you fly
away.”

Yal laughed.

—“For the Spirits’ sake, he’s pretty much hooked on
her.”

—“Damn right,” I assured him. “But it’s horrible. Even
his friend Shudi tells him to leave her alone, that he’s
making a world of something not worthwhile. Bah, what a
mess. And that’s not the worst of it. Today Shudi told
me he wants to paint my portrait.”

Yal frowned.

—“Your portrait? Yours? How come?”

I shrugged.

—“He said it was original because he had never painted a
poor child. I told him that I was not poor at all, but
he gave me that will-you-shut-up look, and he started to
paint me. I just hope it won’t be like that blackboard
of cobwebs, ’cause it’s scary.”

Yal rolled his eyes, and as Rolg played his card, he
played his, and I looked at my cards and stuck out my
tongue.

—“I have no more kings left,” I informed.

—“You’re not supposed to say that out loud,” Rolg
remarked, amused.

Lost in thought, I sighed, threw down a card, and said:

—“But the portrait is not the worst thing either. The
worst is that, in fall, Miroki was giving me more free
time, and now he keeps making me run twenty thousand
errands. He’d even ask me to pick up flowers in the
middle of winter if he could. You know, Elassar? I’m
sick of magicians and nail-pinchers.”

Yal let out a muffled laugh.

—“It shows, it shows,” he assured.

I gave him a detached pout and put things into
perspective:

—“Anyway, I’m not doing that bad. Today, Rux taught me
how to hammer. We fixed a chair,” I explained with
pride.

—“So you’re going to be a carpenter now?” Yalet laughed.
“And how is that reading about crows and loves and
ghosts?”

I smiled.

—“It’s settled. I told Miroki Fal I didn’t like his
book, and he gave me another one. An adventure book. I
thought I couldn’t read anymore, but I read most of the
new book in one sitting. Spirits… To hell with Miroki
Fal’s ghosts…” I huffed, puffing out my cheeks.

Yal and Rolg laughed, and when I realized it was my turn
again, I grunted and spread my cards on the table.

—“Luck is not with me tonight,” I said.

They showed their cards too, and Rolg smiled.

—“Ah, for once I win. Well,” he said, rising slowly to
his feet. “Time goes on and on, and at my age, I am no
longer fit for long vigils. Good night, boys, and sweet
dreams.”

We both answered him, and as the old elf went to his
room, I picked up the cards.

—“You know, Elassar?” I said. “I didn’t tell you, but
I’m very glad you moved out and came to live at the
Den.”

Yal smiled.

—“You’ve told me that maybe ten times in the last five
moons, sari.”

I smiled, tapping the pile of cards to readjust them.
Outside, a long gust of wind shook the door in such a
way that I could have sworn it was the breath of a
dragon. When the door stopped shaking so much, I
unconsciously breathed out in relief.

—“Listen,” Yalet said suddenly. “We need to talk about…
something serious.”

I looked up, puzzled.

—“What is it, Elassar?”

—“Well…” Yal hesitated, shifted to the chair Rolg had
just left, sitting closer to me. “Look, Mor-eldal.
Korther has a job for us.”

I looked at him, incredulous and delighted. The kap of
the Black Daggers of Estergat had a job for us?

—“What job?”

—“But, in fact, to complete the first job, Korther wants
you to prove to him that you’re skilled enough to do it.
And he has given you a challenge. He wants you to go
into a wealthy house in Atuerzo and steal something
valuable. It’s like a test, to prove to him that he can
trust you. I told him you were a quick learner… I don’t
know if I should have told him so soon,” he admitted.

I shook my head.

—“Which house? Any house?”

—“No. A special one. I will guide you to it,” he
assured.

—“But when?” I asked, in suspense.

Yalet watched me carefully and replied:

—“Tomorrow.”

—“Tomorrow!” I repeated. And I smiled, not only because
I was eager to put into practice all that Yalet had
taught me, but also because I did not like to wait, and
it seemed that I would not have to wait long. “So,
tomorrow, I become a Black Dagger for good!”

Yal rolled his eyes and raised his hand, showing his
index finger and thumb.

—“Or at least a Black Pin.”

I looked theatrically offended and gave him a push.

—“Pin, your mother! I’m a Black Dagger,” I said.

—“And where is your dagger?” Yal teased. At my
dumbstruck expression, he patted my shoulder, looking
amused. “Don’t worry, you’ll get one someday, forged
from black steel by the Blacksmith himself. But not yet.
Just think that even I, who was an exemplary student,
only got it at fifteen.”

I gave him a mocking pout.

—“Exemplary student? Pfft, I’m sure you just got your
inkwell out and…”

I fell silent under his imperative gaze, and realized
that Rolg did not know of the Elms’ rigged examination.

—“Well,” I said. “So tomorrow I steal something? And how
will I know if it’s worth anything?”

Yalet rolled his eyes.

—“You’ll know, Mor-eldal. It’s all about instinct.”

I believed him, and as he turned off the lantern, I
slipped from my chair and we lay down. The wind was
blowing, and it came in through the small courtyard and
whistled through the grooves of the house. I pulled the
blanket up tightly and stirred. I was worried. Was it
because of this job for Korther? Probably. It was
because Yerris had told me that flies were friends of
the nail-pinchers and locked up thieves in Carnation
Prison. Not a very nice place, according to the Black
Cat. As if he had guessed my concern, my master squeezed
my shoulder briefly and said:

—“You’ll do fine, sari. I have faith in you.”

I smiled and nodded.

—“Good night, Elassar.”

—“Good night, sari,” he whispered.

Another gust of wind cracked the wood, and I shivered
and curled up in my blanket beside my master. He
inspired confidence and security. So did my nakrus
master. With that thought in mind, I fell asleep and
dreamed of squirrels running through the trees and a
child jumping from rock to rock, down a stream and
singing at the top of his lungs.


%%%


“Turn your head a little to the left, that’s it, that’s
it! And eyes straight ahead… Stop picking your nose, you
disgusting brat! And don’t roll your eyes, I said face
forward. Devils, stop fussing,” the painter exasperated.

Miroki Fal laughed.

—“If you want, I’ll tie him to a chair.”

I looked at the Nail-pincher apprehensively, then
realized he was joking. I blew out a breath and fixed my
eyes on Shudi Fiedman’s. I was getting a little sick of
that nail-pinching elf painter.

—“Just like that, there!” the painter exclaimed,
excitedly. “Don’t move.”

He made several brushstrokes on his canvas and…

—“Shoulders straight!”

I clenched my jaws and braced myself. Immobile like my
master, I thought. Elassar could stand still for hours
on his trunk. The problem was that standing still for so
long made my head spin, and finally, when the evening
sun was already shining on the painting room, I gave up
and sat down on the floor, crossing my legs.

—“What a slacker! Get up,” Shudi ordered me.

—“I’m tired,” I complained.

The painter huffed and puffed and ignored me, so I
guessed that he didn’t need me any more, and at a moment
when he seemed totally absorbed in his painting, I
crawled out and escaped from the room. Miroki Fal had
already gone home, and as it was already late, I
imagined that he did not expect me to return. So I
opened the main door of the Fiedmans’ house, and out of
politeness, shouted out:

—“Good evening, Mr. Fiedman!”

And I trotted out of there, hoping that the painter
wouldn’t call me to come back. That day, I wasn’t
feeling very well. I had a headache, which rarely
happened to me. And my eyes were closing on their own,
as if I hadn’t slept in days. By the time I reached the
Den, I felt as if I had traveled thirty miles. Neither
Rolg nor Yal was there, so I lay down on the mattress
and fell heavily asleep.

—“Draen! Wake up!”

I blinked and saw my Master standing by the door. The
sun had already set, and all was dark.

—“Hurry up, get up, come on. Today, you’re going to do
your first job. On your way,” he insisted in a light
tone.

I sat up and stood up, rubbing my face. I felt horrible.
I couldn’t remember ever feeling so bad. I tried to
brighten up, went out with Yal, and accepted the set of
picklocks he handed me. I kept them under my coat and
followed him like a groggy ghost.

—“You are very quiet,” Yal observed after a moment.
“Come on, cheer up. I’m sure everything will be all
right.”

I gave a grunt. I could not tell where we were going. I
only knew that the house was near the Stone Park and the
ruined wall that surrounded the Atuerzo Quarter. The
house stood in the middle of a garden with bare bushes
that looked like large black spiders covered with snow.
Yal stopped near a tree in the Park and whispered to me:

—“This is the house. There are two doors, the main one
and the service entrance, which is on the other side.
Today, there is a ball at the Citadel, and those who
live in this house are not here. They’ll be back in a
couple of hours minimum, but be on your guard, because
there’s probably some servant and probably some
anti-theft magara. Come on, Mor-eldal: surprise Korther
and bring back something valuable. Good luck.”

I nodded in a daze and stammered:

—“I don’t feel good, Elassar.”

—“Well, don’t worry, the hardest part is getting
started; once you’re inside, it’ll feel like a game.
Come on,” he encouraged me. “And remember what I taught
you.”

I shuffled away but stopped when I saw a night watchman
passing in the street. I backed away and hid behind a
tree. Then I kept walking, my head on fire.

—“Devils, what’s wrong with me?” I muttered weakly.

I would have liked to ask Yalet, but my hands were
already gripping the wall. I climbed awkwardly, entered
the garden and moved forward, hitting my forehead with
my fists.

—“What’s wrong with you, Mor-eldal?” I growled.

I picked up a handful of snow and rubbed it on my
forehead. That, at least, woke me up. I reached the main
entrance, and remembering in time that I must be
discreet, I restrained myself from banging my head
against the door to rest it a little. I leaned on it,
but discreetly, and pulled out a picklock. As I was not
yet an expert, I cast a perceptist spell to get an idea
of the lock. And I saw that the door was locked from the
inside. Sighing, I walked away, around the house, and
approached the entrance for servants. I managed to force
it open, disabling an alarm first. Before pushing it
open, I cast a silence spell and succeeded quite well:
the door opened without a sound. I went in, closed the
door, and seeing a chair beside it, sat down heavily. I
was shaking with cold and felt like throwing up. But
why?

—“Come on, Mor-eldal,” I moaned softly.

After a while, I got up and walked towards what seemed
to be a living room. And I found some stairs. I went up
as quietly as I could. I opened a door at random and
entered a room. As soon as I heard snoring, I said to
myself: not there. Half in the world of dreams, I came
out and went to the back room. It was locked. It took me
a long time to open it, even with my perceptist spells.
When I did, I made sure that the room was empty. I
turned on a very dim harmonic light and… sat down in the
middle of the room.

—“Search, search,” I muttered. I lay down on the
comfortable carpet, and although my energy stem was
already quite consumed, I cast another perceptist spell.
Closets, chairs, mirrors… Ha, did I really think I would
find the owners’ jewelry like that?! My energy stem
would go crazy and make me apathetic before I could find
anything.

I sighed and was about to undo the spell when suddenly I
felt something just below me. A hollow. Could it be
that…?

I pulled the carpet aside and rolled it up, exposing the
wooden floorboards. There was something there. I
nervously reached in with my fingernails and managed to
separate the boards and lift one of them, revealing a
small hole. A pocket? Yes, it was a purse, and what was
inside looked like money. I thought that if the owner
had hidden it here it must be valuable, so I put it in
my pocket, put the board back in, unrolled the carpet,
and… I heard a creak in the corridor.

—“Ribs and clavicles,” I stammered in Caeldric.

I retreated to the bed with the intention of hiding
under it, but then I thought that, if it was the owners,
they would eventually find me. The footsteps were
getting closer. I was already resigning myself to going
straight to Carnation Prison when suddenly I saw
something behind a window. A branch. And it was close. I
quickly opened the window, and just as the door handle
turned, I jumped up and grabbed onto the branch. I
mostly used my right hand—resistant to all adversities—I
surrounded myself with harmonic shadows, and forgetting
for a moment my discomfort, I recovered my tree-climbing
instincts and was down in a flash. I heard the
high-pitched cry of a woman in dismay.

—“An Evil Spirit! Help! A ghost!” she shrieked.

I took off running, climbed over the wall, and entered
the Stone Park without slowing down. The blood was
pounding frantically against my temples.

—“Draen!” I heard someone whisper.

I stopped, wheezing.

—“Elassar! They saw me!” I stammered.

I heard him swear and saw him quickly come out of his
hiding place.

—“What do you mean, they saw you?”

—“A woman did. But she mistook me for a ghost,” I
exhaled.

Yal sighed loudly.

—“Let’s get back to the Cats, quick.”

He took me by the arm, and we walked out of the Stone
Park. We went down the stairs, and in a few moments, we
arrived at a dead end full of disparate objects. I was
shaking and chattering my teeth.

—“Elassar…” I murmured.

—“Wait,” Yal said.

He knocked on a door, and no sooner had I leaned against
the cold wall than Yal grabbed me by the coat and pushed
me inside. There was a lot of light. And it was
stiflingly hot. But I was still shivering. Sitting in an
armchair in front of the fireplace was a brown-haired
elfocan with scaly eyebrows and violet eyes with
vertical pupils like those of Little Prince. As people
would have said: he was a devil. He wore dark clothes,
and in his hands, he held a completely black dagger.

—“The lad from the valley, isn’t it?” the kap inquired.

Yal nodded; Korther smiled at me and said:

—“Good evening. And congratulations on your first job.
Did it go well?”

I opened my mouth and stammered something
unintelligible. The kap looked at me curiously.

—“Uh… okay. Don’t worry, I won’t bite. You stole
something, I suppose, didn’t you? If you didn’t, don’t
worry: it’s good enough for me that you went into a
house and came out in one piece. Come here, come here,”
he said, when he saw me take out the little bag I had
stolen.

He took it gently out of my hands and emptied it into
his palm. Five black marbles slipped out. I heard Yalet
clear his throat:

—“Uh…”

But Korther examined the marbles with interest.

—“How curious,” he muttered. “You’ll be surprised to
know that your apprentice has just stolen salbronix
pearls, Yal.”

I heard Yal sigh with relief, as if he had feared that
these beads were really just marbles.

—“Listen, lad,” the kap added, looking into my eyes. His
violet, reptilian eyes suddenly looked very large to me.
“What do you say I buy them from you for five siatos,
huh? Yeah, I’ll give you five.”

I, who was not in the right state of mind to think,
whispered:

—“It runs.”

Korther frowned slightly.

—“You don’t look well, lad.”

I saw his hand reach for me, and with a sudden glimmer
of caution, I drew back and staggered. This time, Yal
looked at me more closely in the firelight, and his dark
eyes flashed with concern.

—“Sari? Sari, are you all right?”

I shook my head and stammered:

—“Elassar, I want to go home.”

I felt his icy hand on my forehead and heard him gasp.

—“By the Four Spirits of Dawn, you’re burning!”

—“I want to go home,” I repeated.

—“Damn it, take him away, Yal, and put him to bed,”
Korther sighed. “And look after him well, because I want
him ready in ten days.”

I looked at him, my eyes half closed, and mumbled
without energy:

—“Ready for what, sir?”

The elfocan smiled at me.

—“Ready to steal the greatest jewel in Estergat.”

I stared at him, blinked, and suddenly, my head began to
spin, and I gave back everything I had in my stomach,
right there on the floor of the Black Daggers Hostel.
And I even splashed the kap’s boots. Korther’s smile had
turned into a petrified grimace of disgust. I saw him
swallow saliva. I spat, leaning on the floor, my arms
trembling. My mouth was on fire.

—“Patron Spirit!” Yal stuttered, crouching down beside
me. “Sorry, Korther. He’s sick—”

—“It’s all right, take him away,” the kap cut him off.
“You better get well soon, kid. Now, get out.”

Yal picked up the cap I had dropped, put it on me, and
lifted me with both arms with no apparent difficulty. I
clung to his neck as best I could. My mind kept lurching
and sinking. I heard the sound of a door being closed.
Then the rhythmic sound of boots crunching in the snow.

—“Why didn’t you tell me, Mor-eldal?” Yal snorted as he
moved forward, holding me tight.

I groaned, and after a silence, I asked:

—“Am I really sick?”

—“Damn sick,” Yalet huffed.

—“I’ve never been sick,” I sobbed. “Am I going to die?”

His brief silence transported me to that world full of
spirits and ancestors the temple priests spoke of. And
to think I didn’t even know my own ancestors! I waited
anxiously for Yal’s answer, overcome with dread.
Fortunately, it did not take long to come.

—“No, sari,” my master murmured. “What are you saying,
of course you won’t. I’m going to take care of you.
You’re not going to die.”

He kissed me on the forehead. Absolutely convinced that
my master was telling me the truth, I closed my burning
eyes and sank into a deep delirium.

Against all odds, financiers hire hitmen and Little Prince is a noble
=====================================================================

I spent the first five days on my pallet without getting
up. I ate very little, slept a lot, and was delirious in
my dreams. On the third day, a person had visited the
Den; it was neither Rolg nor Yal, but I was not sure
what he was like or even if he really existed. My mind
was dancing like a feather in a hazy, muffled inferno.
On the fourth day, Yal leaned over to me with a wet
towel and said in a tense tone:

—“Enough, sari: stop creating illusions. Undo them at
once. This is dangerous.”

I blinked and realized that I had indeed drawn the face
of my nakrus master floating right in front of me, on
the ceiling, with his magical green smiling eyes.

—“Undo it,” Yal insisted. “Right now. You don’t want
anyone to see it, do you? Please.”

Reluctantly, I listened to him, and with some sorrow, I
saw my master’s skeletal face fade away. I almost heard
him say, “What? You haven’t found the ferilompard bone
yet, Mor-eldal?”. Yalet sighed.

—“Thank Spirits… Listen, sari. Harmonies can be
dangerous if you lose control of them. Remember what I
told you? There are people who have lost their minds by
using them, people who have ended up very badly.
Harmonies are dangerous,” he repeated. He passed the wet
towel over my sweaty forehead again and, after a pause,
gave me a slight smile. “Rest. Think of nothing and
rest.”

On the sixth day, I felt much better, but I did not
leave the house, nor on the seventh or eighth day,
because Yal forbade it. He was away during the day,
working as a proofreader in a printing shop. Once, in
the autumn, I had asked him what he worked for if he
could get rich by stealing jewels, and he had answered
that greedy thieves always end up getting caught, that
wealth does not bring happiness and, in short, that he
preferred to earn his bread honestly.

So, when I wasn’t chatting with Rolg, I spent my time
alone, playing cards, singing, or reading a little book
that Yal had bought me using the five siatos he earned
in exchange for the salbronix pearls. It was called
*Alitard, The Blessed Valley Man, and His Lamb Destiny*,
and it told the adventures of a young shepherd from the
Valley of Evon-Sil: he crossed the entire Prospaterra,
from the Northern Lands to Doaria, fleeing Osmiron, an
evil charlatan, who wanted to steal his lamb because it
was able to speak Drionsan. Finally, the shepherd
managed to trap him in a boat on the Sea of Ash, and the
villain ended his miserable life in the mouth of a
dragon. Good Alitard returned to the valley where he
married a young shepherdess, and they lived happily
together, close to Destiny, the lamb who could speak. I
loved the story. I decided that I, too, would like to
have a lamb to defend. Then I thought of Manras and Dil,
and I said to myself: but I already have two cronies to
defend! Except that it wasn’t exactly the same, because
in the book, Alitard never got sick, everyone was kind
to him except the bad guy, and he never left Destiny
alone.

On the tenth day, in the morning, Yalet gave me an
infusion, and as I drank it, he bit his upper lip,
hesitated, and commented:

—“I don’t know if you remember that, tonight, we steal
the Wada. Tell me, if you think you’re not fully
recovered…”

—“Recovered? I’m fresh as a daisy!” I assured. “What’s a
Wada?”

Yal watched me carefully as he replied:

—“A sort of amulet of great and great value. It’s a gold
sculpture full of precious stones. It hangs on a wall
inside the Stock Exchange Building and, according to
some, is something like the totem of financiers.”

—“The Stock Exchange,” I repeated.

I knew where it was: it was near the Esplanade. Manras,
Dil, and I always made a successful trip there with our
newspapers. Well, “always”… at least in the fall, I
corrected myself. Because in winter I could hardly go
with them once or twice a week, and what I knew best now
was the labyrinth of the Conservatory.

—“And why are we going to steal this… Wada?” I asked.
Yal grimaced, and I said, “Financiers are nail-pinchers
too, right?”

Yal smiled and shook his head.

—“Well… it so happens that the Black Daggers have our
little enmities, too. Look, this whole thing has to do
with a certain Mr. Stralb, the owner of the Stock
Exchange Building. Many moons ago, this financier got in
touch with Korther and offered him a job: it was about
stealing compromising documents on a competitor. Korther
refused. And the financier… well, he’s a nutcase. He got
angry, he insisted, and faced with Korther’s refusals,
he threatened to reveal information about our
brotherhood: it was an idle threat, of course. Korther
turned a deaf ear. And the financier was obstinate,
thought he had discovered Korther’s true identity, and
sent him a hitman.”

—“A hitman?” I repeated, without understanding.

—“An assassin,” Yal explained. I paled. “Fortunately,
this alleged true identity was only one of many Korther
had. He learned that a hitman was looking for him, he
found him, he… er… threatened him, and the killer
disappeared from Estergat overnight. After that, Korther
warned the financier that, if he didn’t leave him alone,
the Black Daggers would ruin his life. The guy may be
rich, but Korther also has means, and more importantly,
he has support. So, in the end, to make the threat clear
and take revenge for the killer thing, Korther has hired
us to help him steal the Wada. When they see that it’s
gone from the Stock Exchange, there’s going to be a
scandal bigger than the Patron Spirit. The financier
will pull his beard off from shock,” he laughed.

I arched an eyebrow.

—“Have you ever seen him?”

—“The Wada? No, I never—”

—“No, no, I’m talking about the financier. You say he
has a beard.”

Yal rolled his eyes.

—“It’s an expression, sari. Even a lady can pull her
beard off from shock.”

I laughed as I imagined it; he put a small package on
the table and pushed it towards me.

—“Butter biscuits!” he announced cheerfully. “So you can
get your strength back,” he said, before standing up.
“Well, I’m off. Normally I’d be back before eight. Rest
well, sari.”

Rolg still hadn’t come out of his room, so Yal left
without saying goodbye to him. That old elf was really a
big sleeper.

As soon as I was alone, I took a biscuit from the
packet, and after examining it for a few moments, I
tasted it. I found it so good and so delicious that I
devoured all ten of them in a “peace-and-virtue”.

After waiting a while and seeing that Rolg was not
waking up, I went to look out the window and smiled
broadly. The sky was blue, and the snow was already
starting to melt. It was a perfect day to go out. I
remembered Yal’s words and shrugged. Now I felt so
energetic that I could have climbed a mountain. I would
rest later. Running to my pallet, I put on my coat, cap,
and boots. Rolg was getting all this stuff from the
Hostel: apparently they kept clothes there for the Black
Daggers in need. After checking that I still had the
yellow feather and the sharp stone in my pockets, I
rushed to the door and went out. I was careful not to
slip on the snowy stairs, left the courtyard behind,
trotted out of the Cats, and went up Tarmil Avenue. The
weather was glorious, and it naturally encouraged me to
poke around and zigzag from store window to store window
and from sidewalk to sidewalk. When I reached the
Esplanade, I recognized a tall, blond boy and called out
to him:

—“Garmon!”

The newsboy turned around, a pile of newspapers under
his left arm.

—“Oh, Sharpy!” he said, smiling. “I haven’t seen you in
a while. Where have you been?”

—“In bed, I got sick,” I explained.

—“Damn! Just like my brothers: not one of them was
spared, only me. But, thank Spirits, they are all fine
now,” he assured. “Are you looking for your cronies?” I
nodded, and he pointed to the Capitol. “I saw them go by
there a little while ago. I think they were headed for
the Grand Gallery.”

—“Thank you, Garmon!” I said.

—“Hey, Bard!” he called out to me as I was already
running off. “Tell me a new word!”

I smiled. Garmon loved to hear me pull out words that
I’d made up or that came straight from the depths of the
Cat Quarter. This time, I threw him one from my nakrus
master:

—“Demorjed!”

The blonde arched an eyebrow.

—“And what does that mean?”

—“It’s a more stylish way to say isturbag!” I replied,
laughing, and ran off towards the Grand Gallery.

I found my friends at the southern entrance. Facing the
passers-by who came and went, Manras shouted at the top
of his lungs:

—“Clash in Tribella! *“The Estergatese”*! Sturgeons and
Winged Serpents clash! *“The Estergatese”*! Clash in
Tribella!”

I stopped, saw the little dark elf selling a paper, and
crouched down to pick up some snow. I made a big ball.
Manras saw me and opened his mouth to shout my name, but
I put my index finger to my lips and, with a roguish
smile, threw the snowball at Dil, who was busy
scratching his head a little further away. It hit the
Little Prince right in the neck, and I laughed and
shouted:

—“Ayo, children of the Spirits!”

Manras greeted me boisterously, and Dil welcomed me with
another snowball that crashed into my face.

—“Good mother!” I exclaimed, with a grimace that soon
changed to a smile.

Snow fights reminded me so much of my past winters with
my nakrus master…! Yes, although it may not have been
easy to imagine, my master and I were great experts in
snowball battles. Of course, he was harder to reach when
he wasn’t wearing his cape. But by the time he picked up
a ball, I had thrown three.

—“Clash in Estergat!” Manras hawked. “Clash between
Little Prince and Sharpy!”

We burst out laughing and went to sit on a low stone
wall without snow, sitting on the newspapers so we
wouldn’t freeze.

—“What time did you start?” I asked.

—“At six o’clock, as usual,” Dil replied.

Manras yawned and put his arms around his knees as his
watchful green eyes looked at the people passing by.

—“So, you got sick?” he said. “And the Nail-pincher
cured you?”

I huffed.

—“No. My cousin went to tell him I was sick. And
apparently, he was sick too. My cousin says these things
are contagious. I’m sure it was the painter who gave it
to me,” I grumbled. “Say, have you guys ever been sick?”

—“You bet, either in winter or spring, but the Cold One
comes every year,” Manras said. “Well, I dunno for the
Little Prince, but when I found him last year, he was
sick, that’s for sure.”

I arched an eyebrow, a thought suddenly running through
my head.

—“But haven’t you guys known each other forever?”

Both shook their heads, and I noticed a certain reserve
on Dil’s face.

—“Dil came last winter,” Manras explained. “I found him
on my way back home after selling papers. He was all
alone, and he’d caught one hell of a cold, so… I took
him home. And my brother said, as long as he works hard,
he can stay. So he stayed,” he concluded with a smile.

Dil shook his head affirmatively to confirm and said:

—“Warok said he didn’t care if I was a devil as long as
I made money.”

A sudden cold shiver ran through me, and I exhaled:

—“Who?”

—“My brother,” Manras translated.

I looked at him, bewildered.

—“Your brother’s name is Warok? Mothers of the Light, I
know a Warok. He’s a dark elf like you. And he has a
friend named Tif.”

Manras grimaced.

—“Well, it’s him.”

I snorted loudly. In the autumn, I had gone to see Warok
five times in a tavern in the Labyrinth to give him
nails to pay for the damage done by the green ink. One
night, when I was returning to the Den, the Ojisary had
blocked my way and I’d given in to his threats.
Fortunately, the fifth time, he had said “get out of
here”, without telling me to come back, and I had not
returned.

—“Blasthell,” I said. “And… do you guys like him?”

Dil darkened; Manras bit his lip and admitted:

—“No.”

—“Ah. Well, I don’t, either,” I admitted.

Manras looked at me with an unhappy face.

—“He’s mean, you know,” he said.

I arched my eyebrows.

—“Even to you guys?”

Manras nodded silently, and Dil darkened even more. I
didn’t like their faces.

—“And why do you stay with him then? You should…”

I fell silent. I was going to tell them to come with me
to Rolg’s house, but I remembered in time that Rolg
didn’t allow anyone who wasn’t a Black Dagger to enter.
Maybe if I asked for his permission first… I should ask
him.

—“I can’t run away,” Manras replied. The little dark elf
had looked down at his blue hands.

—“And why is that?” I replied.

He shrugged and explained:

—“Last summer, we ran away, and when my brother found
me, he got very angry. He almost chased Dil away, but I
told him that running away had been my idea. And it was.
Dil never complains. Maybe it’s because he’s a noble.”
Dil glared at him, and Manras assumed an innocent look.
“What?”

Dil sighed.

—“It was a secret, Manras.”

—“Don’t worry, I won’t tell anyone,” I assured him. “So
your parents are noble?”

—“Yes, but my mother isn’t here anymore, and my father
doesn’t want me because I’m a devil. That’s all. And now
I’m getting back to work,” he concluded. He stood up
abruptly, picked up his papers, and walked away towards
the entrance of the Gallery.

Manras looked at me with a worried expression, and
affected as he was, I decided:

—“We’d better not talk to him about this again.”

The little dark elf nodded, he went away to sell the
papers, and I borrowed one. Standing on the low wall,
like a statue of a learned reader, I began reading the
articles with application. Thanks to Miroki Fal, I had
learned to read in silence, though sometimes, I would
say a few words and let out comments of surprise,
boredom, or incomprehension.

—“Bwah,” I said after a moment, pushing the paper aside.
I jumped down from my pedestal and shouted, “*“The
Estergatese”*! Armed robbery at the Port of Menshaldra!”

I sold my copy in a peace-and-virtue and, joining
Manras, said to him:

—“Talk about the robbery, not the Winged Serpents from
Tribella: what interests people is what happens at home,
believe me, these journalists don’t have a clue.”

And Manras, of course, listened to me.

The Wada: Through the Dome of the Stock Exchange
================================================

When I returned to the Den, Rolg was not there. It was
already afternoon, and I guessed that he must be in some
tavern playing cards, or taking his usual walk to
“stretch his game leg”. I took a nap, and slept so well
that I only woke up when my master shook my shoulder and
said:

—“Wake up, Draen. How are you feeling?”

I opened my eyes and stretched as I replied:

—“Couldn’t be better! Shall we go already?”

Yal shook his head.

—“No, not yet. At midnight, in an hour. And, at three
o’clock, we’ll go into the building. I brought you
dinner, are you hungry?”

—“I’m starving!” I confirmed. As a matter of fact, apart
from the morning biscuits, I hadn’t eaten anything.

I gobbled down the cheese bread and the orange almost
without chewing, while Yal dropped into a chair and
commented:

—“Today has been a hellish day. The boss at the print
shop made us work until nine. Honestly, I thought I was
going to have to make up an excuse to get out on time.”

—“Why was there so much work?” I asked, my mouth full.

—“Oh. Lots of stuff, urgent forms, various orders, and
the like. Hey, easy, Mor-eldal, chew or you’ll choke.”

I rolled my eyes, but ate more slowly, and asked:

—“Where is Rolg?”

—“An old friend of his is sick, so he went to his house
to take care of him,” Yal explained.

I felt a surge of empathy and complained:

—“The Cold One is worse than hunger. Say, Yal, how are
we gonna get into the Stock Exchange?”

Yal’s eyes smiled and twinkled.

—“Through the dome.”

*The dome*, I repeated to myself. And my eyes widened.

—“The round roof from above? And no one will see us?”

He shrugged.

—“Korther has it all planned out. He knows where the
traps are, how to disable them, and… well, we both don’t
have to worry about anything.”

I frowned.

—“But, then… what are we going to do?”

Yal looked at me, amused.

—“Korther shall lead the way, I shall lower you from
above with a rope, and you, Mor-eldal, will steal the
Wada.”

I was startled.

—“Me?”

I still could not quite understand what he wanted me to
do, but the idea of being lowered by a rope from such a
high dome fascinated me. I continued to chew more and
more slowly. Yal smiled at me.

—“Don’t worry: it’ll be a breeze. But, if things go
wrong, remember the saying of the thieves.”

I nodded firmly.

—“He who steals and runs away, may live to steal another
day,” I said. And I struck the table with my hand,
shouting, “Onward! Let’s steal the Wada from that
hitmen-hirer nail-pincher!”

Yal gasped and hissed:

—“Keep your voice down, for the Spirits’ sake!”

I gave him an innocent pout followed by an enthusiastic
smile and finished the orange slices.

When we heard the twelve bells, Yal took his bag with
the rope, and we went out to the park which was just in
front of the Passion Flower Hospital in the Riskel
Quarter. We passed the Stock Exchange, and Yal grabbed
me by the neck to keep me from looking at the building
with too much interest. It had been a spring-like
afternoon, so the snow had melted completely, and there
were still people walking along Artisan Street. However,
in Passion Flower Park, everything was dark and
deserted.

The Great Temple had just rung the first bell when Yal
sat down on a bench and I followed suit.

—“What now?” I muttered.

He replied:

—“We wait.”

So we waited, and for a good while, until we saw a
figure appear on the narrow path. It was not a guard;
otherwise, he would have carried a lantern.

—“Yal?”

—“Sir,” my master replied. And he stood up; so did I.

—“You brought hoods, I hope,” the kap whispered.

—“We have them,” Yal assured.

There was a silence during which Korther seemed to
think. Then he turned to me, and I thought I saw a smile
in the darkness.

—“How’s that flu going, lad?”

—“Swept it away,” I assured him. “So… shall we go?”

Korther turned to scan the back of the alley and
whispered:

—“When you get inside, Draen, don’t touch anything
without asking my permission or I’ll put you on a boat
and sell you to a Tassian slaver…without asking your
permission, you understand me?”

I looked at him in horror at first, then I made an
annoyed pout.

—“Yes, sir.”

—“Good.” He beckoned us through the shadows, “Follow me
at a distance.”

He walked away, and we followed him. We crossed the
almost deserted Artisan Street and arrived at the back
of the Stock Exchange. I glanced behind me, and the next
thing I knew, Korther was gone.

—“Where…?”

Yal hushed me, and after letting a group of singing
drunks pass, he grabbed a ledge and climbed to the
railing of a long balcony of the Stock Exchange. I
hurried after him, and when I landed I saw Korther
lurking near a door of the balcony. He was holding
something in his hand, something which gave a slight
glint. It intrigued me, but when I went to look, the
door was already open.

Silent as cats we entered a dark room. There was no
Moon, no Gem, no Candle, and the distant light from the
street lamps barely made it in. Korther cast a very shy
harmonic spell of light and walked to the only door in
the room. He took out a key, examined it, shook his
head, took another, inserted it in the lock, and turned
it. A few moments later, we were walking down a
luxurious corridor as if we were the masters of this
imposing house. Korther seemed to know it by heart. He
led us to another door, which he opened, and took us up
the back stairs. We climbed at least three flights
before the stairs stopped. When we got there Korther’s
movements became slower and more conscientious. As I
noticed, there were not only alarms on the doors, but
also on the floor. Korther deactivated them step by
step, until he took us into a huge office with a writing
desk large enough to be used by ten people at once. He
opened a window and whispered to Yal:

—“You’ve got a handle there and one a bit further up.
Tie yourself to the rope just in case. Then I’ll untie
you from below. When you get to the dome, break the
glass that’s right behind the Fortune Dragon statue.
Understood?”

Yal nodded.

—“Yes, sir.”

I thought I saw some nervousness in his answer. Korther
patted him on the shoulder.

—“So let’s get to work.”

Yal tied himself tightly, put my cap in the bag, in case
it got away from me, and said:

—“Wait here. Then Korther will tie you up, and I’ll help
you upstairs.”

With some apprehension, I saw him disappear over the
edge of the window. I wanted to look, but Korther
stopped me until the rope was pulled taut. Then the kap
untied the rope and fastened it around me quickly but
firmly.

—“Use the shadows and climb silently,” he said.

I wrapped myself in harmonic shadows and hoisted myself
onto the window sill. I was stupid: I looked down. And
when I saw the ground so far away, I was so afraid that
I closed my eyes and stammered a lullaby that my master
once sang to me:

Survivor,\
don’t be afraid.\
The storm’s going away.\
I’m here, you are not alone.\
Don’t be afraid.\
The storm is already gone.\
Sleep peacefully, my child.


I felt the rope tighten and quickly looked for a grip
without ceasing to repeat the lullaby to myself. For the
last few meters, so to speak, it was Yal who pulled me
up. When I arrived at the top, he hissed to me:

—“You’re legend, Mor-eldal. Will you be quiet? I’d bet a
siato that Korther heard you speak in the language of
the dead.”

Very pale, I stopped mumbling my song and looked around.
A rim perhaps a metre wide surrounded the whole dome,
and at regular intervals stood the majestic statues of
the Stock Exchange. Yal untied the string from one of
these and pointed:

—“The Fortune Dragon is right there.” I crouched close
to him in front of one of the glass windows of the dome.
As he brought out his instruments, I heard my master
whisper, “Sari… What’s that song about? It sounded like
a sinister abracadabra.”

I grimaced, and when I did not answer, Yal turned to me
in puzzlement, and I cleared my throat.

—“It’s a lullaby my master used to sing to me when I was
little,” I replied.

Yal snorted softly and concentrated on breaking the
glass. I helped him strengthen his silence spell, which
basically just quieted the sound waves and reduced them
to a small space. We managed to remove the glass panel,
and Yal whispered to me:

—“The shadows, Mor-eldal. Don’t forget.”

I quickly surrounded myself with harmonic shadows again,
for the lights of the city could be treacherous. While
he tied the rope to the Fortune Dragon, I stuck my head
through the hole. There was nothing to be seen. How was
I going to find the Wada in this darkness? I had never
been in that hall before, but I had seen it from the
outside and knew it was huge. Even with a harmonic
light, I could spend hours looking for it. Unless Yal
and Korther knew exactly where it was, which was very
likely.

When Yal returned, he tied me up, filed the glass so the
rope would not be damaged, and whispered to me:

—“We’re waiting for the signal. Korther went downstairs
to make sure the watchman took the sedative.”

Obviously, this one had taken it because, a moment
later, Yal perceived the signal of a harmonic light
flashing from the hall below.

—“Now it’s your turn, sari. Don’t worry, you’re not
going to fall: you’re tied up tight. Now, listen. I’m
going to lower you a few feet. When I stop lowering you,
start swinging, towards the side where I am. That’s
where the Wada is, in a hollow in the wall; you can
recognize it right away. You’ll probably need several
tries. Don’t lose your temper. When you find the Wada,
you’ll have to be very careful: as far as Korther knows,
there are no alarms on it, but be on your guard.
Normally, it hangs from a simple hook. Tie it up with
the rope you have left, so it won’t fall. And take this
knife, just in case: if the Wada is attached to
something else, use it. If it’s wood or even iron, it
will work, but be careful using it: it’s very sharp.
When you get the Wada, just let yourself hang: I’ll be
watching, don’t worry. And you send me a signal with
three quick lights to confirm that you want me to pull
you up. You got that?”

I swallowed and nodded.

—“I think so.”

I heard him sigh.

—“Well, go ahead.”

I passed through the hole with the rope stretched out
and gradually descended into the darkness. It felt
strange to descend from the dome of the Stock Exchange
by a rope, especially knowing that I still had many
meters to go to the bottom.

—“I can’t see anything,” I muttered.

Fortunately, after a while, I managed to make out some
shapes. It wasn’t much, but it was enough to know that I
wasn’t in the Underground a thousand feet below.

Suddenly, I ceased to feel the vibrations of the rope
and realized that I was no longer descending. Eyes wide
open, I began to swing towards the place which Yal had
indicated, but I did not do so with sufficient decision,
and I remained miles away from what seemed to me to be
the wall. After a moment, I heard a hissing sound,
though I did not know whether it came from above or
below. Then I breathed in deeply and breathed out:

—“Courage, Mor-eldal, you can do it, come on, come on…”

I swung with more strength and finally touched the wall
with my feet. It took me three more tries before I could
hold on to something. Could it be the Wada? To be sure,
I cast a very timid spell of harmonic light and heard
another hiss. This time it was coming from below, no
doubt about it. Korther was losing his patience, I
guessed. In any case, what I saw assured me that the
object I had just clutched was indeed the Wada: it was a
small gold statue in the shape of a manticore woman with
two gems in her eyes and even more gems encrusted here
and there. I clung to the totem and moved the harmonic
light down. There was a hook, indeed, and also… a spell
on that same hook.

I frowned, and after a moment’s hesitation put my right
hand on the hook. To my surprise, I recognized the
pattern: it was a simple anti-theft trap which, when
activated, gave a terrible shock. I was about to undo
it, but I thought better of it and simply deactivated it
because, according to Yal, it looked much more
professional. *Honestly*, I thought, bewildered. I was
hanging from a rope a mile off the ground, and I was
thinking of professional art? Demons.

I lifted the Wada, not without some difficulty, for it
was quite heavy. Fortunately, it was not very big. After
a few shakes and struggles, I pulled it off the hook,
tied it to the rope, clutched it to my chest, and
finally, without taking time to think too much, I pulled
my foot away from the hook that I had been using to hold
onto the wall. I fell. Or at least I did at first. Then
the rope tightened, and my breathing suddenly stopped
before it quickened to a rapid pace again. It took me a
moment to remember the next step: the signal.

Without letting go of the Wada, I performed three light
spells in succession, and my heart leapt as soon as the
dome began to close in. At last, I passed through the
hole, and when my feet touched the solid stone of the
ledge at the top, my legs buckled, and I scrambled to my
hands and knees, though still tied. Yal asked me:

—“Everything okay?”

—“Everything okay,” I replied, with more confidence than
I felt.

With quick precision, Yal untied the rope from the
Fortune Dragon, tied it to the statue just above the
window of the room we had climbed from, and lowered me
down with the Wada. Korther was already waiting for me
inside. He released me, secured the stolen object in his
own bag, and tied the rope. A few moments later, my
master landed inside.

—“Aren’t we going to steal anything from the other
rooms?” Yal muttered.

—“Nothing else,” Korther asserted. “I am here for
revenge, not for money.”

I didn’t know whether to believe him, because I had seen
that his own bag was a little more swollen even before
he had put the Wada in it… However, his tone of voice
sounded convincing. Yal didn’t protest: he put the rope
away, gave me my cap back, and we went back down to the
first floor balcony without any trouble. Korther
retrieved the knife, gave Yal a friendly pat, and
whispered:

—“Good job, boys.”

He jumped over the railing and disappeared into the
shadows of a street. A few moments later, Yal and I left
the balcony, too, and headed for the Cat Quarter. I felt
the tension disappear almost immediately: we were now
safe. And what’s more, we had accomplished our mission.

We were passing through the Esplanade at a leisurely
pace when Yal blurted out:

—“By the Four Spirits of Dawn…” And, in an almost
inaudible whisper, he said in my ear, “Can you believe
it, sari? This is the biggest theft in Estergat in
years. Okay, we’re not going to get much out of it,
since I already owed Korther a favor, because of the
studies he paid for me. But now: no more debts!” He
smiled broadly at me. “And you don’t know how happy a
sajit can feel without debts.”

I smiled back at him, and as we started down Tarmil
Avenue, a sudden thought occurred to me, and I jumped
cheerfully.

—“Are we going to celebrate?”

—“Celebrate?” Yalet laughed out loud. “Well, why not?
How do you want to celebrate?”

I bit my lip and suggested:

—“With butter cookies?”

This time, Yal laughed heartily.

—“I’ll buy you some in the morning,” he promised. “But
don’t take too much liking to them, since they are
expensive. Ah, by the way, I suppose, now that you’re
recovered, you’ll go back to Miroki Fal.”

All my joy went to the bottom of a well. I let out a
long sigh.

—“Pfff… Do I really have to go back?”

—“Is it so terrible?” he scoffed.

I shrugged.

—“No. But the Nail-pincher is… I don’t know, he’s not a
bad guy, but he’s really just as nail-pincher as his
friends Shudi and Dalvrindo and the rest. Those people
have gold coming out of their ears, and their hands are
as sticky as Velirian glue. That’s what Yerris told me,
and it’s true. And hey, Rux… he’s got a good heart, but
he’s drier than a burned bone.” I concluded, “Actually,
I’d much rather be with my friends or even at *The Wind
Rose*. Can I really not wait a few more days? I stole
the Wada,” I added as a strong argument.

Yal grunted.

—“Speak lower, sari… It’s okay,” he relented. “I’ll tell
him you need two more days of rest. But don’t let him
see you running around the streets, or he’ll wonder what
your ways of resting are. And on Youngday, you go back
without fail, eh? Come on, don’t complain: you don’t
realize how much you’ve learned with this job. Not
everything can be learned on the street.”

I looked skeptical, but did not reply. I rubbed my left
hand from the cold and put it in my pocket. Suddenly, I
felt as if someone had thrown a bucket of ice water on
my head. My feather, I thought, dumbfounded. My yellow
feather. It wasn’t in my pocket. Where could it have
fallen?

I glanced discreetly at Yal as we walked, but I dared
not say anything to him. Maybe I had dropped it on the
street in the morning while selling the papers, or… or
maybe in the hall of the Stock Exchange.

—“Well done, Mor-eldal,” I murmured in Caeldric.

Yal looked at me.

—“Did you say something?”

I shook my head. After a silence, I asked in a low
voice:

—“Elassar. If we’d gotten caught, they would have sent
us to jail, wouldn’t they?”

—“Uh… Yes, sari. I’d say they’d even send us to hard
labor. For years. But it all worked out, and you did
great, so tomorrow, I’ll buy you these cookies to
celebrate, huh?”

I felt that he was smiling at me, and I gave him a
hesitant smile that grew firmer as the memory of the
cookies cheered me up. Well, I thought. The feather
couldn’t have fallen into the Stock Exchange, and if it
did, who would recognize it? Yerris, but he wasn’t
there, and besides, he was a Black Dagger. Manras, Dil…
and some other newsboy. No one else. Conclusion:
everything had worked out just fine.

The Ojisary gang spoiled my day
===============================

Sitting comfortably on the thick branch of a
Conservatory oak, I yawned sleepily. The leaves of the
trees rustled in the spring breeze. Miroki Fal said they
sounded like the waves of the sea. He’d been to Seventia
a few years ago and had once even boarded a frigate to
see, from up close, a sowna—a huge sea creature that
covered itself in ice.

There was a serene burst of laughter, and I looked down
at the group of nail-pinchers sitting on the grass a
little way off. There was Miroki, as well as Shudi and
some other companion. A moment ago, they had been
talking about energies and some conservation formula.
Then they had begun to discuss poetry. And in the end,
tired of listening to them, I had taken refuge in my
tree with the newspaper I bought and brought to Miroki
Fal every day.

Since the end of the winter, my life with the
Nail-pincher had subtly changed. In the mornings, I
continued to go to the Conservatory, but as soon as I
had cleaned the plate that Rux gave me, I said to him
“Ayo, Rux!” and I evaporated at once. Rux never
reproached me for it: after all, neither Miroki nor the
house gave much work, and rather than having me
overwhelm him with questions, he preferred that I go
elsewhere to let off steam. I still spent every morning
going back and forth between the Nail-pincher and
Lesabeth with messages, flowers, jewellery, and other
gifts. As a matter of fact, although she never spoke to
me like a sajit, the blond elf was now more friendly
with Miroki… She even seemed to be willing to fall in
love with him. And as one could expect, Miroki was
euphoric and did not skimp on the gifts. More than once
I was tempted to steal one of those gifts, but I knew
that would have been foolish and foolhardy. It was much
less foolhardy to reach into the pockets of the posh
people in the Grand Gallery or on the Esplanade, for
they were so crowded and so careless! At first, I was
afraid of getting caught, but as time went by, I honed
my claws and said to myself: bah, those people have
money, and I’m going to stay put and starve? To hell
with the flies! On the lucky days when I was really
making money, I would invite Manras and Dil to eat for
free as the Patron Saint I was.

I looked down again, and seeing that the group of
magicians were moving, I quickly pretended to be asleep.
If Miroki forgot me and left, I would have a good excuse
to slip away. Like, “Mr. Fal, I fell asleep in the oak
tree, and when I woke up, you weren’t there…” A great
excuse.

—“Draen!”

Well, so much for my idea. I opened one eye, yawned,
stretched, and climbed down from the oak with my
newspaper. Miroki was already walking away with Shudi to
go home. He said goodbye to Shudi at a crossroads and
walked on with a straight and dignified stride: he had
even grown a little beard this winter. As soon as he was
alone I trotted up to him.

—“Mr. Fal! Is it true that you sent my portrait to
Griada?”

I had heard it from Shudi that morning. Miroki nodded.

—“Yes. It’s true. I gave it to a friend for his
birthday.”

—“Ah, well. And how far is Griada?”

—“Hmm. A few days by coach.”

His laconic answers told me that he was distracted and
that, if I kept asking him questions, he would
eventually send me chasing the clouds.

—“How many days?” I asked.

—“Mmph. Four. It depends,” he said.

—“Oh.” He quickened his pace, but I didn’t fall behind
or stay silent. “And in a moon, you’ll be traveling
there to see your family, right?”

Miroki glanced at me and nodded.

—“But you’re coming back, right?”

Miroki shrugged.

—“I don’t know. This is my last year of school. But I’m
not leaving without first asking for Lesabeth’s hand.”

He added his last words in a thoughtful whisper, and
before he could think of sending some rose to the young
elf, I said:

—“Ah, how nice. Say, do you remember when I asked the
professor about the ferilompard? He told me he’d see if
he could find anything. He hasn’t answered yet?”

Miroki raised his eyes to the sky and walked up the
stoop of the red mansion while replying:

—“He hasn’t. Look, kid, that joke’s not funny anymore.
The professor knows you’re making fun of him. There are
no such things as ferilompards. And now leave me alone,
I have some important letters to write.”

He disappeared through the door, and I stood in the
doorway, shocked. What did he mean, there were no such
things as ferilompards? That was impossible. Nonsense,
slugboneries. Maybe the word was in Caeldric, and it was
said in a different way in Drionsan. I shook my head in
disbelief and crossed the threshold at a run.

—“But, sir, it was not a joke!” I assured him.

Miroki Fal was already climbing the stairs, and he did
not answer. A few seconds later, the door to his office
was heard to close.

—“Spirits,” I sighed.

I looked at Rux, who was sitting at the kitchen table
preparing the meal. It smelled good, but… I had lost my
appetite after the whole ferilompard thing. So I said:

—“Ayo, Rux!”

I turned around and quickly left the house. I went down
the streets of the Harp, turned into Imperial Avenue,
and crossed the whole of Atuerzo, passing by the Central
Court. Everything was crowded, and I arrived at the
Esplanade, zigzagging between dresses, carriages, and
coats. At the foot of the huge stoop that surrounded the
Capitol, I saw Draen the Swift. His leg was bandaged
with a dirty piece of cloth, and he held out his cap
with the looks of a miserable, beaten child, murmuring
plaintive pleas. I smiled and approached him.

—“Hey, Swift! How’s the fishing?”

—“Bad, bad,” he sighed. “With this Mortuary Spirit face
I have, well, even the devotees don’t feel sorry for
me.”

Suddenly, he frowned and looked at me.

—“You! Speaking to me again?”

I arched an eyebrow, puzzled.

—“What do you mean, again?”

—“Get out of here!” he said. “Hook it!”

I looked at him even more disconcerted.

—“But… what did I do to you?”

Draen stood before me, looking furious.

—“What you did? Associating with murderous blood, that’s
what you’ve done. That kid you hang out with, Manras…
Don’t tell me you don’t know who he is. You know who his
brother is?”

I blinked and nodded in amazement.

—“I do.”

—“Of course you do! You’re part of the same gang. An
Ojisary,” he spat. “Say it’s not true and I’ll break
your face, namesake.”

I dared not say anything, not after such a threat. I
shook my head.

—“But Manras is not like Warok. He’s a good shyur. I—”

—“Like hell he’s a good shyur! If I see him, I’ll give
him a face as ugly as mine, you understand? Revenge is
something that is taken seriously in the Cats, shyur.
Two of my friends have disappeared. Warok and his gang
took them away, and sure enough, you know where they
are.” He looked at me, his eyes full of venom. “Then
you’d better run for it if I see ya at night in the
Cats. You understand me?”

I glared at him.

—“I don’t know what you’re talking about, but if you
touch even a single hair on Manras or Dil, you’ll see
what I do…” I stepped back as I saw him make a move
towards me and yelled, “I’ll rip your bones out!”

And I ran away. Swift’s words had troubled me. Mostly
because, well, even though I didn’t really consider the
redheaded elf a friend—he was a bit bossy and
unreliable—I’d still put him in the good Cat category,
he’d even taught me a few beggar and pickpocket tricks,
and frankly, I hadn’t expected such an outburst. Friends
of his had disappeared, he’d said? Well… Who knows what
the Ojisaries had done to them. But what did Manras,
Dil, and I have to do with anything?

I climbed up the Manticore Fountain, jumped to settle
myself between the creature’s legs, and cupped my hands.
I drank, wetted my face, and playfully ran my hand under
the spray, thinking of those Ojisaries. Manras and Dil
had not spoken of Warok since that winter day. In fact,
they never talked about him or what they did when they
got home. I, on the other hand, used to tell them about
Miroki Fal’s daily miseries and his good and bad
fortunes with the beautiful Lesabeth. And, until then, I
had thought that they probably just had nothing to tell.
But maybe I was wrong.

So I decided to ask them if they knew anything about
these two friends of Swift’s, and I set out to find
them. I asked several newspaper criers I knew; I asked
the baker in Hale Street if he had seen my companions
passing by, and he replied:

—“And how should I know, son! More people pass here than
loaves of bread.”

I pouted, and spending the three nails I had, I bought
myself a bun and devoured it while I continued to search
for Manras and Dil. I went to all our sales strategic
points, and nothing, not a trace. Had they fallen ill?

I was passing through the Evening Park when I saw a head
of hair as red as the evening sun, and I jumped,
surprised.

—“Sla!” I cried.

Slaryn, Yerris’ Black Dagger friend, was sitting on a
bench, alone. I hadn’t seen her since the beginning of
winter, since the day she’d passed through the Den to
greet Rolg.

She looked up at me with emerald eyes, and a glint in
them shocked me. I approached, hesitating.

—“Sla? Are you okay?”

The dark elf nodded and sat up straighter as she
inhaled.

—“Yes. Long time no see, shyur. How’s the damn life
going?”

—“Ragingly well,” I replied cheerfully before sitting
down on the bench. “And you?”

Sla shrugged.

—“It could be worse.”

I waited, thinking she would specify, but as she said
nothing else, I asked:

—“How’s your mother?”

I noticed her slight flinch. Slaryn took a long breath.

—“Don’t tell me you don’t know. The flies caught her
again, this time for assaulting and insulting an
officer. They sentenced her to eight moons. That’s my
mother all over,” she sighed.

I looked at her, eyes wide. I swallowed.

—“Gee… I didn’t know. When did she—?”

—“At the end of winter,” she grumbled. “Just a little
after Yerris returned.”

I was startled.

—“What? Yerris is in the city? And he didn’t tell me?”

Slaryn blew out a long breath, and I sensed a hint of
exasperation in her eyes.

—“Don’t tell me you don’t know that either. Yerris came
back to Estergat alone. Apparently, he stole money from
Alvon and Alvon sent him away. And… now he’s with the
Ojisaries.”

Her face closed. She slammed her fist into the bench.

—“Korther thinks he’s a traitor. But I know that’s not
true. There’s something fishy going on. Yerris would
have come to see me otherwise. And he would have come
see you, too. And Rolg. But he didn’t, and you know why?
Because they’re holding him prisoner. I’m positive.”

I bit my cheek in shock. Yerris, prisoner of the Black
Hawk, Warok, and his kind? My gaze wandered to a pigeon
that was walking on the path near the bench. I shook my
head.

—“But why did they lock him up?” I asked. “Because he
didn’t steal the documents?”

Slaryn turned abruptly to me.

—“What documents?”

I turned pale. Oh, dammit.

—“Uh… I dunno, I mean… maybe, if they took him, it’s
because they want to use him as a thief.”

Slaryn’s eyebrows were furrowed.

—“I don’t think so. Yerris is not a good thief. He has
other qualities. But as a Black Dagger, he’s not very
good. Things are what they are, shyur,” she grinned,
seeing that I was looking at her, slightly indignant.

—“But he’s a good Cat,” I said. “And, if it’s true that
the Black Hawk and Warok have him locked up, I swear by
the spirits that I’ll get him out.”

Sla looked at me, her eyes squinting.

—“Warok? Who’s Warok?”

I winced, my heroic momentum suddenly interrupted.

—“Well… an Ojisary.”

—“Mothers of the Light,” Slaryn muttered. “How do you
know that guy?”

I breathed in, stirred, and made a vague gesture.

—“Just saw him, you know, in the street. He’s a real
hoodlum. If it’s true he did something to Yerris, he’ll
pay for it. Bad guys always end up paying. I heard it
from a priest. Tell me, Sla. Why don’t you come to the
Den if your mother is at Carnation?”

The dark elf looked pensive.

—“Oh… I have a gang,” she explained. She ran a hand
through her red hair, as if to snap out of her thoughts,
and leapt to her feet. “Well, I’ve got to go, I’ve got
some business to take care of. Give the old man my
regards. I’ll try to stop by sometime, okay?” She
sketched a smile, and as in the old days, tugged at my
cap and threw: “Ayo, shyur!”

And she left the Evening Park with a quick step. I saw
her red hair disappear, and as I curled up on the bench,
I clutched my knees, feeling more and more worried.
First, there were those two friends of Swift’s who had
been kidnapped. And then I learned that Yerris had been
gone for almost two moons already… And everything was
because of the Ojisaries. But who were the Ojisaries
really? Who was the Black Hawk?

I knew two people who might be able to answer these
questions. I looked around, among the statues, the
fountains, the trees, and the walkers in the park, and
at last, I grumbled:

—“Where on earth are my cronies?”

A diamond for the kap, the devils in the Cats
=============================================

After searching for Manras and Dil all afternoon, I
returned to the Den empty-handed and feeling as if I had
walked Estergat ten times from one end to the other.
When I entered the Cat Quarter, I kept my eyes open in
case I had the misfortune to run into Swift, but
nothing, the streets were quiet, and I reached the Den
without incident.

—“Ayo, Rolg, I am here!” I cried, pushing open the door.

I stood still as I realized that all three chairs around
the table were occupied. Rolg. Korther. And a pale brown
human with half his face horribly burned. Yal had
seemingly not yet returned.

At the startled look of the three Black Daggers, I put
on an apologetic face and swallowed.

—“Can I come in?” I said.

Rolg rolled his eyes.

—“Sure, son, come on in.”

I hesitated, because honestly I would have preferred the
Den not to be so crowded, but I went in anyway, closed
the door, and said:

—“Ayo.”

Korther smiled, and his devilish eyes smiled too.

—“Ayo, lad. We were just about to finish our infusion,
don’t worry. How are you getting on?”

—“Just fine, and you?” I replied.

Korther arched an eyebrow, amused.

—“Couldn’t be better. I don’t think you know Taryo, the
famous thief of the Golden Cat. He lives in Taabia, but
he came here to visit me.” He raised a hand and in a
mocking tone pronounced ceremoniously, “Taryo, let me
introduce him to you: this is Draen, our new
generation.”

Taryo’s burned face remained expressionless. His dark
eyes, however, quickly examined me. He reached out a
hand, took his cup, and swallowed all that was left.

—“We’ve talked enough,” he declared. “We shall meet
again in a moon, Korther. Nice talking with you both.”

I arched an eyebrow and stepped away from the door as
Taryo rose and shook hands with Korther and Rolg. When
the Burned Face left, Korther gestured for me to sit
down, and I sat down, puzzled.

—“Actually, I came here to talk with you,” Korther
admitted.

I saw Rolg stand up and give me a small, quiet smile
before nodding and disappearing into his room, limping
and yawning. I turned to the kap of the Estergat Black
Daggers and scratched my head.

—“And what do you want to talk about?”

—“Yal told me you were working for a nobleman. Is that
true?”

I frowned.

—“It is. But I don’t rob him. He’s a nail-pincher, but
I—”

—“I know, I know,” Korther interrupted. “I’m not going
to ask you to steal from him. This nobleman is a
magician, isn’t he? And you go with him to the
Conservatory every day.”

“Mm-yeah. He’s a student. In two weeks, classes end and
he’ll go home,” I said.

Korther darkened.

—“Um. That doesn’t give you much time. But you’re a
resourceful boy, and I’m sure you already know the
Conservatory layout well, don’t you?”

I opened my eyes wide, finally understanding.

—“Am I going to steal something from the Conservatory?”

Korther nodded.

—“A diamond. A sort of semi-relic. The reward is good:
twenty siatos.”

I was left speechless. That made two thousand nails. And
two hundred cheese snacks.

—“Where is that diamond?” I asked.

Korther smiled.

—“You like to cut to the chase, don’t you? Okay. The
diamond is in Yanaler Koscyri’s hands. She’s a teacher…
and she’s also the Supreme Magician of the Conservatory.
But don’t worry, she doesn’t carry the diamond with her.
She probably keeps it in her office. From what I’ve
heard, it’s easily recognizable: it has sixteen facets,
it’s transparent, and, like I said, it’s a magara. You
know how to recognize a magara, don’t you?”

I nodded thoughtfully.

—“What do you need that diamond for?”

—“Ah…” Korther leaned back in his chair, and his violet
eyes twinkled. “Listen, lad. I hire, you steal. You
don’t need to know any more for now. You are still very
young. When you’re older, if you’re still alive, I might
be more explicit with you. But not now.”

He left his seat under my half-jaded, half-stubborn gaze
and added:

—“Be careful, don’t ask prying questions, and think
before you act. If you get caught, not a word about the
Black Daggers, of course. The Justice of Estergat may be
terrible, but it will never be as terrible as the
Justice of the Black Daggers, you understand?” He looked
me in the eye with a strange intensity and put something
on the table. “In that bag you have wax to make copies
of keys and that sort of thing. If you have any doubts
or need any materials, tell Rolg. Good luck to you. See
you soon, lad.”

He opened the door and walked away, leaving me with a
strange feeling in my body. It wasn’t exactly fear, but
rather apprehension. Because it was one thing to snatch
a few nails from a pocket and another to steal the Wada
from the Stock Exchange or a diamond from the Supreme
Magician of the Conservatory. The point is, I didn’t
want to fall into the hands of the flies.

I stood up and approached the closed door of the old
elf’s room.

—“Rolg?” I said. “Rolg, why does Korther need a diamond
if he has already stolen the Wada?”

I waited silently, convinced that Rolg would not answer
me: he never did when he was in his room, as if there
was a monolith inside that took him to the other side of
the world. This time, however, I heard him say:

—“Korther’s not easy to understand, kid.”

The voice sounded very muffled through the door. I bit
my cheek, shook my head, and yawned.

—“He sure isn’t. Good night, Rolg.”

I lay down, but it was hard to fall asleep, and I heard
Yal come home very late, past midnight. I could tell by
his smell and hesitant walk that he had been out with
friends. So I gave up trying to explain to him the new
job Korther had given me, and burying my head in my
arms, I finally fell asleep. I dreamed of the yarack.
The bird flew over the Rock of Estergat with a shrill
cry and dropped a feather, this time a red one. It fell
and fell, and I ran through the streets, trying not to
lose sight of it. I landed in the Labyrinth and found
myself face to face with Warok, who was crouching down
at that very moment to pick up the red feather. Once in
his hands, it turned into a bloody dagger, and I
shouted:

—“Yerris! Yerris! Yerris!”

—“Draen!”

A hand shook me. I abruptly opened my eyes and found
myself facing Yal’s half-asleep face.

—“What are you yelling for?” he growled, massaging his
head.

I sat up, my heart still racing.

—“It’s Yerris!” I stammered. “The Ojisaries killed him.”

Yal stopped massaging his head and looked at me as if I
had gone mad.

—“What are you talking about? Yerris is gone. He
doesn’t—”

—“But in my dream…!”

—“Dreams are dreams, Mor-eldal,” Yal cut me off with a
gasp.

I took the time to think and sighed with relief.

—“That is true. But Yerris—”

—“Stop worrying about Yerris, sari.”

I looked at him and stifled an offended exclamation.

—“You knew! You knew that Yerris was in Estergat and you
didn’t tell me.”

Yal frowned.

—“You didn’t ask me about him, and I thought learning of
his betrayal would do you no good.”

—“Yerris didn’t betray anyone!”

Yal grimaced painfully and rubbed his forehead.

—“Damn… Yes, he did, sari. He betrayed us. From the
beginning, he was a traitor. Let me explain,” he
muttered. “Yerris is actually an orphan that the Black
Hawk took in to train him from a very young age and
infiltrate him into our brotherhood by making him look
like a kid without a gang. For three years, Yerris acted
as a double agent. Trust me: the Ojisaries won’t hurt
him, he’s one of them. What I’d like to know is how it
is that you knew about the connections he had with…
Mothers of the Light!” He paled and looked at me,
suddenly alarmed. “Don’t tell me that scoundrel took you
with him to the Labyrinth!”

I looked at him indignantly and jumped to my feet.

—“Scoundrel, your mother! Yerris is in danger, Slaryn
said so. He’s guilty of nothing. The scoundrel is Warok.
And the Black Hawk. Not Yerris.”

Yal gave a grunt. He got to his feet, staggered, fell to
his knees in front of the bucket of water, and dipped
his head in. He pulled it out dripping with water.

—“Much better,” he breathed and stood up, tossing his
soaked locks back. His eyes were now fully awake.
“Alright, sari. I don’t know where you got the idea that
Yerris is in danger, but it wouldn’t surprise me if that
were the case: when you play with fire, you end up
getting burned, and when you live in the Labyrinth with
those kind of people, anything can happen to you. And
now, tell me. Are you going to risk your life to help a
guy who gave the Ojisaries all the information he could
find about our brotherhood? He put us all in danger. I’m
not saying it’s his fault: they trained him to spy on
us. He probably didn’t even think he was doing anything
wrong.”

That was false, I thought. More false than the Invisible
Palace. I looked at him with a grim face and blurted
out:

—“Yerris wanted to be a Black Dagger, a real one, but
Warok wouldn’t let him. The Black Cat didn’t want to
betray us. He only did because he was afraid…”

I fell silent, for at that moment, Yal took me roughly
by the shoulders.

—“You knew,” he muttered. “Spirits, you knew!”

He shook me, and stunned as I was, my only reaction was
to look at him, wide-eyed. Yal let go of me right away,
and agitated, he glanced quickly at the old elf’s door
before saying in a low voice:

—“Don’t ever mention that gnome again, okay? And don’t
try to look for him. The Ojisaries are very dangerous,
you understand me? If I catch you entering the
Labyrinth, I’m saying goodbye for good, Mor-eldal. Did
you hear me?”

His eyes stared at me so intently that I turned mine to
the ground.

—“Did you hear me?” Yal insisted.

I nodded.

—“Yes, Elassar.”

I heard him sigh loudly. Then a bell rang, and he patted
me on the shoulder.

—“Devils, it’s half past seven already. You’d better
hurry up and get to the Harp. Come on, don’t look so
downcast. Think of those you call your cronies. They
probably need you more than Yerris does. Now, go.”

He smiled at me, but there was a glint of concern in his
eyes. I breathed in, and though still disturbed by the
conversation, I took my cap, kept the little bag which
Korther had given me the day before, put on my coat and
boots, and at last said:

—“Ayo, Yal, good day.”

I left and ran up the hill; I was crossing the Stone
Park when I remembered that I had not bought the
newspaper.

—“Blasthell,” I muttered.

I turned towards the Central Court, certain to find some
newsboy carrying today’s paper of *“The Estergatese”*.
And, bang, I found Manras and Dil, no more and no less.

—“Where have you been!” I exclaimed, as I approached
them.

They had settled down at the bottom of the stoop that
led to the court. The little dark elf greeted me:

—“Ayo, Sharpy! You’re not going with the magician
anymore?”

—“I am, I am, I am even running late. Is that *“The
Estergatese”*? Well, give me one. I’ll give you the
nails later, I haven’t got any now. Say,” I added as
Manras handed me a copy. “Where were you yesterday? I’ve
been looking for you everywhere.”

I saw Dil make a silent face. Manras explained:

—“Actually, we had to move to another shelter. Now we
live in a real house.”

I frowned.

—“In the Labyrinth?”

—“Natural,” Manras replied.

—“And… with the Black Hawk?” I asked in a low voice.

Manras shook his head.

—“No. I hardly ever see my father.” He handed a
newspaper to a shopper and picked up the nails before
asking me, “Are you going to come this afternoon?”

—“Sure,” I said and held up my newspaper. “See you
later, guys!”

I ran up Imperial Avenue and arrived at the red mansion
a few minutes late, but Miroki Fal always left his room
even later, so he didn’t even notice.

—“Ayo, sir,” I greeted him when I saw him appear on the
stairs. He had taught me that educated people do not say
“ayo”. And I had taught him the opposite.

I took his bag of notes and inkwells and followed him to
the Conservatory at a good pace. As we passed the main
door of the building, the nobleman said to me:

—“I’m going to ask her to the prom,” he said, smiling. I
didn’t need him to specify who he was talking about: it
was Lesabeth, of course. He took out the letter and
handed it to me, “Give me that bag and run, I don’t want
someone else to ask her the same thing and beat me to
it.”

I rolled my eyes and trotted up the stairs to the
healers’ wing. I already knew the place inside out, and
since I knew the blonde elf’s schedule by heart, I found
her in a peace-and-virtue. She was attending a class in
an open classroom. The students were few in number, no
more than ten, and thankfully so, because the teacher
had such a tiny voice that they had to lean towards him
around the table to hear him. The Professor of Endarsic
Energy was short-sighted, half-deaf, and
half-in-the-moon, so he did not even notice my presence.
I crept up to Lesabeth and gave her the letter. With a
curious twinkle in her eye, the elf took the letter and
unfolded it under the table. I was just starting to walk
away when she gave me a:

—“Psst!”

I turned back with a sigh, and she whispered to me:

—“Tell him it’s impossible because I’m already going
with my cousin Jarey.”

I sighed again. This was going to spoil Miroki’s day…
and a bit mine, too, for I didn’t like her cousin Jarey
Edans. Once, in the autumn, he had called me, “You
miserable brat!” And, just a few weeks ago, I had run
into him, and he had tried to take away the bouquet of
flowers intended for Lesabeth. Fortunately, I had good
reflexes, and I had scampered away, not without leaving
some petals on the way.

The Professor of Endarsic Energy continued his litany in
a low, monotonous voice. I walked away and, as usual,
wandered through the corridors. This time, however, I
had a specific objective: the wing where the magicians
resided. I had ventured there more than once, and
forbidden entrance or not, I had been wandering around
the Conservatory for almost eight moons now and no one
had ever told me anything. I was sort of the kid that
everyone passed by and no one really saw.

So I walked fearlessly through the corridors, passed the
white cat of the Supreme Magician, and stopped to say:

—“Meow.”

He didn’t even turn his head: the feline was staring
through the glass at the countless rooftops of Estergat.
The air was particularly limpid that day, and the forest
of the Crypt was clearly visible. I leaned my elbows on
the window sill and said:

—“You know? One day, I’ll go there, even if Yal says
it’s dangerous. There’s probably squirrels there, and
not gray ones like the ones in Stone Park. Black and
brown squirrels like the ones in the valley.”

The white cat continued to ignore me with absolute
smugness. I sighed and reached out a hand to pet him.
Unlike most of the cats in my neighborhood, this one
would let me pet him, and he even deigned to purr
sometimes. I only had to be careful to use my left hand
because he didn’t like the other one: the first time, I
had touched him, he had started to hiss.

A wizard passed in the corridor, and I waited for him to
disappear around a corner before saying:

—“Ayo, the cat. Watch the city, I’ll watch your house.”

*And, while I’m at it, I’ll steal a diamond from your
owner,* I thought. I continued down the corridor until I
came to what I knew to be the apartments of the Supreme
Magician. After making sure that no footsteps could be
heard approaching, I touched the door with my right hand
in search of spells. Unsurprisingly, I found one on the
lock. I would have to deactivate it if I wanted to make
a copy of the key. I spent a long time watching its
pattern, fearing that I might blunder at any time and
unintentionally activate the alarm. Finally, I managed
to deactivate it, made a cast with the wax, inserted it
in the lock, waited patiently, spun around several
times, pricked up my ears, and finally got the cast
back… And instead of activating the alarm again, I cast
a perceptive spell through the crack in the door. My
skill in perception was not very good, and as soon as I
hit something, my spell came undone. I only hoped that I
had not activated any alarm.

—“Be careful, Mor-eldal,” I whispered to myself.

And I reset the alarm on the lock before resuming my
daily walk through the corridors: a little visit to the
janitor’s dog, a big hello to the cook—who, depending on
his mood, would give me a bun, a “get out of here,
rascal,” or a plate of delicious broth—and of course, an
epic descent down the long, shiny ramp of the entrance
hall—I never forgot about that one, it was great fun,
and it reminded me of when my master and I used to
scream down the snowy slope near the Cave on the sledge
he had made for me.

When the eleven bells rang, I was already waiting
quietly outside the door of the Nail-pincher’s
classroom. I had guessed right: when I told him that
Lesabeth would not go to the ball with him, Miroki Fal
clapped his hands to his head.

—“Ancestral demons!” he exclaimed. And he slammed his
fist into the palm of his hand, snarling, “That Jarey
Edans…! I’m sure he did it on purpose.”

—“Come on, Mir, calm down,” his friend Shudi Fiedman
laughed. “It’s only a ball. And Jarey isn’t a suitor:
he’s her cousin. And besides, he’s ugly.”

I laughed at the argument, and the three of us walked
down the stairs to the exit. Miroki was in no mood for
conversation, and in spite of his friend’s wise counsel,
he refused to cheer up, and we walked back to the red
house in silence. I let him go up the stairs with the
slowness of a distressed lover, put the bag on the
table, and hurried into the kitchen. I sniffed the air.

—“What’s for lunch, Rux?” I said.

The butler smiled that gloomy smile which, with the
passage of time, now seemed a little more sympathetic.

—“Leek soup for you, kid.”

—“It’s okay,” I said, seeing that he was about to get
up. “I’ll help myself.”

It was always better to help yourself: Rux, whether he
was stingy, short-sighted, or had a meagre appetite,
would fill the bowls as if he were feeding a little
sparrow. Miroki didn’t complain, but I wasn’t so
conformist. So I helped myself to a full bowl and sat
down with gusto.

—“I don’t have to stay the afternoon, do I?” I asked.

Rux cleared his throat and slid a sheet of paper across
the table.

—“We have to go shopping.”

I huffed.

—“Again? But we already went last week!”

—“Things don’t last forever, kid,” the butler replied.

I sighed, and half an hour later, we were already
walking down the slope towards the Esplanade Market. I
carried two large baskets, Rux another, and he filled
them from stall to stall and from shop to shop. At one
point, I sensed the movement of a hand in one of my
baskets and cried out:

—“Hands off, thief!”

I turned to see a figure running out into the crowd,
carrying nothing more or less than the package of meat
Rux had just bought at the butcher shop. And what’s
worse, I recognized him: it was Draen the Swift. Hearing
my scream, Rux hurried to me, his eyebrows furrowed.

—“What’s going on?”

—“Well… I got robbed,” I confessed, embarrassed. “The
meat was there, and then, fwoosh, it was gone. I saw
someone running out.”

—“A hundred thousand demons… What was he like?” he
inquired.

I shook my head.

—“I don’t know, sir.”

Rux frowned even more and sighed.

—“Well, this won’t please Mr. Fal at all. I’ll go and
buy some more meat. And you, be careful not to have
anything else stolen, or I might well think you are in
league with the thieves, hmm?”

His insinuation hurt me, and I looked at him grimly as
he walked away to the butcher shop. I sat with my back
to the wall with my two baskets and watched my
surroundings warily. If ever Swift dared to show
himself… But he did not show himself, fortunately for
him, and I thought with a sigh that he was probably busy
eating the meat.

After the shopping, Rux made me shell all the beans he
had bought, and it was not until about four o’clock that
I managed to get away before he gave me another task,
and I made my way to the press office. By that time, the
evening paper was about to be distributed, and Manras
and Dil would surely be there. I found them, saw their
abnormally messy clothes and strained faces, and gasped
in shock.

—“Cronies!” I called to them. “What happened to you?”

I joined them, and Manras passed his sleeve over his
eyes.

—“Thunders, what are you crying for, shyur?” I worried.

Since Dil rarely explained things, it was the little
dark elf who finally answered in a trembling voice:

—“They set us up. And they pushed us around. And they
called us names.”

I was outraged.

—“Who did?” Before he answered, I said, “My namesake?”

Manras nodded, and Dil stated darkly:

—“And others. We were near the Esplanade, and they came
straight for us. Swift says he lost two friends because
of Warok.”

Manras added:

—“He told us to tell my brother that, if he stumbles
upon him in the Labyrinth, he will cut his throat.”

I was boiling with indignation. If I could have hissed
like the cats, I would have done so. Hadn’t I warned
Swift that, if he touched my friends, I’d rip his bones
out? Well, I wasn’t going to rip his bones out, but I
was definitely going to do something about it.

Seeing the attentive faces of my friends, I looked
determined and finally said:

—“You guys don’t tell Warok. Swift doesn’t know who he’s
messing with. Warok is dangerous, he could hurt him.
I’ll go talk to my namesake,” I decided. “I’ll make it
clear to him. Just tell me one thing, shyurs: in that
new house you moved to, are there other people living
there besides you and Warok?”

Manras shrugged.

—“Natural. There’s people.”

—“How many?”

—“Well… yesterday, I saw some, I don’t know how many.”

—“Five? Ten?” I ventured.

—“Six,” Dil said.

Six. Oh, dear.

—“Are they armed?”

Dil nodded silently as Manras looked at me strangely and
bit his fingers.

—“And… was there a black half-gnome among them?”

Both of them shook their heads. I didn’t know if I
should feel relieved or just the opposite.

—“Can you show me where the house is?”

Manras nodded.

—“Yeah, but my brother said he doesn’t want visitors, so
you can’t come in.”

—“I won’t go in. I just want to see the house without
your brother seeing me. You know what? I’m going to go
find Swift and explain things clearly to him. Whether I
find him or not, I’ll see you in the Evening Park at ten
o’clock, it runs? And you show me the way to the house.”

Manras agreed, mouth agape.

—“What are you going to say to Swift?”

I smiled and gently tugged on his cap.

—“That no one is allowed to touch my friends, and I’ll
prove it to him,” I affirmed. “Ayo.”

—“Ayo, but be careful, Sharpy, he’s taller than you!”
Manras said.

—“So what, even if he was twenty meters tall, I don’t
care!” I replied.

I left, going uphill. I wandered around for a while on
the Esplanade before I got tired and went back to the
Cat Quarter. I entered the Den and called Rolg. There
was no answer. I left the small bag with the cast of the
Supreme Magician’s key and went out. I startled and
smiled when I saw Rolg appear at the mouth of the alley.

—“Rolg! I left the cast on the table; shall I take it
somewhere?”

The old elf limped over and shook his head.

—“Don’t worry, son, I’ll take care of it. Tomorrow
afternoon, I’ll give you the key. It’ll be ready. Where
are you going?”

—“Selling papers. I haven’t got a nail left. If I’m
late, tell Yal not to worry.”

Rolg arched an eyebrow and nodded.

—“Careful with what you’re doing, kid.”

I nodded vigorously and left. I sold newspapers for two
hours before I found Manras and Dil coming out of a
tavern in Riskel again. I joined them, and we did the
taverns all along the street: I carried the papers while
Manras entered the place with the “last” copy and sold
it with a Benevolent Spirit look. It was already several
hours after dark when we decided that we had sold
enough, took the remaining papers back to the office,
and headed for the Cats. This time, it was not I who was
leading, but they. We entered the Labyrinth, they led me
through narrow corridors, up stairs, across terraces
full of junk, then down a ladder before Manras stopped
and took me by the sleeve.

—“That’s the house.”

He was showing me one door among many in a deserted
passage.

—“It doesn’t look like it, but it’s big, and there are
even places Dil and I aren’t allowed to go,” he revealed
in a low voice.

I turned towards him in the darkness. Up in the sky, the
Moon was shining, but its rays were very faint, and one
could hardly make out the shapes of the objects around.

—“Secret places?” I muttered. “And you didn’t go and
look, not even out of curiosity?”

—“My brother will wring my ears off if I do that,”
Manras whispered, and he added, “Say, you’d better not
stay here, or they’ll catch you.”

I nodded and pushed them both out into the corridor.

—“Have a nice day, shyurs.”

Manras moved away, but Dil hesitated and asked in a
whisper:

—“Sharpy… who’s the black semi-gnome?”

I pouted and replied even more quietly:

—“A friend of mine who used to work for the Black Hawk.
I think Warok played a dirty trick on him. He’s been
missing for two moons.”

Dil hesitated for a few more seconds before walking
away, muttering:

—“Good night, Sharpy.”

I pressed myself against the wall as the door to the
shelter opened, illuminating the corridor. My two
companions entered, and the alley was darkened. After a
few moments, I approached with discretion and listened.
I heard voices, and bravely stuck my ear against the
door.

—“… a misery!” a voice hissed.

—“It’s not our fault, we were robbed!” Manras exclaimed.

I heard a thump and a groan, and I turned pale.

—“Enough of the cheap excuses! You know what? Forget
about the papers. Starting tomorrow, you’re staying
here, and you’re going to work for real. You’ll see, you
incompetent brats.”

I heard a burst of laughter.

—“Say, boy, you’re not going to put your own brother in
the well, are you?”

—“What do you take me for, you scoundrel?”

—“Hey. For what you are: the Black Hawk’s favorite son.
You didn’t mind putting the gnome in there—”

—“Shut the hell up, Lof!”

He said something more, but at that moment, I heard a
bark, and I leapt away from the door. Dogs were
approaching in the alley, led by a tall figure. I took
off running, and the barking increased. I climbed a
gutter as fast as I could and heard a scream below. The
door swung open and a lantern lit up the corridor. I
subtly shrouded my face in shadows and climbed on. This
house was very high. If I fell I would die instantly.

—“Stop! Come down from there, four-legged spider!” a
voice shouted from below.

—“Shut your damn dogs up, Adoya!” Warok bellowed.

Their voices became imprecise and I finally reached the
top. It was a terrace, not a roof. I looked down. There
was nothing to be seen. I took a few steps back from the
edge and glanced at the buildings on the rock. One good
thing about the Labyrinth was that most of the houses
touched each other, so I walked from terrace to terrace
and roof to roof without touching the ground even once
to get out of the heart of the Cats. After I had got
back to the ground, I walked very carefully and did not
relax until I came to the dead end of the Den; and even
then, the thoughts that came to me were of no comfort.

The well, Warok said. What well? Where had he put
Yerris? And what were my two best comrades going to do
now?

—“Blasthell,” I let out.

I should have given them the nails I had, maybe then
Warok wouldn’t have been so angry. But I hadn’t thought
of that.

—“Blasthell,” I repeated.

I went up the stairs and pushed the door open silently.
Everything was dark. Yal was asleep. I took off my cap,
boots, and coat, and lay down quietly. Before I closed
my eyes, I let out once again a quiet:

—“Blasthell.”

Night farewells over some muffins
=================================

For the next few days, I felt more useless than a
squirrel searching an empty log for an acorn. I searched
for Swift, in vain; I searched for Slaryn, in vain; I
spied on the house of my comrades and learned nothing;
and in the end, I circled around like a nakrus looking
for a bone to absorb, and found only scraps. I had no
more success with the Supreme Magician and the diamond.
On the second day, I only managed to enter the room to
find that there was no diamond there. So I made casts of
the other private door locks and slipped into places
that were probably closed for a reason: I saw an alchemy
laboratory full of flasks, a room full of weapons, and a
desk with so many magaras that, saturated with energy
from all sides, I felt ill all afternoon and feared I
had been hit by some evil spell. Fortunately, the next
day, I was already fully recovered. Since I was going
from failure to failure, I also made a copy of the key
to the Conservatory’s kitchen. I didn’t tell Rolg, of
course: supposedly, all these copies of keys were to
contribute to my work, but, uh… well, a thief with a
full belly always worked with more zest, didn’t he?

There was only one day left before school ended, when I
finally came across the right door. I had just delivered
a message to Lesabeth, to which the blonde elf, for the
first time since I knew her, gave a quick answer. She
wrote in Owram, the learned language, so I couldn’t read
it; however, from her expression, I guessed that she
wasn’t sending the Nail-pincher to go chase the clouds,
but quite the opposite.

With the message safely in my pocket, I began my trek
through the corridors. I had two hours to try three
keys. The first was to a brulic laboratory. I went in,
opened the drawers, rummaged around, and came out as
silent as a shadow. The second key was to an office in a
tower, and the room was empty. It was not hard for me to
see why: there was a noxious energy floating in the
room. I walked away from there without looking around
and almost forgot to reactivate the magic locking
mechanism in addition to turning the key. How many
places in the Conservatory had been sealed off after an
experiment gone wrong? Obviously more than one.

The third key led me to the living quarters of a retired
professor who was a friend of the Supreme Magician. I
had seen him several times talking with her, and I
thought: well, why not try this way? And I had made a
cast of the key. After making sure that the corridor was
empty, I entered the room, locked myself in, and… I
discovered that the professor was there, sleeping in his
bed.

For a few seconds, I did not move an inch. Then I
listened to his breathing and said to myself, *Bah, he
looks sound asleep*. So, casting the most sophisticated
silence spells I had ever performed, I slid the drawers
of his desk open. I saw nothing more or less than three
gold coins in one of them. I took them with a cautious
glance at the old Professor. He was still sleeping.

And then my eyes fell on an object lying on the
nightstand, right next to the bed. A transparent object.
I approached it, my eyes wide. Were my eyes deceiving
me, or did this look very much like a diamond? I reached
out with my right hand and took it. I smiled as I felt
the energy vibrating inside. I looked at the pattern and
couldn’t make out anything. It was too complex. I
counted the facets. Sixteen. I counted them again, since
I couldn’t believe it, and when I counted sixteen again,
my smile widened. At last.

A sudden snore made me pale like death. Hastily, I put
the diamond in my pocket, along with the three stolen
siatos, and stepped back without losing sight of the
Professor. I cast a silence spell to open the door as
quietly as I could, locked it and activated the alarm.
As if a ghost had been there, I thought.

I walked away, and in the adjoining corridor, I met the
white cat. I stroked it as I passed and began to sing:

Yeyeyeyeh eh eh eh!\
Oh cat, cat with no colooor!\
With no colooor,\
running through the darkmans on his ghosty feet,\
a gwak cat walks down my street.\
Yeyeyeyeh, eh, eh, eh!\
The gwak cat goes singing\
while the twinkling moon guides his paws.\
He got lost!\
Oh, no!\
Where’s the cat?\
Where’s the cat?\
The cat scurries and chases a white light.\
And he got lost!\
Oh, no!\
Where did the cat go?\
Where did the cat go?


As I sang, I walked down the corridors, turning corners,
towards the classroom where Miroki Fal was studying
diserrance. According to what he had once explained to
me, diserrance was the art of oric forces. Thanks to
them, he was able to levitate; however, when I said to
him “let me see, sir, let me see!”, he refused to show
me. He said that oric energy was dangerous and very
powerful, and he also said that the great experts knew
how to make real monoliths which could transport you
from one place to another almost instantly. I remembered
that my master once told me of a reckless nakrus who had
to cross an entire mountain range to retrieve one of his
arms after losing it along the way. A horror.
Fortunately, people capable of such madness were rare.

I waited patiently for Miroki to come out of the
classroom, and when I saw him appear, I noticed that he
had deep dark circles around his eyes. I had been so
focused on stealing the diamond that I had hardly paid
any attention to the Nail-pincher in the last few days…
And to tell the truth, he had been looking a bit strange
lately, as if he was discouraged; so, determined to
change that, I hurriedly took out the answer from
Lesabeth and handed it to him.

—“It’s from Miss Lesabeth, sir,” I said cheerfully.

To my surprise a melancholy glint passed into Miroki
Fal’s eyes as he read the note. Shudi, the painter, read
over his shoulder and gasped.

—“To the theater? Spirits! That’s better than a ball,
Mir! Rejoice! What’s wrong with you?”

Miroki did not answer. Slowly he put the message in his
pocket and went down the stairs. Shudi went after him,
and putting the Nail-pincher’s bag on my shoulders, I
followed them. As we left the Conservatory, Shudi said:

—“Will you tell me once and for all what’s going on with
you? Lately you’ve been acting weird.”

Miroki Fal sighed heavily.

—“It’s my father. He wants to ruin my life and force me
to marry Amelaida Arym.”

Shudi choked on his saliva.

—“Mothers of the Light… Who?”

—“The daughter of the governor of Taabia!” Miroki
exclaimed. He hit himself on the forehead. “For him,
marriage is a matter of politics. He hasn’t the
slightest idea of what love is. He’s a heartless person.
The only thing he cares about is power! Can you imagine,
Shudi? My life ruined. Lesabeth is my life. Don’t you
understand that?”

—“Uh…yes, I do,” Shudi sighed. “I understand.” He sighed
again and gave him a sympathetic pat on the shoulder.
“Come on, Mir. Don’t be discouraged. Lesabeth is the
daughter of the Satrepas. She’s not from a bad family.
Maybe—”

—“No,” Miroki cut him off with a dull growl. And he
stopped dead in the middle of the park that surrounded
the Conservatory. “Listen, Shudi. I sent a magigram to
my father asking his permission to marry Lesabeth.”

—“What?” Shudi coughed. “But you didn’t even ask her
yet!”

Miroki smiled faintly.

—“I did. I… I met her at… um… at the Hippodrome, last
Sacredday. We walked together in Kamir Wood, and… I
asked for her hand, and she said yes. It was wonderful.”

His face saddened, and he shook his head.

—“But my father answered me the day before yesterday.
And I still haven’t absorbed what he said in his letter.
Anyway, Shudi. Forget about it. This is a family matter.
It’s not worth your while to worry about it.”

His friend glanced at him uneasily as they resumed
walking, and after a silence, he said:

—“Talk to him when you go to Griada. I’m sure if you
talk to him face to face—”

—“Let’s leave it at that, Shudi,” Miroki interrupted
him, exhaling. “Let’s leave it at that.”

The painter did not insist, they parted at a crossroads,
and I followed the Nail-pincher to the red house without
saying a word. I was puzzled by this marriage business.
So, when we entered, I asked:

—“And why don’t you just marry Lesabeth? Wouldn’t it be
simpler?”

Miroki Fal looked at me as if he were looking through a
ghost, and without breaking his absorbed face, he took
out the message from Lesabeth, heaved a heart-rending
sigh and climbed the stairs. Who could understand the
nail-pinchers! I put down the bag, and before Rux could
say anything to me, I was gone like the wind. I took for
granted that, if I didn’t eat there, no one could tell
me that I wasn’t doing my duty.

I ran through the wide streets of the Harp without
removing my hand from my pocket and the diamond. On the
way, I passed three police cavalrymen who were riding
towards the Conservatory at high speed. *They already
know*, I concluded.

I forced myself not to quicken my pace and repeated to
myself one of Yal’s lessons: act natural, act natural…
My tension diminished as I reached Atuerzo and vanished
as I reached the Cats. I saw Fiks, from afar, in the
Grey Square, and I greeted him with a:

—“Ayo, Fiks!”

The old workman, who was chatting with some friends,
turned his head as I was already leaving the square. I
went on down to the Den, and… I noticed that someone was
following me. I stopped dead in front of the small
courtyard of Rolg’s house, turned my head, and opened my
eyes wide.

—“Good mother!” I exclaimed.

It was Adoya, the Ojisary, and he had one of his dogs
with him, who, if he had not been tied with a leash,
would have lunged at me. I heard a sharp bark and
reacted as quickly as a squirrel: I ran into the
courtyard, opened the door, and closed it again in a
cold sweat. I blocked it with the bar and closed the
curtains completely, then…

—“Sari?”

I screamed, jumped up, and turned around. Yal was
sitting on his pallet, sewing up his shirt. Well, he was
half up at that moment. He went to the window, and I
said:

—“No, don’t!”

The dog’s barking could still be heard outside. Yal drew
aside the curtains and frowned.

—“Who is that guy?”

The barking was now fading. I stammered:

—“Dunno. His dog wanted to throw itself at me.”

I didn’t tell him the truth because… well, if I told him
the whole truth, I’d have to admit that I’d entered the
Labyrinth without taking his advice. The barking could
no longer be heard. I breathed a sigh of relief,
retrieved the chair by the door and sat down before I
noticed something strange. What was Yal doing in the Den
at this hour?

—“Not working today?” I asked.

Yal grunted, stepping away from the window.

—“That guy looked really suspicious. If you see him
again, move to another sidewalk.”

—“Yes, yes, and to another street, don’t worry, but what
about the print shop?”

Yal sat back on his pallet with a grimace.

—“I fired myself. The boss kept asking us to work
overtime, and on top of that, he wanted to cut my pay. I
sent him to go chase the clouds.”

I smiled.

—“Well done!”

He rolled his eyes and added, more seriously:

—“Besides, Korther gave me a new job. Nothing too risky,
but… I’ll have to leave Estergat for a while.”

That left me dumbstruck.

—“No!” I protested. “What about me?”

Yal laughed and gave me a mocking look.

—“What do you mean, you? Korther only paid for one place
in the coach, not two, so you stay in Estergat. Besides,
you’ve got a job to do already, as far as I know,” he
said, clearing his throat.

—“Ha! Not any more,” I assured him. And I pulled out my
diamond like a trophy. “I found it in the bedroom of an
old magician who snored through the whole thing, as
simple as that!”

—“Hide it,” Yal snorted, glancing toward the window.
“Wait, no, hand it to me. I’ll give it to Korther before
I leave.”

I gave it to him and sat beside him on the pallet,
watching him finish his sewing.

—“So you’re leaving today? So soon?”

—“The coach leaves tomorrow morning. Bah. The sooner I
leave, the sooner I’ll be back. It’s to deliver an
object to a Black Dagger of Kitra.”

—“And where is that?” I asked.

Yal looked at me as if I had asked him what a tree was.

—“Kitra is the capital of Raiwania, sari.”

I struck my knee.

—“Oh, right! Not long ago, I read an article in the
newspaper about the Great Festival of Kitra. I heard
they put on big shows that bring in people from all over
Prospaterra. That must be impressive. Now, what’s that
thing you have to take with you?”

—“Um,” Yal coughed, amused, “I’m not going to say it.
Ah, do you mind going and buying me a snack? I’m hungry
as a dragon.”

—“So am I,” I ratified, jumping to my feet. And as I saw
that he was handing me some coins, I raised a hand,
solemnly. “No! This time I’m paying.”

I removed the bar from the door, glanced cautiously down
the street, and seeing no dogs, I breathed a sigh of
relief. I went to *The Wind Rose* and asked, bringing my
hands to either side of my mouth:

—“Mr. Innkeeper, two cheese snacks!”

—“Right away, Mister Bard!” he replied, amused. “You
have company today, eh?”

—“A dragon that sings more than I do!” I affirmed,
pointing to my stomach.

The nearest drinkers laughed, and more than one raised
an eyebrow as I placed the gold coin on the counter; a
gwak with a gold coin… was suspicious. But the
tavern-keeper didn’t care where I got the coin. He gave
me the snacks and the change and said:

—“Enjoy your meal, lad.”

I snatched a bite from the snack and walked out of the
tavern very busy chewing. By the time I reached the Den,
I had already finished my share.

—“Devils, is this from *The Wind Rose*?” Yal complained
as he took the snack. “Their bread is drier than dirt. I
always buy everything at *The Ballerinas*. It’s a little
more expensive, but it’s a delight.”

—“Boo, boo. Fussy eater,” I said. “It’s damn good!”

—“Dry as dry wood,” Yal replied, smiling.

—“Nail-pincher,” I called him.

Yal’s smile widened, and he ate the snack without
complaining further. After helping him with his
preparations, which were few, I spent the afternoon with
him playing cards until he said he had to go to the
Hostel. I said goodbye, and hours later, when he came
home and found me lying on my stomach on my pallet,
rereading *Alitard, The Blessed Valley Man, And His Lamb
Destiny*, he gave me a curious look.

—“You didn’t go out?”

I shrugged.

—“Well, no.”

He smiled, dropped into one chair, put the boots on the
other, and said:

—“Korther said he’ll give you the siatos bit by bit,
whenever you ask him for some, and also that, if you
want, you can buy material from him at a discount. He
said that…” He rolled his eyes, mimicking Korther’s
voice, “The lad has substance.”

I smiled and then frowned.

—“How am I gonna go ask him for the goldies if I don’t
know where he lives? I can’t remember where’s the
Hostel.”

—“Mmph. It’s in a dead end next to Bone Street. Korther
doesn’t really live there, but it’s where we talk
business. Rolg will show you if you can’t find it.” He
fiddled with the deck of cards and added, “Now that the
Fal is about to finish the academic year, I suppose
you’ll be out of the job.”

—“Yes, he’s going to Griada in a week,” I replied,
turning a page in my book. “I’ll find another job. Don’t
worry, I already know the tricks. I’m a seasoned Cat.”

Yal did not answer. When I looked up, I saw his
thoughtful expression. I returned to my reading but
barely read a sentence before I looked away again.

—“Elassar.”

—“Mm?”

—“How long will you be staying in Kirta?”

—“Kitra,” he corrected me.

—“That. How long?”

—“I told you, for some time. A moon. What do I know.
It’ll already take over a week to get there by
stagecoach, and I’ll probably be in the city for a
while.”

I bit my lip.

—“Is it dangerous?”

—“Dangerous? You bet. The journey? On the Imperial Road,
there are hardly any bandits anymore. It’s very
controlled,” he assured. “No idea how things are in
Raiwania.”

I straightened up.

—“And in Raiwania, do they speak Drionsan too?” He
nodded, and I stood up, “And there could be monsters
attacking people?”

—“Dragons, red nadres, manticores, harpies… Nothing to
worry about,” he replied in a mocking tone.

I rolled my eyes. I knew he was joking, but his words
helped me make up my mind. I moved closer, took off my
silver pendant, and gave it to him.

—“Here. I know that thing about amulets and charms is
nonsense… but it will protect you from manticores
anyway. And it will bring you luck. It brought me luck
when I traveled through the mountains. Ah, but when you
come back, you’ll give it back, okay?”

Yal looked at me, amused.

—“Well, Mor-eldal, thank you. But I don’t think that…
Well, all right, I’ll take it. Thank you,” he repeated.

I smiled at him.

—“You’re welcome.”

I returned to my pallet, and after seeing that Yal was
putting on the pendant and examining it with curiosity,
I continued reading Alitard and his lamb. Rolg returned
shortly afterwards. I heard him talking outside with the
neighbor across the street; that’s right, for a few
weeks now, we had new neighbors. The beggar from the
ruined house had been evicted, and an old lady had moved
in with her granddaughter and great-grandson after
rehabilitating the house. The newborn child could be
heard screaming almost every night. However, when I saw
Rolg come in with muffins in his hands, I thought that
the grandmother’s generosity more than made up for it.

—“Is that for us?” I asked, excitedly.

Rolg smiled.

—“She wanted to thank us for helping her out last week
with the move. Leave at least one for me to taste, huh?”

He put them on the table, and I took a bite and
exclaimed with pleasure. I rushed to the door, opened
it, and seeing that the old dark elf was by her window
watering flowers, I called out:

—“Thank you, Grandma, they’re very good!”

She replied with a smile and a wave of her hand. I went
back to the table, having already swallowed the rest of
my muffin, and took another. The three of us did not
leave a single crumb, and we sat at the table chatting
the night away. We talked a great deal about Kitra and
Raiwania. Well, let’s say they talked, and I listened,
because I had no idea about history and politics.
Apparently, Arkolda and Raiwania had once been the same
country, but a quarrel had divided them over half a
century ago. Both were parliamentary republics, unlike
the Northern kingdom of Tassia, where moreover not all
races had equal rights. As far as I knew, both republics
looked down on their Tassian neighbors, and I knew of
several songs in which they were called sons of dogs,
tyrants, and infidels, for while Arkolda and Raiwania
worshiped the Daglat and their ancestors, the kingdom of
Tassia worshiped the Goddess of the Rock, and for that
very reason longed to reclaim its ancient possessions on
the Sacred Rock of Estergat.

When Yal caught me yawning, he smiled and said:

—“We’d better get some sleep. Besides, tomorrow I’ll
have to get up very early. If I lose the coach, Korther
will wring my ears off.”

We wished Rolg good night, extinguished the lantern, and
when we were lying down on our couches, I whispered to
Yal:

—“Elassar.”

—“Mm?”

I bit my lip and asked in a low voice:

—“Did Rolg find you too?”

There was a silence. I remembered that the only time I
had asked him how he had become a Black Dagger, long
ago, Yal had evaded the question. And I feared that this
time, too, he would not answer me.

—“No,” Yal said then. He turned on his pallet and
breathed out, “No. He didn’t find me. But he has raised
me since I was ten.”

—“Was it Korther, then?” I asked.

Yal huffed softly.

—“Not either. No. It’s just… my parents were already
Black Daggers. They died trying to find a supposed
treasure hidden in the Valley of Evon-Sil. Greed lost
them,” he muttered.

I almost regretted having been so curious; almost. I
reached out a hand and squeezed his briefly, as if to
prevent him from thinking of sad memories. After a
silence, I whispered:

—“Elassar. You have the necklace, don’t you?”

—“Oh. Of course, sari,” he replied. “I even put it on so
I wouldn’t forget it. I’m not going to lose it.”

I nodded, smiled, scratched my head, and closed my eyes,
gradually slipping into a peaceful sleep.

From scare to scare and from death to hell
==========================================

When I arrived at the Red Mansion, it was still very
early, not even seven o’clock. I had woken up with Yal
and wanted to accompany him to the Gates of Moralion
where he was to take the coach. That is why I arrived so
early, and of course, the door was closed. I was going
round the house, passing through the little garden, and
shuffling along, when I saw the Nail-pincher leaning
against an upstairs window with his head in his hands.
He was paler than death.

—“Mr. Fal?” I said in a worried whisper. I came running
up just below the window. “Are you all right?”

Miroki shook his head, and after a pause, during which I
gave him all sorts of worried looks, he threw something
at me and muttered something so low that I did not hear
him. But I picked up the key and understood that he was
inviting me in. I rolled my eyes, put the key in my
pocket, and climbed over the wall to the window.

—“Spirits, kid,” the Nail-pincher gasped in a weak
voice. “You shouldn’t do that. It’s dangerous—”

—“There’s no fear,” I said, ignoring him and going
inside.

It was the first time I had been in his bedroom. It was
at least three times the size of Rolg’s house, and it
had a canopy bed so wide that six gwaks could have slept
in it. I handed Miroki Fal the key which he had thrown
to me, but he made no move, and seeing him so
unresponsive, I left the key on his desk and said:

—“You look drugged. What’s the matter with you?”

Miroki Fal took a wobbly step and put his hand on a
sheet of paper that was on the writing table. He exhaled
loudly.

—“Listen, kid. This is for Rux. I want you to… tell him
not to tear it up. And to accept it. He deserves it.
This is my will and testament.”

—“Testa… ment?” I repeated. “What’s that?”

Miroki Fal breathed in, shook his head, and, like a sick
man more dead than alive, approached the bed. Very
slowly, he sat down. I glanced at the will with
hostility, and with increasing concern, I took a few
steps towards the Nail-pincher.

—“Mr. Fal, have you been up all night? You look as if a
dragon has been trampling on you. Did you go to the
theatre with Miss Lesabeth?”

Miroki Fal shook his head again and lay awkwardly on his
bed, his breathing so rapid that I felt the tension rise
in me.

—“I went there,” he croaked. “I told her what my father
said. And she sent me to hell.”

He passed a hand over his eyes, and suddenly, to my
amazement, he let out a sob.

—“I am… too miserable. Everything is turning against me.
I can’t take it anymore, kid. I-I-I’m heartbrok-ken,” he
stammered.

I stared at him, bewildered, not knowing how to react to
this. The Nail-pincher was in love, he was about to
finish school, and he was crying!

—“Come closer, Draen,” he continued. In spite of myself
I came closer, and he took me by the wrist with a
strength that surprised me, given his condition. He
whispered, “Sit down, boy. Be like that poor, innocent
child who found Sir Lin the Knight on the battlefield
and listen to my last words. I, Miroki Fal, renounce
this life of prison and solitude. I am alone. I have
never been so alone. Lesabeth has abandoned me. I have
friends, but they too are attached to their families, to
their lineage, and one day, they will become masters who
will stop dreaming. For them, our artistic conversations
will never be more than empty illusions, youthful
fantasies. One day, their parents will ask them to
marry, and they will have no choice. And if I were like
them, I’d marry Amelaida Arym, secure the family’s
future, do business like my father, visit my lands…” His
chest contracted abruptly. “But I don’t want such a
life,” he sobbed. “Then,” he continued, catching his
breath, “this is my choice. My father thinks he can do
with me as he pleases. But he is mistaken. He can do
nothing with a dead son. I hate death,” he muttered.
“But I hate my father more.”

I shuddered with horror, realizing that he was being
dead serious. For a moment, I thought of breaking free
and running away from there. I sat nervously on the edge
of the bed.

—“Mr. Fal,” I said. “Mr. Fal! What are you saying? You
don’t want to die—”

—“I’m already dying,” he cut me off in a whisper. “I’ve
had a nice glass of jaodaria. There’s probably only a
few minutes left for it to really take effect.”

His sob changed to a dull laugh. I had blinked. I knew
what jaodaria was. The plant grew in the valley, and my
Master had taught me to recognize it and avoid it: it
was deadly poisonous. I took a deep breath. I wanted to
cry for help. Maybe guessing it, the Nail-pincher said
calmly:

—“Nothing can help me now. There’s no antidote for
jaodaria.”

I felt my eyes fill with tears. It was all so absurd! A
natural anger came over me.

—“Isturbag!” I insulted him. “Twenty thousand times
isturbag!”

Miroki smiled faintly. His eyes did not glow with
madness but rather with sad resignation.

—“Think of me now and then, boy,” he murmured. And he
took a deep breath. I looked at him in horror as he
exhaled, “I can feel it already. I can feel death
coming. At last. I loved life, Draen. I loved it. If
only I had been born far away from here. If only
everything were not so complicated. If only…”

Gradually his arms began to lose their strength. His
hand let go of my wrist and fell heavily onto the
mattress.

—“Coward,” I stammered. “You’re a damned coward, Mr.
Fal. Why didn’t you tell your father to go pick bones in
a tree? Mr. Fal,” I repeated in a pleading tone.

His breathing became more and more irregular. He was
going to die, I realized. He was going to die for good.

—“Demorjed, a thousand times demorjed… I’ll never
forgive you for that…” I hissed in Caeldric.

Trembling a little, I sat down on the bed and put my
hands on his chest. I concentrated. My nakrus master
said that jaodaria spread slowly through the body, but
nothing could stop it. Except, perhaps, the mortic
energy. He had stopped it once, when I was very small
and very stupid and I was about to die because of one of
those plants. The question was whether I would be able
to do the same. I began to extract the energy from my
bones and passed it on to Miroki. I also modulated his
own mortic energy, turned it into jaypu, and worked to
neutralize the intruding bodies while trying to remember
the lessons of my master. It wasn’t easy at all. I was
neutralizing the lethal particles, but it seemed that
more and more were coming, and at one point, I
despaired:

—“This can’t be happening… He’s going to die on me.
Hell, I’m going to lose the Nail-pincher, Elassar, help
me…”

I continued untiringly until I feared that my energy
stem would eventually be too consumed by so many spells.
I stepped aside, my heart cold and drained. I wasn’t
going to become apathetic for a nail-pincher, however
nice he was. I let all the air out of my lungs and
buried my head in the soft pillow. Now all I wanted to
do was to run away from there. Nothing I had done had
been of any use. I had used up a lot of energy and felt
exhausted. After a long time, I opened my eyes and saw
Miroki’s dead face. I could not contain a sob. I
embraced him and recited in Caeldric:

The Great Death loves us all,\
the living and undead,\
and takes us to its home\
with a gentle embrace.


It took me a while to realize that the nobleman was
still breathing. With some difficulty, but not as much
as before. He was still in a state of
semi-consciousness, but… everything suggested that he
was going to make it. I could not believe it. With a
heart full of hope, I checked my impressions and sighed
at last, truly relieved.

—“You damn crazy nail-pincher,” I grumbled.

I jumped to my feet, and as he blinked in a daze, I
walked to the desk and took the paper of the will.

—“Do you see this paper, Mr. Fal? Do you see it well?”

I tore it before his eyes, and a slight flinch told me
that he had seen what I had done.

—“So now you know, Sir Isturbag,” I said in a dry voice.
“If you want to kill yourself again, you’ll have to get
on your feet first to rewrite the will. And now I’m
leaving, and I’m not coming back, because you’re nuts,
and until you can reason and marry Lesabeth, I’m not
saying ayo anymore.”

I dropped the will, spat on it, and climbed out of the
window before the Nail-pincher could react. I ran like a
gust of wind down the street and quickly left the rich
neighborhoods behind, determined not to return except to
raid them for nails and that’s all. Blasthell. It was
upsetting to have to say goodbye to the Nail-pincher in
this way. Mostly because, deep down, I thought he was a
nice guy. But, spirits, I was not prepared to deal with
such people, with such extravagant ideas. I had saved
his life, he could not complain. He had frightened me
enough as it was that I shouldn’t have to save him a
second time.

I walked along the Esplanade and looked at the people,
filled with a tension that I could not quite shake off.
I sat down in a corner between two empty stalls, put my
arms around my knees and put my head between them.
Gradually I calmed down and banished all thoughts of
nail-pinchers and wizards.

—“Manras and Dil,” I murmured.

Those two, on the other hand… I had to help them. And
Yerris too, wherever that “well” was. And one thing was
clear to me: there was no point in spying on the shelter
night after night. This time, I had to go in. I took a
deep breath. If I had entered the Stock Exchange and the
Conservatory residences, I could also enter the lair of
a Labyrinth gang, couldn’t I?

I looked up and down the Esplanade. It was getting
busier, the shops were opening, and the city of day
labourers was gradually stretching out. I saw a bunch of
kids walking to school with their school bags. And
another group of kids who were dragging their feet, near
the Capitol, waiting for the temple hour to go begging,
panhandling, or “mumping”, as they called it. When I
caught the eye of a passing officer, I snapped out of
it, got up, and moved away. I walked down Tarmil Avenue
and into the Cat Quarter. I went to the Den, but Rolg
was out or perhaps still asleep, so I headed for Bone
Street without his help, intending to ask Korther for a
picklock. Trying to remember some detail, I visited all
the dead ends of the street before choosing the one that
I thought most resembled the one I had seen on that
night. After a moment’s hesitation, I knocked on the
door, took a few steps away and hid behind a barrel. To
my disappointment, no one opened the door. I sighed and
was about to turn back when a hand grabbed me by the
neck.

—“What are you doing here, rascal?”

I stiffened, and as soon as they let go of me, I turned
to run, but then I recognized the face of Alvon, Yerris’
mentor. Well… rather his *former* mentor. He was still
wearing exactly the same clothes, with his blue cape,
his red hat, and his green boots. He definitely didn’t
meet that thieves’ standard of discretion that Yal had
told me about.

—“Sir,” I said. “I’m looking for Korther.”

The terrible look Alvon gave me made me take a step
back.

—“Who are you?”

He hadn’t recognized me, I realized.

—“I am Draen. Yerris’ friend. Don’t you remember me?”

As soon as I said the name of the Black Cat I knew I had
blundered. Alvon grabbed me by the shirt and threw me
out of the dead end, growling:

—“Get out of here! Korther’s not here.”

Regaining my balance, I looked at him with a mixture of
annoyance and apprehension. His closed expression
invited me to step back and leave for good. Damn. With a
mentor like that, it was almost surprising that Yerris
didn’t betray him out of spite. Okay, maybe I was
exaggerating, and besides, the Black Hawk didn’t seem
like a better person at all, but, hell, now I fully
realized how lucky I was to have Yal as my mentor.

Anyway, since I had no picklock, I would have to manage
some other way. I went down the slope and didn’t stop
until I reached an alleyway already deep in the
Labyrinth. There I climbed the irregular front of a
house and jumped onto a balcony, then to another higher
one, and, without caring about the glances I was given
by some Cats sitting on the terraces, I walked along
those until I was just above the corridor of Warok’s
shelter. The sun had not yet risen high enough to
illuminate the richer quarters, but in the Cat Quarter,
the light was coming up with the dawn, and I could see
the clouds spreading in the distance. The clouds from
the southwest were ominous, my master had taught me to
recognize them, and I foresaw that it would soon begin
to rain heavily.

I was not mistaken: it began to rain cats and dogs. I
went down the dead end, made several turns in the area,
took refuge on the threshold of a house, and greeted
some Cat who, having seen me prowling about there every
afternoon, was beginning to know me. It was well into
the morning when I went up again to my terrace, which I
used as a spy tower. The sky was still very dark, but it
was only drizzling, and, wet and muddy as I was, it
couldn’t hurt me much anymore.

I leaned over the edge to observe the dead end when I
heard a noise behind me.

—“If you move, I’ll pierce you,” a voice said.

I froze, wondering what exactly that “pierce you” meant.

—“Turn around,” the voice ordered.

I did as I was told, and the fear rose ten steps at once
when I saw Warok. He was holding a strange device in his
hands. I didn’t know what it was, but it was definitely
dangerous.

—“So, the little Black Dagger wants to go and keep the
big one company, eh?” Warok scoffed. “You’ve been
lurking around us for quite a while. You’re starting to
get on my nerves. Did your brotherhood send you?”

I swallowed and shook my head.

—“What’s that?” I asked, gesturing with my chin towards
the dark elf’s weapon.

He smiled wickedly.

—“Wanna know? It’s a crossbow, shyur. You see the bolt?
Well, if I fire it, it goes through your throat and
kills you. If you run away, it kills you. You
understand?”

I nodded nervously.

—“I’m not going to run away, I swear,” I promised.
“Where did you take Yerris?”

The dark elf shook his head.

—“Do you really want to know, shyur?”

He took a step forward, and I flinched as I saw the bolt
coming closer.

—“You’re scared, huh, shyur?”

His green eyes were watching me as if they were
evaluating me. My gaze went back and forth from his face
to the bolt as I searched for a possible escape. But,
blasthell, how was I going to escape with death looming
over me?

—“You will follow me without making a fuss,” Warok said.
“And that way you can see Yerris. Is that okay with you?
I told you so, shyur,” he added as I silently nodded.
“Only the prudent survive in the Labyrinth.”

This time, I felt the weapon touch my cheek, and I
looked away, clenching my jaw. Inwardly, I was thinking:
don’t kill me, don’t kill me… And my expression surely
showed my silent pleading, because, out of the corner of
my eye, I caught a malicious and mocking glint in
Warok’s eyes.

He guided me with his crossbow down the stairs of the
building. We passed a sleeping man and came to a dead
end. Warok did not open the door where I had seen Manras
and Dil disappear. He opened another one, further back.
He led me inside and pointed the crossbow at me, making
me rush in, fell flat on my face, and scrape my knees.

Once, last fall, when I was selling newspapers, a guy
called me a rowdy rascal and shoved me so hard that he
made me throw all the papers and hit a street lamp.
Since then, I had learned that there were many such
guys, and I had classified them as unsympathetic. Well,
that day, I learned that the unsympathetic were much
less terrible than the heartless.

I was kicked in the ribs, and Warok ordered me:

—“Get up.”

How was I not going to get up with the crossbow aimed at
my head. However, the paralyzing fear was beginning to
give way to a more foolish panic, and I sputtered:

—“Please, Warok, don’t do this. Let me go. Please!”

—“Silence,” he thundered.

He closed the door, put down the crossbow, and grabbed
me by the arm with a look that meant, “don’t even dare
play a trick on me”. I saw him pull out a rope, and he
pressed me against the wall with a firm hand. I thought
of sending him a blast of mortic energy, but what if it
didn’t work? The only time I’d ever done that was to
scare a lynx. I didn’t think Warok would be scared off
by a discharge; instead, he would get angry and end up
using his crossbow. Unless I threw a real shock, a very
strong one, perhaps… Fear overcame reason, and I
gathered as much mortic energy as I could, hoping that
my still recovering energy stem would not burn up
completely. I released the discharge through my hands
that he was binding and heard him make a muffled noise.
He fell upon me. Unconscious? It seemed so. The problem
was that my hands were already tied. Quickly, I passed
them in front, crouched down beside the crossbow, and
pulled the bolt out before opening the door wide and
getting out of there as fast as I could. I reached the
exit of the dead end, clumsily climbed the ladder,
dodged a woman carrying a large basket of clothes, and
ran for it, trying to untie the knot at the same time. I
only succeeded when, already far from the cursed dead
end, I stopped in a corner of the Wool Square and used
the bolt and my teeth alternately to break the rope.
Finally free, I broke the bolt in a rage and ran back to
the Den. Warok knew where I lived. And for that reason,
I had to go home and warn Rolg. I had to tell him that
there were madmen looking for me and… maybe he could
advise me. Maybe the Black Daggers could give me a hand.
I hoped the old man would be home…

I hastily climbed the wooden stairs, pushed open the
door and cried out:

—“Rolg!”

I rushed to the bedroom door and, without thinking about
it, turned the handle and said:

—“Rolg, please, you have to help me!”

To my astonishment, when I pushed it open, the door
opened. And I stood speechless. In the light from the
other room I could see the old elf huddled by the bed.
His face had long black marks that expanded and
contracted sharply, his eyes were red and bright, and
his teeth… his teeth were as sharp as a lynx’s. He
looked to me like one of those old men who had been in
the woods for a long time. It reminded me of one of
those monsters which appeared in the tales of terror in
*“The Gazette”*. And immediately I remembered what my
nakrus master had once told me. He had told me of a
people of mutant sajits whose jaypu was unbridled in
such a way that they were able to transform into…
something very similar to what my eyes were seeing right
now. Drasits, he had called them. And he said that some
sajits called them demons. *‘Many of these demons hate
us more than normal sajits,’* my master had revealed to
me in a storytelling tone. *‘Demons worship life and, to
them, necromancy is the worst aberration in the world.’*
And here I was, face to face with one of them.

But, despite everything… it was still Rolg, wasn’t it?

Don’t tempt the devil
=====================

Looking at Rolg in fascination, I let out all the air in
my lungs and uttered a timid:

—“Rolg?”

Rolg half stood up, as if struggling to straighten up,
he let out a guttural growl and roared:

—“Don’t come near me! Go away… Go away and don’t say
anything or…”

He did not finish his threat; his hands grew darker as
he brought them to his head, his teeth sharpened more,
and I thought I saw his face change shape. He gave
another animal snarl and hissed:

—“Go away and don’t come back!”

With a strange agility, he rushed towards me. I only had
time to open my eyes in terror before Rolg slammed the
door of his room shut. I heard him barricade it from the
inside and thought no more about it: I ran out of there
with the impression of living a nightmare. First, Miroki
Fal and his will, then Warok and his crossbow, and now
Rolg forgot to lock his door, showed me his teeth, and
threw me out!

—“I knew he was hiding something,” I said as I walked up
the street, still shaking a little. “I knew it!”

What I didn’t understand was why he was driving me off
like that. Well, okay, a demon was supposed to be a
horrible creature, one that ordinary sajits didn’t like…
a monster, in the end, as dangerous as a nakrus. And as
a matter of fact, this time, perhaps it was a truly
dangerous being, given the difficulty Rolg seemed to
have in controlling himself. His words still echoed in
my head: go away and don’t come back! And it had to
happen just after Yal had left Estergat. Did Yal know?
Did he know that the elf who had taken him in and housed
him for seven years was what the sajits called a demon?
*A demon*, I repeated to myself, incredulous. That was
all I needed. If demons really hated necromancers,
blasthell, how lucky I’d been not to open my mouth too
much the year before. I only hoped that Yal would make
sure to keep my skeletal hand a secret…

With a loud sigh, I put my right hand to my chest, where
my silver necklace had hung for years, but all I found
was the hurried beating of my heart. No doubt about it:
by removing the pendant, the Spirit of Bad Fortune had
cast the evil eye upon me.

I didn’t know where to go, so I went to *The Wind Rose*.
I approached the counter, sat down on one of the stools
and said:

—“Mr. Tavern-keeper, today’s menu.”

It was already around noon, and the place was full. Many
eyes turned to me in surprise. The tavern-keeper did not
look at me with less surprise, but served me a plate of
porridge with a roll. I paid him and began to eat,
without a word, hearing without listening to the quiet
din of the tavern. I finished, cleaned myself up with my
sleeve and slid off the stool.

—“Hey, kid!” the tavern-keeper called to me, poking his
big bearded head over the counter. “What’s the matter
with you? Aren’t you going to sing something for us
today?”

I shrugged.

—“It’s just… today’s a weird day,” I said.

—“Gosh! Don’t tell me you’re depressed?” the
tavern-keeper worried.

A red-headed guy named Yarras intervened:

—“Even the most seasoned can be sometimes. Come on, kid,
tell us what’s wrong. By any chance, did you get caught
by the flies?”

I shook my head.

—“No, it’s not that.”

I noticed that more than one table was now listening.
They were probably wondering what could have depressed
the daily bard of *The Wind Rose*.

Yarras frowned.

—“I see. Trouble with some gang, huh?”

I grimaced and nodded.

—“Big trouble.”

With the main mystery solved, people turned their
attention back to their meal. After all, what gwak had
never had trouble with some band? However, instead of
losing interest in my case, Yarras motioned for me to
come closer. I did so. This redhead was not born
yesterday. From what I’d heard, he was the defender of
The White One, the matron of the most famous public
house in the Cat Quarter, *The Blue Flame*. In short, it
wasn’t any Cat, and he knew a lot about survival tricks.

—“A pint for the kid,” he said. “It’s my treat,” he
added.

He gave me the jug, and we sat down at a small table in
the distance. Yarras’ eyes were watching me over his own
glass.

—“So? Who do you work for?”

I frowned.

—“For no one.”

Yarras rolled his eyes.

—“But of course. So you’re a loner, you earn goldies,
you eat like a nail-pincher, and you don’t have a band.
Am I right?”

—“You’re damn right,” I said.

—“Mmph. That’s a dangerous position, shyur. And I don’t
really believe it. Don’t you have any friends?”

I bit my lip and nodded silently.

—“Yes, I do. I used to sell newspapers with them. But
not anymore.”

Yarras darkened.

—“Devils. Were they popped off?”

I shook my head.

—“No, no, they’re alive. Or at least… I hope so. But
they’re not letting them out.”

I hesitated, looked into his eyes, and suddenly an
instinctive fear came over me. What if Yarras was an
Ojisary? The sip of beer I had swallowed suddenly seemed
very bitter.

—“Hey, kid,” Yarras said to me. “You feeling all right?”

I swallowed and nodded.

—“Tell me, Yarras,” I whispered. “You’re not an Ojisary,
are you?”

Yarras’ eyes widened.

—“By the beard of the Holy Patron Saint,” he let out in
a whisper. “You’re in trouble with the Ojisaries? Gosh,
that’s what you call rotten luck. This isn’t just any
gang, kid, this is the Black Hawk’s gang.”

I sighed, relieved to know that Yarras, at least, was
not fraternizing with that gang. He looked at me, his
eyes squinting, and leaned over the table.

—“I hear that guy is amassing a fortune. You don’t
happen to know anything about it, do you?”

I shook my head.

—“No. All I know is that these guys took a friend of
mine at the end of the winter. And that they caught two
guys from Swift’s gang a few weeks ago. And that they
almost caught me today.”

Yarras looked at me with interest.

—“You escaped? Well done,” he praised me.

I gave him a pale smile, for I was not yet in the mood
to claim victory. Yerris was still in that “well”; as
for my comrades… who knows where they were.

Yarras looked thoughtful.

—“Tell me. Do you have a safe refuge?”

I winced and shook my head. I didn’t. Not anymore.

—“Mm. Look, the only thing I can do is give you some
advice. Get yourself in a gang. A good one that protects
you. If you keep acting like a lone wolf, I see a very
dark future for you, boy.”

I absorbed the advice and looked at him hopefully.

—“Do you have a gang?” I asked.

Yarras smiled, amused.

—“So to speak. Let’s say it’s more like a network of
friends.” He paused. “You know *The Drawer*?”

—“I know where it is, but I’ve never been there,” I
confessed.

—“Too bad, it’s the best tavern in the Cats, but don’t
tell this one,” he joked, gesturing briefly eloquently
to the tavern-keeper of *The Wind Rose*. “Look, if by
nightfall you don’t find a band, come by that place. I
won’t promise anything, but maybe someone will be
interested in hearing some stories about the Ojisaries.
Such information is worth its weight in gold,” he
whispered with a small smile.

The redhead finished his beer, stood up, and patted me
on the back, causing me to hit the table.

—“Take care of yourself, Bard.”

—“Ayo, Yarras,” I said, catching my breath. I saw him
greet the tavern-keeper and walk out of *The Wind Rose*
with a quiet gait. A few moments later, I finished my
beer, walked past the counter to return the mug, and
threw:

Spirit of Passioooon!\
I’ve been caught by the chains\
of this sweet turtledove.\
Prisoner of mad loooove!


The tavern-keeper laughed.

—“Here comes our real bard back!”

I smiled at him, said ayo, and set off with the same
quiet gait as Yarras, the ruffian of The White One. I
took his advice: I went in search of Slaryn and her
band. I was sure she would accept me. The problem was
that her hideout was probably in the Labyrinth, and it
was even more likely that I wouldn’t find it before
nightfall.

Keeping as far away from Warok’s shelter as possible, I
wandered through the Labyrinth from alley to alley. Most
of the people I passed barely glanced at me, if at all,
but others looked at me so brazenly that I wondered if
Rolg had passed on his black marks to me. However, when
I reached the Wool Square, I glanced into the water of a
large puddle and saw myself as normal. Well, that was
something.

As the hours passed and the sun was about to set, I lost
hope and headed for *The Drawer*.

The Labyrinth was now more agitated. It was teeming with
life. The Cats who went out during the day to earn their
bread were all coming back more or less happily, some in
groups, others alone. Some of the windows were lit with
lights, others were in the dark, but that did not mean
that the houses were empty. I passed the shelters of
some gwak bands who were preparing to sleep, but I did
not dare to approach because… to fall in with a band,
like that, without knowing them at all, could cause me
more trouble than I already had.

I reached the street of the tavern when I heard a noise
behind me and saw a shadow moving. Immediately, I ran to
the door of the tavern, opened it, and closed it behind
me before looking inside. It was not very big, it was
warm, two lanterns were shining, and the tables were all
occupied. There was a hubbub of loud voices; it smelled
of liquor and sweat; and on the tables there were no
silver bets but gold.

My entrance hadn’t attracted much attention, and I
walked up to the counter, biting the nails on my left
hand and looking around. I was looking for Yarras. I
couldn’t find him and spun around several times;
suddenly, the door opened and the redhead appeared.

—“Ayo company!” he said. “Hi, Sham.”

—“Hello, rogue,” the tavern-keeper replied with obvious
affection. He was a dark elf with purple hair, very
light blue eyes, and bluish skin almost as black as
Yerris’. “What can I get you?”

—“Radrasia,” Yarras replied. As he approached, he caught
sight of me and smiled, “Well, well. So you didn’t find
a gang, huh?”

“Do you know the boy?” the tavern-keeper asked. Sure
enough, he had noticed me before, but now he was looking
at me with more interest.

—“Of course I know him,” Yarras said, leaning on the
counter. “That gwak comes by *The Rose* every day, and
sometimes he sings verses to us like the Soshira
Children’s Choir. The kid will make a good town crier,
trust me. Unfortunately, some thorns got in his way, and
I told him to come by here.”

—“What kind of thorns?” an old man inquired.

There was still noise in the little tavern, but not so
much as before. I glanced at the faces, and as Yarras
seemed to be waiting for me to answer, I said:

—“The Ojisaries.”

This time, all fell silent. Yarras smiled.

—“Big thorns. He says the Ojisaries have captured
friends of his. I wonder why they go around capturing
gwaks.”

—“Bah! They must make them fast to send them begging,”
someone suggested. “No need to complicate things and
look for cats’ wings.”

—“They’d have to have quite an army of beggars for the
Black Hawk’s business to be doing so well,” Sham, the
tavern-keeper, replied in a skeptical tone. “I smell a
rat.”

—“Never better said,” Yarras smiled. “And I’m sure our
little guest knows something. They tried to capture him
too. And he escaped.”

Many made a face. And I grimaced too.

—“I don’t know anything,” I said. “I just want to go
save my friends.”

—“And a good fellow, too,” the old man approved who had
spoken before. “Come here, kid. What’s your name?”

—“Draen,” I replied.

—“Draen. Tell me, have you entered Ojisary territory?”

I nodded and heard some murmur and comment praising my
stupid courage.

—“What did you see?” the old man asked.

I shrugged.

—“I don’t know. I’ve been lurking around there for days.
And today… Warok threatened me with a crossbow and said
he was tired of me spying on them, and he made me go
down the corridor and I…” I fell silent, and remembering
that I was not to speak of the mortic discharge, I
shrugged again and concluded, “And I escaped.”

—“While he was holding a crossbow… Impressive,” the old
man said. “What did you say this guy’s name was?”

—“Warok,” I said.

—“Mm. Do you know any others?”

I nodded, and under everyone’s watchful eyes, I
remembered Yarras’ words and said:

—“Information is worth its weight in gold.”

The old man rolled his eyes and pulled a siato from his
pocket.

—“This, if you tell me everything you remember. Runs for
you?”

—“Sure runs,” I said. “Tif, Lof, Adoya. And the Black
Hawk. That’s all the names I know. Tif is a big fellow,
a blond caitian, about eighteen, with a head of
isturbag. Lof, I’ve never seen him. Adoya is a white
human with brown hair, quite tall, with a bunch of bad
dogs. I know there are others, but I only know those.”

The old man looked at me, his face thoughtful.

—“Good. And tell me, how do you know your captured
friends are still alive?”

I turn pale.

—“They are alive,” I said.

—“Yes, but how do you know?” the old man insisted.

I blinked.

—“I… I heard Warok behind the door. I heard him talking
about a well. He even put a companion of his in there.
Warok is a Spirit of Evil. A real one.”

—“A well,” the old man muttered.

—“A well?” repeated an elf who carried more weapons than
teeth. “If he really did put him in a well, that might
be a nice way of saying he killed him.”

I glared at him.

—“He didn’t kill him! Yerris is alive!”

They did not listen to me, and the regulars began to
chatter, and I felt as if I had spoken for nothing. At
least they were interested in the Ojisaries, but I could
see that they were not willing to risk anything to help
me. I picked up the siato without reproach from the old
man, and after seeing Yarras and the tavern-keeper
seated at his table, deep in conjecture as to the
connection between this well and the new wealth of the
Black Hawk, I stepped away, lingered a moment longer in
the tavern, and seeing that no one paid me any
attention, I said to myself: to hell with the gossips. I
opened the door and left.

As soon as I reached the end of the dark and silent
street, I felt my prey instincts rekindle.

—“Hey! Hey, kid!” a voice said behind me. I turned
around, and in the darkness, I saw Yarras approaching.
“Where are you going?”

—“I don’t know,” I confessed.

—“Mm. Well,” the ruffian said to me, “anyway, now you
know: if you have anything interesting, you come over
*The Drawer*, there’s always some curious person ready
to give coins.”

—“But I don’t want coins,” I protested. “What I want is
for the Ojisaries to release my friends and leave me
alone.”

I heard him gently clear his throat.

—“Yes. I know that, kid. Listen,” he said, laying a
fatherly hand on my shoulder. “That you tried to save
your friends proves you’re a good gwak, with a big
heart. True friends are like brothers: you give your
life for them. But… when they’ve given theirs before,
there’s nothing you can do, you understand me? Nothing.
Come on,” he patted my shoulder as my eyes filled with
tears. “Look for a gang and stop lurking around the
Ojisaries. In time, they’ll forget about you. Don’t
tempt the devil.”

When I didn’t say anything, he gently nudged my head,
turned around, and walked back to *The Drawer*. I pulled
a sleeve over my eyes. What Yarras was implying filled
me with horror. Was it possible that this well was, in
fact, just a fancy word for Yerris being spirited away
and never coming back?

As the priests said that good spirits wandered the
world, helping their loved ones, I looked around and
whispered:

—“I’m not crying, Black Cat. I know a Cat doesn’t cry,
let alone a gwak Cat, but you please make sure you
didn’t die for real.”

I swallowed and started walking. I walked away from the
Ojisaries’ territory, down some stairs, and then finally
decided that I had strayed far enough and sought
shelter. I climbed up a house, crossed several terraces,
and finally chose one, lay down, and, exhausted as I
was, fell asleep almost immediately.

Cats and mice
=============

I was awakened by repeated taps on my shoulder.

—“Hey, kid!”

I opened my eyes and saw the face of a woman with a
broom in her hands. She was patting me with it to wake
me up.

—“This is private property. Get out quickly.”

She didn’t say it in a bad tone, and her expression
didn’t look angry, so I nodded without hurrying too
much, got up with a yawn, and stretched.

—“Quickly, I said!” she exclaimed.

—“Yes, ma’am,” I answered.

I walked away to the ladder that descended into a
corridor, and under the watchful eye of the woman, I
left and began to sing:

Oh fine bird, at first light\
you announced your arrival.\
The sun is up! The sun is up!\
The day is born and you were singing.\
The dawn awoke and you were flying\
from flower to flower.\
Oh, heavenly songster!


I spent the whole morning walking. I could not find
Slaryn, so I went to the Black Quarter with its
labyrinthine hovels, and then I crossed the Black Bridge
and for the first time entered Menshaldra, the city of
the boatmen. I discovered a new world full of barges,
ropes, and the smell of fish. I shuffled along, watching
strong men carrying huge loads to the barges, and I saw
a child my age shouting at the top of his lungs to say
something to his father at the other end of the boat.
About noon, I bought myself a meal in a tavern in the
harbour, and after listening to some old sailor’s
exaggerated stories of some monster he had slain in his
youth, I crossed the Valiant Bridge back to the main
bank of Menshaldra, and my eyes fell upon the distant
forest of the Crypt. The prospect of entering it gave me
wings, and I took off at a run, cutting across the
fields, crossing the White Way, and reaching the edge in
half an hour. I inspected the trunks with caution.

Hadn’t Yal said that the forest was the property of the
Fal? Since I had saved his life, Miroki could not
complain if I entered his territory. Especially since,
in practice, a forest could only be the territory of
those who occupied it, like foxes, squirrels, insects,
and… maybe red nadres. I shrugged. I had lived in more
dangerous forests than this.

So I went in without further hesitation, and tried not
to lose my sense of direction, for from the Peak, I
could tell that the Crypt was no small wood. First, I
climbed the hill to the top of the Ravines, and beyond
the last trunks, I could see the Rock of Estergat, the
river, and just below, the mine buildings, and the
quarry. I turned my back on it all and went deeper into
the forest.

The trees were not the same as those in the valley: they
were smaller, but also bulkier and gnarlier. The truth
is that the forest delighted me. I found a trunk several
meters wide, and I could not resist the temptation to
climb it. I found myself face to face with a black
squirrel and smiled happily at him with all my teeth. I
saw him disappear, quick as a flash.

—“Ayo, ayo!” I said.

I continued to climb a large branch, and then I lay
there, huddled in the heart of the tree, and took a
refreshing nap such as I had not taken for a long time.
When I woke up, the sound of birds, insects, and leaves
was so familiar that I thought I was back in the valley.
Except that I was not in the valley, but in the Crypt,
only a few miles from the capital of Arkolda.

I didn’t know what time it was, and as I thought about
it, I realized that before, only a year ago, I had never
given a thought to what time I was living. To tell the
truth, I didn’t care much about it now either, but with
the temples ringing the bells every half hour, it was
hard not to give it some importance.

I got down from the tree, and instead of going back, I
continued to explore. I recognized some of the plants
which my master had taught me, but most of them were
unknown to me. I came to a flowery clearing and spent
the last hours of the day doing what I had done all my
life: climbing nearby trees, eating some known insect,
carving the head of a lynx on a stick I had collected,
and occasionally looking up and watching the clouds go
by. At one point, I thought I recognized the smiling,
cadaverous skull of my master, and I stared at it until
the cloud changed into a kind of mushroom.

I spent the night in the big tree where I had napped. It
was there that I had seen the first squirrel in the
forest, and it was there that I felt most secure. I
thought a great deal that night, and as I could not
sleep, I climbed to the top of the tree and looked at
the stars. I could see them very clearly in the midst of
the great black veil.

—“Elassar,” I whispered. “Did you really want me to see
this? Estergat is wonderful, but…”

But it was also full of dangers, I added inwardly. My
nakrus master wanted me to make friends that had two
legs and two hands, and I had not failed to do so.
However, now that I knew the sajit world, I felt so
attached to it that I could not see myself leaving
Estergat even if a ferilompard bone was placed in my
hands. If leaving my master had been very hard, it would
now be very difficult for me to have to part with Yal,
my friends, and this happy whirlwind that was Estergat.

—“You knew,” I said, my eyes looking up at the stars.
“You knew you were sending me away for a long time, and
you didn’t tell me.”

I breathed in and thought that, deep down, I had always
known. Except that a year ago I was just a kid, and now
I was almost eleven, I had learned to reason, and most
importantly, I had learned to change my destiny and find
what I wanted. And I didn’t want to leave my friends
behind. Manras and Dil deserved more.

With this assurance in mind, I returned to the heart of
the tree, slept like a lebrine bear and, at dawn, headed
for Estergat with a new carved stick in hand and a
conqueror’s gait.

I went straight to the Labyrinth. I crossed the Moon
Bridge, drank water in the adjoining square, and went on
up to the Cat Quarter. I came to the dead end of the
Ojisaries, holding my staff and with the bearing of that
Mad Magician Hero of whom Miroki Fal had spoken more
than once. I wrapped myself in harmonic shadows and
pushed open the door through which I had seen Manras and
Dil enter the other day. It was locked. I smashed the
window with my staff, and despite my silence spell, the
shattering was, to my taste, quite loud. I did not
worry. I crept inside and whispered:

—“Manras! Dil!”

I cast a harmonic light spell and… found myself facing
the unfamiliar face of a short, dark-haired human who
looked at me with dazed, blinking eyes. There was no one
else in the room.

—“Blasthell,” I let out.

I made my light as dazzling as possible, leapt onto the
windowsill, and ran out of there like a wild hare. I
heard a scream behind me, but by the time the Ojisaries
heard what had happened, I was long gone.

I stopped once outside the Cats, near the Wild Garden.
Then, catching my breath, I walked along the bank of the
river. Another failure, I thought. But it could have
been worse. Much worse.

I laughed.

The Ojisaries were beginning to have good reasons to
want to tear my bones out. And I was going to give them
plenty more, I thought decisively. Maybe I couldn’t get
Yerris out of the well, but I was going to teach those
sons of bad mothers that no one could get up the
Survivor’s nose.

However, I needed reinforcements. I thought I knew where
to find them. About noon, after buying myself a not
exactly cheap meal in a tavern in Riskel, I passed
through the Esplanade, took an eagle-eye look around,
and seeing no one of interest, I returned to the Cat
Quarter, to Wool Square. There, I saw a bunch of gwaks
around my age sitting in a corner, and I approached
them. I guessed their half-curious, half-wary faces. I
stuck my stick in the ground and said:

—“Ayo.” Some answered me with a brief, inquisitive nod.
I resumed, “I’m looking for Swift. Do you know him?”

One of them, with black curly hair, stood up slowly.

—“The name sounds familiar. Why are you looking for
him?”

—“I want to talk to him. Don’t you know where I can find
him?” The gwak shook his head, and after an intense
hesitation, I said, “If you help me, I’ll give you a
goldy.”

The boy’s eyes shone more with suspicion than lust. He
replied with a haughty expression:

—“I ain’t one to sell people for goldies.”

—“What? But who’s talking about selling?” I exasperated.
“I just want to talk with him.”

The boy shook his head, turned his back on me, and sat
down again with his companions. I sighed under their
looks that explicitly said: get out. I got the hell out.
Sometimes, it wasn’t easy to communicate with the other
gwaks. Either they hugged you and taught you a thousand
things, or they ignored you and were suspicious. I
realized that talking about goldies had not been a very
clever tactic on my part. We gwaks may have been
scoundrels, thieves, profiteers, and rogues, but we had
dignity, and none of us could be bribed just like that.

Well, as my nakrus master used to say, it was impossible
to never make mistakes. But, as he also said, that was
no reason to accumulate them. And yet, I would do just
the opposite in the valley…

I was walking down an alley, deep in thought, when I
caught a red shadow out of the corner of my eye, passing
by the end of the passageway, and I jumped, startled.

—“Sla!” I cried.

I started to run, turned the corner and braked suddenly
when I saw the figure in the red cape walking away. It
was a cloak, not hair. Disappointed, I shuffled my feet
and the stick, tracing a zigzagging path through the
muddy passage. I was just leaving the neighborhood,
following the Timid River as it cascaded down to the
Estergat River, when I saw a gang of four youths appear.
They were heading straight for me. I recognized Swift,
and, not sure whether to be pleased to see him or
frightened by such an intimidating approach, I stopped
and waited for him and his three companions to join me.

—“Ayo,” he said.

—“Ayo,” I replied.

The red-haired elf crossed his arms and looked me up and
down.

—“I was told you were looking for me. What’s the matter,
namesake? You want to settle the score or come clean?”

—“Neither,” I replied. “I just want to know if you’ve
heard from your friends who’ve gone missing.”

Swift looked at me darkly.

—“Don’t be a scoundrel. You know what happened to them.
They killed them. It’s clear.”

—“I don’t know what happened to them,” I growled. “And I
want to know. The Ojisaries are devils. This morning, I
broke a window at their place,” I informed him. “And
tonight I’m gonna make them madder. If you want to help
me… that would be good.”

In his eyes, I saw surprise, disbelief, and then a
glimmer of respect mixed with mockery.

—“You’re a fool if you think I’m gonna get into Ojisary
territory for revenge,” he said finally. “I’m not going
to risk my life to get some dead bodies. Understand me,
shyur, in the Cats, there are kittens like us and lions
like that gang, or Frashluc’s. And now, Sharpy, if you
were planning to set a trap for me so the Ojisaries
would get me too, you’ve failed miserably.”

I looked at him, bewildered and deeply hurt.

—“Blasthell, but what are you saying?” I exclaimed. “I
don’t set no traps, you hear me? I’m honest.”

My namesake made an apologetic face.

—“Maybe you’re right. But you’re gonna give me your
goldy anyway, shyur. For the trouble.”

I took a breath and made another mistake: I gave him the
coin too quickly. This, no doubt, made it clear that I
had more. Instead of taking the coin, he grabbed my
wrist and with his other hand struck me on the arm which
held the stick. He disarmed me, one of his companions
took all the money from my pocket, and I protested:

—“That’s not fair, namesake. I’m a good gwak,” I assured
with impotent rage.

Swift retrieved all the money and, without letting go of
my wrist, he said quietly:

—“So am I, shyur. And brothers do favors for each other.
I’ve crossed half the Cats to see you. And you reward me
for it. It’s only fair.”

He took a ten-nail coin and put it in the palm of my
hand.

—“So you don’t get discouraged,” he said. “A word of
advice: give up on the Ojisaries, whether you’re with
them or against them, it’s all the same. I say that as a
friend. Ayo, namesake.”

He put his hand in his pocket, as if counting the coins
by touch, stepped back, turned around, and left with
obvious satisfaction, followed by his companions. And on
top of that, he took my stick with him.

—“Bloody scoundrel…” I muttered.

The only thing they had left me, besides the rhombus
silver coin, was the sharpened stone. I caught the
curious glance of a passer-by, who must have been
watching the whole thing, and I glared at him as if to
say “Whatcha lookin’ at?”, and I returned to the heart
of the Cat Quarter. This time, I really didn’t know what
to do. Continue to pester the Ojisaries? They would
eventually catch me. But if only I could find out where
they had taken Manras and Dil… Warok had said he was
going to make them work harder. But work like what and
where?

I entered the dead end, hid behind a pile of baskets,
and pondered. I remembered the words of my nakrus
master: courage and bravery. And a mixture of excitement
and terror came over me little by little as my
resolution grew stronger.


%%%


I crouched behind a barrel, shrouded in harmonic
shadows. A man was standing guard in the dead end.
Clearly, after my visits, they were on guard. I looked
up at the terraces, but saw no one. Which didn’t mean
anything.

My plan was simple and already half accomplished. First,
I had torn a piece of paper from an old abandoned
newspaper, sat down on a terrace in the Cats with a
small pile of coal, and for the first time in my life, I
had written a letter. Well, more like a sentence. And I
hoped it would be understandable, because I hadn’t dared
to come back to the Den to get my Alitard book and
compare the signs. My sentence read: *«Deliver the Black
Cat at Wool Square or I’ll tell the Black Daggers
everything.»* Which may have, in fact, turned out to be
something closer to, “Give Black Cat to Wool Square or
talk black daggers”. Or maybe something worse. That’s
why, just in case, I had drawn a black cat, a sheep with
lots of wool, a mouth, and a black dagger. It was much
more understandable.

In reality, my plan could be a fiasco. Especially if,
upon reading the sentence, the Ojisaries thought: and
what the hell is this brat going to tell the Black
Daggers? But it would be even worse if they said: what
the hell are these scribbles?

The Ojisaries could also appear in the Wool Square
without the Black Cat. That was most likely. But, then,
if only one came, maybe I could talk with him, convince
him to tell me… I don’t know, *something*. At least that
Yerris and my newspaper comrades were okay.

As the lookout turned his back to me and chewed his
smograss leaf, I very quietly put the piece of paper on
the barrel. I picked up a pebble from the ground and
placed it on top. I only hoped that it would not rain
during the night. I turned around and went to sleep in
the Wool Square. I was not the only gwak to settle
there, though I was one of the few who did so alone. I
lay down as comfortably as I could on my earthen bed and
fell deeply asleep.

I woke up when someone crushed my ribs with his boot and
shone a light in my face.

—“It’s him.”

I recognized the voice. It was Warok’s. Something cold
touched my throat, and I swallowed as I realized what it
was.

—“If you make a sound, I’ll bleed you,” Warok warned me
in a whisper.

He made me stand up and did not move his dagger away for
a moment. I said nothing. I was too scared. How in the
world could the Ojisaries dare to threaten me in the
Wool Square, in the midst of so many gwaks? I must say
that I hadn’t even imagined such a cruel possibility.
And I felt a little stupid.

I could see the eyes of the gwaks on us. They pretended
to be asleep, but I could feel that they were not all
asleep. Warok pulled me away from the wall against which
I had been lying and forced me to jerk forward across
the square. Three others accompanied us. Two went in
front, and one behind. They had not skimped on the
escort this time. I looked for an escape route. And I
saw none the whole way.

We reached the dead end without a word. Warok took me
into the same room where he had taken me two days
before. One of the hooded men who had accompanied Warok,
the shorter one, took something out of his pocket, while
a newcomer, also wearing a mask, lit a lantern and said:

—“Hold on a sec.”

He brought the lantern so close that I shut my eyes, and
feeling the blade of the dagger press more firmly
against my neck, I gave a terrified groan.

—“That’s enough, Warok,” growled the one with the
lantern. “Let go of him.”

There was a silence, and the voice of the lantern bearer
turned cold as he repeated:

—“Let go of him.”

Warok hissed in my ear:

—“I’ll settle accounts with you later.”

And he let go of me. I stood still for a few seconds,
and then I backed away from the five figures and hit the
wall. In my mind, I saw the green ink on Warok’s shirt,
the mortic discharge, the broken window, and the message
I had left, and I thought, Spirits, with all the tricks
I had played on them, how could there be any chance that
those lunatics would let me live? Seeing my time coming,
my survival instinct drove away my dignity, and I
cowered on the floor, wanting to tell them that way that
I was only a child, that I was no threat, that they
would please have mercy on me.

—“Get up,” growled the man with the lantern.

I stood up, my eyes flooded with tears.

—“Do you know the kap of the Black Daggers?” the Ojisary
asked.

I swallowed my tears and stammered:

—“Yes, sir.”

—“Korther, isn’t it? You people call him Korther.”

—“Yes, sir,” I repeated.

—“What did you tell Korther about us?” the Ojisary
inquired.

I shook my head.

—“Nothing, sir. I swear. I haven’t seen him for weeks.
I’ve only seen him three times in all. I hardly know
him. I don’t know nothing. I left the message because I
wanted to know if Yerris was okay…” I sobbed.

There was silence. And one of the other two commented:

—“He’s a Black Dagger. Perhaps it would be less risky to
ransom him.”

The one with the lantern snorted.

—“Ransom? You’re kidding yourself, Lof, if you think
Korther’s going to pay a dime for a brat who risks his
life to save a traitor to his brotherhood.” He paused,
and despite the light still blinding me a little, I
thought I saw a glint in his dark eyes. He raised an arm
and placed something in Lof’s hand. “Take him away and
don’t let me see him again. Don’t worry, kid. You ain’t
gonna die.”

—“Then we’ll have to ask the alchemist to make one more
dose a day, I suppose,” Lof commented, clearing his
throat.

—“And he’ll make a lot more if I ask him to,” the one
with the lantern replied. “Take him away, I said.”

He left the lantern with the other Ojisary, turned and
walked out. After a brief silence, Lof stepped forward,
and I began to tremble even more.

—“Swallow this,” he ordered me. “Swallow.”

I swallowed without chewing. Immediately, Lof went to
search me and found that my pockets were completely
empty except for a ten nail coin and my sharpened stone.
He took off my boots and examined them as if he were
looking for something.

—“Where is the magara you used against Warok?” he asked
at last.

I was feeling a strange effect come over me, and it took
me a while to figure out what Lof meant with his
question. They thought I had used a magara to knock
Warok unconscious when I escaped last time.

I stammered and lied:

—“It broke. I threw it away.”

They believed me, I think. But the glare in Warok’s eyes
was no less criminal for all that. A deep torpor came
over me, and I staggered, stuttering:

—“Spirits, what are you guys going to do to me?”

I fell heavily to the ground, feeling as if the world
was turning into a black whirlpool that was dragging me
far, far away. The last thing I heard was Warok’s
neutral voice saying:

—“I’ll take him to the well.”

A Priest in the Well
====================

When I awoke, the first thing that struck me was the
suffocating heat, as if I had been put in an oven. I
heard breathing, murmuring, and a coughing fit. Finally,
I felt a sharp pain in my left arm. It felt as if it had
been broken. I opened my eyes. And for a long time I
just stared at the stalactites hanging from the rocky
ceiling. A strange energy floated in the air and
enveloped me in a dizzying mantle.

At last, I sat up and blinked, half-fainting, feeling as
if I had rolled down a flight of stairs. I was sore all
over. Someone had taken my shirt off, and I could
clearly see the marks of the blows. I had little doubt
as to who had done this barbarism: it could only have
been Warok. Well, at least he had left me alive… hadn’t
he?

My eyes wandered around. I was in a cave. There were
turrets of rock going up and others going down. I was
standing on a sort of wooden platform, and near me there
were people. They were all children, older or younger.
There were about twenty of them and most of them were
asleep. Strangely enough, in the cave there was a faint
light which seemed to come from… I turned my head. From
another cave?

A shadow blocked my view, and I slowly looked up at a
smiling human face. It was missing a tooth.

—“Welcome to the Well, shyur,” he said. And, as I looked
at him, dazed, he added, “Ayo.”

I nodded slightly, dazed.

—“Ayo.”

The boy smiled wider at me.

—“You look more awake than most. What’s your name?”

I rubbed my face. Why did I feel so tired? I said:

—“Draen.”

—“Just Draen?”

—“Draen the Sharpy.”

—“Ah,” the boy smiled. “I’m Rogan. Rogan the Priest. If
you don’t mind, I’ll keep your shirt. It’s a little
tight on me, but the one I had on was about to go back
to the spirit world.”

I saw that he was pointing to some tattered clothes—a
simple rag would have been more substantial than those.
I shrugged. My shirt was the least of my concerns. It
was deadly hot in that cave anyway.

—“They beat the shit out of you,” Rogan added.

I huffed and puffed and confessed:

—“I’m aching all over.”

—“No wonder. These guys are crazy as hell.” He gestured
eloquently with his index finger to his temple. “The
more you mess with them, the more they beat you up.
Fortunately, no one here dares to stay long, because of
the white foam. They get the beads, give us food, and
then get out of here.”

I followed the direction of his gaze and came to see a
large gate with strong bars. Beyond it, all was dark.
Rogan crouched beside me.

—“How old are you, shyur?”

—“Almost eleven,” I replied.

Rogan arched an eyebrow.

—“Really? I would’ve sworn you were around nine.”

I rolled my eyes and curled up, resting my forehead on
my knees. I felt terrible. A hand patted my shoulder,
and I winced in pain.

—“Oops. Sorry, shyur,” Rogan said. “I wanted to tell you
not to worry. It’s normal for you to feel drained: it
happens to all of us at first. Like I said, it’s this
cave that’s enchanted because of the foam. With time,
you get used to it. It goes like this: the first two
days, you just watch, then the next two days you pick up
one pearl, then two for five days, and after that, like
any spirit, you have to bring back three a day before
the bong rings!”

I lifted my head, looked at him, and suddenly thinking
of Yerris, I turned to the others, looking for him.

—“Where is Yerris?” I asked.

Rogan gave me a surprised look.

—“Yerris? Who’s Yerris?”

I rose awkwardly to my feet, and under the exhausted
eyes of the awakened gwaks, I walked barefoot on the
platform. I examined them all. And finally, a terrible
disappointment came over me. Yerris was not there.

—“They killed him,” I stammered.

Rogan had approached cautiously, looking worried.

—“Did they capture you with a friend?” he inquired.

I shook my head.

—“No. They caught the Black Cat a long time ago.”

Rogan suddenly seemed to understand.

—“Ah! You mean the Black Cat, the Mysterious Vagrant.
But are you really a friend of his? That’s news. This
Cat is always looking for who knows what in the white
foam. He spends hours wandering around this hellhole
like it doesn’t affect him. You always think he’s dead,
and he always comes back. Dart!” He exclaimed when he
saw me staggering. He caught me and helped me to lie
down. “There, there, you don’t want to faint on your
feet, do you? Come on, don’t think about anything else
and go to sleep. The meal will come soon.”

I shook my head and muttered with deep relief:

—“Yerris is alive.”

I let out a long sigh. Yerris, the Black Cat, the
harmonica player, my street mentor, he was alive! I
smiled, and for a moment, I turned my attention to the
energy that was vibrating around me. I had an epiphany,
and suddenly I knew where this feeling of exhaustion was
coming from, as if a leech was sucking the life out of
me. It was the energy of this cave that was gradually
absorbing my jaypu. Without much difficulty, I
transformed a strand of morjas into inner energy, and I
felt myself come back to life.

I sat up again.

—“You’re a stubborn one,” Rogan snorted.

I scratched my head and realized that my cap had been
taken off as well. Bah. It was of no use to me where I
was, anyway. I turned to Rogan, who was watching me with
a half-serious, half-distracted pout.

—“How long have you been here?” I asked.

Rogan huffed.

—“Spirits, what a question… Well, I think twenty-eight
bongs. That is, twenty-eight days, probably. Most of the
ones sleeping are newbies,” he added, gesturing vaguely
to the gwaks lying on the boards. “You know? Right now,
I think you’re the only one I’ve seen getting up and
staying up with his eyes open this long after his first
awakening. Don’t tell me you have dragon blood in your
veins?”

I smiled slightly.

—“Ah, who knows. What was that about pearls?”

Rogan looked at me thoughtfully, and then he stood up.

—“Follow me and see.”

I followed him to the end of the cave, to another about
the same size, from which all the light which
illuminated the first came. It was a blinding light,
white and unearthly. I sneezed, and after blinking for a
moment, I could see the foam of which Rogan had spoken.
A few feet away from me was a tunnel flooded with light.
And to my left there was another, and a third a bit
further…

—“Looks like milk, doesn’t it?” Rogan said. “It’s like
being in the Tunnels of Light. The only difference is
that, instead of Spirits of Light, we’re gwaks doomed to
be exploited by our hunters to death. We mine pearls
from these tunnels and give them to our captors. May
they rot and their spirits remain trapped in nothingness
forever,” he declared.

The absorbing energy was even denser there, and I was
overcome with fear as I realized that it was dragging my
jaypu with its claws. I backed up to the entrance of the
other cavern and then croaked:

—“It’s horrible.”

Rogan shrugged.

—“You’ll get used to it, shyur. You even get used to the
heat. I can’t help but think: if only they could have
captured me at the beginning of winter!” he joked. He
shook his head, became more serious again, and led me
back to the platform, saying, “It’s warmer here than
when you’ve got the fire in the fireplace burning
beneath you. And I say that from experience. Do you know
I once nearly burst into flames in a fireplace?”

I looked at him in disbelief.

—“For real?”

—“For real and in Drionsan.” He stopped midway to tell
me, “I was a Charity kid in the temple. And I was a
chimney sweep. I almost died of suffocation more than
once, I swear. Then my master died. The new one was a
real scrooge, he was starving me to death!” With a face
that could not be more serious, he added: “One night, I
received a visit from my ancestors. They said to me,
‘So, Rogan! You, who are so learned and intelligent, are
going to let this glutton gorge himself while you
starve?’ You can imagine how frightened I was when I saw
them appear before me, because I don’t even know my
ancestors, but they found me. Unbeliever the one who
doesn’t believe me! And I saw them very clearly, I
swear,” he assured me. I looked at him, smiling, both
amused and fascinated by his story. The Priest held up
both hands and concluded, “I took the advice, of course:
how could I stand against my ancestors? So I turned in
my brush, fled to the Cats, and changed profession.”

He didn’t specify which one, but I guessed it anyway.
The daily life of the gwak was a “beg-or-bite, satiate
your hunger, do not get caught”. A hectic profession to
which children of all styles and characters aspired, but
it had to be said that this Priest seemed to be a
singular gwak.

I noticed the silence and understood that he was waiting
for me to speak and perhaps to show my trust too by
telling him about myself. I hesitated and finally said:

—“I come from the valley. From the valley of Evon-Sil.
But I’ve been in Estergat for a year.”

The chimney sweep got a dreamy look on his face.

—“Dart. I’ve always wanted to get out of this dunghill
and go on an adventure in the mountains. They say that
the anchorites go into the valley to communicate with
the Spirits of the Sun. That’s what a priest told me,
but he wasn’t even close to being an anchorite,” he
laughed, pretending to prop up a huge belly. He sighed.
“Blessed are those who worship the spirits and feed the
body as well as the soul. No matter how much I worship
them day and night, they won’t even give me a bite.
There must be a reason! A jailer at Carnation once told
me: you wretched bastard, gwaks like you, disinherited
people with no ancestors and no name, are better off
spirited than alive; when you are spirits, at least you
help the unfortunate; on the other hand, when you are
alive, you are a scourge to society, worse than bedbugs
and fleas!” he proclaimed, waving an accusing index
finger. And he sighed again. “Boo. That jailer would be
quite happy, now, if he saw me in this hell.” He paused
meditatively and said, changing his tone, “I’m going to
get a drink of water. Are you thirsty?”

—“Ragingly, very,” I said.

—“Then come with me.”

I followed him to what turned out to be a small natural
spring filled with hot water. That is where the
suffocating mist that hung in the air came from. I drank
deeply. I had the impression to be sweating like crazy
in this cavern of fire.

—“We’re inside the Rock, right?” I asked.

Rogan sat on the ledge and gazed up at the ceiling and
its stalactites with an absorbed expression. I heard him
inhale and exhale slowly as he nodded.

—“That’s what it looks like. Unless we’ve been sent
straight to hell and think we’re alive when in reality
we’re already dead.”

I winced at his answer, for it was not particularly
optimistic. I sat down on the platform as far as I could
from the opening of the other cavern and spent a long
time studying the walls as if I hoped to find some
secret door. At last, I resigned myself to talking with
Rogan about things which had nothing to do with our
present situation. He went on and on about his
incredible adventures, most of which I felt had little
truth in them, but I did not care. Like a good ward
brought up by the priests, he spouted religious verses
galore, turning the most mundane experiences into heroic
feats, worthy, almost, of being sanctified and reported
by the most respectable minstrels. He went so far as to
rebuke me when I blurted out a blasphemy without
realizing it. Apparently saying “by the Damned Spirits”,
was wrong. Well, shoot.

I don’t know how much time had passed when my energy
reserves finally ran out, and I stopped answering Rogan,
half passed out, and fell into a deep sleep. I awoke
when a hand shook me, and opening my eyelids, I met the
blue eyes and black face of Yerris. My mind was
completely numb.

—“Get up,” he said.

His voice came to me as if from the depths of an abyss.
Yerris helped me to my feet and walked with the whole
group of children to the gate. A figure was handing out
buns. With a trembling hand, I took the one that was
being held out to me and stepped back, feeling dizzy. My
arm did not hurt so much any more, but anyway, Warok’s
blows were not what was affecting me most at the moment.

—“Eat,” the Black Cat said. “It will give you strength.”

I looked at him and found him thinner than before. He
had grown, but as he was a semi-gnome, I doubted that he
would grow much more. His clothes, like everyone else’s,
were torn all over, and his expression was so serious…
It made me shiver.

I took a bite of bread and saw the Black Cat grow even
darker if he could. He sat down on the edge of the
platform, and I followed him as he chewed. What he said
was true: the more I swallowed, the more I felt like my
internal energy was coming alive. It even seemed as if
the energy of the cave stopped interfering with me so
much. How strange. We were both just finishing our buns
when Yerris suddenly said:

—“How the hell did they catch you?”

His voice seemed almost accusatory. I finished my last
bite before answering:

—“Been looking for you.”

Yerris gave me an altered look.

—“You were looking for me? Me?”

I nodded.

—“A few weeks ago, Sla told me you were missing. I
looked for you and…”

—“But she didn’t tell you why?” Yerris snorted in a
nervous whisper. “Didn’t she tell you that the Black
Daggers accused me of being a traitor and disowned me?”

I nodded again and with the tip of my tongue caught a
crumb that had stuck to a tooth. I swallowed and
replied:

—“Yes, yes, she told me. But she also thinks you’re not
a traitor.”

Yerris stood still then made a curious face.

—“Really?” he murmured. He shook his head. “Well, it’s
not true, shyur. I’m a traitor of the worst kind. I
betrayed you, Rolg, my mentor, yours, Korther… And even
Sla. It was… what I was supposed to do, you understand?
I grew up among the Ojisaries. The Black Hawk taught me
to spy. He gave me money. I… I sold you all out. And the
biggest mistake I made was telling Alvon. I asked him
for help. I was stupid. The only thing I got was…
everyone disowning me.”

He shrugged his shoulders, his face grim. I looked at
him, distressed. His voice had changed, as had his
manner of speaking, more composed and mature, as if he
had aged fifty years in a few moons. He rubbed his
forehead and added:

—“I’m sorry, shyur. But you shouldn’t have come looking
for me.”

I pouted stubbornly and answered him as Dil the Slacker
might have done:

—“I don’t care. I came here, and that’s that. The Black
Hawk may have taught you how to spy on us, but you
taught me how to survive in the city. So I owe you one.
And I’m gonna get you all out of here.”

A mocking glint appeared in Yerris’ eyes.

—“Impressive, shyur. Now, please, bring your feet down
to earth and open your eyes: you’re in a cave, in the
heart of the Rock, in an old salbronix mine reactivated
and rehabilitated by the Black Hawk. The only way out is
this grate. And it is made of black steel. Not even a
thousand files could break a bar. This is the real
thing. Welcome to the Well, shyur.”

He stood up abruptly and walked away before my stunned
eyes. Damn, how he had changed! Well, that he was not in
a good mood was understandable, but still… He looked
almost as tragic as Miroki Fal.

—“You don’t remember him being like that, do you?”

I turned and saw Rogan approaching, chewing
energetically on his last bite of bread.

—“How do you feel?” he added.

—“Better,” I assured him. “Much better. What do they put
in the bread?”

Rogan grimaced.

—“Ask the Black Cat. He knows everything. He says it’s
an elixir of strength. But, just between us, from the
look on his face every time the meal arrives, I’d say it
can’t be that good.” He shrugged and announced: “Well,
time to go fishing.”

I got up to follow him and the others to the second
cavern and asked him curiously:

—“What are those pearls you’re fishing for?”

—“Black pearls. The Black Cat says they’re salbronix
pearls. Something very precious. But for us, they are as
useful as river pebbles. The problem is that they’re
harder to catch: they’re buried in holes, and you can’t
imagine what holes! Narrow as gutters and dangerous as
claws.” He showed me his right hand. It was full of
scars and scratches. “The rock is sharp as a dagger.
Scary, huh? And there are things on the ground, but you
can’t see them, because everything in there is so bright
you can’t see anything. There are even things that look
like they’re moving and they go… boo!”

I was startled to see that several of the gwaks were
chuckling to themselves. Now they all seemed more awake
than a few hours before. I heard them talking to each
other as they entered the white foam. If some were new,
the energy did not seem to affect them as much as it did
me. They moved away. Soon I could only make out darker
spots, and eventually I lost sight of them in the light
and the curves of the tunnels.

Alone in the cave, I crouched near the entrance of one
of the tunnels. The foam covered all the walls, even the
ceiling, and it was hard to tell where the light ended
and the rock began: basically, only the light could be
seen. I could clearly feel its energy groping at me, as
if it were looking for a gap to bleed me. It was not
very reassuring to know that, even if I got as far away
from the white stuff as possible, I could not escape its
effects entirely. After some hesitation, I reached out
with my right hand and touched the foam. It was hot, but
not burning; a pure, wild energy, as natural as the
morjas of a bone, but dangerous… My skeletal hand
clearly felt the danger.

I stepped back and returned to the platform. Strangely
enough, the energy was attacking less there, maybe it
was because of the wood, I didn’t know. In any case, I
lay there for a while, examining the energy and thinking
about various things, before I got up and decided to
approach the gate. It was on the opposite side of the
white foam cave, wedged between columns of rock along
which small streams of water trickled. It was less than
two metres wide.

With my right hand, I touched the black steel and found
that there was no alarm. The bars were so strong that
the Ojisaries probably hadn’t even considered the
possibility of our breaking them or the lock. As a
matter of routine, I examined the lock with a perceptive
spell, then my attention turned to the chain and padlock
that held the gate double shut. I also noticed the marks
on the rock, on the side of a column, as if someone had
struck it repeatedly with some object until they
realized that the effort was useless. I squinted to see
something in the tunnel and saw nothing. I had a sudden
idea, so I cast a harmonic light and tried to throw it,
but my spell came undone a few feet away. I sighed, and
my exploration completed, I returned to the platform.

When my new companions returned with the salbronix
pearls, it was with shuffling feet and a pitiful
appearance. One by one they placed the beads in a bowl.
Some came in sucking their wounds; others just climbed
onto the platform and fell asleep heavy as bags of nuts.
To think that in two bongs I would join them for the
fishing…

Salbronix pearls, I thought suddenly as I approached the
bowl to observe them. Hadn’t Korther said that the black
beads I stole that night the Cold One struck me were
salbronix pearls? Curious, I reached for the cup and…
suddenly an older gwak slapped me.

—“Don’t touch it, shyur.”

I stepped back a little and saw the gwak lie down and
watch me, his eyes squinting, before his eyelids closed
completely.

Rogan was one of the last to appear, and I saw him a
little more energetic than the others. He’d been here
for a moon already, and as he said, he’d gotten used to
it. He gave me an absent-minded smile, stopped in front
of the platform, and counted the heads aloud: one, two,
three, he counted to twenty-two. He seemed satisfied.
Then he went to pick up the cup, and after counting the
pearls, he carried it to the gate. Then he returned to
the platform with a yawn, passed over the bodies lying
there, and came and sat down beside me.

—“And a new day is coming to an end, shyur,” he
pronounced.

He laid his head on the wood and, yawning again, closed
his eyes. After a few seconds of silence I said,

—“Priest. Are you awake?”

—“Mm,” he said.

—“Where is the Black Cat?” I asked.

Rogan opened one eye and breathed out softly.

—“Wandering around in hell, as always,” he replied.

I frowned thoughtfully. I could not believe that Yerris
was still fishing for pearls. So what could he be doing
in this parasitic foam? I decided to wait for his
arrival and ask him. However, as time passed, my
strength weakened and Yerris did not return. I fell into
an exhausted sleep, and when I awoke to the first sound
of the metal bong, hunger drove me straight to the gate
with the other children. Behind the bars, a man
approached with a large bag. He was wearing a sort of
mask, so I could not see his face.

—“Good morning, children! How are you?” he said.

The gwaks replied with varying degrees of enthusiasm,
some saying that they were fine, others that they were
hungry.

—“The loaves are coming, my children, hold on a moment,”
he answered.

Calmly, under the eyes of all of us, he picked up the
pearls, counted them, kept them in a little bag hanging
from his belt, and finally, one by one, he distributed
the loaves.

—“I cut my hand, sir!” one of the children informed.

The Ojisary took his hand, glanced at it, and sighed.

—“You need to be more careful, boy. Wait till I finish
handing out the loaves, and I’ll bandage your hand,
okay?”

At one point, as he saw one gwak push another to come
forward, he clicked his tongue.

—“Tsk! One at a time, kids, one at a time.”

When everyone had had their share, he proceeded to
bandage the injured child’s hand while the rest of us
watched and ate our breakfast. Whistling a cheerful
tune, he applied a yellow stuff to the wound and wrapped
it in a bandage, pausing at times to ask questions and
joke. He had just told a silly joke about someone who,
trimming a tree, realized that he was sitting on the
wrong side of the branch and fell. More than one gwak
laughed, and in spite of myself, I smiled.

—“And where’s the newbie?” the Ojisary asked. I felt the
gwaks turn around and their eyes land on me. “It’s you,
isn’t it? Come here, come here. Tell me. Do you know
some joke? Here in the well, newcomers must always tell
one. Rule number one.”

I gave him a not-complying pout, but as the others
waited for me to say something, I rolled my eyes.

—“What, you don’t know any?” the Ojisary asked,
surprised.

—“Natural I do, I know quite a few,” I replied. I even
knew jokes about bones and necromancers, but those would
not have been very timely. I settled on one told to me
by Garmon, the newsboy. I cleared my throat, swallowed
the bread I had in my mouth, and after making sure that
everyone was listening, I said in a low tone: “A man
goes to buy shoes and sees some he likes, so he takes
some nails out of his pocket and says to the cobbler:
how many? The cobbler looks him up and down and says:
well, you decide, sir, but usually people take two.”

The Ojisary burst out laughing with the gwaks and said:

—“That’s a good one, very good. Welcome to the
pranksters’ club!” He ruffled the hair of the kid with
the injured hand. “Done, kiddo, it’s healed. Try using
the other hand for fishing, it runs? Okay, enough
kidding: back to work! Today it will be sixty-seven
pearls, just like yesterday. Don’t fight and behave
yourselves. See you tomorrow!”

—“Tomorrow, sir!” we said in chorus.

The Ojisary went with his lantern into the tunnel, I saw
him go up the stairs, and—bong!—the metal door closed.

I gobbled up what was left of my breakfast, and looking
for Yerris, I saw him at last in the cave of light,
already departing to go fishing. I rushed towards him
with the intention of catching up to him, but when I
reached the other cavern, he was already disappearing
into one of the three tunnels of light, moving with
strange agility. The other gwaks were a little slower to
decide to go to work. Some were wandering around the
platform, others were squabbling, bickering, and joking,
making quite a racket, having recovered all their
energy. Rogan, however, had followed me, and after a
silence, he said:

—“You know what I think, shyur? That the Black Cat is
looking for a way out. But he doesn’t realize that,
right now, he’s the only one who can get that far down
these tunnels.” His dark eyes glowed under the light of
the white foam. He concluded in a low voice, “Then, if
he makes it out, he’ll make it out alone.” He shrugged
and smiled at me, “A good joke, the one you told us
earlier. I’m sure the Masked One will ask you for
another one tomorrow: that guy loves jokes, even the bad
ones.” He ruffled my hair. “Enjoy your last holiday
bong, shyur.”

With a shudder, I saw the Priest fearlessly walk away to
a different tunnel than the one Yerris had taken. The
light engulfed him.

Princes hunt pearls in vampiric tunnels
=======================================

I walked through the foam, feeling with each step how
the white paste consumed my energy, which I had
recovered only a few hours earlier.

I’d been in the Well for quite a while, or as Rogan
would have said, quite a few bongs. I think about
twenty. In all that time, only three notable things had
happened. First, with each passing day, I felt more
impervious to the parasitic energy of the mine, as if
the habit or the food we were given prevented it from
seeping into my body to drain my strength. Secondly,
there were now thirty of us in the Well: the Ojisaries
had brought three new ones at once, two weeks ago, and
then three more in the following days. And, third and
last, Slaryn was one of the newcomers. She had not
arrived alone, but accompanied by two of her friends,
Guel the Soothsayer and the Mole. As Slaryn said, they
had caught her lurking in the Ojisaries’ territory,
followed her back to her hideout, and captured her along
with the Soothsayer and the Mole. She would not
elaborate. At first, I thought the reason she was so
quiet was because the energy of the cave was making her
dizzy, but then I realized that she just didn’t want to
talk about the bloody blunder she had made. To my
disappointment, she became as uncommunicative as Yerris.
The two Black Daggers watched each other from a
distance. You could count on the fingers of one hand the
words they had exchanged in the last two weeks. They
didn’t say much to me either. Sla would just gently
nudge me whenever I came near her and say a “how’s it
going, shyur”, and it didn’t seem to matter to her if my
answer was a “so-so”, “blasthell, I want to get out of
this hole!” or a “wind in the sails!”. When I went to
sit next to Yerris, he would remain silent or make brief
fatalistic comments like: our life isn’t worth a damn,
shyur, the salbronix pearls have saved it so far, but
until when? And, if I asked him any questions about his
wanderings in the more distant tunnels, he would
invariably reply: get lost. And I would walk away,
disappointed, and, guessing that Sla was not going to
talk to me any more than the Black Cat, I would go and
find Rogan and listen to his ramblings about the
spirits, the Sacred Book, and the sad but glorious and
honest fate of the gwaks. To say the truth, the other
companions didn’t listen to him much; rather, they
laughed at him and his theatrical gestures, but I found
the Priest to be a great gwak, especially because he not
only spoke well, but he knew how to listen and answer my
questions; in short, we formed a good duo, he, as my
spiritual guide, and I, as his bard and personal
questioner.

Apart from that, the days could be summed up as waking
up with the bong, eating the magic bread, going fishing,
and sleeping. I had noticed subtle changes over time.
For example, the light didn’t hurt my eyes as much.
Which was quite useful, almost as useful as my right
hand when I put it in the sockets and took out the
pearls without getting grazed like the others.

Precisely at that moment, I put my hand into a cavity
submerged in the haze of light and felt around. Nothing.
Cautiously, I withdrew it and continued to advance in
that sea of light, striving to tread carefully on the
treacherous rock at the bottom.

At first, I had feared that my hand would be damaged by
the foam and that the energy would destroy the magara,
but I had quickly realized that the foam, while it
prevented me from casting external spells, could only be
dangerous to the jaypu. Yerris liked to call it vampiric
foam, and the Soothsayer called it dragon drool, but
unlike sea foam or saliva, it didn’t get wet or stick to
our skin either. It was like a white, static mist that
covered the underground walls, obscuring and protecting
the cavities where the salbronix pearls were formed.

I heard voices echoing, and I frowned as I perceived an
intonation of discord. After a brief hesitation, I
approached the noise and saw two companions, both a
little older than me and both human. They were Syrdio
and the Mole. While the former was standing on a rock,
the latter had crept into a crevice of the tunnel and
wore an expression of intense concentration.

—“I’ve got it!” he then exclaimed, stepping away from
the wall.

—“Well, hurry up, pass it to me, shyur!” Syrdio said
without moving from his rock.

—“I’ve already given you one!” the Mole protested.

Syrdio looked at him contemptuously.

—“I don’t care, you owe me this one, if you don’t want
me to pull your beard off in front of everyone.”

The Mole glared at him, but he gave him the pearl. I
couldn’t believe my eyes.

—“But blasthell!” I said, approaching. “Mole, what are
you doing? Why are you giving him the pearl?”

The Mole did not answer, and sketching a smile, Syrdio
said:

—“Stay out of this, brat, it’s none of your business.”

—“You’re the brat here,” I replied.

Syrdio shook his head, climbed down from the rock, and
pushed my head back.

—“I said stay out of it.”

I saw him walk away and bit my cheek, tense, before
asking the Mole:

—“Why did you give him the pearl?”

My companion made a face as if to say, “I don’t know”,
which could mean either “Syrdio knows something about me
that I don’t want anyone to know”, or a simple “Syrdio
scares me and I don’t know how to say no to him”.
Knowing him a little, I bet on the latter. I shook my
head and said:

—“I’m still missing one. How about you?”

The Mole frowned.

—“Three.”

—“Gosh,” I huffed. “Well, hurry up or you’ll end up
collapsing on the way. You’re not a veteran fisherman
yet, remember? Well, I’ll give you a hand; I can’t last
as long here as the Black Cat, but almost. It runs?”

The Mole nodded silently, and we continued to search for
pearls together. When we returned, my companion was
deathly pale. Rogan was waiting for us at the entrance
to the cave.

—“Spirits, it took you guys so long!” he exclaimed. “I
thought the devils had pinched you already.”

—“They didn’t pinch me, but him, almost,” I replied,
helping the Mole out of the lake of light. Rogan walked
over to help him too. “Are we late for the sermon?”

—“Not at all!” Rogan gasped. “I only give the sermon
when you’re present, Sharpy. You’re the only one who
listens to me, anyway.”

I laughed and observed:

—“The Soothsayer listens to you, too.”

—“Boh, who knows, who knows,” the Priest said as we
entered our cave. “That one, you never know if she’s
listening or daydreaming. Besides, by now, she’s
probably already delivered her soul to the Spirit of
Dreams.”

We went up to the platform, and I found that Guel the
Soothsayer was indeed fast asleep. We left the Mole with
her, and I took the pearls from his pocket and leapt
nimbly from the platform to add them to the cup with my
own. I stopped by the gate for a moment. I remembered
that, before, I could hardly see the tunnel, and now I
could make out the walls and even the first steps of the
staircase, about forty yards away. Was it because the
light in the cave was brighter and I didn’t realize it?
I didn’t know, but in any case, the black steel was as
indestructible as before. I picked up one of the bars,
shook myself more than I shook it, and said:

—“Bonehead.”

I let go of the bar and walked back to the platform,
jumped off, landed on it, and sat cross-legged in front
of the Priest.

—“Well, well! Yesterday you told me what happened to the
Travelling Spirit of Saint Lakan,” I reminded him. “He
got on board and sailed and sailed to the horizon. What
happened to him afterwards?”

Rogan rolled his eyes.

—“He kept sailing to infinity. That’s why they call him
the Traveling Spirit, shyur. Come on, forget that saint!
Today I’m going to tell you what happened, long ago, to
a gwak who got caught stealing a loaf of bread and got
angry at the Patron Spirit. You’ll love it, it’s in
verse and all. I learned it from an old well cleaner. I
may get a few rhymes off the rails, but basically, it
goes like this. Listen, listen.”

I paid intense attention, and he recited:

I’ve been pinched by the flies!\
Oh, woe me! What shall I think\
of the world and its compassion\
if for one loaf of bread\
I’m sent to prison?\
Mr. Baker, please have mercy\
on this gwak, and hear the plea!\
Mercy for the starving man?\
No, thief, expect no such thing!\
Look at that barefooted brigand,\
that motherless little brat…\
Stop, thief, stop!\
Everyone ran after him.\
Oh how terrified he was!\
Suddenly, the boy stumbled\
and collapsed near the pier.\
Stop, thief, stop!\
Hurrah! We caught the kid!\
And the neighbors kicked him.\
How the bandit screamed!\
They left him for dead.\
They threw him in the river.\
The boy, still breathing,\
clung to a boat and raised\
his dying eyes to the sky:\
Why, Patron Spirit, why\
why was I even born?\
Why give me life if you’re going\
to steal it from me now?\
Then, both laughing and crying,\
I told him: oh you thief.\
Patron, you are a crook\
and I’m a thieving cat.\
But the ones who beat me up,\
well, those are even worse than that.\
With those words, he fell asleep,\
the bread-stealing little kid,\
lulled by the grayish light\
that glows in the morning dawn.\
Six o’clock. A fly comes by,\
lifts him up. “He is so light!”\
He carries him to the heart\
of the home that nothing heals,\
behind iron bars that taunt\
miscreants and nasty thieves.\
Here I am before the judge,\
and condemned for what I did.\
I shall not leave this refuge\
till I go to eternity.\
Perhaps then, Patron Spirit,\
you’ll tell me what I’ve done wrong,\
unless you yourself can’t, either,\
explain the justice of this world.


Rogan ended his story with a theatrical gesture and
asked:

—“How do you think of it?”

—“Beautiful,” I confessed. “But dark and desperate. Who
knows, maybe this gwak escaped from the slammer and went
on to steal bread. Besides, you should make a song about
it. Something like this…” I cleared my throat and
intoned:

The flies got me, oh my gooooosh!\
Woe me! Woe me, and what have you!


Rogan burst out laughing. A companion woke up with a
start, gave us a half-awake look, and fell back into a
deep sleep. On the other side of the platform I saw
Syrdio discreetly chuckle and make some comment to his
neighbor.

—“Well, something like that,” I concluded.

—“Surely even the Black Cat heard you,” Rogan laughed.

As always, the Black Cat was the only one absent. After
a moment’s hesitation, I approached the Priest and
leaned over, whispering:

—“Today, I followed him.”

Rogan’s eyes lit up.

—“For real?”

—“Yes. He’s going very far,” I said. “I think he’s
searching the whole ground, like he’s looking for a
hole. But I don’t think he’s found anything.”

—“And he didn’t see you?” Rogan wondered.

I shrugged.

—“Well, no. I have my tricks. Tell me, Priest.”

—“What?”

I examined a wound in my foot that was almost closed
then lay down on the wood with my arms behind my head.

—“Well… I don’t know,” I hesitated.

Rogan looked at me with mocking curiosity.

—“What’s up?”

I shook my head gently.

—“What are the Ojisaries going to do, when we run out of
pearls?”

—“Ouch.” The Priest grinned, “Now that’s an unhealthy
question, Sharpy.”

—“Yes, but that’s what the Black Cat said: pearls take
many years to form and that’s why the mine was abandoned
a long, long time ago.” I paused for a moment. “I wonder
how he knows so much.”

—“He’s the Black Cat,” Rogan said in reply. And when he
saw me yawning, he added, “Well, get some sleep, or
you’ll end up unhinging your jaw.”

I nodded, yawned again, found a comfortable position,
and said:

—“Sweet dreams, Priest.”

And, as usual, I fell asleep in a few seconds. I dreamed
that I was running up the hill towards the Cave. I could
feel the cold wind from the valley against my cheeks. It
smelled of earth and grass and forest. *‘Elassar!’* I
shouted happily. *‘Elassar, I found the ferilompard
bone! I found the ferilompard bone!’* Sitting on his
trunk, my master looked up with peaceful eyes, turning
them away from his book of necromancy. And he said to
me: *‘Took you long enough, Mor-eldal…’*

Bong! I woke up suddenly and sat down on the platform. I
was one of the first to arrive at the gate, and I could
see the Masked One moving through the tunnel. I had once
asked him why he covered his face. He said it was to
protect himself from the evil energy. I didn’t tell him,
but I doubted it was very effective.

—“Good morning, children!” he greeted us as usual.

—“Morning!” I said like the others. My hands clung to
the bars and my gaze was fixed on the bag full of bread.
I was hungry as a dragon.

—“How’s it going, everyone?” the Masked One asked.

He received an uproar of responses, most of which were
positive. He picked up the pearls and counted them.
There had to be eighty-four. I had not counted them the
day before, others always took care of doing that. The
Masked One frowned.

—“Hey, kids, what’s this? Three are missing!”

His disgruntled voice made me flinch, and I hurried back
with the others. The only one who did not back away was
Yerris. The Black Cat looked at the Masked One with a
grim expression.

—“I counted eighty-four,” he said calmly. “The three new
ones only take one more. They can’t take any more.”

—“I counted eighty-one,” the Masked One replied. His
voice was so dry that, being used to seeing him cheerful
and affectionate, it frightened me a bit, and I was not
the only one. He said, “I’m sorry, but until you give me
three more, you’ll have nothing to eat. You know the
rules. Bring me those three pearls quickly. If you don’t
have them in an hour, there will be trouble. Is that
clear?”

A silence of anger and despair answered him. The Masked
One gestured with both hands.

—“Rules are rules, kids. It’s not my fault.”

He picked up the bag of bread and left. No sooner had we
heard the bong of the metal door than a little girl,
Venoms as everyone called her, rushed to the gate and
began to rant against the Masked One. A boy tried to
calm her down, others swore they had brought the three
pearls, and most of us were fidgeting, worried. With a
rigid face and his pointed ears twitching slightly,
Yerris turned, not to the cave of light as I had
expected, but to Syrdio.

—“Syrdio,” he called in a somewhat strained voice.
“You’re going to help me find these three pearls. Runs
for you?”

The boy’s face seemed very pale. I saw him swallow and
nod. When I saw them both walking away towards the cave,
I had the impression that in reality they were merely
acting. For I was almost certain that Syrdio already had
those three pearls in his pocket. Did he perhaps think
that we would give him a day’s holiday just to please
him?

—“What a troublemaker,” I muttered.

I approached the place where the Mole, the Soothsayer,
and the Priest were sitting. The former did not look
much better than the day before.

—“Priest,” I said. “What does the Masked One mean, when
he says there’s going to be trouble?”

Rogan pouted.

—“That they’re gonna beat the crap out of us if we don’t
give them these three pearls. Something similar happened
before you were here. Only, that time, we knew there
weren’t enough pearls. We thought, well, what can they
do to us? Well, they came with the dogs and the
crossbows, and they gave the Black Cat a thrashing, all
because this crazy guy interfered and told them: ‘this
is our house, you isturbags’. They got very angry. And
they told us they wouldn’t give us any magic bread until
we brought them the pearls we owed them. At first, we
didn’t understand the threat, they even kept bringing us
bread! But, after eight bongs… Well, we still owed them
pearls because some of us were lazy and dragging our
feet and… we started to feel really bad, as if the Evil
Spirits had crept into us and started tearing everything
apart. I swear to you, it was hell. They left us like
that for… I don’t know, maybe two days. Then they gave
us the magic bread, the real one, and we recovered in a
flash. We learned our lesson pretty damn well.
Unfortunately, Syrdio came after that,” he added in a
whisper. He looked up at the stalactites and drummed on
the wood of the platform, feigning a detached air.
“Well, well. But, this time, it’s not going to happen,
because Syrdio… I mean, the Black Cat will be fishing
for those three pearls in less than an hour. There’s a
reason he’s the veteran of the Well,” he said with a
smile.

Indeed, shortly afterwards, the Black Cat returned with
the three pearls. Syrdio limped after him. His nose was
bleeding and a bruise was forming on his forearm. Nat
the Diver, who had been a friend of his long before he
was in the Well, was speechless when he saw him.

—“What happened to you?” he asked.

—“I hit a rock,” Syrdio grunted.

Rogan scoffed and gave a sympathetic exclamation.

—“No kidding! By any chance, did the rock have two ears
and black cat whiskers?”

I let out a loud laugh. Syrdio gave us an annoyed look,
and without replying he limped off to the water source
to clean himself. Still smiling, I turned and approached
the gate, where Yerris was already waiting, sitting on a
rock between two columns. I glanced at his fist and
confirmed my impression that both had fought, and that
the Black Cat had won. I put my hands through the bars,
and my eyes pierced the darkness. I thought I could even
make out the metal door, though it was obviously out of
my sight, up the stairs. And in my head, I could already
hear the long-awaited bong ringing. I was hungry…

—“Shyur.”

It was Yerris calling me. I looked at him curiously and
met his assessing blue eyes.

—“Can I ask you a favor?”

I squinted, intrigued.

—“Natural,” I said.

—“I can’t stand watch and look for a way out at the same
time. Watch Syrdio, will you? If he does anything wrong,
don’t interfere: you just tell me.”

I smiled and nodded.

—“All right. So you’re really looking for a way out. Did
you find anything?”

He shook his head and sighed.

—“No. Nothing at all. But, even if I did find something,
we can’t run away just like that. Not without the
sokwata.”

I frowned and approached.

—“The sokwata? What’s that?”

Yerris grimaced as if he just thought: I shouldn’t have
said that. My curiosity was aroused, and I insisted:

—“What’s that?”

The Black Cat glanced at the others. They were all on
the platform, waiting for the Masked One to return.

—“Don’t tell anyone about this,” he muttered. “Does it
run?”

I nodded and sat down beside him, attentive.

—“I want your word as a Cat,” the semi-gnome demanded.

I smiled, and as Rogan sometimes did, put my fist to my
chest, and said:

—“I give you my word as a Cat that I won’t tell anyone.”

There was a silence, and I waited patiently for Yerris
to say something other than: we are doomed. In a very
low voice, he admitted:

—“It’s not that it’s a secret, really. It’s just that I
don’t like talking about it. Actually, the Ojisaries
call sokwata what they put in the magic bread. Sokwata
is what changed us… into this.” He hesitated under my
puzzled gaze and then resumed, “It’s a mutation potion.
The alchemist who invented it… screwed up big time. The
Ojisaries captured him, I don’t know, maybe six moons
ago, I have no idea. Anyway, I was locked in a cell in
their lab at the end of winter and… I was the first one
to test their potion. It changed me. It changed us all.
There are things I can do now that I couldn’t do before.
There’s no way you haven’t realized that. Our jaypu is…
different. We’re more resistant to outside energy. One
day, the alchemist threw a magara at me. And the spell
bounced off me. It barely affected me.” He breathed in,
thoughtful, then concluded in a whisper, “That’s what
sokwata is, shyur: a figment of some deeply idiotic
alchemist’s imagination. It might almost seem practical
and advantageous if you didn’t need it to live. It’s
just as you heard. The alchemist told me as it is: if I
die, you die. And he did it on purpose, believe me: he
knows that, when the Ojisaries won’t need him anymore,
they’ll get rid of him. That’s why… He’s made sure that
our mutated bodies need his sokwata to keep functioning.
Without it, we die, shyur. Without the sokwata,
everything becomes hell. And, if we run without it, we
are dead. Popped off. Spirited. Say it the way you
prefer. Dead,” he repeated.

I swallowed as I took it all in. It coincided with
Rogan’s story. Through those loaves of bread, the
Ojisaries had made us take a magical product to allow us
to fish for pearls in that mine, and to make matters
worse, we were now dependent on that product to stay
alive. It seemed to me that the bars of the grate were
getting bigger and more impenetrable.

—“Blasthell,” I muttered. “But… but, Yerris, everything
you say… is it true?”

The Black Cat rolled his eyes and shook his head.

—“Forget it, shyur. It’s not worth thinking about. It’s
just that… well, I suppose I’d better tell you about
mutation potions before that Priest puts ideas of curses
and spells and witchcraft into your head…”

—“The Priest doesn’t do that,” I protested.

Yerris smiled.

—“Well, I hope I didn’t break your spirit. I haven’t
told Sla about it yet. She seems to be angry with me, so
I don’t dare, I don’t want her to wring my ears off.”

I twisted my upper lip in surprise.

—“Angry with you? Sla?”

—“She doesn’t say a word to me.”

I huffed.

—“You don’t tell her anything either.” I paused for a
moment, and then I let out all that was in my heart:
“You two are acting so strange that it seems as if
lightning has fallen on your heads. And you’re blowing
me off every time I come to talk to you; it’s a bit
unfair, because last year, I was listening to you all
the time and you kept talking on and on. Haven’t you
noticed?”

Yerris gave me a surprised look, turned his eyes to
Slaryn, who was chatting with the Soothsayer, and
smiled.

—“Gosh… Maybe you’re right,” he admitted. “You know,
shyur? What I’ve noticed, anyway, is that in one year,
you’ve become an authentic gwak Cat. I still remember
when you used to say to me, Yeeeerris! What is that
thing with legs and horns? An ox, shyur! And that huge
mushroom that lady is carrying in her hand, eh, Yerris?
An umbrella!” We laughed, and he tossed one of the
salbronix pearls into the air before retrieving it on
the fly. After a silence, he said, “By the way, how’s
your mentor?”

I pouted.

—“He’s gone to Kitra for a job. But maybe he’ll be back
soon and…”

—“And he’ll find he has no sari,” the Black Cat
completed. He shrugged. “What can I say, shyur? The life
of a gwak is hard; it won’t surprise anyone that you
disappeared overnight. Yal is a guy with a good head on
his shoulders. He’ll soon come to the conclusion that
you’re dead, he’ll mourn you for a while, and he’ll go
on with his life. And we’ll go on fishing for pearls and
swallowing sokwata until the day we can’t find any more
pearls, and then the Black Hawk will forget about us,
let us starve, and condemn the mine again after getting
richer than Captain Tedious did with the gold-making
machine.”

At my appalled expression, he smiled, and raising his
forefinger and thumb in a mocking religious gesture, he
said as if completing a prayer:

—“Peace and virtue.”

His smile widened, he shook his head, amused, and added:

—“Maybe that’s the solution: accept our condemnation and
live with it. All in all, we were already living like
doomed souls up there, except in a different way. And we
could be worse off. It’s not cold, we’ve got a bed
softer than stone and tunnels so well lit we’re not even
afraid our torch will go out… It’s paradise!” he
laughed.

I looked at him, my eyes wide. Just then, a BONG!
sounded, and the Black Cat rose nimbly to his feet,
concluding:

—“And, on top of that, we get lunch brought to us!” He
gave me a joking smile. “Get up, great prince, the table
is served.”

I huffed, and a smile stretched my lips. The Black Cat
was finally in a better mood. Maybe it was due to a hint
of madness, but… whatever the reason, I was glad.

Hope does not die in hell
=========================

I caught three black pearls.\
The first one was a pain in the ass to get.\
The second one, I didn’t have to work so hard.\
The third one was the one I wanted the most,\
’cause it opened the way to home.


Murmuring more than singing, I left the three beads in
the bowl by the gate, and no sooner had I straightened
up than I heard a:

*Bong!*

I was startled and frowned. I had just returned from
fishing. It couldn’t be lunchtime. Were they bringing in
new miners? It had been about two weeks since the
incident with the three missing pearls, and in all that
time, we had only seen the Masked One. Now, however,
other people were coming. I saw four figures coming down
the back stairs. They were not recruits, for recruits
always arrived asleep, carried by the Ojisaries. They
came down the tunnel with a lighted torch. Two of them
were adults. And the other two were children. My heart
leapt as I recognized them all. It was Warok, Tif… and
my cronies.

—“Move along,” the dark elf growled at his little
brother.

He gave him a heel, and Manras let out a plaintive moan.
I stepped away from the gate, looking very pale. They
weren’t going to put the little elf and Dil in the Well,
were they? Warok was Manras’ brother… he wouldn’t dare,
would he?

As I backed away, I hit the edge of the platform. My
companions, who had also returned from fishing, seemed
as attentive as I, but seeing me so cautious, none of
them dared to approach. After all, it was clear that
they were not going to give us any bread or tell us any
jokes like the Masked One.

At last, they came to the gate, and with a brutality
which inflamed my hatred, Warok slammed his little
brother against the bars.

—“Look at them, you wretch!” he cried in a terrible
voice. “Look at these gwaks and tell me now if you
really want to condemn them to death! You stupid brat!
How could you be so stupid! Do you want me to leave you
in there with the little monsters like you? Huh? Tell
me, do you want to rot in this hole?” He slammed him
against the bars again, dropped him, and in horror, I
saw him pull out a dagger. “Open the door, Tif!”

The blonde caitian took out the keys, undid the lock,
and opened the gate. It was only the fifth time it had
opened since I’d arrived, and it was the first time the
Ojisaries didn’t bring their dogs. Warok pushed Dil
inside, and Little Prince hit one of the rock columns
before turning and stammering:

—“Don’t hit Manras, don’t hit him! I was the one who
wanted to open the door for the alchemist! It was my
idea!”

Warok looked at him with contempt.

—“Your idea, my eye. You’re as brainless as a sparrow,
Dil. Stay there and bring us three pearls. It’s the only
useful thing you can do.”

Tif was already closing the gate. Manras shouted:

—“No, no, no! Diiiil!”

He stretched his little bluish hands between the bars,
desperate to see his friend locked up in that infernal
cave. I did not dare to move, but at that moment, when I
saw Warok grab Manras by the neck and order him to be
quiet, I rushed to the gate without even thinking about
it and shouted:

—“Get off him, isturbag!”

Manras stopped shouting and looked at me, dumbfounded.
Dil looked no less astonished. I met Warok’s gaze and
repeated firmly:

—“Get off him.”

Warok arched an eyebrow and lost interest in Manras.

—“Well, if it isn’t the little Black Dagger. How’s your
miserable life?”

—“Wind in the sails,” I said.

He did not seem to like my answer. The Ojisary said to
me:

—“Come closer.”

I had stopped near Dil. I did not move.

—“Come closer or you’ll have nothing to eat! Do you hear
me?” he barked.

I gave him a mocking look of defiance. The presence of
my cronies made me do foolhardy and stupid things. At my
rebellion, Warok’s face changed to a mask of irritation.
This time, he raised his dagger. I opened my eyes wide,
turned my head to my companions, perhaps seeking help,
and watched their nervous expectation. Rogan was deathly
pale.

—“Watch out, brat,” Warok said to me with calm. “If I
wanted to, I could throw this knife at you and pierce
your heart. Nobody would care, and I least of all. Come
closer, I said.”

I was only three or four steps from the gate. I took a
step. I took another step. I found myself near the bars,
and to my surprise, Warok did not hit me. He did not
even touch me. When he spoke, his voice sounded merely
condescending.

—“Tell me, human boy. If I remember correctly, you came
in here in mid-Celestial. Do you know how long you’ve
been here?”

I glanced at Manras, who was watching the scene with
silent commotion. I shook my head and replied:

—“A moon?”

Warok smiled, mockingly.

—“More than that. We’re already in Redmoon. Do you
remember what the sun was like? Well, try to remember
and not forget, because you won’t see it again.” He
beckoned me to get as close as possible and whispered
softly in my ear, “You’re smart, look for more pearls,
keep them and don’t tell Lof, and if you work well, I’ll
set you free one day. Besides, since you seem to care
about my little brother, I, in your place, would obey,
and maybe like that, you prevent me from putting him in
this hole too, hmm?”

Under my rather hostile gaze, he stepped aside, pushed
Manras, I don’t know whether to conclude the lesson or
to get him to react, and went off into the tunnel with
Tif. Manras, however, did not move: he gazed alternately
at Dil and me, as if he were asking for help. And that
he should ask for help from gwaks locked behind a black
steel gate broke my heart. Before Warok could turn and
call out to him, I hurriedly made a gesture like a long,
thin object and pointed to the lock. *The keys*, I meant
to say. And I whispered:

—“Be careful.”

—“Manras!” Warok barked.

Breathing rapidly, the little elf hastened to follow his
elder brother. But when he reached the first step of the
stairs he turned and with a sign like the ones we used
to use when we sold newspapers, he said to me, “It
runs,” And he disappeared up the stairs.

I breathed in and clutched the bars, tense and anxious.
It took me a moment to understand why I felt so nervous.
And it was because, before, despite the Black Cat’s best
efforts, I had never had much hope of ever getting out
through some mysterious tunnel to the surface—even he
didn’t seem to believe it. On the other hand, having
Manras bring us the keys… it wasn’t infallible, but it
was more believable. Much more believable than having
Warok free me. Blasthell. That elf must have really
taken me for a fool.

When the bong of the metal door sounded, I suddenly
thought of something. Manras had waved at me at the end
of the tunnel, when the torch was already almost gone,
as if he had no doubt that I could see him. Probably
because he knew that, from my place, he would have seen
himself through the shadows. And not because I was a
mutant or a sokwata or anything, but because he was one
too.

I widened my eyes at such a horrible suspicion and
turned abruptly to Dil. The little devil was staring at
the cavern and the platform. But he didn’t seem stunned
by the energy either, merely shocked by what he was
seeing.

—“Little Prince,” I called to him. I approached
promptly. “Hey, Little Prince! Is it true you tried to
save the alchemist?”

Dil looked at me darkly and nodded. I insisted:

—“What happened?”

Biting his fingers, he confessed:

—“Mr. Wayam has deceived us. He is the alchemist,” he
explained. “We used to work with him. We cleaned his
machines and put a black paste in the bread. We didn’t
know it was meant for you. We didn’t know anything about
it, I swear, Sharpy…”

I touched his arm to comfort him and to encourage him to
continue. He went on:

—“He gave us poison candy every day. In fact, inside,
there was the same dough that we used to put in the
bread. But we didn’t know it was bad…” Dil looked at me
guiltily and added, “Yesterday, Wayam convinced us to
help him escape, and he said… we would escape with him,
because if we didn’t, we would die. But Adoya caught
us.”

—“The one with the dogs?”

Dil nodded. I winced and let out a curse followed by a
sigh of relief. At least the Ojisaries hadn’t lost the
alchemist. Which was very good news, because without the
alchemist, we were doomed.

Out of the corner of my eye, I saw movement at the
entrance to the cave of light and turned to see Yerris
appear. He had probably heard the bong, for he headed
straight for the gate. I couldn’t help noticing that at
the same moment the Priest slowed down, and instead of
approaching, he remained on the platform, watchful.
Rogan felt a certain fear of the “Mysterious Wanderer”,
as he sometimes called him. I guessed that, before I
came to the Well, they had had some disagreement.

—“Who is this gwak?” Yerris inquired as he joined us.

—“Dil the Little Prince,” I introduced him. “A comrade
of mine.”

—“And they brought him conscious?” Yerris observed,
quizzical.

I hesitated, then told him what had happened in a few
sentences, admitting that Dil and Manras had worked for
the Ojisaries, but not mentioning that they had been
directly involved in our ensokwatation. After a silence,
I left him deep in thought, took Dil by the arm and led
him to the platform.

—“Don’t be discouraged, you’ll see, we’re not so bad
here,” I said and thundered, “Priest! Meet Little
Prince. The most slackerish gwak in Prospaterra,” I
said. I nudged Dil’s head with an affectionate hand,
stole his cap, and put it on, straightening like a band
kap. “Let’s greet our new brother in the Brotherhood of
the Miners of Salbronix! Sit down, sit down. This is
Rogan the Priest. And this one is Guel the Soothsayer:
she guesses things in the future, and according to her,
we’ll be out of here in a week.”

—“It’s always in a week,” Rogan laughed.

The Soothsayer, though exhausted from fishing, gave him
a look that was both haughty and defiant.

—“It will be in a week,” she affirmed.

I continued with vivacity:

—“That one over there is Natorg the Mole. He’s from the
north, and a year ago he was a breaker in a coal mine,
so, you see, he’s an expert. The big snoring nose is
Draen the Sweeper. Another namesake of mine. He used to
sweep up Tarmil Avenue and earn his keep. That one over
there is Parysia the Venoms. Ah! And that one is Natorg
the Diver. He knows more about picking pockets than all
of us put together. He’s a friend of my namesake Swift.
And this one is Syrdio the Galloper.”

I continued to give the names of all the companions.
After Warok’s departure, all had become quiet and were
already half or completely asleep—only Rogan and the
Soothsayer were trying to stay awake—so when I shut my
mouth, there was silence, and for a moment, only the
slow gurgling of the spring could be heard. I frowned.

—“But what the…!”

I stood up on the platform and spun around. I counted
the heads. Twenty-seven, twenty-eight. Twenty-nine with
me. And thirty with the Black Cat. Someone was missing.

—“What’s going on?” the Soothsayer asked.

In response, I said:

—“Where’s Slaryn?”

Rogan and the Soothsayer looked for her in turn,
surprised.

—“That’s odd. She was here before, we came back
together,” the Soothsayer asserted.

—“In the past or in the future?” Rogan replied
mockingly. He lay back on the planks to peek under the
platform, the only place in the cave that remained out
of reach of a panoramic view. Unsurprisingly, he
announced: “Nothing.”

Frowning, I stepped off the platform and trotted towards
the Black Cat. He was standing and clutching the bars
with both hands. I stopped short when I heard him speak.
And my eyes widened as I saw a figure on the other side
of the gate. It was Slaryn.

—“Good mother,” I let out in a stunned whisper. I ran
over to her.

Slaryn was saying:

—“It’s the only way out; you said so yourself: you found
nothing in the other tunnels.”

—“It’s dangerous,” Yerris gasped.

—“Staying here isn’t any better: I’m already out,”
Slaryn replied. “Don’t worry, they won’t catch me.”

—“Sla!” I interrupted, clutching at the bars,
overexcited. “How did you do it?”

The Black Dagger girl gave me a mysterious smile.

—“With dark magic, shyur, and a touch of talent. Those
two scoundrels were so distracted, it was a breeze. I
promise I’ll get you out of this. I don’t know when, but
I’ll get you out.”

—“Without sokwata, it’s no use, Sla,” the Black Cat
reminded her.

—“Trust me,” Slaryn replied.

Her gaze went to something to my right, and only then
did I realize that Dil, Rogan, and the Soothsayer had
followed me. Surprised, I noticed that the dark elf’s
eyes were fixing Little Prince.

—“Hey. What’s your name, shyur?”

The boy shrugged before answering:

—“Dil.”

—“Dil,” Sla repeated. “Tell me, Dil. You saw the
tunnels, didn’t you? Do you know how long it took you to
get here?”

Dil gave her a wary look. I became excited at such a
sensible question.

—“*Dead round*! Dil, answer Sla, it’s important.”

—“Did it take you long to come?” Sla insisted.

Little Prince shrugged again.

—“A little, not too much.”

Despite Dil’s hesitant answers, Sla and the Black Cat
managed to work out the reverse route: stairs, the metal
door, a slightly long tunnel, and another door that led
right into the Ojisary refuge, into the Labyrinth. From
there, Dil said there were almost empty rooms and that
the door to the lab was on the other side of the outer
corridor, in the building across the street. Finally,
Slaryn ruffled Dil’s hair through the bars and said:

—“Thank you, shyur. You just did me a great favor.”

I saw Dil return a faint smile; as Slaryn took a deep
breath and prepared to walk away, the Black Cat called
to her in a strained voice:

—“Sla.” The dark elf turned around. After making a
muffled sound, Yerris cleared his throat and adopted a
light, familiar tone as he said, “Be careful, princess.”

Slaryn rolled her eyes, waved a hand, and went away. We
saw her go up the stairs, and even after she was gone,
we stood by the gate and waited for the sound of the
bong. We heard it, but not as loudly as we had expected.
Slaryn must have cast a silence spell. I glanced at the
Black Cat. And, sensing his concern, I whispered to him:

—“Don’t worry, Yerris. Sla is a Black Dagger. The
Ojisaries won’t see her.”

The semi-gnome swallowed and nodded slowly.

—“If she can’t do it, I can’t do it either anyway. I’ve
never been very good at harmonies.”

After a moment’s silence, Rogan, the Soothsayer, Dil,
and I left the Black Cat lost in thought and returned to
the platform. The possibility that Sla would escape
filled me with hope. I didn’t know how she was going to
get us out of there, but… it was always a consolation to
know that someone out there was going to try something.
Seeing all my other companions sound asleep, I realized
my own exhaustion, and soon my thoughts were overwhelmed
by one desire: sleep. We sat down in a corner of the
platform, and I heard the Soothsayer murmuring.

—“In a week.”

Rogan and I exchanged a mocking but hopeful pout, both
wishing intensely that the Soothsayer’s prediction would
come true. And as Rogan murmured a prayer to his unknown
ancestors, I nudged a slightly dazed Dil to lie down on
the boards and said:

—“Go to sleep. When the bong rings, we’ll eat and go
fishing together, won’t we?”

I laid my head on the wood, closed my eyes and heard
Little Prince whisper:

—“Sharpy… I don’t like it here.”

I rolled my eyes, ran a hand over my young friend’s
head, and before I could think of seeking an answer, I
fell sound asleep.

Rushing toward freedom
======================

The bong woke me up, and as usual, I was up and about
before my consciousness returned to the waking world. I
stretched, yawned, scratched myself, and finally opened
my eyes wide. All my companions were waking up, and some
were already moving towards the gate with varying
degrees of energy… all except Dil, who was still fast
asleep. I pulled him by the feet and sang to him:

—“Come on, lazy demorjed prince, wake up, the bong is
ringing and the bread is here!”

At last, I managed to rouse him from his lethargy, but
we were the last to arrive at the gate and take the
bread. As on the previous day, there was one loaf left,
and the Masked One asked:

—“Didn’t you find the one that was missing?”

We shook our heads, and Syrdio the Galloper said:

—“If she didn’t come back, it means she’s dead. She’s
not coming back. It’s not fair to make us take three
more pearls.”

The masked man shrugged and replied the same as the day
before:

—“Bring me her body, and I’ll settle for ninety.”

Begging silently, I reached between the bars for the
remaining bread. What good was it to him, anyway? He
didn’t need sokwata. However, Lof ignored me, put the
bread back in the bag, and repeated his refrain:

—“Don’t fight and behave yourself. See you tomorrow.”

—“See you tomorrow, sir!”

Unlike usual, I said nothing and just watched the Masked
One walk away. When the exit bong sounded, I took
another bite of my bread and went to sit on the platform
with Rogan and Dil. While the former ate with a
distracted look, Little Prince chewed his bread
energetically. He hadn’t liked the first day in the
tunnels of light at all, and I bet he wouldn’t like this
one either. Especially because today I had thought of
sending him fishing for good; the day before, I had
collected the three pearls for him, I had stayed in the
tunnels forever because it was getting harder and harder
to find pearls, and I didn’t want to do that again. I
sighed. The sooner we went to get those pearls, the
sooner we would be back.

—“Let’s go,” I said when I had finished my bread. “I’ll
teach you to fish.”

Dil followed me without protest, and we were already
entering the central tunnel full of light when I heard a
bong! and stopped dead.

—“Sla,” I murmured, my heart beating wildly.

I turned suddenly and hurried back to the cavern of the
platform. My companions had approached the gate and were
scanning the tunnel, full of hope. They all knew of
Slaryn’s escape. Some thought she wouldn’t return, like
Syrdio, but others were more optimistic and imagined
that she would return with an army of Savior Spirits.
When I heard someone snort and turn to walk a few steps
away, I could see the person walking down the tunnel and
understood the general disappointment. It was Manras. I
smiled and hurried to slip between the gwaks.

—“Manras!”

—“Sharpy!” he exclaimed, his voice trembling. He ran to
the gate and handed me a bunch of keys. I couldn’t
believe it. With trembling hands I took the bunch of
keys, and the Black Cat snatched them from my hands.

—“We can’t go out like this,” he explained. “Slaryn said
she had a plan.”

I glared at him.

—“Give me the keys!”

—“No,” Yerris refused calmly.

I felt the tension rising between the gwaks.

—“Then give them to me,” Syrdio intervened with an
imperative hiss.

—“Open the door!” Nat the Diver said.

—“I want to get out!” Venoms said.

—“Silence!” Yerris thundered.

Syrdio pushed him roughly against the iron bars of the
gate, and the Black Cat’s eyes glittered. I got scared
and yelled:

—“That’s enough!”

But no one listened to me in the tumult that broke out.
Syrdio punched the Black Cat in the face, leaving him
dazed. He took the keys away from him, and I only
managed to grab Dil so that he wouldn’t be crushed by
all the gwaks who were crowding against the gate.

—“Quiet!” Rogan roared.

Amazingly, the noise subsided slightly, but Syrdio
continued to try the keys in the lock. He found the
right one and opened the gate. But there was still the
padlock. He opened it on the second attempt, and they
helped him to remove the chain. When the gate opened
completely, a silence of excitement and fear fell. The
first to step through was Manras, in the other
direction: he rushed towards Dil, grabbed his arm, and
also held on to mine, and suddenly seemed much calmer.
He’d just got his real family back. I smiled.

—“Bless your soul, Manras,” I said, moved.

The little dark elf returned my smile, and I looked up
at my fellow miners. The first gwak to get bold and
decide to cross the threshold was Parysia the Venoms.
She took several steps and… Syrdio held her by the arm.

—“Wait a moment,” he said. “First, we have to arm
ourselves. Pick up every rock you can find. Quickly.”

I obeyed him, and with Manras and Dil, I rushed along
with the others to collect stones from the cave. When I
had collected several, I returned to the gate. The Black
Cat and Syrdio were staring at each other and grumbling.
I dared not come too near, but I could hear them anyway.

—“You’re going to get us killed,” Yerris growled.

—“Better a handful of us die than all of us,” Syrdio
replied.

The Black Cat gave him a sarcastic smile.

—“We’ll all die anyway if the alchemist doesn’t give us
the sokwata, isturbag.”

—“Isturbag yourself, Black Cat. I may have been a fool
when I stole those pearls. But now you are the fool: the
gate is open. We’re free.”

—“No. We’re dead,” Yerris corrected him darkly.

Syrdio ignored him, and seeing that there were already a
number of us waiting by the gate, he looked at us, and
his expression clouded, perhaps because he understood at
that moment that we were waiting for his permission to
cross the threshold.

—“Give me some stones,” he demanded. We gave some to
him, and after putting them in his pockets, he said
firmly, “We’re all going to get out of here alive. First
rule: don’t make any noise. If an Ojisary appears and
gets in our way, throw stones at his head. Is that
clear?” We nodded. Syrdio swallowed and said, “Well,
let’s go.”

I spread my arms to hold back Manras and Dil and waited
for the others to pass. Rogan stopped by the open gate,
looking at us with surprise.

—“Aren’t you coming, Sharpy? You like the well so much
you want to stay in there?” he joked.

I rolled my eyes and turned to the Black Cat. He had not
moved an inch, and his dark face did not ease my
concern.

—“Come on, Yerris,” I encouraged him.

The semi-gnome sighed but nodded.

—“I have no choice. Let’s go.”

I gave him some stones, and we ran out to the back
stairs. We passed the other gwaks, and gesturing to
Manras and Dil, I explained in a whisper:

—“They know the way.”

Manras had left the metal door open, and I hoped that
the ruckus we had made before had not reached the ears
of the Ojisaries. As soon as we passed through the door,
the tunnel plunged into almost complete darkness. Only a
very faint light could be seen at the bottom. After a
moment’s hesitation, I decided to cast a spell of
harmonic light. It didn’t matter that they knew I could
use harmonies: they already knew I was a Black Dagger
anyway.

The way was very easy, there was no other way possible.
We went on for a while, and then my light faded, and I
did not put it back on, for the light at the end of the
tunnel was already sufficient for us to see. Although we
were thirty gwaks, we made less noise than a spider, or
so it seemed to me. I only hoped that there was no troop
of Ojisaries hiding behind that door.

I raised a hand to stop them all, and, Spirits be
praised, they obeyed me. I stood in front of the door,
touched it, and cast a perceptive spell through the
cracks, but I only succeeded in stupidly consuming my
energy stem. I winced and, taking a breath, turned the
handle. The door creaked as it opened. There was no one
behind it.

Before I could react, Manras slipped through the
opening, dragging Dil behind him, and he motioned for us
to follow him. We did through an empty room with
half-formed rock walls. We passed through a doorway into
a corridor. And at last, after so long in an underworld,
we saw the light of day. It was dim and unlit, but it
was daylight. As soon as we saw it, some of us lost all
caution and ran straight for the light. I followed them,
fearing that at any moment the Ojisaries would cry out
and draw their weapons, and my fear was well founded. As
soon as we opened the door to the outer corridor I heard
a roar which was half muffled by the pouring rain.

—“Aleeeert!”

The first gwaks threw stones at the watchman as they
ran, splashing in the mud and screaming at the top of
their lungs. The Ojisary shouted, backing away:

—“Sons of rats! Demons…!”

A stone hit him in the temple, and the Ojisary slumped
down. We passed over him, almost flying, when another
door in the corridor suddenly opened and another Ojisary
appeared. They threw stones at him, and one of us, who
had taken the bowl in which we put the pearls, smashed
it over his head. The Ojisary collapsed. One less.

—“Run! Run!” we shouted to each other.

At one point, Manras slipped in the mud, and I braked to
help him up. The little dark elf’s eyes were so wide
open that they seemed ready to pop out of their sockets.

—“Sharpy!” he shouted.

A third Ojisary had just appeared a few feet away with a
dagger in his hand. Before he fell on us, I pulled out a
large stone and threw it at him, hitting him squarely on
the nose and causing him to howl in pain.

—“RUN!” I bellowed.

I pulled Manras by the sleeve, and we ran like two wild
devils, following Dil and the others. The dead end was
not very long, and as soon as we reached the alleys of
the Labyrinth, we all scattered.

—“Stop or I’ll shoot!” a voice shouted.

I had just entered an alleyway with Dil and Manras, and
in dismay, I turned my head to see that an Ojisary was
aiming at Rogan, who was running behind us. Before I had
time to take in the situation, the Ojisary fired, the
bolt went off with a whistling sound, and the Priest
fell to the ground.

—“Rooogan!” I cried. Horror almost paralyzed me, but a
part of my mind told me that it was nonsense to remain
paralyzed in an emergency.

I turned back, but instead of stopping where the Priest
had fallen, I reached the Ojisary before he could reload
his crossbow, and fired a mortic blast at him. I saw him
stagger, surprised. He dropped the crossbow, but he did
not lose consciousness. With a stone, I struck him with
all my strength and saw him finally fall and give up
pulling out his dagger.

The rain was pouring down, and the screams, if there
were any, did not reach my ears. Clumsily, I stepped
back and returned to where Rogan lay. I bent down and
held out a numb hand to the bolt that had been stuck in
his side. I withdrew my bloody hand. The Priest was
breathing rapidly.

—“D-Draen?” he gasped.

—“Rogan,” I whispered in a high-pitched voice. “Are you
in a lot of pain? Tell me you’re not going to die,
please, Priest…”

The Priest clawed at the mud with a scarred hand, and I
took it from him, sobbing, as he said with effort:

—“Thank you… Sharpy. I… never really had… a real friend.
No one could ever stand me… as well as you do. You were
my friend, weren’t you? Please tell me you were.”

His interspersed words became incomprehensible. Manras
and Dil, instead of running as I had asked them to, had
come closer and were now looking at the scene with
distressed and bewildered expressions. Tears rolled down
my cheeks. I breathed in sharply.

—“I am, Rogan. I’m your friend. You’re not going to die.
Please don’t die…”

I heard someone shouting something through the rain, and
a few seconds later, I felt a hand on my shoulder
shaking me violently. It was Yerris.

—“What the hell are you still doing here?” he shouted.
“Move it!”

I looked at him as if what he was saying didn’t make any
sense, and I shook my head. In a neutral voice, perhaps
a little shaky, I said:

—“We need to get him to a doctor. Help me, Black Cat.”

The semi-gnome seemed to be about to shout at me again
to move, but then he changed his mind and gave me a
hand. We both took Rogan by one shoulder and dragged him
down the muddy alley, down the slope. Manras and Dil led
the way. From time to time, I glanced in horror at
Rogan’s semi-conscious face, and as we moved forward, I
tried to change his morjas to jaypu to give him more
energy, but it was not easy to concentrate, and for some
reason, my spells could not find their way to the bones.

We passed several silent Cats who perhaps did not even
really pay attention to us. We were gwaks, after all,
and many preferred to know nothing of our troubles.
However, when we came out of the Labyrinth and into the
Spirit Square, in the lower part of the Cats, we saw a
man with a cart passing by, and I shouted to him:

—“Please, sir! Please help us, he’s dying!”

The man in the cart, perhaps thinking it was some trick
to rob him, did not pull the reins at once, but
something he then saw through the rain, perhaps the
blood on our hands and on Rogan’s shirt, convinced him
that we were not pretending, and his heart must have
broken, and to my great relief, he brought the cart to a
halt.

—“Merciful spirits!” he exclaimed. “What has happened to
him?”

—“A bolt, sir! An isturbagged man shot him,” I
explained.

To his credit, the man in the cart did not hesitate even
for a second to help us hoist Rogan into his cart, and
he calmly said:

—“I’ll take him to the Passion Flower Hospital.”

—“Can we go with him?” Yerris asked. “Please.”

The man nodded his consent.

—“Get on,” he said.

I watched Yerris out of the corner of my eye as we
climbed. The further we got from the Labyrinth, the more
I could feel him relax. Kneeling beside Rogan, I checked
to see if his heart was still beating and whispered:
“Courage, Rogan, don’t die, don’t die…” We were driving
up Tarmil Avenue at a good pace when the half-gnome
leaned over and murmured in my ear:

—“Listen, shyur. The alchemist gave me some sokwata. I
tried to save him, but… he was chained. He said that
with what I took, we’ll last two moons if we only use it
when we start to feel the lack of sokwata. He also said…
that, at the moment, there is no cure, but he thinks he
would be able to make one. I don’t know if I trust him,
but… it could be true.”

Without looking at him, I heard all these words without
really processing them. Rogan was dying: I couldn’t
think of anything else. Yerris patted me on the
shoulder.

—“Try not to say too much if they ask you questions at
the hospital about what happened, it runs? It wouldn’t
do us any good if the flies got their noses into it and
took the alchemist away from us; I doubt it, but hey…
Just say that you found the Priest in that state and
that you didn’t see anything more. And if you can avoid
being asked questions, all the better. It runs?” he
repeated.

I swallowed and nodded. Yerris hesitated and added:

—“Trust the Priest. He speaks much of spirits, but he
will not become one until he has wrinkles and white
hair.” I felt his hand squeeze my shoulder as a gesture
of consolation and farewell. “Let’s meet tomorrow at
noon in the Evening Park, huh?”

I nodded again and saw him jump out of the cart and run
out into the rain. Who knows where he was going.

We made the rest of the journey in complete silence.
Under other circumstances, I would have been delighted
to see Estergat again and to feel the wind, the warm
summer rain and the open air, but at the moment, I could
only stand still as a mixture of tension, oppression,
and fear gripped my whole body.

At last, we passed through the garden of the Passion
Flower, and as soon as the carriage stopped, our savior
suggested that I should go and ask the doctors inside
the hospital for help. I ran out, entered the hospital,
cried out in anguish, and soon returned with two nurses
carrying a stretcher. They took Rogan down, and I was
about to follow with Manras and Dil, when I saw the man
in the cart waving the reins, and as he drove away, I
shouted in the rain:

—“Thank you, sir!”

I don’t know if he heard me, but that man would go home
with the blessing of a gwak. I ran off behind the
nurses. We followed them through the main hall, and then
they took a corridor, and a tall, chubby, light
blue-skinned kadaelf in a white coat stepped in our way.

—“Where do you think you are going, boys?”

—“The one on the stretcher is our friend,” I explained,
agitated.

The nurse pouted, looking us up and down.

—“I see. I’m sorry, but you can’t come in here. This is
the operations section. Follow me, please. What’s your
friend’s name?”

—“Rogan,” I said. And I glanced down the hall. The
nurses with the stretcher had disappeared.

—“Mm-mm,” the kadaelf said. He had slipped behind a
small table in the main hall and was grabbing a quill
and binoculars as he settled in. “Rogan what?”

I arched my eyebrows.

—“Well… I don’t know, sir. But he’s badly hurt.”

—“What happened?”

I sighed.

—“I don’t know. We found him already wounded. A madman
must have attacked him, I don’t know.”

—“So his injury wasn’t an accident?”

I shook my head and, under the surprised looks of Manras
and Dil, replied:

—“Maybe it was an accident. I don’t know what happened,
I only know that Rogan is very badly injured, and if you
don’t save him, your ancestors will wring your ears off
for it.”

The kadaelf looked at me through his binoculars, his pen
hanging on his notebook.

—“Does your friend have money to pay for the care?”

Blasthell. I hadn’t thought of that. Sajits and their
omnipresent money… I grunted and said:

—“I don’t know. How much is it?”

—“Well… that depends on what the doctor in charge of him
decides. If he has money, he will pay for the care and
stay. If he doesn’t, he’ll have to work for the Hospital
until he’s paid for the service he received. How old is
he?”

I put on an air of ignorance and said:

—“Twelve, or thirteen maybe, I don’t know.”

—“Does he have any relatives I can contact?”

I bit my lip, shrugged, and lied:

—“Dunno.”

—“You don’t know much about him for a friend,” the
kadaelf observed, “What’s your name?”

I opened my mouth, closed it again, and under his
increasingly surprised gaze at my silence, I decided
that I had said enough, grabbed Dil and Manras by the
sleeve, and stepped back.

—“Eh!” the kadaelf protested. “Where are you going?”

—“Leg it, comrades,” I muttered.

I turned and ran out of there. We did not stop until we
reached the Esplanade. The rain had subsided, and it was
hardly raining at all. There was even a glimpse of
sunshine through the clouds, and I saw a few bold people
coming out into the street without umbrellas.

—“Why did we run, Sharpy?” Manras asked, huffing.

I shrugged.

—“Because I didn’t like that guy’s questions.”

I walked across the huge square to the Manticore
Fountain and finished cleaning my hands of blood and
mud. Immediately afterwards, I sat down on the stone
curb and looked around. I drew in a breath, listened to
the sounds of the city, the shouts of the vendors, the
creaking of the horse-drawn carriage wheels, the
insistent rustling of the leaves of the trees that lined
the square, and a broad smile stretched my lips as I
found myself free at last. However, when I thought of
Rogan again, my smile faded.

—“These Ojisaries will pay dearly,” I said.

Dil was throwing the stones he had in his pockets one by
one, and Manras, with his elbow resting on the wall, was
playing with a green leaf on the water of the fountain,
pulling it by the stem. Neither of them seemed to be
paying much attention to what they were doing. And of
course they wouldn’t: only an hour ago, we were still in
Ojisary territory, throwing stones at our exploiters.

—“Sharpy,” Manras said without looking up. “Do you think
your friend will make it?”

I swallowed my saliva.

—“As he would say, let us pray for the spirits to heal
him. And for the doctors,” I added.

Dil dropped his last stone, sat down beside me, and
said:

—“So we’re never going back to the Ojisaries again, are
we?”

—“Mmph. No, of course not,” I said. “Listen. They’ll be
looking for us, that’s for sure. And I don’t think
they’ll forgive us for what we did… stoning them and
all. If they get their hands on us, they’ll kill us, for
sure.” I saw them widen their eyes and smiled casually.
“Bah, don’t be afraid, shyurs: the Ojisaries won’t catch
us, because we’re gwak Cats, and gwak Cats have more
than one trick up their sleeve. First and foremost
important thing,” I said, standing up energetically, “a
gwak Cat thinks much better on a full stomach, so… I’m
gonna take advantage of the fact that I look like a
dirt-poor kid in critical need, and in no time at all,
I’ll be back with something to eat, how’s that?” I took
off Dil’s cap with a nimble movement, and seeing that
they were going to follow me, I added in an expert tone,
“No way I’m going to panhandle in a group, it scares the
customers. Don’t move from here.”

I wandered off alone to the stalls that surrounded the
square, and making sure that there were no guards in the
vicinity, I went in search of charitable people. I
spotted a promising face near an apple stall and
approached, holding out my cap and putting on a
distressed and pleading face. But, just as I began my
lament, the man went to pay for a small bag full of
apples, became distracted when he heard me, dropped
several coins on the floor, and swore:

—“Demons.”

—“Don’t worry, I’ll get them for you!” I said.

I picked them up. If I had had a shirt on, I could have
slipped some coin discreetly into the sleeve, but under
the gaze of the apple buyer and seller, I could hardly
put anything in my cap without them seeing me. After
promptly retrieving them, I handed them to the man,
waited for the seller to give him the bag of apples, and
complained:

—“Please, sir. I’m hungry. Give me something, by the
Spirits of Mercy.”

The man, who had had time to feel sorry for me and see
my goodwill, gave me nothing more and nothing less than
one fivenail. I huffed.

—“Thank you, sir! May your ancestors bless you.”

The man smiled benignly and without a word walked away
with his bag of apples.

Wow, wow, I thought, incredulous. The first person I
asked that day and they gave me one fivenail. Now I
understood why my namesake Swift said that a career as a
beggar was more profitable than as a newsboy. Smiling, I
ran to buy a loaf of bread and returned to the Manticore
Fountain. My friends had hardly moved, and both saw me
appear with enthusiasm, so I deduced that the Ojisaries
must not have given them much to eat either. I divided
the bread into three pieces, gave them the two larger
ones, kept the third for myself, and for the first time
in a moon and a half, I ate real bread, fresh from the
oven and free from strange products.

—“What are we gonna do?” Manras asked, as we finished
eating.

I swallowed my mouthful and replied:

—“The first thing is to keep the Ojisaries from getting
their hands on us. You guys, if you see any Ojisaries
that you know, you tell me, okay? And we scram the hell
out.”

—“Even if it’s Lof?” Manras asked. “That guy’s not so
bad.”

—“Even if it’s Lof,” I confirmed. His question, oddly
enough, consoled me a little because, for some strange
reason, I had feared that one of the stoned Ojisaries
might have been Lof the Masked One. It wasn’t that I
rationally sympathized with him either, but… well, he’d
taken care of us every day, feeding us… I didn’t want to
hurt him.

At that moment, the bells of the Great Temple rang, and
I counted.

—“Seven o’clock,” I said. “Time to look for a good
shelter. Onward, shyurs.”

They followed me, and we walked down Imperial Avenue at
a good pace. After passing through an almost empty
market, I cut through a deserted street, heading
straight for the Estergat River. My young friends did
not say a word until we crossed the Fal Bridge and
entered the area of Canals and factories.

—“Sharpy! Where are we going?” Manras asked then.

I replied cheerfully.

—“To the birdhouse!”

I took them to the Crypt. They did not at all share my
confidence in the trees of this beautiful home, and
realizing it, I said to them, as we entered the forest:

—“Listen, instead of streetlights, there are trunks, and
instead of streets and squares, there are paths and
clearings, but apart from that, it’s a bit the same, and
here we won’t find any crazy sajit coming to disturb us.
Besides, from there, we see the stars as well as in the
valley. You’ll see it when night falls, unless the
clouds don’t go away. Come on, let’s go on!” I
encouraged them.

And breathing in the smell of the forest, a strange
euphoria came over me, and I laughed when I saw the
unconvinced faces of my friends.

—“Come on, come on!”

I trotted between the trunks and bushes and heard their
cries behind me.

—“Sharpy, don’t let us behind!” Manras begged me.

I stopped, turned around and smiled as I saw them
running in a hurry.

—“Onward, comrades,” I urged them. “Come on, we’re
almost there.”

—“I heard… that there are monsters in this forest,”
Manras said as he joined me, panting.

—“Boo. Monsters are everywhere,” I assured.

At last, we came to the foot of the huge tree which had
been my shelter the last time I was here, and Manras,
having seen me climb nimbly, imitated me and smiled
broadly as he reached my branch.

—“Come on, Little Prince!” he encouraged.

—“Courage and bravery!” I approved. “We’re like kings up
here. Aren’t we, Manras?”

—“Ragingly!” the little dark elf confirmed.

And as Dil still hesitated, resting and removing his
foot on a protrusion of the trunk, I said to him:

—“The wolf is coming!”

The little prince was startled, and although I do not
believe that he had fallen for it, he finally decided to
climb up to us. The sky was already getting darker when
the three of us settled down in the heart of the tree.

—“Now let’s snooze, shyurs!” I said.

I listened to their breathing and the songs of the night
birds and insects. Despite the afternoon rain, our
shelter was relatively dry thanks to the leaves. The
disadvantage was that I could hardly see the sky because
of the leaves.

—“Sharpy!” Manras murmured.

I yawned and turned my head.

—“What?”

—“What’s that noise?”

—“What noise?”

—“The pwiii,” Manras explained, imitating the sound.

—“Oh. That’s an owl,” I said.

—“Ah,” Manras sighed. “And what is an owl?”

I smiled in the growing darkness, for Manras’ question
reminded me of the ones I’d asked Yerris and Yalet the
year before, except instead of asking me what a guard
was, he asked me what an owl was.

—“It’s a bird,” I replied.

After a silence, Manras whispered:

—“Sharpy. Are you awake?”

—“Hmm…”

—“You think my brother’s mad at what I did?”

I opened my eyes and choked back a snort.

—“Blasthell, Manras. Natural that he should be angry,
and not a little. That cove is a devil.”

There was silence.

—“I don’t want to see him ever again,” Manras murmured.

I smiled understandingly and reached out a hand to shake
his arm.

—“Then come with me, shyur. Send your brother hunt
clavicles. We’re friends, right? And more than that.
You’re the one who got us all out of the well with the
key. We’re brothers, real brothers, the kind that
protect and support each other. You, Dil, and me. Ain’t
we?”

I could see his smile in the shadows, but it was Dil who
answered:

—“We are.”

And Manras supported him:

—“Ragingly.”

The silence fell and gradually our breathing became more
regular. It took me a long time to fall asleep, because
to feel free again, after being locked up in a hell for
so long, was an unforgettable experience. I only hoped
that Rogan could remember it for a long time, too.

Korther the Good
================

We were so comfortable in that tree that we slept like
lebrine bears, and when I opened my eyes and saw the
clear sky, I made sure that we still had time before
noon, and then I closed my eyes and lazed around
listening to the birds. For the first time in many
wakings, I felt energized. This salbronix mine may not
have affected us as much as normal sajits, but fighting
against its energy weakened our strength anyway. There,
on the other hand, lying in the shelter of the old tree,
I felt a deep sense of well-being.

After a while, I pushed Manras’ feet gently and slipped
out of our nest. I went down to the base of the tree and
looked for lunch without going too far. I found an
insect I recognized, and chewed it energetically, then I
came across what seemed to me to be a lettuce, but just
in case, I did not touch it. Eventually, I found three
large snails, put them in my pocket and carried them
back to the foot of the tree, where I began to sing:

Taran tran tran!\
Blessed souls, wake up!\
The sun is already up!\
The sun is already uuuup!


With a few more shouts and exclamations, I encouraged
them to come down from the tree and handed them each a
snail.

—“Enjoy!”

Wide-eyed, they both watched me eat my own snail, and
Manras threw out a:

—“Ew, I’m not eating that, Sharpy.”

I gave him a mocking look and said:

—“Nail-pincher.” And with an emperor’s air, I turned to
what seemed to me to be the northwest and walked
forward, clamoring, “Let’s get back to the city, we’ve
got an appointment with the Black Cat!”

When I glanced back a moment later, I could see that
Manras and Dil were chewing their snail without looking
disgusted. And I smiled broadly.

That day, the sky was blue and bright, and a warm breeze
blew strands of hair before my eyes as we reached the
edge of the forest. I blew my hair aside and cried out:

—“Last one to the Moon Bridge is a slugbonehead!”

There was still a good trot to go, and though we started
off fast, jibing each other, after a while we moderated
our pace, but when we got between the houses on the
White Path, we saw the bridge with its two slender
towers, and we ran like mad. Almost all three of us
arrived at the same time. “Almost,” because I got there
first. I smiled at them, breathless.

—“Aha! Sorry, shyurs!”

A baritone voice answered me:

—“Out of the way, kids, coming through!”

We moved quickly away from the middle of the bridge and
let an old man pass with a cart full of bags. One of
them was half open, and I could see that it contained
apples. I sighed and thought, if only there were apple
trees in the Crypt.

We passed the guards who were watching the bridge
without their giving us more than a passing glance, and
we walked the rest of the way to the Evening Park along
the river walk, stopping whenever we saw anything
interesting, and glancing around apprehensively from
time to time, as if we feared that suddenly some
Ojisaries would attack us in broad daylight, armed with
their crossbows and accompanied by their dogs. We
arrived at the park safe and sound.

I did not know what time it was, and to find out, we
went first to the little temple near the park. As we
entered and passed through the room full of benches, the
priest, a slender, young, bright-eyed human, looked at
us with compassion on his face, and answered my
question:

—“It is almost noon. Hold on, my child,” he added. He
was looking at me. “This morning a charitable lady came
to bring me some used clothes. If you wait a little
while, I may be able to give you something.”

The prospect of being late for the appointment bothered
me, but who could refuse such a generous offer? I
nodded, smiling.

—“That’s very kind of you.”

The priest lit a candle, placed it before the Altar of
the Ancestors, and made a devotional sign before walking
away and disappearing through a small door. I looked at
the candles with a pensive pout. One candle of those
were probably worth more than ten nails… The priest
reappeared almost at once, carrying in his hands a
rather white shirt and an old cap with a hole in the
peak. As I reached out my arms, ready to take my gift
and give thanks, he held back.

—“In return, don’t forget to say a prayer to the
ancestors of the Temple of the Gracious. That’s what my
temple is called.”

I smiled and solemnly raised my fist to my chest.

—“No prob, I’ll say a prayer. Thank you, priest,” I
added, as he handed me the shirt. I slipped it on and
put on the cap, saying, “Bless the generosity of the
priest of the Temple of the Gracious, and may his
ancestors and those of the Gracious keep him long. Peace
and virtue. Is this all right?” I inquired.

The priest laughed, amused.

—“It’s all right. Go out and let the ancestors watch
over this sacred place. And may they watch over you too,
little ones. Remember that every soul, no matter how
humble, is watched over by the spirits of our ancestors,
as long as they show them respect.”

Manras and I nodded, and I caught the troubled look the
priest gave Dil. I frowned. Surely this priest didn’t
believe that “devil-eyes” like Dil could be evil
creatures, did he? Boo. I’d rather not ask him. We
quickly left the temple and returned to the Evening Park
just as the bell of the Great Temple was ringing at
noon. We sat down on a stone bench in the central
square. The problem with the park was that it was so
large that it was hard to know where Yerris expected to
find us. And the good thing was that the Black Cat,
precisely because he was jet black, would be easily
noticed. We would see him from a distance. I was running
a curious hand over my new shirt, noting that, though
worn, it was of good quality, when Manras pulled me by
the sleeve.

—“Sharpy! That’s him, right?”

I looked up and saw him. He was not coming alone, he was
accompanied by Slaryn, the Soothsayer, and the Mole. I
smiled, and when I saw that they too had seen us, I did
not bother to stand up and waved at them.

—“Ayo, shyurs,” Sla and Yerris said at the same time as
they joined us.

—“Ayo, ayo!” I replied. And I rose at last, glad to see
all four of them. “How did you manage to find each
other?”

Yerris cleared his throat, and Slaryn smiled.

—“Let’s just say that your escapade has given people in
the Cats a lot to talk about. Yerris and I had agreed to
meet at… a place, and as soon as I heard what happened,
I went there and found the Black Cat waiting for the
princess to rescue him.” The Black Cat rolled his eyes,
and the dark elf confessed, “Actually, there wasn’t much
I could do. Your companions have scattered all over the
Labyrinth. The Soothsayer and the Mole were in the Wool
Square.”

I grimaced and said:

—“That place is not a good shelter.”

I remembered well that the Ojisaries had caught me there
with extreme ease.

—“No, it’s not a good shelter,” Sla conceded. “That’s
why we found another one. Though Yerris isn’t too
convinced.”

The semi-gnome put on an apologetic face.

—“I’m just not particularly thrilled about taking refuge
in the Labyrinth to escape from demons who live
precisely in the Labyrinth.”

—“Weren’t you the one who said the Labyrinth was a place
full of wonders?” the Black Dagger girl scoffed.

—“And it is. But not right now,” Yerris cleared his
throat. And he gestured expansively. “It doesn’t matter,
we don’t have anything better right now. The thing is,
right now, the Ojisaries are the laughing stock of the
Labyrinth, and they’re madder than a cat with water up
to its neck. Have you heard from the Priest?” he asked.

I shook my head, darkening.

—“No. I was thinking of going there now to check on him.
Where’s that shelter?”

—“Huh, huh,” the Soothsayer interjected with a small
smile. “To find out where he is, you either have to be a
soothsayer or someone has to show you the way.”

I cocked my head to one side, curious.

—“That’s a good thing.”

—“Yerris will show you,” Sla decided. “I have to go…
negotiate.”

I looked at her with a puzzled face; Yerris and she
exchanged a look, and I widened my eyes in amazement,
thinking I understood.

—“Negotiate with the Ojisaries? To free the alchemist?”

—“No, no, no,” Slaryn laughed. “Negotiate with our kap.
Be careful what you say,” she added in a whisper that
the Mole, the Soothsayer, and my comrades no doubt
heard.

I swallowed my words, and as I saw Slaryn take a step
back as if to leave already, I cried out:

—“I’m coming with you!”

Slaryn stopped short.

—“What? No, shyur. You can’t… This is ridiculous. He
barely knows you. You’re no help to me.”

—“I’m not going there to negotiate,” I assured him. “He
owes me twenty goldies, that’s all.”

And I intended to pay for Rogan’s care with those
siatos, I added mentally. The two Black Daggers looked
at me with even more surprised faces than the others.

—“Twenty goldies,” Yerris muttered in disbelief.
“Blasthell, what have you done, shyur?”

I shrugged.

—“A thing.” I smiled as I saw them positively impressed
and said, “So, can I go with you, Sla?”

Slaryn nodded thoughtfully, and Yerris cleared his
throat.

—“I’m sorry, but I don’t take care of snotty brats,
shyur, I have things to do. As for leaving them alone in
the shelter, I don’t think it’d be a good idea…”

—“Eh, snotty brat, your mother! Who do you think you
are?” Manras interrupted indignantly. “We don’t need you
to take care of us. We’ll go and sell newspapers and
earn our bread.”

I nodded, concerned.

—“It runs, but don’t get away from the flies, and if
there’s an Ojisary or any isturbag coming at you, you
start screaming like scaluftards.”

—“Natural,” the little dark elf replied.

It was safer, anyway, to wander around Riskel or Tarmil
quarters than to go into the Labyrinth without a good
band to protect yourself. I said goodbye to them after
telling them we would meet on the Capitol steps at six
o’clock; leaving Yerris, the Soothsayer, and the Mole, I
left the park with Slaryn. We walked briskly up the
streets of Tarmil, and we were already crossing the
Avenue when I asked her:

—“Are you going to ask Korther to help us?”

Slaryn walked with long strides. Probably out of
caution, she had hidden her long red hair under a pretty
orange veil.

—“The problem is that with Korther, you don’t ‘ask’, you
negotiate. When I went to see him the day before
yesterday, he didn’t seem very willing to lift a finger
to help us. He said he’d think about it…” She huffed
sarcastically. “In any case, Korther is not one to act
hastily. So it’s a good thing your friend got you guys
out.”

—“But without the alchemist, we won’t last more than two
moons,” I reminded her.

Slaryn winced.

—“We’ll figure it out. One way or another.”

I wanted to believe her, and we did not speak the rest
of the way. So that we would not have to go through the
Cat Quarter, Sla made us go around, through Atuerzo and
down the Old Wall stairs, and landed almost directly in
the street of the Hostel. She slipped into the dead end,
and after glancing at me, knocked on the door in such a
way that it sounded like a password. The door opened a
little and the very pale face of a dark-haired,
relatively young human appeared. I did not know him.

—“Ayo, Aberyl,” Sla said.

—“What about this one?” inquired the man named Aberyl.

—“It’s a sari,” the dark elf replied calmly.

Without asking for any further explanation, Aberyl
opened the door wide. We entered. The last time I had
been there, I had hardly noticed the interior. This
time, I could see it with more peace of mind. There was
a table with chairs, an armchair, an unlit fireplace,
and above it, a picture of a village street. Although
Korther could not logically be short of money, this room
was not luxurious, at least not like Miroki Fal’s room.

—“Where is Korther?” Slaryn asked.

—“He’ll be here soon,” Aberyl answered. He knocked a few
times on the only inner door there was, sat down at the
table, and continued to do what he had apparently been
doing before we arrived: putting a small pile of grey
powder into a flask.

—“Do you know what satranin is?”

He was asking me, perhaps because I had come closer to
the table out of curiosity to see what he was doing. I
nodded.

—“Yalet told me it was a sedative.”

Aberyl smiled without revealing his teeth.

—“Hmm. A strong sedative that can put a person to sleep
if they breathe it in close.”

I took a step back, cautious.

—“Gosh. And you use it often?”

Aberyl shrugged, amused.

—“Sometimes. For example, when you work at night, you
make the landlord breathe it in, and you have the whole
house to yourself for several hours.”

This impressed me, and I suddenly imagined how, running
with that vial from Ojisary to Ojisary, I would leave
them all asleep and manage to get the alchemist out of
their territory and save all my companions… The scene,
though most likely unrealistic, brought a vengeful smile
to my face.

Suddenly, the inner door opened, and Korther appeared.
The elfocan quickly examined me with his reptilian devil
eyes before settling on Slaryn and sighing patiently.

—“Hello, young people. What can I do for you?”

—“You know damn well, Korther,” Sla said dryly. “My
mother is going to wring your ears off when she gets out
of the slammer and finds out you left her daughter
locked up in a salbronix mine run by the Ojisaries. What
will the other Black Dagger kaps say when they find out
you’ve left your saris in the hands of criminals and
done nothing? What will they say when they find out that
you have done nothing to prevent them from being treated
like guinea pigs, experimented on, and mutated? What
will our confreres say when they hear that your saris
came crawling back to the Ojisaries to ask for sokwata
because you refused to help them?”

I was stunned. Each question was phrased with increasing
irritation. Without appearing very surprised, Korther
raised his hands soothingly.

—“Calm down, dear. You won’t get anywhere by getting
angry or blaming me for what the Ojisaries did to you.
You’re right: as a kap, I’m compromising myself to help
out the young people of the brotherhood. But I don’t
compromise myself by helping reckless people who start
spying on the Ojisaries to save a traitor. I explained
that to you quite clearly last time.”

—“Yerris is not a traitor!” Slaryn growled.

—“He was. I’m not saying he was doing it with good
grace. But he was and is a traitor.”

Slaryn’s eyes sparkled.

—“At least give him a chance, Korther. He wanted to be a
Black Dagger. I did not. And you’re disowning him and
not me. It’s not fair.”

—“Life is unfair, my dear. And I don’t forgive easily.”

—“If I die, it is my mother who will not forgive you,”
Slaryn replied.

Korther shook his head, sighing.

—“And it would pain me, I assure you.” He stepped
forward, hands in his pockets. “Look, Slaryn, the
situation doesn’t look so hopeless. Yesterday you told
Alvon that you had sokwata for two moons, didn’t you?”

—“For… a little more,” Slaryn admitted. “The Ojisaries
managed to catch a few kids again, I don’t know how
many. We still haven’t found all the sokwatas who
managed to escape.”

The kap nodded meditatively as I turned pale. So we
hadn’t all escaped? Blasthell…

—“Good. Fantastic,” Korther said. “So, maybe you have
enough for three or even four moons, right?”

Slaryn gave a sarcastic pout.

—“Fantastic?” she repeated. “It seems anything but
fantastic to me. Four moons of life is a pittance. But,
anyway, the Ojisaries are going to kill us before then
because no one is doing anything to end this gang, least
of all Korther the Heartless.”

—“There you go, blaming me again,” Korther pointed out
to her patiently. “Look, darling, just two days ago the
Black Hawk was a nobody, and now he’s competing squarely
with Frashluc of the Cats himself. Do you know what we
Black Daggers are in all this, Slaryn? Professional
thieves, a little adventurers, mercenaries… but we are
not warriors, nor heroes, nor suicidal. Your friends had
a chance of a thousand demons when they escaped. Now,
the Black Hawk has probably recruited more people. He
could recruit an army. If he was really pocketing ninety
salbronix pearls a day, he must be rotten with money.
Those pearls, you don’t sell them for less than fifteen
siatos each, and maybe even more.”

Fifteen siatos, I thought, frowning. And the kap had
only given me five siatos for the five pearls I had sold
him in winter. He had tricked me.

—“I think I forgot to mention to you, too,” Korther
added, “that the Black Hawk and I came to a mutual
agreement some time ago. I paid him a good sum, and the
scoundrel agreed to destroy certain information.
Information, by the way, that Yerris had stolen from my
office last year here at the Hostel. He confessed to it
himself. It was carelessness on my part, I admit, but
don’t tell me this wasn’t an infamous betrayal on the
part of that innocent saint who seems to have won you
over so well, my dear.”

Slaryn returned a troubled look and ran his hand over
her forehead, muttering a:

—“Spirits.”

I bravely stepped in:

—“Korther. Yerris didn’t want to betray you. Those guys
forced him and—”

—“They trained him for that,” Korther cut me off. “Don’t
side with him, lad. You’re in enough trouble already.
Well, let’s see. You’ve come here to ask me to forget my
deal with the Black Hawk and that I give you a hand in
capturing this alchemist because, as I understand it,
he’s your only salvation. Have you not thought that you
may have been led to believe that this sokwata is a very
difficult potion to make and that in reality it is not
so difficult? Who knows, maybe the formula for making
the sokwata isn’t that complicated and can be picked up
by another alchemist, or maybe,” he said, “maybe this
whole thing about you dying if you don’t take sokwata…
Maybe they just made that up to scare you.”

Slaryn let out a sarcastic growl.

—“Yes of course! Yerris told me what happened to them
when the Ojisaries stopped giving them sokwata: after a
week, they were almost dying.”

—“Almost dying,” Korther pointed out. “Maybe they put a
poison in the bread so they would draw the wrong
conclusions. Or maybe, after a while, they would have
detoxed or unmutated or whatever.”

Slaryn hissed:

—“Impossible: the alchemist himself told Yerris that,
without sokwata, he would die.”

—“Yerris,” Korther repeated. Under the dark elf’s glare,
he rolled his eyes. “I’m not saying your story isn’t
true, Slaryn: I’m only saying that, so far, we have no
proof of anything.”

—“That’s because you don’t listen to me, isturbag!”
Slaryn cried. She seemed about to add something, let out
an exasperated growl, made an angry gesture, turned
around, opened the door, and left, slamming it behind
her.

I blinked in amazement, and for a moment, I was tempted
to follow her, but then I remembered my twenty siatos.

—“Mothers of the Light,” Korther sighed, sitting down in
his armchair.

—“Strange business, eh?” Aberyl let out, leaning back in
his seat.

—“You said it, Ab. You said it,” Korther murmured.

The pale-skinned human slipped the bottle of satranin
into his pocket and said:

—“I don’t know about you, but I personally don’t really
like the idea of letting thirty kids die because of an
alchemist and a criminal. I know it could be risky, but…
having a good alchemist in our brotherhood, it could be
very useful to us.”

Korther looked at him as if he had gone mad. He huffed
and looked away in disbelief.

—“You and your wacky ideas, Aberyl. Listen, for the
moment, as far as we know, the only thing the Black Hawk
has done is to capture gwaks and put them in a mine to
get salbronix, exactly like the factory owners do in the
Canals, and nobody stops them, right? Bah. You’re not
going to call me heartless too, are you? Am I now
supposed to deal with the problems of the Cat gwaks?
Come on, Ab, come on, I’m not going to make a hasty
decision that gets us a whole criminal gang on our backs
and sinks us all at once. Then our colleagues would
definitely laugh at me and laugh their asses off. And I
would go down in history as Korther the Good, that kap
who, in an attempt to save thirty gwaks, squandered his
fortune and ended up tragically murdered by a criminal
who had only just known what a gold coin was barely some
days ago. Oh, come on!”

He clicked his tongue dismissively, and I saw the
corners of Aberyl’s mouth go up acutely.

—“You’re getting yourself all worked up, Korther.”

—“Worked up? Me? Bah!”

—“How good it is to have a clear conscience when one
closes one’s eyes at night, ready to begin a new day,”
Aberyl pronounced in a wise tone.

He readjusted the blue scarf in front of his face and
stood up. Korther gave him a mocking look.

—“Stop it now, Ab. My conscience is very clear. I have a
thousand matters on my mind, I do what I can.”

—“Frawa won’t forgive you,” Aberyl commented calmly.

Korther rolled his eyes.

—“If Frawa would stop going in and out of jail and take
a little more care of her daughter, maybe that one
wouldn’t have ended up sympathizing with a traitor and
preferring the streets to the Den. But, hell, now that
Rolg’s gone, maybe you’ll offer to take in that young
cat,” he scoffed.

—“Mm. Too feisty for my taste,” Aberyl said. His very
clear blue eyes smiled. They landed on me, and flinching
at their sudden notice of my presence, I adopted the air
of one who hears without listening and waits patiently
for the adults to pay attention without the slightest
intention of prying. “The boy, on the other hand, looks
quieter. He wouldn’t happen to be the one who helped you
steal the Wada, would he?”

Korther smiled.

—“Himself. What do you want, lad?”

I looked at him hopefully.

—“Well… You see. I’ve got an injured friend at the
Passion Flower Hospital. And I need money to pay for the
care.”

—“Ah, you’ve come to claim the twenty siatos, haven’t
you?” I nodded, and Korther reached into his pockets.
“Here you are—seven siatos in silver coins. Give that to
them. And if that’s not enough, I’ll give you more.”

I did not complain, I smiled, picked up the coins, and
said:

—“It runs. Thank you, sir. Say, is it true that Rolg has
left the Den?”

Korther winced and cleared his throat.

—“Yes. He’s gone.”

My face got darker.

—“But where?”

Korther gave me a mysterious pout, and his eyes assessed
me carefully.

—“The Spirits know where. In his absence, let us
remember him as a man of heart, eh?”

I turned deathly pale.

—“He died?”

I remembered vividly the last time I had seen him,
covered in black marks, with sharp teeth, and in short,
transformed into a demon. What if, in fact, he was in
mortal danger that day and died because I didn’t help
and…? The elfocan laughed out loud.

—“No. That old elf is alive and well, more alive than
any of us. He just took a vacation, that’s all. Everyone
needs a change of scenery now and then.”

I sighed with relief and stared at him. He’s alive, and
more alive than any of us, I repeated to myself. Didn’t
my nakrus master say that demons worshiped Life,
convinced that they were more alive than normal sajits?
Korther knew. He knew that Rolg was a demon. Who knows,
maybe Korther was one, too, I thought with a shudder.
Well, as long as he didn’t find out I had an undead
hand… I inhaled and shook my head. The thought of the
change of air made me think of Yal, and I asked:

—“What about Yal? Where does he live now?”

—“Dear Heavens, Yal has not yet returned from Kitra,”
Korther informed me. “He has been very busy. In fact, he
doesn’t even know about your adventure in the mine. I
didn’t want to worry him. It shouldn’t be long before he
returns.”

I nodded thoughtfully, and Korther smiled at me.

—“Hey, lad. Tell me, you’re aware of everything the
Black Daggers have done for you, aren’t you?”

More like of everything that Rolg and Yal had done for
me, I mentally corrected. But I nodded anyway, and
Korther continued:

—“I understand why you tried to save Yerris. I am not
accusing you. And it may even be that you are right and
that Yerris is simply a poor, tortured, misunderstood
gwak.”

I opened my eyes wide, filled with hope.

—“So you’ll forgive him?”

Korther pouted.

—“Uh… Let’s just say I don’t feel like forgiving him
yet, but maybe one day, if he proves to me that he knows
how to be loyal… Who knows, life is full of surprises.”
I heard Aberyl stifle an amused gasp as he leaned
against a wall. Korther continued, “Anyway, you, you’re
still a sari of the brotherhood and as such, you’re
going to do me a small favor. If anything happens, like
if the Ojisaries capture any more children or… anything
that you think is important, you come here and tell me.
These days, if I’m not at the Hostel, Aberyl will be.
You got that?”

I shrugged.

—“Ragingly.”

Korther smiled again and patted my cheek.

—“Well, go see this injured friend and let’s hope he
recovers.”

I nodded vigorously, glanced at the pale human, and said
to them both:

—“Ayo.”

I got out of there and was already thinking so much
about Rogan and the hospital that I forgot to take a
detour and went right through the Grey Square in the
Cats. When I heard a loud “Hey, kid!”, I jumped up, my
heart racing, convinced I was seeing Ojisaries
everywhere. Then I saw old Fiks sitting on a stone bench
with some companions, and I let out a sigh of relief.

—“It’s been a long time since we’ve seen you around
here, Bard,” the old worker greeted me.

I smiled and approached.

—“Fiks, good to see you, you gave me quite a scare. How
are you?”

—“Well, as you see, chatting with the whole crew,” the
old workman replied, while his companions continued to
talk cheerfully. “You look very pale, as if you had not
seen the sun for moons. Tell me, by any chance, you
didn’t do anything funny that sent you to Carnation, did
you?”

I huffed, making a vague gesture.

—“Nah. I don’t know any flies.”

—“Oh? Well, that’s just as well,” Fiks smiled, with the
face of one who wants to say that, altogether, it’s none
of his business. “Anyway, I know you’re a good guy!”

I smiled back at him, and then I saw familiar figures
beyond Fiks on the other side of the square. It was my
namesake Swift with two of his band. And his watchful
gwak gaze was upon me. Blasthell. Suddenly, I was fully
aware of the coins in my pocket, I felt them in danger,
and I threw:

—“Well! Gotta go. Ayo, Fiks.”

I turned and ran out of the square and up a street in
the direction of Atuerzo. At one point, I glanced back,
and seeing that Swift was following me and fast, I
widened my eyes, accelerated, and a dull fear came over
me. My namesake was not called “Swift” for nothing, for
in a few moments, he caught up with me and seized my
arm.

—“Hey, Sharpy! Why are you running?”

—“Let me go!” I shouted at him.

Swift arched his eyebrows.

—“Devils. What got into you?”

I glared at him and pulled to free myself. The
red-haired elf let go of me, putting on a peaceful
expression.

—“Hey, namesake, you’re not giving me that face because
of the goldies you gave me last time?”

—“I didn’t give them to you, you stole them from me,” I
growled.

I gritted my teeth as the two companions of Swift’s
joined us. We had reached the street that ran along the
remains of the Old Wall, right on the border with
Atuerzo. There were quite a few passers-by, but they
passed us without even glancing at us. I stepped back,
glaring at Swift.

—“Stay away from me, isturbag.”

I saw a mocking and exasperated glint in Swift’s eyes.

—“I just wanted to say I’m glad you made it out of hell
alive. And now, call me isturbag one more time and I’ll
slap your face, shyur.”

I shrugged, and having already gone a good many paces
away, I said to him:

—“Demorjed!”

I turned my back on him and ran off. Fortunately for me,
Swift did not pursue me this time.

When I arrived at the hospital, I left my seven siatos
with a clerk, asked to see Rogan, and a young nurse led
me to a large room full of beds and patients, where she
left me to look for my friend. I wandered between the
beds, and for a terrible moment, did not see him, and
thought I would not find him. But then I saw him at the
back, near the window which looked out on to a
courtyard. He was lying asleep, his skin so pale it was
frightening to see. I knelt beside him and looked at the
bandage, and then I looked back at his face. I touched
his forehead and concentrated. By dint of my insistence,
despite the sokwata skin that protected him from my
spells, I managed to find a way to speed up the
transformation of the morjas in his bones and turn it
into jaypu. It wasn’t much, but it always helped a
little, or so my nakrus master said. When I had almost
completely consumed my energy stem, I whispered to him:

—“Priest, you’re going to be fine. My ancestors told me
so, and though I do not know them, they are not
mistaken. He who does not believe me is a miscreant!”

I smiled as I realized that I had unconsciously imitated
his excited tone. In the room, the murmurs of the nurses
and the complaints of the awake patients could be heard.
After a moment’s silence, I stood up and realized that
the boy in the bed next to Rogan’s was looking at me
with a sarcastic expression. I frowned, as if to say,
“Whatcha lookin’ at?”, and he seemed that, in the end,
he was going to keep silent, but then he let out:

—“Gwak.”

I arched my eyebrows. It wasn’t the first time I’d heard
that word used as an insult, but it was the first time a
kid had said it to me like that, as if he was worth more
because he had parents and I was less because I didn’t.
Well, hell, I wasn’t. Several lines went through my
head, some of them quite good, like “nail-pinching
shrimp”, or “unlicked cub”, but in the end I preferred
not to make a scandal: I straightened up and ignored him
as decently as I could.

—“Priest,” I said in a low voice. “I’m sorry the company
isn’t as good at the hospital as it is at the well. But
you’ll see how you’re getting back in a
peace-and-virtue, and soon you’ll tell us one of your
stories, and I’ll make a song out of it.” I smiled.
“I’ll come back tomorrow and bring you an apple. You
said it was your favorite fruit. Or maybe a flower. I
already know which one. A moonflower. My master said it
was good for everything. Except… in the valley there
were many, but here I haven’t seen any. Don’t worry, if
I don’t find one, I’ll bring you something else, okay?”

Rogan, of course, did not answer. But I was sure he had
heard me. Finally, with a sigh, I walked away. And I
held back from punching the bandaged leg of the
nail-pinching shrimp. Because I was an honorable gwak,
and proud of it.

Survive but do not betray
=========================

The refuge where Yerris took us was, in fact, almost
invisible to anyone who did not know where it was. It
was a rocky cavity located behind one of the countless
buildings of the Labyrinth. It had two openings. One,
“the chimney”, was a simple hole in the rock perhaps a
span wide. The other was “the door”, which was actually
a narrow opening at the bottom of a particularly lost
dead end where the residents of the neighborhood dumped
all their garbage and old, unusable utensils. The Mole
and the Soothsayer had tried to tidy up the mess and had
put a wooden crate at the entrance to keep the rats out,
but there was no escaping the smell.

I called our shelter the Cave, for its small size and
appearance reminded me of the cave of my nakrus master.
Well, it was not exactly the same. There was no chest,
no lantern, and no mattress either, and the first night
I spent there, squeezed between Manras and Yerris, made
me wish I were like my nakrus master. He didn’t need to
sleep, the smells didn’t make him gag, and his muscles
didn’t get stiff. But, well, as he would have said, the
life of a nakrus also had its drawbacks.

We spent eight days playing cat and mouse with the
Ojisaries. Every time we left the cave, we looked
paranoid. We took exaggerated detours, we always went
out in groups of at least three, and we avoided the
squares in the Cat Quarter. Truthfully, we didn’t run
into any Ojisaries. Yet Yerris and Slaryn prowled the
neighborhood looking for our companions from the Well.
They found Syrdio and Nat the Diver: both had returned
with the band of the Swift. They also found the Venoms
and Damba, another boy. But that was all. Twenty were
missing. I understood the concern of the Black Cat and
the Solitary: if any of them had escaped the Ojisaries
and were feeling the effects of the lack of sokwata, who
knows if they would even be able to move and go back to
our exploiters? Yerris assured that the effects were…
very unpleasant. I remembered well the Priest’s image of
the Evil Spirits entering the body to tear everything
apart. I had no desire to experience it myself. Yerris
had told us very clearly to return to the shelter
immediately, as soon as we felt our eyes burning or
anything else wrong. He hadn’t told us where he kept the
sokwata, and I must say I didn’t insist on knowing, not
after the penetrating look he gave me, assuring us that
it was better we didn’t know.

Not a day went by that I did not visit Rogan in the
hospital to help him heal with my spells. I found him
asleep every time, except on the eighth day when he
blinked and looked at me with eyes that seemed to be in
another world entirely. I said “ayo,” to him, full of
hope, but he did not answer me, and after seeing him
close his eyes again, I left a paper in the palm of his
hand. I had cut it out of a newspaper that afternoon. It
was a pretty engraving of the Rock as seen from
Menshaldra.

I stood up.

—“Forward, shyurs,” I said to Manras and Dil.

We left the hospital and happily headed for the Cats. By
the time we got back, it was already dark. Despite
Manras’ complaints, we took the longest route to get as
far away from the Ojisary territory as possible and
reached the Timid River, which had its source in the
Rock, before entering the Labyrinth from the east. Some
of the alleys we passed were full of people of all sizes
and colors; others were deserted. After climbing some
narrow stairs and crossing a small wooden bridge, we
finally came to the dead end… or rather the Reeking
Alley, as Manras called it. With wrinkled noses, we
passed through the narrow corridor as fast as we could.
If I had dared to open my mouth wide, I would have
uttered an “ayo, ayo!” as we entered, but I said
nothing, for besides, Yerris said that the less the
neighbors heard us the better, because otherwise, they
would be quite capable of expelling us. As soon as I
entered the cave I heard a groan, and I squinted in the
darkness.

—“Guel?” Manras said.

It was the Soothsayer. She was lying in a corner,
trembling. Huddled not far away, the Mole said in a
weak, dark voice:

—“She’s very bad. And me… I’m not much better. I think
it’s because of this… sokwata. We’ve been here for
hours. Sla and the Black Cat aren’t coming. They’re not
coming,” he repeated. The tension vibrated in his voice,
as if he was trying to stifle the pain.

My mood darkened suddenly, and I crouched down beside
him, asking:

—“Does it hurt a lot?”

The Mole did not answer. He merely lay back and gave a
long, jerky sigh. The silence was filled with
expectation and anxiety. I don’t know how long it was
before Manras whispered:

—“My eyes are burning, Sharpy.”

I swallowed and confessed:

—“Mine too.”

And it was true. My eyes burned as if the Cold One had
taken hold of me, and I felt twinges all over my body.
The sensation intensified as time went on. The
Soothsayer was silent: she seemed to have fainted. The
Mole, on the other hand, repeated between his teeth:

—“We’ve gotta move. Sla is not coming. We’ve gotta get
out…”

To get out, it runs, but to go where? The only solution
was to go to the Ojisaries, and they were on the other
side of the Labyrinth, maybe half an hour or more away,
considering our condition. No, I thought. The Black Cat
would come back. He would come back, and he would bring
us sokwata. Damn him if he didn’t…

The fall into hell, gradual at first, suddenly sped up.
The pain went from being bearable to being a real
torment. I thought of Rogan’s words and truly believed
that the Evil Spirits had been unleashed within me. Then
I thought of Rogan, and imagining that he was suffering
the same as we were, I found strength enough to drag
myself out and mutter to the silent night:

—“Help us… help us…”

I don’t know how long I kept repeating the same thing
until, when I couldn’t take it anymore and saw death
coming, I put things into perspective and said to myself
that the Ojisaries might have made us fish for pearls,
but at least they gave us sokwata. A life as a prisoner
miner was better than death. All I had to do was get up,
get my comrades on their feet, get out, and put one foot
in front of the other until… until reaching those who
had left us in this state. If Slaryn had not appeared at
the dead end, I think I would have made up my mind, but
the voice of the Black Dagger gave me hope. I felt a
hand shake me.

—“Sharpy! Blasthell, are you all…? Move over, let me
through.”

She pushed me aside more than I pushed myself aside.
Anyway, Sla just went to check that we were all in, and
she went:

—“Where the hell is Yerris?”

This suddenly took away my hope. What? Slaryn didn’t
know where the Black Cat was?

—“Help us,” I stammered. “Sokwata. Sla… the Priest…”

—“Devils, don’t ask for a confessor, you’re not dying
yet. I’ll bring you the sokwata. Don’t panic. Yerris
says that last time he went two days without taking it
and he survived. I won’t be long.”

It took her forever to come back. Well, at the time, I
was not really aware of her return, in fact I was not
aware of much except that I was in pain. I only know
that I had found a small stick near the entrance and was
biting it fiercely. Hands grabbed the stick and tried to
pull it out of my mouth. They succeeded, they forced
something down my throat, and suddenly I felt a wave of
energy come over me, as if my body abruptly remembered
how it was supposed to work. The pain gradually faded,
my eyes stopped burning, and my mind began to reason
again. I heard the wheezing of my companions, blinked at
the harmonic light that Sla held, and saw the Black Cat
beside her.

—“Yerris,” I gasped. My hands were shaking with fear,
but everything seemed to be back to normal now.

—“Spirits and demons,” Yerris muttered. He sounded even
more exhausted than we were. “Sorry, shyurs. I’ve been
an idiot.”

I frowned, not quite understanding his words, and turned
my head to make sure that Manras and Dil were recovered
now. They both looked as frightened as I was. What we
had just been through was a nightmare. Sla undid the
harmonic light, and strangely enough, darkness didn’t
engulfed us: it was already dawn outside.

—“It wasn’t your fault, Yerris,” Sla finally said.

—“It is,” Yerris growled. “I should have foreseen it. I
know how these gwaks are. They attack before we attack
them. And they are incapable of trusting anyone. They’re
devils.”

—“Nonsense,” Slaryn said calmly; “you would have done
the same thing in their place.”

Yerris did not reply, and increasingly puzzled, I
inquired:

—“What are you talking about?”

Yerris was unusually irritated. He replied with a
muffled hiss:

—“About that isturbagged gwak. Syrdio. And Diver. Last
night, one of them pretended to be ill. I believed him
and went to fetch them some sokwata. I was a fool. They
followed me and…”

He was silent, and I turned pale as I guessed.

—“They stole the sokwata from you.”

—“You’ve got it round,” Yerris sighed, altered. “Dead
round. And now who knows where they put it.”

I shook my head in confusion.

—“But then… how did you give sokwata to us?”

Yerris inhaled, and Slaryn replied:

—“Syrdio gave it to him.”

—“Let’s say he sold it to me,” Yerris corrected through
his teeth. “Blasthell, how I’d be glad to wring that
damned nail-kisser’s neck. If he was before me just now…
Gaaah… Damn gwaks!”

As if he wasn’t one himself, I thought. I almost smiled
at that, but my lips immediately twisted at the thought
that Syrdio was now in possession of the sokwata. As
Yerris continued to hurl imprecations, I interrupted
him, hesitating:

—“But, Yerris… what did you give him in return? Money?”

The dim light and my sokwata eyes allowed me to see the
Black Cat’s grimace.

—“Money,” he confirmed. “Until he realizes that the
money we give him is not worth it. On that day, he’ll
stop giving us sokwata so he can live a few years at the
cost of our lives. I have no hope that this scumbag will
be able to share life time. He’s worse than an isturbag.
He’s a murderer—”

—“That’s enough, Yerris,” Slaryn cut him off. “Stop now.
We’ll come to an agreement. Let’s be logical: it’s not
in their best interest to have seven, no, actually,
*nine* enemies.”

—“Seven,” the Black Cat replied. “The Venoms and Damba
have joined forces with Swift’s band. There are seven of
us against about fifteen gwaks, almost all of them
between twelve and fifteen. Argh, did I say seven?
Forget Manras and Dil. So we’re five. And, you yourself
told me that the Mole never threw a punch in his life—”

—“Hold your tongue,” Slaryn growled. “That’s not what I
said. The Mole knows how to defend himself, right?”

—“Yes, of course, he defends himself by running away,
haha!” Yerris scoffed. Slaryn gave him a shove,
exasperated. Unlike the Solitary and me, the Black Cat
didn’t notice the Mole’s ashamed expression; Yerris may
have been a good cat, but tact wasn’t his strong suit,
to say the least.

I rolled my eyes and observed:

—“Well, maybe that’s the solution: we pay them back,
snatch the sokwata, run away, and the sokwata is all for
us.”

—“First, we’d have to know where they’re hiding it,”
Slaryn said. “And after that, we wouldn’t want Swift to
lend Syrdio a hand and come down on us the next day.”

The Mole ventured:

—“But maybe if we let them have half of it…”

—“Perhaps they would calm down for a while,” Slaryn
conceded. “But only for a while.”

There was silence. Then the Soothsayer intervened:

—“Black Cat. How much money did they ask you for?”

Yerris coughed, embarrassed.

—“Well… he says the dose is one goldy. So, he said, next
time… we should bring him two each.”

Conclusion: Yerris had not been able to pay and had
committed himself to us all. There was silence as we
processed the news. One goldy per week was doable if we
worked at it. But it was still a low blow.

—“We’ve got to get that alchemist out,” Slaryn blurted
out.

We nodded silently. However, the idea was very nice, but
putting it into practice was suicidal. Besides, if we
left the Ojisaries without an alchemist, our companions
who had been brought back to the Well would curse us to
their last breaths.

After another long silence, I realized that, despite the
sokwata, the sleepless night had left us all exhausted,
and finally, putting off worrying about anything else, I
imitated my comrades, went back to lie down, and
yawned…before suddenly straightening up and exclaiming:

—“Mother of your ancestors! Rogan! Rogan has no
sokwata!”

I got up so quickly that I hit a lower part of the
ceiling, and the blow was so violent that I saw stars.

—“Thunders, shyur! Go easy,” Yerris snorted, taking me
by the arm. “We’ve got enough problems already, so don’t
break your head.”

After suffering like hell all night, it was amazing how
a single blow brought a flood of tears to my eyes, but
they were not just from pain.

—“Yerris! You’ve got to help me, I’ve got to get Rogan
some sokwata. Tell me where that scaluftard is, I’ll cut
off his ears if he doesn’t give me some sokwata. Tell me
where he is!”

Yerris sighed and nodded.

—“Stay here, Sharpy. I’ll be right back.”

I refused categorically and went out with him, holding
my head. I had even a wound, I realized. My hands were
bloody.

—“Blasthell. What a slugbonery,” I croaked, trying to
swallow my tears.

At the end of the blind alley, the Black Cat stopped to
take a look at my wound, he grimaced, and only said:

—“Blasthell.”

He took a direction, and I followed as best I could.
Each step echoed in my head. Although the Labyrinth was
starting to be familiar territory for me, I got a little
lost with so many alleys, especially since I wasn’t in a
state to pay too much attention to where we were going.
The sky was clearing, and although it was still very
early, there were already workers on their way to the
factories; nevertheless, the atmosphere was still silent
and sleepy. At last, we came to a slightly wider dead
end where some gwaks were sleeping like bliss. Swift,
however, was awake and sitting on a barrel, filing his
nails with a knife. When he saw us approaching, the
red-haired elf did not move, but he did not take his
eyes off us. When we were a few feet away, he calmly
said:

—“You again, Black Cat? Ayo, Sharpy.”

I had tried to clean my cheeks as much as possible, but
my voice seemed a little shaky when I said:

—“Ayo.”

The band kap tilted his head to one side, his gaze
alternating between the two of us, as Yerris declared:

—“I need to talk to Syrdio.”

—“If it’s to talk business, it’s with me,” Swift warned.
And at last he stepped down from the barrel, slipping
the knife into his sleeve with obvious skill. “Is it
about the sokwata?”

—“It is for the Priest,” I explained. I cleared my
throat to give my voice a little more firmness, “He’s in
the hospital, and right now he may be suffering a living
hell. I have to go save him.”

Swift nodded, looking understanding.

—“I see. You want me to help you, huh? What the hell
happened to your head? Were you the one who clobbered
him, isturbag?”

—“Isturbag yourself,” the Black Cat growled. “The gwak
hit himself; I don’t beat up my mates. Look, I just
wanted to tell you something, Swift: you think you’re
very clever now that you’re taking advantage of us like
the Ojisaries did, but it won’t last. We’ll free the
alchemist. And your sokwata, we won’t give a damn about
it. And your sokwata friends, I’ll make them pay, you
hear me? I will make them pay dearly.”

His hostility shocked and frightened me at the same
time, for to attack Swift in such a way and in his own
shelter was not particularly prudent. The kap looked
theatrically impressed.

—“How vengeful. Look, I’m just protecting my people,
that’s all. You wouldn’t tell them where the sokwata
was. So I understand that they’re not telling you now.
Because they too hold grudges.” He glanced at his gang.
Several of them had woken up and stood up without coming
near. He resumed, “I happen to have a dose left here in
my pocket. And I’ll give it to my namesake for free.
Because I’m a charitable gwak.” He put his left hand in
his pocket and handed me a small black pill. I looked at
it curiously, and as I was about to take it, Swift
pushed it aside slightly and added, “All the other doses
for you and the Priest, you could have them for free
too… on one condition.”

I frowned.

—“What condition?”

Swift glanced sideways at the Black Cat before putting
an arm around my shoulders and walking away a little,
saying softly:

—“I haven’t forgotten that I taught you a lot of
pilfering tricks this winter, and you were doing better
than fine, I remember. Do you remember?”

As nodding would have given me a headache, I replied
with a:

—“Natural.”

He smiled.

—“Natural,” he repeated. “And since I’ve learned that
you’re a Black Dagger… Well, since you’re still a kid, I
won’t ask you to hunt the Crown of the Fallen, but I
want to offer you a deal. You join my gang, and you give
me half of what you earn. In exchange, as I say, a free
ration of sokwata for two and also a good shelter to
sleep in and not… the dump where the Black Cat has set
you up.” He smiled, mockingly. “What do you say?”

The offer was very tempting. My gaze slid
surreptitiously to Swift’s pockets, so close. Did he
keep more sokwata pills there? I turned my eyes back to
the elf’s scarred face and hesitated, trying to
understand, despite my aching head and fatigue, what my
namesake was offering me. In other words, he was telling
me: join my gang, mate, and let’s join forces. And it
also meant a: cut ties with the Black Cat and the
Solitary and send them to go chase the clouds. I shook
my head and said:

—“I can’t. Yerris is my friend.”

Swift arched an eyebrow.

—“What does that have to do with anything?”

I wavered.

—“Well… I can’t leave him.”

My namesake looked skeptical.

—“Isn’t it more like you’re afraid to steal for a
living?”

I returned his stubborn look.

—“The hell, it’s not that. The nail-pinchers, I can pick
their pockets without batting an eye. No, it’s more
that…” I shrugged and, since I had his full attention, I
took advantage of it. “I have another proposal. A better
one. You give me the sokwata for free. Four rations. For
me, the Priest, and my cronies from the newspapers. And,
in exchange, I’ll join in, but only by day, and I swear
you’ll get more earnings than losses. But you don’t say
a word to the Black Cat or the Solitary, it runs?”

Swift now looked at me thoughtfully.

—“For now, I’m fine with it. It runs. If the Black Cat
finds out, it won’t be from me. But I think he already
suspects we’ve come to an agreement, namesake. Today,
I’m giving you the day off because of your head. But,
tomorrow, I’ll see you on the Esplanade, near the
manticore, at eleven. Don’t be late.”

He ruffled my hair, and I uttered an “ouch” of pain. I
walked quickly away with the Black Cat, under the
indifferent, curious, or mocking glances of Swift’s
companions. We walked through the narrow streets in
silence. I, with my headache, was not in the mood to
talk. After a while, Yerris said:

—“You’re going with him, aren’t you?”

In his voice, I heard a hint of disappointment. I rolled
my eyes.

—“Blasthell, no. We just talked, that’s all.”

The Black Cat looked at me out of the corner of his eye,
uneasy, and I felt uneasy in turn. But I didn’t see
myself saying to him at all, “look, Yerris, don’t worry,
now I’ll team up with Swift, but just a little, I know
you don’t like him, but, think about it, he’s the one
with the sokwata now, now’s not the time to fall out
with him, right?”. I sighed and reaffirmed my opinion:
my agreement was a survival agreement, not a betrayal.

When we left the Cat Quarter behind, the Black Cat said
goodbye to me, saying:

—“I’ll see if I can find… more companions from the Well.
I’m sure there are others out here. Ayo, shyur.”

I could hear his tone of voice, but I did not pay much
attention to it. On the way to the Passion Flower
Hospital, I thought only of my fatigue and my head.
Halfway there I realized that I had left my cap in the
Cave. As I passed through the Esplanade and met the
frowning eyes of a fly, I saw my hands covered with
blood, and fearing to attract attention, I hastened to
wash them in a fountain and to run water over my wound.
By the time I reached the hospital, daylight had fully
broken and the sun was shining over all the lower part
of Estergat. I walked through the main hall, greeted the
kadaelf, who was working as a secretary that morning,
and went straight to the large ward where Rogan was. The
scene I saw left me very pale. Two nurses were standing
by the Priest’s bed, trying to calm him down. Rogan was
delirious, uttering inarticulate cries and others which
I understood very clearly.

—“Confession, confession, I want to die!” he said.

I rushed to the bed, and when he saw me, he seemed to
calm down a little. He croaked in a heart-rending tone:

—“Sharpy, kill me, for the sake of your ancestors, kill
me…”

Seeing him calmer, one of the nurses moved away, and I
took advantage of a moment when the other turned his
head to put the sokwata pill in Rogan’s mouth.

—“Swallow, Priest, swallow,” I whispered.

I took his hand and saw how his face gradually relaxed,
his eyes became less bright, and then his lips moved. He
stammered:

—“Spirits.”

He said it with so little force and exhaled so long that
I thought he had just breathed his last, and I called
myself names because I had not insisted that the Black
Cat give me at least a dose of sokwata in advance for
the Priest. Maybe his seizure, coupled with his flank
wound, had been too much for one body. But the Priest
was resilient, and when I laid my head on his chest and
heard his heartbeat, relief prevented me from protesting
when the nurse begged me to leave because they were
going to change my friend’s bandage. They did not want
me to stay and see, so I went away, but not without
checking a second time that the Priest was now fast
asleep. I did not go very far. I walked out of the
hospital, dragged my feet through the park, and not
having the courage to walk so far back to the Cave, I
climbed a tree with a short trunk and thick branches,
curled up, taking care to put my head gently between my
arms, and closed my eyes at last, breathing peacefully.
Like that, lulled by the sounds of the city, the
birdsong, and the gentle summer breeze, I fell into a
long sleep. And I dreamed of a necromantic child, wild
and innocent, ignorant and happy, who, back in the
valley, climbed the trunks of his friends the trees and
sang with laughter: karilon lu, karilon lu, Summer, be
welcome, I sing and greet you, karilon lu, karilon lu…

Jewels and explosives
=====================

With the agility of a veteran, Nat the Diver tossed me
the wallet, I caught it and tossed it to Damba. Damba
shoved it under his shirt, gestured “I’m going home”,
and in a peace-and-virtue, melted into the crowd on the
Esplanade. After a careful look around, Diver approached
me with one of his wolfish smiles.

—“How about we make a double, mate?”

To make a double meant, in our jargon, to steal
something of value that would get us through the next
day without working and perhaps even longer. The week
before, we had snatched a porcelain teapot from a store.
And the week before that, I had nicked a pocket watch,
sold it for two siatos to Yarras, the White’s ruffian,
and treated my cronies, the Mole, and the Soothsayer to
a hot meal in a tavern in Tarmil. I knew Yerris was
aware of my new job, but he had merely given me a
worried look and said, *‘Be careful, shyur’*. He who had
said before that only the judgment of the Black Daggers
kept him from being a pickpocket… he wasn’t one to give
me any lessons. Especially since, in my opinion, it was
much more reckless to steal something from Korther than
from a posh, distracted nail-pincher in the middle of
the street.

Diver looked at me quizzically. I put my hands behind my
head, yawned, and nodded.

—“Runs for me.”

We walked away together to the front steps of the
Capitol. I climbed a few steps and scanned the square
for Manras and Dil. I had left them with the papers near
the central police station, and I was almost sure I
sighted them. Five o’clock in the afternoon had just
struck, and the place was more crowded than on a
holiday. In fact, it almost was, since that very night
the midnight celebrations for the Wells Moon would
begin, during which the streets would be covered with
garlands of flowers in honour of the ancestors, their
spirits, and what have you. Looking out over the
Esplanade and the sea of hats, I uttered an imprecation
and said:

—“Diver! Do you remember that scoundrel that almost
split my head open with his stick?”

—“The one from last week?”

—“Yeah. I think I just saw him. Ah, that son of a rat…
Do you know why he went after me?”

My partner laughed.

—“Natural I know! You called him cheap because he
wouldn’t give you a nail. And he gave you a good
thrashing. I had time to steal his wallet and even his
handkerchief!”

He laughed as he remembered, and I pouted. The arm where
I had first been hit with the stick still hurt a little.
It was lucky he hadn’t hit me on the head, because my
wound from three weeks ago had barely just healed. I
sighed and leaned on the railing, saying:

—“But you didn’t hear what he said to me before he let
me go. You know what he said to me?”

Diver rolled his eyes and hazarded:

—“Rascal, little devil, tick, dead flea?”

I smiled.

—“Aside from that. He read me my future, I swear, he
told me: Wretch, you’ll end up at Carnation one of these
days, but I hope you die before then and get eaten by
rats…! And other such things. Of all the curses I’ve
received so far, that damn isturbag takes the cake. I
swear. Some people don’t just deserve to have their
wallets stolen. Blasthell! If I ever see him in the
Labyrinth, I won’t be the one to receive the blows, I
tell you, I’ll throw a rat in his face! My ancestors are
witnesses, that scoundrel is a nasty devil with no heart
and…”

Diver interrupted me, impatient.

—“Make it short, Sharpy. And stop rambling about your
ancestors, you sound like the Priest. There’s a lot of
heartless devils out there, ya know. Starting with the
Ojisaries. And if we want to do something to get the
alchemist back, we need money to buy good weapons,
because now they’re more than ever on guard, and it
won’t be enough to throw stones at them… or rats. So,
let’s go ahead and make that double. I got an idea.”

I bit my cheek and followed him without protest, still
swallowing the urge to hurl venom at that nail-pincher,
the sight of which had brought back bad memories. We
passed an apple stall, and Diver took a fruit; not to be
outdone, I did the same. Nat was not much older than I
was, a year older, perhaps, not more than two, but he
had been stealing and begging for a living since he had
the use of reason, and his actions were so natural that
I sometimes wondered if he even thought of them.

We arrived in front of the Stock Exchange, and the Diver
stopped.

—“Wait. I was thinking of setting up a scene, but we
need reinforcements.”

—“Damba is gone,” I objected.

—“Yeah…” Nat bit his lip, thoughtfully. “I wonder where
the hell Syrdio is; ever since you became a partner, we
haven’t seen him…” He gave me a sidelong glance. “Your
cronies could give us a hand.”

I frowned.

—“They’re working right now.”

—“And they’d earn a lot more if they changed their
livelihood,” Diver laughed. “You yourself say that
lately they don’t even earn thirty nails between them.”
As I hesitated, he insisted: “Come on! You can see
they’re dying to help us. Manras, at least. The other
one’s a bit slow on the uptake. Besides, they’ll only
serve as a distraction, don’t worry.”

I gave in, and we went to find Manras and Dil. We found
them sitting on a stoop talking with other newsboys. I
arched an eyebrow in amusement. Working hard, eh? I was
about to call out to them when Manras suddenly stood up
and shouted angrily:

—“Take it back!”

And before my stunned eyes, he threw himself at a blond
newspaper crier. They both screamed, rolled on the
pavement, grabbed each other’s clothes, and unwittingly
hit a passer-by, who kicked them, annoyed. But Manras
did not even notice, busy as he was biting the blond
boy’s arm… Joining them at last, I smacked Manras on the
back of his neck to get him off, and I pushed him aside,
grumbling:

—“What the blasthell is wrong with you, shyur! Don’t you
know that fighting is only good for isturbags?”

At least that was what Yal had told me once, long ago,
when he found me at the Peak, covered in bruises. The
little dark elf made a stubborn pout, and as the other
newsboy whimpered and cried his eyes out, sucking his
bite, he fought back:

—“He laughed at me! He told me I’m an idiot ’cause Dil
reads me the titles ’cause I can’t read them.”

Nat the Diver burst into a great laugh and exclaimed:

—“Well, the world is full of idiots then! Stop whining,
isturbag!” he said to the blond newsboy contemptuously.
“Manras may not be able to read, but at any rate, he can
bite, have you noticed that? Learned beast, cultured
turnip!” And he took Manras by the arm, saying, “Come,
shyur. Don’t be so touchy. Leave these papers and come
with us, we have some serious work for you.”

Manras’ eyes lit up.

—“For real?”

—“For real,” the thief smiled.

I did not like his tone. I beckoned Dil to follow us,
and as he carried all the papers, I cleared my throat.

—“Uh… Dil. Are you really going to take all this stuff
with you?”

Little Prince looked surprised.

—“Well, natural. You have to return them if you don’t
sell them.”

Despite Diver’s taunts, we accompanied him to the
newspaper office to return the papers, and it was not
until we were back on the Esplanade, sitting on the
stone curb of the Manticore Fountain, that Nat explained
his plan.

—“It’s simple,” he said in an excited tone. “We’ll do
the panhandler trick and jump a jewelry store. Actually,
*that* jewelry store you see there.”

I paled a little and tried to read the letters on the
window.

—“Canostre Jewelry?”

—“Dead round. First, wait here, I’m going to get a mate
of mine who knows how to dress well. He owes me one, so
he’ll be along for the ride for sure. He’ll be the
customer with the nails, and you’ll go in and ask for a
handout. My friend will be generous, he’ll give you some
nails, and bingo, you’ll leave saying thank you, no
hanging around. It runs?”

I frowned.

—“I didn’t get that,” I admitted. “Your friend’s the one
giving us the handout?”

—“The handout and, above all, the ring or brooch that he
will have snatched, natural!” Diver explained, amused.
“I don’t go there, because the jeweller knows me. He’s
made of the same wood as the one who gave you that
beating last week, Sharpy: he’s a heartless devil. It’s
payback time!”

I stared at him intently. Manras said:

—“I like it! How much does a ring cost?”

—“Goldies,” Diver replied.

—“Goldies!” the little dark elf enthused.

I glanced darkly at Manras and said to Nat:

—“You know that, if they catch us and send us to the
Carnation, we’re dead? The guards won’t give us
sokwata.”

Diver’s eyes sparkled.

—“We’re not gonna get caught. Come on, don’t cop out,
Sharpy. Besides, Syrdio asked me to do something big,
because he needs the money as soon as possible. He said
he already has a plan to get the alchemist out.”

I was not surprised. Yerris and Sla, too, had one;
indeed, they had more than one. I had contributed by
buying picklocks from Korther with the money I had left
over after paying for the Priest’s care. The Black
Dagger kap had sold them to me cheaply, reminding me
that if the flies heard the word “Black Dagger” from my
mouth, goodbye to our friendship. That was two weeks
ago. And Yerris and Sla had yet to put any of their
grandiose, hyper-secret plans into action. I sighed.

—“And what is Syrdio’s plan? To buy knives and stab the
Ojisaries?”

My companion shrugged.

—“He didn’t explain it all to me, but he said Swift
thought it was a good plan.”

Yet he wouldn’t be a part of it, I guessed. I crossed my
arms.

—“The Black Cat already has a plan, and I don’t want to
spoil it. So I’m not going into the jewelry store.
Unless you tell me where the sokwata is.”

The reaction was immediate. In the blink of an eye,
Diver darkened and gave me a wary look.

—“No.”

His answer drew a breath of exasperation from me, and I
stood up.

—“But, Diver! You know me. You know I can share. And I
won’t tell the Black Cat if you don’t want to. I swear I
won’t. But just think, if anything happens to you and
Syrdio, you condemn us all.”

Diver gave me a mocking look and stood up.

—“Dead round. That’s why you better have my back. Now,
honestly: are you with us or are you with the Black
Cat?”

We looked each other in the eye. We almost bared our
teeth. Well, in fact, he was waiting for the moment when
I would lower my head or throw myself at him. As for me,
I was trying to find a convincing argument. But I
couldn’t find one. I clenched my jaws, and then Manras
stepped in and said:

—“You’re not going to fight him, are you? Fighting is
only good for isturbags,” he reminded me very wisely.

I looked at him incredulously and mockingly, shook my
head, and said with dignity:

—“You’re a coward, Diver. I thought we were friends. If
you won’t tell me where the sokwata is, you don’t trust
me. And I don’t work with people who don’t trust me.
Let’s go, shyurs.”

I began to walk away, and Manras, though hesitant,
followed me. Dil, on the other hand, did not hesitate
for a second. Then Diver blocked my way.

—“Hey, wait a minute, Sharpy.” He was agitated. “All
right. I’ll tell you. Only you. But, if you tell the
Black Cat, I’ll never forgive you.”

I smiled happily.

—“It runs. I’ll be as dumb as a post.”

Diver hesitated before bringing his lips to my ear and
saying:

—“It’s on the bank of the Timid River, in a hole between
the rocks. A few yards down from Elves Street.”

I nodded with the firm intention of checking it out,
despite all the… uh… confidence I had in the expert
thief.

—“Thank you, Diver,” I said, and smiled broadly this
time. “Let’s go relieve that jeweler.”

Diver patted me on the shoulder and gave me a troubled
look, as if thinking: “Blasthell, when did I become so
trusting?”


%%%


The robbery lasted, so to speak, the time of a blink of
an eye. Manras, Dil, and I entered the jewelry store
adorned with fine garlands of flowers picked from the
Esplanade, and I bellowed in a plaintive tone:

—“Gentlemen! Give alms for the poor children on this
holy day! We are hungry. Please.”

There were three customers in the shop. One of them was
Nat’s friend, a guy in his twenties, dressed head to toe
in nail-pincher clothes. The jeweler was already
pretending to throw us out when our accomplice
exclaimed:

—“Poor souls!”

He looked so good-natured, so innocent, that when he
threw me the coins and, between them, a ring, I said
from the bottom of my heart:

—“Thank you, sir! May your ancestors repay you.”

And we came out of there, all calm, with three or four
alms nails and a ring that was worth goldies. The trick
was done. The three of us melted into the crowd on the
Esplanade, we soon found Diver, and I slipped him our
booty.

—“Did everything go well?” Nat inquired.

—“Ragingly!” I said, smiling with all my teeth.

—“That isturbagged man saw nothing!” Manras laughed.

Dil just shook his head and sighed, looking like he was
thinking: frankly, what friends I’ve ended up with…

We left Diver to leg it with the ring and, still excited
by our little success, my comrades and I began the
descent towards the Avenue of Tarmil; we stopped at
every shop window, trotted here and there, and observed
leisurely the daily life of the honest people.

Finally, we headed back to the Labyrinth, not forgetting
to make detours and to stay on the lookout, because ever
since the Ojisaries knew that we had a supply of
sokwata, they were furious, and three days earlier,
without going any further, they had almost captured
Slaryn and the Soothsayer. Yerris was even thinking of
changing his refuge, because he was suspicious of Syrdio
and wasn’t sure he would hold his tongue if caught and
questioned.

When we reached Swift’s refuge, it was already dark, and
although it was not cold, they had lit a small fire for
light. I saw Damba sitting on a barrel and asked him:

—“Where’s Diver?”

He shrugged.

—“No idea. He’s not home yet.”

I frowned, and for a moment, I imagined that a
gwak-hunting fly had caught him and taken the ring,
locking Diver up in Carnation Prison in the process.
But, no, that was impossible: Diver almost never let
himself be cornered by flies, and even less so with a
booty like that.

So I patiently waited for him with Manras and Dil while
the other gwaks played dice, betting nails. Then Swift
appeared with Syrdio and Diver. All three, on seeing me,
stopped dead in their tracks. A little voice told me
that something was wrong, and I checked it at once when
the Diver came up and pushed me with both hands and
growled:

—“Traitor!”

I opened my eyes wide, puzzled.

—“What the blasthell?”

Swift intervened.

—“Oh, oh, take it easy, Diver: let’s behave like
gentlemen,” he said calmly, as he approached. “Tell me,
namesake, and be sincere. Did Diver tell you where the
sokwata was?”

I petrified and glanced at Diver before I lied:

—“No.”

The red-headed elf grabbed me by the arm and slammed me
against a wall. I didn’t feel that he was exactly acting
“like a gentleman”. He hissed:

—“I said, be sincere. He confessed. And if it wasn’t you
who stole the sokwata, then you must have told the Black
Cat.”

I looked at him in horror.

—“We were robbed of the sokwata?”

This time it was Syrdio who took me by the other arm and
squeezed it so that I let out a groan of pain.

—“Admit it, Sharpy: you betrayed us.”

I shook my head, bewildered.

—“No! That’s not true! I’ve been with Diver all
afternoon. I just got back—”

—“Don’t deny it: the sokwata is gone!” Syrdio roared.

I saw a glint of panic in his eyes, and I knew that no
reasonable argument would convince him. I still shouted:

—“I didn’t betray you!”

And I struggled to free myself. Swift stepped aside, but
Syrdio did not, and Manras threw himself on the latter,
punching him on the shoulder and shouting:

—“Get off him, isturbag!”

Without letting go of me, Syrdio struck him with his
free hand. This was more than I could bear. That he
would come after me, okay, but that he would come after
my comrades? No even in his dreams! Out of my mind, I
rushed at Syrdio, clawing him and throwing him to the
ground.

—“Traitor!” he shouted at me.

—“You crazy bastard!” I said.

To my pride, though Syrdio was older, he did not prevail
over me. We were separated. Swift took me by the waist
and, though I continued to struggle, he lifted me into
the air before setting me down a few feet away with a
bellowing:

—“That’s enough. Listen to me: whether you stole it or
not, until we get the sokwata back, you’re out. Do you
understand me?”

I did not answer him. I glared at Syrdio, took a few
steps back, surrounded by Manras and Dil, and with a
proud pout turned my back on them all and limped away.
Little Prince cleared his throat as we walked away.

—“Didn’t you say fighting was only good for isturbags?”

I could hear a hint of amusement in his voice. I sighed
loudly and massaged my jaw.

—“I did say that. But the thing is, I am a true savage.
I come from the mountains, I have an excuse.”

And I grumbled against Syrdio for a long time. I only
fell silent when we entered *The Drawer* to ask Sham for
dinner. Though people were celebrating the festivities
all over town, in this tavern there was more reverence
for cards and worldly things than for the ancestors, and
so there was the same noisy, familiar atmosphere as
usual. A nail-pincher would have thought the place a den
of delinquents; to me it was almost a family. I used to
come here for dinner almost every night with my cronies,
and everyone there treated me well. My mood at the
moment, however, was not particularly cheerful, and to
the “ayo, bard!” some of them threw at me, I replied
half-heartedly with an “ayo, ayo”.

—“You look like you’ve run into a red nadre, kiddo!” the
tall dark elf said to me as he set three dishes of
porridge on the counter.

I elbowed Manras when I saw him smile and grabbed a
plate, replying:

—“Not a red nadre, no, it was a bipedal cat with
phalanges and non-retractable claws.”

The tavern filled with laughter.

—“He’s coming out with such words!” the old Fieronilles
praised, mockingly. “But where did you study, boy, at
Deriens?”

—“At the school of the street!” I said.

I smiled as I saw them all laughing at my joke, and I
gulped down the porridge, and licked the plate, and as
Dil chewed with the speed of a snail, Manras and I
wandered between the tables looking at the cards of
those who were playing, listening to the betting, and
the loud voices, and the jibes, and little by little I
became drowsy. I was sitting on the floor, yawning and
stroking Chestnut, old Fieronilles’ dog, when the door
opened suddenly and a whole gang of people came in
talking animatedly.

—“It’s clear there was a fight!” one said. “What do you
bet it was Frashluc?”

And another, a certain Lotto the Tinkerer, announced:

—“Guess what, you guys! There’s a party at the
Ojisaries. We heard them bellowing from Wool Square.”

As they all asked for more details, the newcomers
complied, but it wasn’t much: they just knew that there
had been a mess in Ojisary territory. One said it was
just an argument between them, another bet his eyes that
the people of Frashluc had given them a warning for not
paying enough taxes, and others thought it most likely
that they were uncorking bottles to celebrate their
cursed ancestors over a mountain of siatos. I listened
attentively, and then, realizing that I was not going to
learn anything more about the subject by standing there,
I got up, pulled Dil by the sleeve, and the three of us
walked out towards the Cave. I didn’t know whether
Frashluc, that big kap of the Labyrinth, might have any
interest in attacking the Black Hawk—some even said he
had better not—but what I did know was that the regulars
in *The Drawer* made up a thousand stories out of
rumors. In any case, if there had been a ruckus among
the Ojisaries, it was also possible that Yerris and Sla
had carried out their plan without warning us. Unless
the gwaks in the well managed to escape some other time,
but that last possibility seemed unlikely.

The Soothsayer and the Mole were already in the Cave,
fast asleep. My comrades stayed inside, but I went out
into the Reeking Alley again, not quite sure what to do.
To prowl again in Ojisary territory and risk getting
caught? No, no way I’d do that, I thought, shivering. I
crossed the little wooden bridge, went down the narrow
stairs, and sat down on the steps to wait for the Black
Cat and Sla. I waited a long time. Nothing. Well, it was
no wonder; lately, we hardly saw them, and they didn’t
always come to sleep with us, but… hell, if they had
tried something and the Ojisaries had captured them… I
despaired just thinking about it. After all, I didn’t
think the Ojisaries would show them any compassion.

I was so engrossed in my thoughts that I was slow to
notice the bulky figure coming down the alley at a run,
and I leapt to my feet to avoid it trampling on me. For
a terrible moment, I thought it was an Ojisary. But then
I heard him mutter a curse, saw his face, recognized
him, and breathed a sigh of relief.

—“Black Cat!” I whispered. “Phew, I thought they killed
you.”

The semi-gnome gasped, catching his breath, before
saying:

—“Sharpy. I need your help.”

These words brought a hopeful smile to my face.

—“For real?”

—“For real,” he confirmed.

And he gave me one of the two bags he was carrying. I
whistled through my teeth.

—“What’s in there, hydra heads?”

—“Explosive magaras.” I looked at him, my eyes wide, and
he cleared his throat. “I’ll explain later. Let’s go.”

To the Alchemist’s rescue
=========================

Dumbfounded, I followed the Black Cat as quickly as I
could down the dark alley, carrying a load that would
undoubtedly have spirited me away instantly had it
activated. The sky was overcast and the moon was barely
shining, but being a sokwata I could see more than
enough to avoid the obstacles, the hanging laundry, and
the stone protrusions.

Yerris led me to a place dangerously close to the
Ojisary territory, situated a little lower down the
slope. We descended the deserted stairs which bordered
one of the steepest escarpments of the Labyrinth. At the
foot of it stood buildings with terraces. The Black Cat
entered a backyard full of junk and stopped in the
middle. He whispered:

—“This is going to be grandiose.”

I looked at him, eager to know. But the Black Cat said
nothing more, he put down his bag of explosive magaras
and approached the rock face about thirty metres high.
He boldly climbed up a bit and scanned for a few moments
before dropping down nimbly and saying in a low voice:

—“During my explorations in the Well, I found a hole
hidden by light. This hole leads to a dark cavern, free
of vampiric foam. And from there, there are two tunnels.
At the bottom of one, there’s a closed black steel door
that leads spirits know where. But the most incredible
thing is that at the end of the other tunnel, you can
see the sunlight. Just imagine how I felt when I
discovered it. You could barely see it, but what the
heck, after spending so much time searching and
searching in the mine tunnels, here I am finding a piece
of the sun! And, to make matters worse, after pushing
aside all the wobbly rocks I could, I saw…” He jerked
his chin toward the terraces plunged in the darkness of
night. “That.” He smiled at me. “It took me a long time
to recognize the place. But, now, I don’t have the
slightest doubt. A few days ago, I recognized the same
old woman on a balcony. I don’t have the slightest
doubt,” he repeated.

I looked at him, speechless. I couldn’t believe he kept
it to himself and didn’t tell us.

—“I tried to enlarge the hole,” the Black Cat continued.
“But it was impossible. And I said to myself, ‘Ayo
freedom, I’ll stay in this well until I die’. But, then,
that young friend of yours brought us the keys, and we
went out and… Sla and I started looking for the hole
from outside. We found it. And, well, now we’re gonna
blow it. And we’re gonna get our companions out of the
mine. And we’ll be done with those Ojisary scoundrels
once and for all.” He paused and turned to me. “Hey,
shyur. What do you say? Did you swallow your tongue?”

I cleared my throat.

—“No, no. It’s just that… Damn, it’s… unbelievable, but…
Black Cat, I don’t know if I understood correctly. We
make a hole and we take out the gwaks, did I get it
round?”

—“Dead round,” the semi-gnome agreed.

I shook my head, put my bag next to his very carefully,
walked over to the wall, and turned around.

—“But, Yerris, the Ojisaries still have the Alchemist.
If they don’t capture us, we’ll be the ones to go to
them eventually. Syrdio and Diver have lost the sokwata
they had.”

Yerris rolled his eyes, and not seeing him appalled by
the news, I guessed.

—“Good mother! You snatched it?”

—“Sla did,” Yerris said. “In case the gwaks in the Well
need it. And don’t believe everything those isturbags
tell you: the sokwata they had hidden there wasn’t even
half of what the alchemist gave me. They must still have
a lot of pills hidden around. I should have done the
same thing before they stole it from me, I know, but I
was too busy helping Sla find goldies and pay for those
explosives… to worry about two isturbagged gwaks.
Anyway, things are what they are.”

I exhaled sharply.

—“You could have asked me for help before. I’m a Black
Dagger. I know how to ease the nail-pinchers.”

—“By pulling a few nails out of their pockets?” Yerris
scoffed. “Explosive magaras are expensive, shyur. They
can’t be bought with pilfering tricks.”

I defended myself:

—“I stole the Wada and a diamond. That’s not pilfering.”

Yerris turned his head towards me and let out a muffled
laugh.

—“Well. That’s different,” he conceded. “But, anyway,
the explosives are here, and thanks to you: remember you
gave Sla the picklocks to sneak into a house. I did the
shopping. Each to his own, shyur. And now, let’s get to
work.”

He began to move all the junk away from the wall,
probably so that it wouldn’t fly into the air when he
activated the magara. I still wasn’t convinced by his
plan.

—“Yerris. And the alchemist?” I insisted.

—“Don’t worry about that,” the Black Cat said in a
mocking tone.

—“And how do you expect me not to worry?” I replied
sharply. “We get the gwaks out of the mine, it runs, but
for what? To come back to the mine the next day?”

—“No,” the Black Cat said, setting a pile of baskets
down beside me. “The Ojisaries simply won’t be able to
send us back to the mine, because there won’t be a mine
anymore.”

I stood dumbfounded, and understanding what the Black
Cat intended to do, I let out a muffled noise.

—“Good mother… I get it now.”

—“Really? Not all of it, I think,” Yerris told me in an
amused tone. “Because, hopefully, before we blow it all
up, you and Sla will come with the alchemist through the
tunnel. You’re good harmonists. You will take him out of
his lab, along with his devices to make the sokwata. No
Ojisary will expect the alchemist to escape through the
tunnel, because, as far as they know, there is no way
out that way.” He smiled, “It’s doable. It may all work
out… or it may not. But, at this point, nothing is lost
by trying even the craziest thing. Don’t you think so,
shyur?”

I barely hesitated before nodding. The idea of having
something to do gave me wings.

—“It runs. Then I’ll work with Sla. Where is she?”

—“She’ll be here any minute. She… she went to buy a
silence magara from Korther. We’ll wrap the explosive
magara with it, so maybe we won’t wake up the whole
neighborhood. Besides, we’ll wait for the fireworks for
the Wells Moon Festival: they start at eleven and last
for a few minutes. Best case scenario, no one will
notice anything.”

—“Well, you’ve got it all pretty much worked out,” I
said, impressed.

—“Natural, we’re Black Daggers,” Yerris replied with
some pride.

I arched an eyebrow, pleasantly surprised.

—“Has Korther forgiven you?”

Yerris swallowed hard.

—“Uh… No, not exactly. But I made him a number of
promises and… at least he didn’t stick his black dagger
in my throat.”

I swallowed. Reassuring.

—“She’s coming,” the semi-gnome added in a whisper.
“But, devils, who is the one with her?”

I turned and saw the two figures coming down the stairs.
There were distant rumors of the festivities in town,
but where we were, all was silent. Slaryn reached the
small courtyard and joined us, followed by the hooded
man. He looked familiar.

—“Un-be-lie-va-ble,” the dark elf girl snorted. “Korther
gave us a dark lantern, a magara of silence, and he even
sent us an onlooker. And that’s not all: he offered us a
safe place to hide the alchemist. Surprisingly, he’ll
even turn out to be altruistic and all.”

Yerris let out a small, skeptical laugh, but he didn’t
dare comment because of the presence of the onlooker,
who approached with a hand out.

—“Aberyl, at your service, gwaks. Yerris, isn’t it? The
last time I saw you, you were but a brat, but you are
still as black and, I, as white.”

The young Black Dagger shook Yerris’s hand vigorously,
and then he shook mine as well. I could feel his slight
flinch and the glance he gave my hand before he let go…
I turned pale. Had he felt something strange? He said
nothing and declared in a light tone:

—“I have come to have my name go down in history. From
this night forward, all shall know me as Aberyl, the
Hero of the Gwaks. So the entrance is this way?” he
inquired, glancing with interest at the rock face.

Yerris and I exchanged a look and smiled. Aberyl looked
really happy to be able to help us.

—“It’s about three meters high,” Yerris informed. “If it
all blows up like it’s supposed to, the bottom part of
the tunnel should be less than three feet high, I think.
What I’m wondering is why the miners of the past made
another tunnel farther down and didn’t make an opening
here, when they had an exit so close.”

—“Um… Interesting,” Aberyl said. “And do you guys know
how explosives work?”

—“We know the theory,” Slaryn replied. “But I haven’t
done any testing.”

Aberyl nodded thoughtfully.

—“For that, I can help you.” He laid a hand on the bag
of explosives and asked, “Can I?”

Yerris hesitated then gestured.

—“Go ahead.”

Aberyl untied the rope, opened the bag, and fished out a
strange circular gadget. Despite my curiosity, I dared
not approach it. As my nakrus master used to say,
*‘Don’t get your skull near the bone cracker vulture if
you can avoid it’*.

—“They don’t look bad,” Aberyl agreed. “You bought them
from the Artificer, didn’t you? I heard he was passing
through town. He sells high, but so far I’ve never had
any complaints about any of his items. How many are
there?”

—“In all? A hundred, approximately,” the Black Cat
replied.

Aberyl huffed, and suddenly he laughed, and his
laughter, stifled by his muffler, sounded a little
sinister.

—“A hundred! And what are you waiting for to blow up the
Rock?” he asked cheerfully.

Yerris cleared his throat.

—“It’s the mine that’s gonna blow up, not the Rock…”

Aberyl made an amused throaty noise, and horrified, we
saw him throw the magara in the air before catching it
in flight.

—“A hundred of these things… must have cost you an arm
and a leg.”

The semi-gnome muttered something low and said:

—“It cost us quite a bit, yes. And now will you stop
playing with it?”

—“Oops.” Aberyl grabbed the magara again on the fly and
let out, “My bad. You’re right. We’ve talked enough as
it is: let’s get to work.”

There followed an ominous process in which I helped him
entangle five explosive disks on a wire. When Aberyl
said that everything was perfect, Yerris climbed up to
the hole, fixed the string of disks as the Black Dagger
told him to, and no sooner had he come down than a boom
sounded and took me so much by surprise that I jumped
up, thinking that the magaras had activated themselves.

—“Calm your nerves, mate!” Sla scoffed. “It’s the
fireworks.”

I breathed a sigh of relief followed by unintelligible
curses. I didn’t like this explosion plan. Frankly, I
would have preferred my master to be there to raise a
small troop of skeletons and send them straight for the
Ojisaries. Surely some would die of heart attacks and
the others would run off like frightened squirrels.

I came back to reality when I saw Aberyl take the
silence magara and turn towards us. He waved his hand at
us.

—“Get out of the way, this is deadly.”

No kidding? I knew that from the start. We took the
bags, dropped them off far away, and finally the three
of us hid behind the corner of the adjoining building.
As Yerris poked his nose out, not wanting to miss the
show, Sla pulled him by the shirt, exasperated.

—“Black Cat!”

—“I just want to see,” he protested.

—“Curiosity killed the cat,” Slaryn replied. She
hesitated and added, “Let’s hope this works, otherwise…”

The Black Cat turned and smiled as he came very close to
her.

—“It will work, Princess,” he whispered. “It has to
work.”

I looked at them, my eyes wide. They weren’t going to
kiss just when things were about to explode, were they?
Suddenly, Aberyl appeared around the corner at a run,
colliding with the Black Cat and saying:

—“Cover your ears!”

Despite the magara of silence, the explosion was loud,
even after covering our ears. Several seconds later,
rocks and stones could still be heard rolling. I
staggered away from the wall of the building and took
the first look around. An impressive cloud of dust had
risen, and I coughed as I approached. I cast a
perceptive spell and smiled broadly as I saw that there
was no longer any obstacle. The tunnel was open.

—“Clear!” I said.

—“Careful, shyur,” Yerris called out to me, grabbing my
arm and pulling me back. “It could be that not all the
magaras exploded.”

I backed away with him, but after waiting a while and
seeing that no neighbors were coming and that nothing
else was exploding, we decided to approach. The
fireworks were already over, and there was silence in
the small courtyard. When I heard Aberyl assure us that
the five disks had been drained of their energy, I
nimbly pulled myself through the hole and cast a spell
of harmonic light. The tunnel was so narrow and low that
not any sajit could have passed through it. I took a few
steps forward, and when I came to a slight bend in the
tunnel, I thought I could see a distant light at the
bottom. Was it the vampiric foam? It was more than
likely.

I was about to take another step when my right foot
kicked something. Curious, I crouched down and examined
the object. It was a bone. And it looked very old. Out
of reflex more than necessity, I absorbed the morjas,
and while doing so, my other hand found another bone.

—“What on earth is this, a graveyard?” I muttered.

—“Draen!” Yerris whispered.

He called to me from the mouth of the tunnel, and seeing
him try to carefully assemble the bag of explosives, I
left the bones and rushed to the Black Cat to help him.
Once up, Yerris went:

—“All right. First, I’ll get all the gwaks out and then
I’ll place the explosives. I don’t think Aberyl can
follow me through the vampiric foam: it’s still a long
walk to the cave. But everything will be fine, don’t
worry. You go with Sla to find the alchemist. In two
hours, at most, everything will be ready.” As I nodded,
he took me by the arm and whispered, “Hey. Be very
careful. The Ojisaries may not have been able to capture
us yet, but if they catch you in their territory,
they’ll pop you off, you hear me? And don’t forget,
shyur: if anything happens to Sla, you’ll pay for it.”

I shuddered as I felt his hand grip my arm more tightly
and shook my head.

—“Don’t get in a huff, Black Cat. I’m doing what I can.”

Yerris sighed, let go of me and patted my shoulder.

—“I know. Onward and good luck.”

I smiled, patted him on the shoulder, leaned on him to
get up, and slipped out. Aberyl had just placed two
wooden boxes under the hole, to form a small staircase.
He pushed aside one stone, kicked another, and under my
curious gaze, rubbed his hands together, and said
calmly:

—“Securing the way out is essential.”

I nodded and, remembering that Yal had once said
something similar to me, I said:

—“Yal says, for a good thief, there’s no going without
getting away… No, wait, there’s no getting away without
going. He says that there’s…”

Sla grabbed me with a gasp.

—“Let’s go, shyur! We’ve got things to do.”

I followed her without protest, through the shadows of
the night, and I thought I heard behind us Aberyl
serenely utter a:

—“Good luck.”

Resurrector
===========

Finally, the assumption that the Ojisaries had uncorked
bottles of wine to celebrate the Wells Moon Festival
proved to be correct: there was singing, loud voices,
and laughter coming from one of the terraces of the
buildings that lined the notorious corridor. I only
hoped that in a few moments the singing would not turn
into a roar of alarm.

It took us forever to reach the roof of the house at the
end of the dead end. But we reached at last the Ojisary
territory. Slaryn crawled over the tiles to the wall of
one of the buildings and, wrapped in harmonic shadows, I
followed her. At the beginning of the journey, the dark
elf had told me over and over again things like: don’t
make noise, don’t complain, and don’t forget to use the
shadows exactly as I do. I sighed silently. Sometimes I
felt like Slaryn thought I was a five year old.

Slaryn glanced down the alley, and I crouched between
the tiles and the wall of the building, waiting
patiently for her to move. Finally, the Black Dagger
tugged at my sleeve to get my attention, and I saw her
disappear under the roof. I walked on all fours, and as
I bent over, I saw a bunch of shadows coming down a
gutter. She made no sound. In the corridor, there were
two lanterns. One was quite close to where Slaryn
landed, and the other was near an Ojisary who stood
guard at the entrance to the dead end.

After making sure the Ojisary wasn’t looking in our
direction, I too grabbed onto the gutter and began to
descend as quietly as I could. It wasn’t my best night,
as my arm and jaw still hurt from Syrdio’s punches;
nothing serious, but it was distracting. When I got my
feet on the ground, I crouched down next to Slaryn, and
she showed me her hand as if she was holding back from
slapping me. Only then did I notice that my shadow spell
had worn off. Damn. I cast it again, thought Slaryn had
a good reason to be angry, and lowered my ears. She gave
me a gentle warning tap and began to skim along the
wall. We passed the first lantern light, and Slaryn
chameleonized herself so well that she made me pout in
admiration. I tried to imitate her—after all, that was
what she had asked me to do. However, my intuition told
me that my harmonies were not as good. Finally, the
Black Dagger stopped in front of what must have been the
door leading to the laboratory. She pulled out a key,
and I widened my eyes as I realized that by some means
she had managed to make a copy. I approached as quietly
as I could, and suddenly my foot struck something and I
froze as I heard a slight creak. I looked down with a
murderous glance at the stone I had stumbled upon, but
when I saw it I forgot all about it for a moment. It was
my sharp stone! The Ojisaries must have thrown it away
the day they captured me. Feeling a sense of joy at my
discovery, I picked it up, and as Sla had just opened
the door, I hurried after the Black Dagger like a
shadow. I could hardly believe that the Ojisary who was
standing guard had not seen us.

We walked down a corridor, and I noticed that she was
making more noise than I was… I rolled my eyes. We were
in the middle of the hideout of the Ojisaries, putting
our lives on the line, and I was comparing skills?
Spirits…

After a few steps, Slaryn stopped in front of a door,
and this time she pulled out a picklock. Feeling a bit
useless, I decided to at least make sure there were no
alarms on the door. There was none. Slaryn slapped my
hand, concentrated, and finally forced the door open;
she went in, I went in, and she closed it behind us.

We found ourselves in a darkened room. There were no
windows. Only darkness. The light spell I cast barely
lit up, and Slaryn gave me another tap. She turned on
the dark lantern, and I could see a long table cluttered
with a lot of bottles and strange instruments. I heard a
metallic clang just as the light shone on the pale face
of a middle-aged gnome, bearded and disheveled, lying on
a straw mattress. His eyes blinked, and Slaryn murmured
rapidly as she approached:

—“Mr. Wayam, we’ve come to set you free!”

The Alchemist let out a little laugh that made my hair
stand on end. He lifted his chains.

—“And how?” he croaked in a nasal voice. “I am chained.
Not even the strongest acid can break the black steel.
You will not be able to free me.”

—“Yes, I will,” Sla replied, crouching down beside him.

—“You won’t.”

—“I have hydra blood.”

This time, the alchemist remained silent.

—“It can work,” he finally admitted.

—“It will work. The sokwatas have been released,” Sla
informed in a low voice. “And we’re going to get you to
safety.”

The gnome glared at her.

—“To another jailer?”

—“No. But it was you who invented and manufactured the
sokwata. And you’ll have to keep making it until you
give us a final cure. That sounds like a fair deal to
me,” Sla concluded.

The alchemist bit his cheek as Sla pulled out that
miraculous hydra blood, and I shook my head in
disbelief. What did he have to ponder so much? Perhaps
so much prison and so much alchemy had affected his
head.

I was surprised at what I saw next: after putting some
kind of black powder on the chain, Sla spat on it. But I
saw the result immediately: in a few moments, the black
link shrank to a thin string and finally melted away.

—“Amazing,” the alchemist murmured, in wonder. “I’ve
never seen hydra blood before. Where did you find it?”

—“On the black market,” Slaryn replied. She had
proceeded to do the same with the chains that held the
feet, and the alchemist was quickly freed. The dark elf
gestured, “Tell us what you need to make the sokwata,
and we will bring it with us.”

The alchemist stood up, and without saying a word, he
held out an index finger to a bottle. I took it and put
it in my bag. He went around the table, and each time he
pointed to something, I put it away. I even took a
little notebook full of notes. Then he stopped, raised a
hand, scratched his beard, and nodded.

—“That’s it. I think.”

—“You think?” I gasped.

—“Mm,” the alchemist confirmed. He rubbed his eyes. “All
the essentials, yes. Can we… go now?”

I exchanged a look with Slaryn. She nodded.

—“Let’s go. Just be quiet. And don’t be surprised by the
way we’re going.”

—“I’ll be more surprised if you can get me out of there
alive,” Mr. Wayam retorted.

I could hear Slaryn’s exasperated sigh. She gently took
him by the arm and guided him to the door. Then I
realized that the gnome was limping, and I huffed.

—“Why are you limping?”

My question seemed accusatory, even to me. But, hell,
wasn’t our task difficult enough that the alchemist was
lame to boot? It could have been worse, I thought, he
could have been missing both legs. Or worse still, he
could have been dead.

The alchemist glanced at me, seeming to notice me for
the first time.

—“They beat the crap out of me a few hours ago. Isn’t
that a good reason?”

His tone implicitly added a: *You little impertinent*. I
pouted, and when Sla imposed silence on us, I sealed my
lips. We walked back down the hall to the door that led
to the outer corridor. The door to the tunnel and mine
was on the other side. And, frankly, I didn’t see how we
were going to get through the dead end to it without the
Ojisary who was on guard seeing us.

Sla opened the door quietly and surrounded herself with
harmonic shadows before slipping out, grabbing the
alchemist by the sleeve. I followed them, carrying the
bag full of vials and instruments. I closed the door
behind me, and just as Sla opened the door opposite with
another spare key, I saw movement at the dead end, saw
four Ojisaries pointing their crossbows at us, and
shouted:

—“Run!”

The bolts whistled right at me, I rushed at the
alchemist to push him inside, and I didn’t understand
how the hell no projectile hit me, then, already running
through the inner corridors, I found that in fact one of
the bolts had torn my shirt.

—“Isturbaaaags!” I shouted. And I hurried the alchemist,
hurling a string of curses at him, for the cursed man
was limping and this was not the time for that,
blasthell!

Fortunately, the gnome was not so lame, and we reached
the tunnel door before the Ojisaries entered the
corridor door. We heard them rush down the hallway, and
Sla closed the door, shouting:

—“I have nothing to barricade it with!”

I wanted to reply, but my throat was constricted by
terror. I was about to continue running when suddenly
the alchemist grabbed my bag, thrust his hand in and
pulled out a bottle.

—“Mmno, it’s not this one,” he muttered.

He took out another, squinted as if trying to see in the
darkness, and I was already about to suggest him that he
sang some requiem because we were all going to die when,
to my amazement, he threw the flask against the floor
near the door. And flames shot up.

—“Good mother!” I stammered. Were those the essential
bottles for making sokwata? Yeah, tell me another one…

The alchemist took off at a run, and Sla and I followed
him, outpacing him to the famous metal door of the
bongs. And what an icy shower I received when Sla tried
to pull the door open and couldn’t.

—“Back!” she cried.

I recoiled, not sure why, and when I saw Slaryn pull out
an explosive disk, I quickly grabbed the alchemist and
pushed him back. The explosion deafened me, and Slaryn
had to take me by the arm to remind me that we had
Ojisaries on our tail, already entering the tunnel. They
had abandoned their heavy crossbows to jump over the
flames, but they still had their daggers. They were
certainly puzzled by the path we were taking, and
perhaps mocked that we thought we could break out the
alchemist and the gwaks. How would they imagine that we
had another way out?

The metal door was dented and open. We rushed down the
stairs, and Sla shouted:

—“Black Cat! They’re chasing us!”

We reached the bottom of the stairs, and I could see the
masterpiece that would finish off the salbronix mine:
the explosive magaras were laid out all over the tunnel.
And the black steel door had already been blown open.
Despite Sla’s cry of alarm, the Black Cat received us
with a smile of relief.

—“Good evening, Mr. Wayam. Don’t worry, you’ll get out
of there alive. Run, Sla, get him to safety. Take the
tunnel.”

—“I’ll take care of blowing it up,” Aberyl said.
Clearly, despite what the Black Cat had said, the Black
Dagger seemed to be handling the vampiric foam attacks
quite well. He was crouched near an exploding magara,
attached to a wire that ran directly into the tunnel.
“This is going to be deadly,” he added.

Deadly, he said! Damn deadly, yes! I tugged the
alchemist by the sleeve, and he quickly followed me,
running towards the other cave. Everything was empty:
the freed gwaks must have been already safe in the
alleys of the Labyrinth.

—“Black Cat!” Sla shouted. “Listen to Aberyl: he knows
what he’s doing.”

Yerris hesitated, however, and I thought I understood
his dilemma: he was anxious to make sure that the tunnel
would explode and be unusable for a long time. I said to
him:

—“Black Cat, with those explosives, everything’s going
to fall on us, run, scaluftard!”

And I did not wait any longer, for I could already hear
the roars of incomprehension from the Ojisaries who were
running into the tunnel, not imagining for a moment that
death was surrounding them on all sides. Driven by the
urgency of survival, I could only think of running and
not losing sight of the alchemist. The bearded gnome, on
seeing the mist of light, exclaimed:

—“What the hell is this horror?”

He did not seem so horrified, for he nevertheless
entered the tunnel of light without even slowing down.
We had gone perhaps fifty paces when Aberyl shouted
something and ran past us like a hare. And then came the
explosion.

I threw myself to the ground, and the mist of light
curiously muffled almost all the bang that came. This
did not prevent me from scraping my knees on the sharp
rock. Without thinking, I breathed in, found no air, and
rose, asphyxiated, just to see the hell breaking loose
on the surface. The stones fell, the light vibrated as
if complaining of such a crash, and the worst of it was
that I could see no one.

After a few moments of clumsily moving forward, I
managed to shout:

—“Black Cat! Sla!”

I repeated my call until my foot struck something soft,
and with a frozen heart, I hastened to seize the body.
To my relief, I felt a reaction, and hands grabbed me. I
pulled it up. It was the alchemist. But he was half
passed out.

—“Mr. Wayam,” I stammered. “Are you alive?”

The alchemist huffed without opening his eyes.

—“For now. But that won’t be true for long. I feel like…
like life is just flowing away.”

I understood his problem and turned pale. Devils. If we
didn’t get out of there soon, the mist was going to
absorb all his jaypu and kill him.

—“Come on, cheer up, we have to get out of here or
you’ll die.”

I lent him my support, and we went forward a few steps
before I heard voices.

—“Don’t kill me, I have a wife and children, please!”

The scene I saw as I came around a bend chilled my
blood. Barely a few feet away from me, I saw Slaryn, and
not far away, on a small rocky islet, stood the Black
Cat, threatening an Ojisary with an explosive disk. And
it wasn’t just any Ojisary, I realized with a shudder,
finally recognizing the voice. It was Lof, the Masked
One, the one who brought us magic bread every day and
told us jokes.

—“Nooo!” I roared. “Black Cat, don’t kill him!”

I left Slaryn to deal with the alchemist and ran to the
Black Cat.

—“Don’t do that,” I said. “It’s the Masked One.”

The Black Cat looked at me as if he did not recognize
me.

—“Round, shyur. It’s the Masked One: a heartless Ojisary
who used to tell us jokes as he watched us suffer.”

—“No,” I said, altered. “Don’t kill him. It’s not right,
Yerris. Please. Let’s not waste any more time, let’s get
out of here. The alchemist is dying.”

The Black Cat looked into Lof’s eyes, made a pout of
disgust, and growled.

—“If you try anything funny, I’ll make you eat this
disk, Lof.”

He stepped down from the islet and led the way while I
hurried to help Sla with the alchemist. Seeing that his
jaypu was going down at great speed, and knowing that we
still had a long way to go to get out of there,
according to the Black Cat, I knew we wouldn’t make it
in time. I was already on the verge of falling into deep
despair when I thought that I could do something. I took
his arm with both hands and concentrated as we moved
forward. Since turning his own morjas into jaypu would
have required a concentration that I did not have, given
the situation, I set about turning mine and sending him
waves of jaypu. It helped that I had absorbed the morjas
from those bones I had picked up in the escape tunnel,
and after a while, the alchemist regained some
composure.

—“Impressive,” I heard him whisper.

I paled, hoping he wouldn’t draw too many conclusions
about what had just happened. Was using the morjas of
the bones to bring life back to someone who was not yet
dead necromancy? I did not think it was. That was
healing, not resurrecting. So when I saw that Lof was
following behind us, looking more dead than alive, I
took his hand and helped him in the same way. Perhaps I
did not try as hard, but in any case, the Masked One
managed to follow us with more vigour.

The way to the hole seemed endless. I was already
thinking that Yerris was lost, but then he stopped, felt
the wall through the light, and nodded.

—“This way.”

We plunged into the light and entered a cave through a
rather narrow hole. So much so that I could hardly fit
the bag full of flasks through. Who knows how many
totally unnecessary things the alchemist had made me
pack?

We breathed, relieved, as we left the vampiric foam
behind.

—“I was beginning to think you’d stayed buried,” Aberyl
greeted. His veiled figure stood near the exit tunnel.
“Come on, gnome, get up, you’ll be fine.”

But the alchemist had sprawled on the rock, croaking
incomprehensible things. I shuddered. No way, he hadn’t
gone mad, had he…?

—“Get up,” Aberyl repeated.

Between them, he and the Black Cat helped the alchemist
through the narrow tunnel, and they jerked forward as
Sla led the way, lighting the way. I tugged the Masked
One by the sleeve, and he staggered forward behind me,
dazed.

The vampiric light had not seemed so far away from the
tunnel entrance. But the way back seemed long, very
long, because all I wanted to do was get out of there,
go back to my comrades, and sleep soundly.

—“Watch out for the magaras,” Aberyl then said. “I put
in all the ones that were left. I don’t suppose you
wanted to keep any of them as souvenirs, did you?”

Growling under the weight of the alchemist, the Black
Cat replied:

—“Blasthell, no. Let it all blow up, the tunnel, the
mine, the foam, and all their holy dead.”

Aberyl glanced over his shoulder at him with a smile and
nodded.

—“Then so be it.”

We were almost there. And I held back from pushing
Aberyl, eager to finally leave this tunnel. They started
to come out. I put one foot on the wooden box and was
about to pull the Masked One to help him when he
suddenly tripped over the wire.

—“Mind the wire!” Aberyl cried. He rushed to untangle
Lof, but when he touched a magara, his eyes flashed with
terror. He leapt back and shouted, “Blasthell, take
cover! Run!”

And he ran. I tried to follow him, but in my haste, I
stumbled on the makeshift steps. As I tried to regain my
balance, I thought I saw, like a flash, a familiar
figure standing there at the corner, and Aberyl was
pushing him back, and then… everything exploded.

It all happened so quickly that it took me a long time
to realize what had happened. The Masked One threw
himself upon me, I screamed, was hit by something that
left me half unconscious, stopped screaming, and choked
on the dust. I coughed and heard distant screams. The
rocks had shattered. That was why I could not understand
how I could still be alive, unless… With a hand that I
could hardly move, I touched the hand of the Masked One.
He was covering me completely… and he wasn’t moving. His
jaypu had vanished.

My eyes filled with tears. Lof had saved my life. He had
sacrificed himself for me. But… why? My body was now
shaking violently. I felt someone remove the dead weight
above me and pull me from the pile of rocks. I coughed
again, and looked at my right hand, and seeing it a
little torn, I let go what little energy I had left to
regenerate it. It would not do to have survived more or
less everything and then have the sajits catch me with
an undead hand… Two strong arms grabbed me, and I tried
to stand, but one leg would not support me. I looked
down, saw it bloody, then looked up and recognized the
familiar face.

—“Elassar,” I murmured.

If he said anything to me, I did not hear it. My ears
were ringing. I turned my head toward the tunnel. It was
gone. Now it was just a pile of rocks.

Yal tried to move me forward, but when he saw that I
could only limp along, he lifted me up, whispered
something again, I think, but I could not tell what, and
walked away from the courtyard as quickly as he could.
In my head, the stones and the explosion continued to
echo over and over again, and I continued to feel the
Masked One above me like a shield. As the Priest would
have said, may his ancestors welcome him as a brother.


%%%


