<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Frundis: a semantic markup language</title>
    <link rel="shortcut icon" type="image/x-icon" href="/frundis/favicon.png" />
    <link rel="stylesheet" href="/frundis/style.css" />
  </head>
  <body>
<div class="toc">
  <h2 id="toc-title" class="toc-title">Frundis: a semantic markup language</h2>
  <ul>
    <li><a href="#s1">Overview</a>
    </li>
    <li><a href="#s2">Documentation</a>
    </li>
    <li><a href="#s3">Installation</a>
    </li>
    <li><a href="#examples">Examples</a>
    </li>
  </ul>
</div>
<h1 class="Sh" id="s1">Overview</h1>
<p><span class="frundis">frundis</span>
is a tool for compiling documents written in the
<a href="/man/frundis_syntax-5.html">frundis language</a>,
a semantic markup language primarily intended for
supporting authoring of novels, but also well suited for
many other kinds of documents.
The
<a href="/man/frundis-1.html"><span class="frundis">frundis</span> tool</a>
can export documents to LaTeX, XHTML 5, EPUB, markdown
and groff mom.</p>
<p>The language has a focus on simplicity. It provides a few
flexible built-in macros with extensible semantics. It
strives to provide good error messages and catch typos,
while still allowing one to finely control output for a
specific format when needed.</p>
<p>Here is a list of its main features:</p>
<ul>
<li><p>Common elements such as links, images, cross-references,
lists (including verse), simple tables, table of
contents, etc.</p></li>
<li><p>EPUB with arbitrary metadata. Indexed HTML files.</p></li>
<li><p>User defined
<em>tags</em>
with configurable rendering.</p></li>
<li><p>Raw blocks, file inclusion, filters, conditionals, macros
and variables.</p></li>
<li><p>Roff-like syntax: simple, clear and friendly to grep and
diff.</p></li>
</ul>
<h1 class="Sh" id="s2">Documentation</h1>
<p>Both the tool and the language are documented in manual
pages:</p>
<ul>
<li><p><a href="/man/frundis-1.html">frundis(1)</a>
describes command line usage.</p></li>
<li><p><a href="/man/frundis_syntax-5.html">frundis_syntax(5)</a>
describes the language.</p></li>
</ul>
<p>For a quick overview, look at the
<a href="#examples">example below</a>.
There is also a
<a href="faq.html">short FAQ</a>.
There is a 
<a href="https://www.youtube.com/watch?v=QqDOkcrh1eU">recorded presentation</a>
(in French) by kaoseto that took place at the
<a href="https://capitoledulibre.org/">Capitole du Libre</a>.</p>
<h1 class="Sh" id="s3">Installation</h1>
<p>Follow the README instructions in the
<a href="https://codeberg.org/anaseto/gofrundis">codeberg</a>
repository.
The program is written in Go and released under the ISC free
software license (BSD-like). </p>
<p>Questions, suggestions, patches and pull requests are welcome!</p>
<h1 class="Sh" id="examples">Examples</h1>
<p>There are some simple examples in the
<code class="path">doc/examples</code>
directory, and in the manual pages.</p>
<p>Moreover,
<span class="frundis">frundis</span>
is being used in production since around 2015.
All the free (as in freedom) books by kaoseto in the
<a href="https://bardinflor.perso.aquilenet.fr/kaosfantasy/index-en.html">kaosfantasy</a>
multilingual project are using
<span class="frundis">frundis</span>.
The sources can be found
<a href="https://codeberg.org/kaoseto/kaosfantasy">on Codeberg</a>.</p>
<p>As stated above,
<span class="frundis">frundis</span>
is not limited to producing books. As an example, we give as
follows the code that was used to generate this page:</p>
<div class="frundis-code">
<pre><span class="comment">.\&quot; This line is a comment.</span>
<span class="comment">.\&quot;</span>
<span class="comment">.\&quot; Setting some global eXport parameters for title,</span>
<span class="comment">.\&quot; favicon, css, and language.</span>
<span class="ppmacro">.X</span> set document-title &quot;Frundis: a semantic markup language&quot;
<span class="ppmacro">.X</span> set xhtml-custom-ids 1
<span class="ppmacro">.X</span> set xhtml-favicon /frundis/favicon.png
<span class="ppmacro">.X</span> set xhtml-css /frundis/style.css
<span class="ppmacro">.X</span> set lang en
<span class="comment">.\&quot; Markup tag declarations: option -c will provide the name</span>
<span class="comment">.\&quot; of the html element, while -t will provide the html</span>
<span class="comment">.\&quot; class and frundis tag. The frundis utility will complain</span>
<span class="comment">.\&quot; if you use an undeclared tag, catching typos. The -f</span>
<span class="comment">.\&quot; option means that this tag declaration has to be used</span>
<span class="comment">.\&quot; for XHTML output only, allowing for example to provide a</span>
<span class="comment">.\&quot; different declaration for a LaTeX, EPUB or markdown</span>
<span class="comment">.\&quot; output.</span>
<span class="ppmacro">.X</span> mtag <span class="option">-f</span> xhtml <span class="option">-t</span> frundis <span class="option">-c</span> span
<span class="ppmacro">.X</span> mtag <span class="option">-f</span> xhtml <span class="option">-t</span> path <span class="option">-c</span> code
<span class="comment">.\&quot; Macro definition to not repeat too much the same</span>
<span class="comment">.\&quot; boilerplate and improve maintenance.</span>
<span class="ppmacro">.#de</span> frundis
<span class="comment">.\&quot; Semantic markup using a previously defined tag</span>
<span class="macro">.Sm</span> <span class="option">-t</span> frundis frundis <span class="escape">\$@</span>
<span class="comment">.\&quot; Now we can write .frundis instead of the preceding</span>
<span class="comment">.\&quot; line. \$@ is substituted by the list of arguments.</span>
<span class="comment">.\&quot; Useful to add punctuation, for example. End of macro</span>
<span class="comment">.\&quot; definition:</span>
<span class="ppmacro">.#.</span>
<span class="comment">.\&quot; Table of Contents</span>
<span class="macro">.Tc</span>
<span class="comment">.\&quot; A Section (without numbering)</span>
<span class="macro">.Sh</span> <span class="option">-nonum</span> Overview
<span class="comment">.\&quot; Call to user defined .frundis macro</span>
<span class="macro">.frundis</span>
is a tool for compiling documents written in the
<span class="macro">.Lk</span> /man/frundis_syntax-5.html &quot;frundis language&quot; ,
a semantic markup language primarily intended for
supporting authoring of novels, but also well suited for
many other kinds of documents.
The
<span class="macro">.Lk</span> /man/frundis-1.html Bm <span class="option">-t</span> frundis frundis Em <span class="escape">\&amp;</span> tool
can export documents to LaTeX, XHTML<span class="escape">\~</span>5, EPUB, markdown
and groff mom.
<span class="macro">.P</span>
The language has a focus on simplicity. It provides a few
flexible built-in macros with extensible semantics. It
strives to provide good error messages and catch typos,
while still allowing one to finely control output for a
specific format when needed.
<span class="macro">.P</span>
Here is a list of its main features:
<span class="macro">.Bl</span> <span class="comment">\&quot; Begin list</span>
<span class="macro">.It</span>
Common elements such as links, images, cross-references,
lists (including verse), simple tables, table of
contents, etc.
<span class="macro">.It</span>
EPUB with arbitrary metadata. Indexed HTML files.
<span class="macro">.It</span>
User defined
<span class="macro">.Sm</span> tags
with configurable rendering.
<span class="macro">.It</span>
Raw blocks, file inclusion, filters, conditionals, macros
and variables.
<span class="macro">.It</span>
Roff-like syntax: simple, clear and friendly to grep and
diff.
<span class="macro">.El</span> <span class="comment">\&quot; End list</span>
<span class="macro">.Sh</span> <span class="option">-nonum</span> Documentation
Both the tool and the language are documented in manual
pages:
<span class="macro">.Bl</span>
<span class="macro">.It</span>
<span class="macro">.Lk</span> /man/frundis-1.html frundis(1)
describes command line usage.
<span class="macro">.It</span>
<span class="macro">.Lk</span> /man/frundis_syntax-5.html frundis_syntax(5)
describes the language.
<span class="macro">.El</span>
For a quick overview, look at the
<span class="comment">.\&quot; cross-reference to the element with identifier “examples”</span>
<span class="macro">.Sx</span> examples &quot;example below&quot; .
There is also a
<span class="macro">.Lk</span> faq.html &quot;short FAQ&quot; .
There is a 
<span class="macro">.Lk</span> https://www.youtube.com/watch?v=QqDOkcrh1eU &quot;recorded presentation&quot;
(in French) by kaoseto that took place at the
<span class="macro">.Lk</span> https://capitoledulibre.org/ &quot;Capitole du Libre&quot; .
<span class="macro">.Sh</span> <span class="option">-nonum</span> Installation
Follow the README instructions in the
<span class="macro">.Lk</span> https://codeberg.org/anaseto/gofrundis codeberg
repository.
The program is written in Go and released under the ISC free
software license (BSD-like). 
<span class="macro">.P</span>
Questions, suggestions, patches and pull requests are welcome!
<span class="macro">.Sh</span> <span class="option">-nonum</span> <span class="option">-id</span> examples Examples
There are some simple examples in the
<span class="macro">.Sm</span> <span class="option">-t</span> path doc/examples
directory, and in the manual pages.
<span class="macro">.P</span>
Moreover,
<span class="macro">.frundis</span>
is being used in production since around 2015.
All the free (as in freedom) books by kaoseto in the
<span class="macro">.Lk</span> https://bardinflor.perso.aquilenet.fr/kaosfantasy/index-en.html kaosfantasy
multilingual project are using
<span class="macro">.frundis</span> .
The sources can be found
<span class="macro">.Lk</span> https://codeberg.org/kaoseto/kaosfantasy &quot;on Codeberg&quot; .
<span class="macro">.P</span>
As stated above,
<span class="macro">.frundis</span>
is not limited to producing books. As an example, we give as
follows the code that was used to generate this page:
<span class="comment">.\&quot; We include a file defining macros for code block</span>
<span class="comment">.\&quot; inclusion and footer.</span>
<span class="macro">.If</span> macros.frundis
<span class="comment">.\&quot; Code is filtered through a small html coloration</span>
<span class="comment">.\&quot; script and included &quot;as-is&quot; by defining and using a</span>
<span class="comment">.\&quot; filter tag.</span>
<span class="ppmacro">.X</span> ftag <span class="option">-t</span> frundis <span class="option">-shell</span> &quot;perl html_coloration.pl&quot;
<span class="macro">.include-code-with-filter</span> frundis index.frundis
The
<span class="macro">.Sm</span> <span class="option">-t</span> path macros.frundis
file with the macro definitions is the following:
<span class="macro">.include-code-with-filter</span> frundis macros.frundis
<span class="macro">.footer</span>
</pre></div>
<p>The
<code class="path">macros.frundis</code>
file with the macro definitions is the following:</p>
<div class="frundis-code">
<pre><span class="comment">.\&quot; This file defines macros for including frundis code</span>
<span class="comment">.\&quot; blocks, as well as a macro for the footer.</span>
<span class="comment">.\&quot;</span>
<span class="comment">.\&quot; We define a display block tag for code sections.</span>
<span class="ppmacro">.X</span> dtag <span class="option">-f</span> xhtml <span class="option">-t</span> frundis-code <span class="option">-c</span> div
<span class="comment">.\&quot; Define a macro for including code from file macro with</span>
<span class="comment">.\&quot; a custom filter.</span>
<span class="ppmacro">.#de</span> include-code-with-filter
<span class="macro">.Bd</span> <span class="option">-t</span> frundis-code
<span class="macro">.Ft</span> <span class="option">-f</span> xhtml &lt;pre&gt;
<span class="macro">.If</span> <span class="option">-as-is</span> <span class="option">-t</span> <span class="escape">\$1</span> <span class="escape">\$2</span>
<span class="macro">.Ft</span> <span class="option">-f</span> xhtml &lt;/pre&gt;
<span class="macro">.Ed</span>
<span class="ppmacro">.#.</span>
<span class="comment">.\&quot; We define a display block tag for the footer.</span>
<span class="ppmacro">.X</span> dtag <span class="option">-f</span> xhtml <span class="option">-t</span> footer <span class="option">-c</span> footer
<span class="comment">.\&quot; Define a “footer” macro to be called for all the</span>
<span class="comment">.\&quot; frundis pages in the website.</span>
<span class="ppmacro">.#de</span> footer
<span class="macro">.Bd</span> <span class="option">-t</span> footer
<span class="macro">.Ft</span> <span class="option">-f</span> xhtml &lt;hr&gt;
This website&apos;s content is licensed under
<span class="macro">.Lk</span> https://creativecommons.org/licenses/by/4.0/ cc-by
or the ISC license, as you wish. The website does not make
use of any cookies. Contact: 
<span class="ppmacro">.X</span> mtag <span class="option">-f</span> xhtml <span class="option">-t</span> email <span class="option">-c</span> code
<span class="macro">.Sm</span> <span class="option">-t</span> email anaseto AT bardinflor.perso.aquilenet.fr .
<span class="macro">.Ed</span>
<span class="ppmacro">.#.</span>
</pre></div>
<footer class="footer">
<hr><p>This website’s content is licensed under
<a href="https://creativecommons.org/licenses/by/4.0/">cc-by</a>
or the ISC license, as you wish. The website does not make
use of any cookies. Contact: 
<code class="email">anaseto AT bardinflor.perso.aquilenet.fr</code>.</p>
</footer>
  </body>
</html>
